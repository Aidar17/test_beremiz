#include "beremiz.h"
#ifndef __POUS_H
#define __POUS_H

#include "accessor.h"
#include "iec_std_lib.h"

// FUNCTION_BLOCK FUNCTIONBLOCK
// Data part
typedef struct {
  // FB Interface - IN, OUT, IN_OUT variables
  __DECLARE_VAR(BOOL,EN)
  __DECLARE_VAR(BOOL,ENO)
  __DECLARE_VAR(REAL,V_PWR)
  __DECLARE_VAR(REAL,V_BAT)
  __DECLARE_VAR(REAL,V_PWR_MIN)
  __DECLARE_VAR(REAL,V_PWR_MIN_IN)
  __DECLARE_VAR(REAL,V_PWR_MAX)
  __DECLARE_VAR(REAL,V_PWR_MAX_IN)
  __DECLARE_VAR(REAL,V_BAT_MIN)
  __DECLARE_VAR(REAL,V_BAT_MIN_IN)
  __DECLARE_VAR(REAL,V_BAT_MAX)
  __DECLARE_VAR(REAL,V_BAT_MAX_IN)

  // FB private variables - TEMP, private and located variables

} FUNCTIONBLOCK;

void FUNCTIONBLOCK_init__(FUNCTIONBLOCK *data__, BOOL retain);
// Code part
void FUNCTIONBLOCK_body__(FUNCTIONBLOCK *data__);
// PROGRAM PROGRAM0
// Data part
typedef struct {
  // PROGRAM Interface - IN, OUT, IN_OUT variables

  // PROGRAM private variables - TEMP, private and located variables
  READ_PWR READ_PWR0;
  __DECLARE_EXTERNAL(REAL,V_PWR_MIN)
  __DECLARE_EXTERNAL(REAL,V_PWR_MAX)
  __DECLARE_EXTERNAL(REAL,V_BAT_MIN)
  __DECLARE_EXTERNAL(REAL,V_BAT_MAX)
  __DECLARE_EXTERNAL(REAL,V_BAT_0)
  __DECLARE_EXTERNAL(REAL,V_PWR_0)
  FUNCTIONBLOCK FUNCTIONBLOCK0;

} PROGRAM0;

void PROGRAM0_init__(PROGRAM0 *data__, BOOL retain);
// Code part
void PROGRAM0_body__(PROGRAM0 *data__);
#endif //__POUS_H
