/**
 * @file C:\Beremiz\beremiz_test_projects\VOLTAGE_PWR_BAT\build//sofi/freertos/src/beremiz_task.c
 * @author Shoma Gane <shomagan@gmail.com>
 *         Ayrat Girfanov <girfanov.ayrat@yandex.ru>
 * @defgroup beremiz 
 * @ingroup beremiz 
 * @version 0.1 
 * @brief  TODO!!! write brief in 
 */
    /*
     * Copyright (c) 2018 Snema Service
     * All rights reserved.
     *
     * Redistribution and use in source and binary forms, with or without modification,
     * are permitted provided that the following conditions are met:
     *
     * 1. Redistributions of source code must retain the above copyright notice,
     *    this list of conditions and the following disclaimer.
     * 2. Redistributions in binary form must reproduce the above copyright notice,
     *    this list of conditions and the following disclaimer in the documentation
     *    and/or other materials provided with the distribution.
     * 3. The name of the author may not be used to endorse or promote products
     *    derived from this software without specific prior written permission.
     *
     * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
     * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
     * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
     * SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
     * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
     * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
     * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
     * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
     * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY
     * OF SUCH DAMAGE.
     *
     * This file is part of the sofi PLC.
     *
     * Author: Shoma Gane <shomagan@gmail.com>
     *         Ayrat Girfanov <girfanov.ayrat@yandex.ru>
     */
#ifndef BEREMIZ_TASK_C
#define BEREMIZ_TASK_C
#include "link_functions.h"
#include "resource1.h"
#include "beremiz_task.h"
#include "os_service.h"
#include "beremiz_regs_description.h"


#define MAX_MODBUS_MASTER_NODES 6

link_functions_t * p_link_functions; 
static u32 time_duration = 100;
static u32 time_period = 1000;
#define MDB_DINAMIC_ADDRESS_SPACE_SIZE 0
#define MDB_ARRAY_ADDRESS_SPACE_SIZE 0
#define CAN_FEST_ENABLE 0
u8 mdb_address_space[MDB_DINAMIC_ADDRESS_SPACE_SIZE];
u8 mdb_array_address_space[MDB_ARRAY_ADDRESS_SPACE_SIZE];
static int add_modbus_address_space(void);
static int add_regs_description(void);
static void modbus_terminate(void);
#if CAN_FEST_ENABLE
#include "can_open_dict.h"
extern CO_Data ObjDict_Data;
static int change_can_dict(void * dict);
#endif
/**
 *
 * 
 */

TASK_CODE int main(void *arg){
    osEvent event;
    TickType_t next_wake_time;
    u64 common_ticktime_msec;
    uint32_t task_tick;
    uint32_t loop_tick;
    /* Remove compiler warning about unused parameter. */
    u32 size_data,size_bss;
    void * p_link = arg;
    size_data = put_preinit_data();
    size_bss = clear_bss_data();
    if(p_link!=NULL){
        p_link_functions = (link_functions_t *)p_link;
    }else{
        p_link_functions = (link_functions_t *)&_sofi_link_start;/*!<init array if function*/
    }
    p_link_functions->printf("USER_TASK:preinit data size %lu\n",size_data);
    p_link_functions->printf("USER_TASK:bss section size %lu\n",size_bss);
    p_link_functions->printf("USER_TASK:sended address %p\n",p_link);

    /* Initialise xNextWakeTime - this only needs to be done once. */
    next_wake_time = p_link_functions->os_kernel_sys_tick();
    task_tick = 0;
    loop_tick = 0;
    common_ticktime__ = 10000000000ULL;//rewrite from SNEMA
    common_ticktime_msec = common_ticktime__/1000000;
    common_ticktime_msec = common_ticktime_msec==0?1:common_ticktime_msec;
    p_link_functions->printf("USER_TASK:common_ticktime_msec %llu\n",common_ticktime_msec);
    p_link_functions->printf("USER_TASK:common_ticktime %llu\n",common_ticktime__);
    p_link_functions->led_user_on(100);
    if(add_modbus_address_space()!=0){
        p_link_functions->printf("USER_TASK:error on enhancmt modbus space \n");
    }
    /*enhancement dinamical self address space at controller end */
    /*make modbus master thread if need it start */
    if(add_regs_description()>0){
    	p_link_functions->printf("USER_TASK:add user regs description table with %u\n",BEREMIZ_REGS_VAR_NUM);	
    }
#if CAN_FEST_ENABLE
    if(change_can_dict(&ObjDict_Data)<0){
    	p_link_functions->printf("USER_TASK:error while init can fest dict \n");	
    }else{
        p_link_functions->printf("USER_TASK:can dict moved to master value");
    }
#endif

    /*make modbus master thread if need it end */
    __init();
    while(1){
        p_link_functions->refresh_watchdog();
        p_link_functions->os_delay_until( &next_wake_time, 1);
        if(((task_tick++)%common_ticktime_msec)==0u){
            PlcLoop(NULL);
            loop_tick++;
        }
        if(((task_tick)%time_period)==0u){
            p_link_functions->led_user_on((u16)time_duration);
        }
        if(loop_tick==10 || 
            ((common_ticktime__ > 1000000000)&&(loop_tick==1))){//set state only after several times passes
            regs_access_t reg;
            reg.flag = U16_REGS_FLAG;
            char name[] = "user_task_state";
            regs_template_t regs_template;
            regs_template.name = name;
            if(p_link_functions->regs_description_get_by_name(&regs_template)==0){
                *(u16*)regs_template.p_value |= (u16)INTERNAL_FLASH_TASK_RUNNING;
                p_link_functions->write_reg_to_bkram(regs_template.p_value);
                p_link_functions->os_thread_yield();
            }
        }
        event = p_link_functions->os_signal_wait(0,0);
        if((event.status == osEventSignal)&&
           ((event.value.signals == STOP_CHILD_PROCCES)||(event.value.signals == STOP_PROCESS_FOR_CHANGE_OS))){
            char name[] = "user_task_state";
            modbus_terminate();
            if(event.value.signals == STOP_CHILD_PROCCES){
                regs_template_t regs_template;
                regs_template.name = name;
                if(p_link_functions->regs_description_get_by_name(&regs_template)==0){
                    *(u16*)regs_template.p_value &= ~((u16)INTERNAL_FLASH_TASK_RUNNING);
                    p_link_functions->write_reg_to_bkram(regs_template.p_value);
                    p_link_functions->os_thread_yield();
                }
            }

#if CAN_FEST_ENABLE
            if(change_can_dict(NULL)>=0){//to default value
                p_link_functions->printf("USER_TASK:can dict moved to default value");
            }else{
                p_link_functions->printf("USER_TASK:error while moving can fest dict to default\n");	
            }
#endif
            p_link_functions->printf("USER_TASK:terminate self ");
            p_link_functions->os_thread_terminate(p_link_functions->os_thread_get_id());
        }
    }
}
static int add_modbus_address_space(){
    int result;
    result = 0;
    osPoolId mdb_enh_pool =NULL;
    u32 len1 = MDB_DINAMIC_ADDRESS_SPACE_SIZE;
    u32 len2 = MDB_ARRAY_ADDRESS_SPACE_SIZE;
    if(len1){
        mdb_enh_pool = p_link_functions->modbus_bind_address_space(BEREMIZ_ADDRESS_SPACE_START,mdb_address_space,&len1);
    }else{
        len1 =1;
    }
    if(len2){
        mdb_enh_pool = p_link_functions->modbus_bind_address_space(BEREMIZ_ARRAY_ADDRESS_SPACE_START,mdb_array_address_space,&len2);
    }else{
        len2 =1;
    }
    
    if((len1==0) || (len2==0)){
        p_link_functions->printf("pool for enhancment modbus space is full \n");
        //clear all space
        if(mdb_enh_pool !=NULL){
            for(u8 i=0;i<mdb_enh_pool->pool_sz;i++){
                dinamic_address_space_t* pointer;
                pointer = p_link_functions->os_pool_get_by_index(mdb_enh_pool,i);
                if(pointer!=NULL){
                    if(((u32)pointer->data >= (u32)(&_ram_start_address)) && \
                            ((u32)pointer->data < ((u32)(&_ram_start_address) + (u32)(&_task_ram_size)))){
                        p_link_functions->os_pool_free(mdb_enh_pool,pointer);
                    }
                }
            }
            len1 = MDB_DINAMIC_ADDRESS_SPACE_SIZE;
            len2 = MDB_ARRAY_ADDRESS_SPACE_SIZE;
            if(len1){
                mdb_enh_pool = p_link_functions->modbus_bind_address_space(BEREMIZ_ADDRESS_SPACE_START,mdb_address_space,&len1);
            }else{
                len1 =1;
            }
            if(len2){
                mdb_enh_pool = p_link_functions->modbus_bind_address_space(BEREMIZ_ARRAY_ADDRESS_SPACE_START,mdb_array_address_space,&len2);
            }else{
                len2 =1;
            }
            if((len1==0) || (len2==0)){
                result =-1;
            }
        }else{
            result =-1;
        }
    }

    return result;
}

static void modbus_terminate(void){
    static osThreadId modbus_master_id;
    for (u8 i=0;i<MAX_MODBUS_MASTER_NODES;i++){
        char MODBUS_THREAD_NAME[] = "modbus_master_1";
        switch(i){
        case(0):
            MODBUS_THREAD_NAME[14] = 0x30;
            break;
        case(1):
            MODBUS_THREAD_NAME[14] = 0x31;
            break;
        case(2):
            MODBUS_THREAD_NAME[14] = 0x32;
            break;
        case(3):
            MODBUS_THREAD_NAME[14] = 0x33;
            break;
        case(4):
            MODBUS_THREAD_NAME[14] = 0x34;
            break;
        case(5):
            MODBUS_THREAD_NAME[14] = 0x35;
            break;
        case(6):
            MODBUS_THREAD_NAME[14] = 0x36;
            break;
        }
        modbus_master_id = p_link_functions->os_thread_get_id_by_name(MODBUS_THREAD_NAME);
        if(modbus_master_id!=NULL){
            p_link_functions->os_thread_terminate(modbus_master_id);
            p_link_functions->printf("USER_TASK:terminate preview thread");
        }
    }
}

static int add_regs_description(void){
	int result = 0;
	if(BEREMIZ_REGS_VAR_NUM){
		if(p_link_functions->regs_description_add_user_vars(beremiz_regs_description,(u16)BEREMIZ_REGS_VAR_NUM)==0){
			result = 1;
		}
	}
	return result;
}
#if CAN_FEST_ENABLE
/** if dict == null, set to default
* return less zero vslue if error occured
*/
static int change_can_dict(void * dict){
    int result = 0;
    static osThreadId can_task_id = NULL;
    char CAN_TASK_NAME[] = "can_task";
    can_task_id = p_link_functions->os_thread_get_id_by_name(CAN_TASK_NAME);
    if(can_task_id != NULL){
        if(dict!=NULL){
            while(p_link_functions->os_signal_set(can_task_id,(s32)dict) < 0){p_link_functions->os_delay(1);}
        }else{
            while(p_link_functions->os_signal_set(can_task_id,(s32)CAN_DICT_DEFAULT) < 0){p_link_functions->os_delay(1);}
        }
    }else{
        result = -1;
    }
    return result;
}
#endif
#endif //BEREMIZ_TASK_C
