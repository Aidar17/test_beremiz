/**
 * @file beremiz_regs_description.c
 * @author Shoma Gane <shomagan@gmail.com>
 *         Ayrat Girfanov <girfanov.ayrat@yandex.ru>
 * @defgroup beremiz
 * @ingroup beremiz
 * @version 0.1 
 * @brief  TODO!!! write brief in 
 */
/*
 * Copyright (c) 2018 Snema Service
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
 * SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY
 * OF SUCH DAMAGE.
 *
 * This file is part of the sofi PLC.
 *
 * Author: Shoma Gane <shomagan@gmail.com>
 *         Ayrat Girfanov <girfanov.ayrat@yandex.ru>
 */
#ifndef BEREMIZ_REGS_DESCRIPTION_C
#define BEREMIZ_REGS_DESCRIPTION_C 1
#include "beremiz_regs_description.h"
#include "regs_description.h"
#include "sofi_config.h"
#include "beremiz_task.h"
#include "bkram_access.h"
#include "matiec/iec_types_all.h"


extern u8 mdb_address_space[];
extern u8 mdb_array_address_space[];



extern __IEC_REAL_t CONFIG__V_PWR_MIN;
extern __IEC_REAL_t CONFIG__V_PWR_MAX;
extern __IEC_REAL_t CONFIG__V_BAT_MIN;
extern __IEC_REAL_t CONFIG__V_BAT_MAX;
extern __IEC_REAL_t CONFIG__V_BAT_0;
extern __IEC_REAL_t CONFIG__V_PWR_0;


regs_description_t const beremiz_regs_description[BEREMIZ_REGS_VAR_NUM]={
    
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__V_PWR_MIN))->value,/*saves address*/0,/*description*/"V_PWR_MIN",/*name*/"V_PWR_MIN",/*type*/FLOAT_REGS_FLAG,/*ind*/0  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__V_PWR_MAX))->value,/*saves address*/0,/*description*/"V_PWR_MAX",/*name*/"V_PWR_MAX",/*type*/FLOAT_REGS_FLAG,/*ind*/1  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__V_BAT_MIN))->value,/*saves address*/0,/*description*/"V_BAT_MIN",/*name*/"V_BAT_MIN",/*type*/FLOAT_REGS_FLAG,/*ind*/2  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__V_BAT_MAX))->value,/*saves address*/0,/*description*/"V_BAT_MAX",/*name*/"V_BAT_MAX",/*type*/FLOAT_REGS_FLAG,/*ind*/3  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__V_BAT_0))->value,/*saves address*/0,/*description*/"V_BAT_0",/*name*/"V_BAT_0",/*type*/FLOAT_REGS_FLAG,/*ind*/4  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__V_PWR_0))->value,/*saves address*/0,/*description*/"V_PWR_0",/*name*/"V_PWR_0",/*type*/FLOAT_REGS_FLAG,/*ind*/5  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},

};
#endif//BEREMIZ_REGS_DESCRIPTION_C
