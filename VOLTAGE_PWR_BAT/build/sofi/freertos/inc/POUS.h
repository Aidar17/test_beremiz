/**
 * @file C:\Beremiz\beremiz_test_projects\VOLTAGE_PWR_BAT\build//sofi/freertos/inc/POUS.h
 * @author Shoma Gane <shomagan@gmail.com>
 *         Ayrat Girfanov <girfanov.ayrat@yandex.ru>
 * @defgroup beremiz 
 * @ingroup beremiz 
 * @version 0.1 
 * @brief  TODO!!! write brief in 
 */
    /*
     * Copyright (c) 2018 Snema Service
     * All rights reserved.
     *
     * Redistribution and use in source and binary forms, with or without modification,
     * are permitted provided that the following conditions are met:
     *
     * 1. Redistributions of source code must retain the above copyright notice,
     *    this list of conditions and the following disclaimer.
     * 2. Redistributions in binary form must reproduce the above copyright notice,
     *    this list of conditions and the following disclaimer in the documentation
     *    and/or other materials provided with the distribution.
     * 3. The name of the author may not be used to endorse or promote products
     *    derived from this software without specific prior written permission.
     *
     * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
     * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
     * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
     * SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
     * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
     * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
     * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
     * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
     * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY
     * OF SUCH DAMAGE.
     *
     * This file is part of the sofi PLC.
     *
     * Author: Shoma Gane <shomagan@gmail.com>
     *         Ayrat Girfanov <girfanov.ayrat@yandex.ru>
     */
#ifndef POUS_H
#define POUS_H
/*add includes below */
#include "sofi_beremiz.h"
#include "type_def.h"
#include "sofi_init.h"

/*add includes before */
#include "beremiz.h"
//#ifndef __POUS_H$deleted after handing SNEMA generator
//#define __POUS_H$deleted after handing SNEMA generator
#define BKRAM_VERIFY_NUMBER_USER 0x17b9a70a66554445 //random value after compile

#if defined __cplusplus
extern "C" {
#endif
#include "matiec/accessor.h"
#include "matiec/iec_std_lib.h"

// FUNCTION_BLOCK FUNCTIONBLOCK
// Data part
typedef struct {
  // FB Interface - IN, OUT, IN_OUT variables
  __DECLARE_VAR(BOOL,EN)
  __DECLARE_VAR(BOOL,ENO)
  __DECLARE_VAR(REAL,V_PWR)
  __DECLARE_VAR(REAL,V_BAT)
  __DECLARE_VAR(REAL,V_PWR_MIN)
  __DECLARE_VAR(REAL,V_PWR_MIN_IN)
  __DECLARE_VAR(REAL,V_PWR_MAX)
  __DECLARE_VAR(REAL,V_PWR_MAX_IN)
  __DECLARE_VAR(REAL,V_BAT_MIN)
  __DECLARE_VAR(REAL,V_BAT_MIN_IN)
  __DECLARE_VAR(REAL,V_BAT_MAX)
  __DECLARE_VAR(REAL,V_BAT_MAX_IN)

  // FB private variables - TEMP, private and located variables

} FUNCTIONBLOCK;

void FUNCTIONBLOCK_init__(FUNCTIONBLOCK *data__, BOOL retain);
// Code part
void FUNCTIONBLOCK_body__(FUNCTIONBLOCK *data__);
// PROGRAM PROGRAM0
// Data part
typedef struct {
  // PROGRAM Interface - IN, OUT, IN_OUT variables

  // PROGRAM private variables - TEMP, private and located variables
  READ_PWR READ_PWR0;
  __DECLARE_EXTERNAL(REAL,V_PWR_MIN)
  __DECLARE_EXTERNAL(REAL,V_PWR_MAX)
  __DECLARE_EXTERNAL(REAL,V_BAT_MIN)
  __DECLARE_EXTERNAL(REAL,V_BAT_MAX)
  __DECLARE_EXTERNAL(REAL,V_BAT_0)
  __DECLARE_EXTERNAL(REAL,V_PWR_0)
  FUNCTIONBLOCK FUNCTIONBLOCK0;

} PROGRAM0;

void PROGRAM0_init__(PROGRAM0 *data__, BOOL retain);
// Code part
void PROGRAM0_body__(PROGRAM0 *data__);
//#endif //__POUS_H$deleted after handing SNEMA generator

#if defined __cplusplus
}
#endif
#endif //POUS_H
