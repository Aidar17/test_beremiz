FUNCTION_BLOCK functionblock
  VAR_INPUT
    V_PWR : REAL;
    V_BAT : REAL;
  END_VAR
  VAR_OUTPUT RETAIN
    V_PWR_min : REAL := 31.0;
  END_VAR
  VAR_INPUT RETAIN
    V_PWR_min_in : REAL := 31.0;
  END_VAR
  VAR_OUTPUT RETAIN
    V_PWR_max : REAL := 0.0;
  END_VAR
  VAR_INPUT RETAIN
    V_PWR_max_in : REAL := 0.0;
  END_VAR
  VAR_OUTPUT RETAIN
    V_BAT_min : REAL := 4.5;
  END_VAR
  VAR_INPUT RETAIN
    V_BAT_min_in : REAL := 4.5;
  END_VAR
  VAR_OUTPUT RETAIN
    V_BAT_max : REAL := 0.0;
  END_VAR
  VAR_INPUT RETAIN
    V_BAT_max_in : REAL := 0.0;
  END_VAR

  IF V_PWR> V_PWR_max_in
  THEN V_PWR_max:=V_PWR;
  END_IF;

  IF V_PWR< V_PWR_min_in
  THEN V_PWR_min:=V_PWR;
  END_IF;

  IF V_BAT> V_BAT_max_in
  THEN V_BAT_max:=V_BAT;
  END_IF;

  IF V_BAT< V_BAT_min_in
  THEN V_BAT_min:=V_BAT;
  END_IF;
END_FUNCTION_BLOCK

PROGRAM program0
  VAR
    READ_PWR0 : READ_PWR;
  END_VAR
  VAR_EXTERNAL
    V_PWR_min : REAL;
    V_PWR_max : REAL;
    V_BAT_min : REAL;
    V_BAT_max : REAL;
    V_BAT_0 : REAL;
    V_PWR_0 : REAL;
  END_VAR
  VAR
    functionblock0 : functionblock;
  END_VAR

  READ_PWR0();
  functionblock0(V_PWR := READ_PWR0.V_PWR, V_BAT := READ_PWR0.V_BAT, V_PWR_min_in := V_PWR_min, V_PWR_max_in := V_PWR_max, V_BAT_min_in := V_BAT_min, V_BAT_max_in := V_BAT_max);
  V_PWR_min := functionblock0.V_PWR_min;
  V_PWR_0 := READ_PWR0.V_PWR;
  V_PWR_max := functionblock0.V_PWR_max;
  V_BAT_0 := READ_PWR0.V_BAT;
  V_BAT_min := functionblock0.V_BAT_min;
  V_BAT_max := functionblock0.V_BAT_max;
END_PROGRAM


CONFIGURATION config
  VAR_GLOBAL RETAIN
    V_PWR_min : REAL;
    V_PWR_max : REAL;
    V_BAT_min : REAL;
    V_BAT_max : REAL;
    V_BAT_0 : REAL;
    V_PWR_0 : REAL;
  END_VAR

  RESOURCE resource1 ON PLC
    TASK task0(INTERVAL := T#10s0ms,PRIORITY := 0);
    PROGRAM instance0 WITH task0 : program0;
  END_RESOURCE
END_CONFIGURATION
