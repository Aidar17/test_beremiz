void FUNCTIONBLOCK_init__(FUNCTIONBLOCK *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->V_PWR,0,retain)
  __INIT_VAR(data__->V_BAT,0,retain)
  __INIT_VAR(data__->V_PWR_MIN,31.0,1)
  __INIT_VAR(data__->V_PWR_MIN_IN,31.0,1)
  __INIT_VAR(data__->V_PWR_MAX,0.0,1)
  __INIT_VAR(data__->V_PWR_MAX_IN,0.0,1)
  __INIT_VAR(data__->V_BAT_MIN,4.5,1)
  __INIT_VAR(data__->V_BAT_MIN_IN,4.5,1)
  __INIT_VAR(data__->V_BAT_MAX,0.0,1)
  __INIT_VAR(data__->V_BAT_MAX_IN,0.0,1)
}

// Code part
void FUNCTIONBLOCK_body__(FUNCTIONBLOCK *data__) {
  // Control execution
  if (!__GET_VAR(data__->EN)) {
    __SET_VAR(data__->,ENO,,__BOOL_LITERAL(FALSE));
    return;
  }
  else {
    __SET_VAR(data__->,ENO,,__BOOL_LITERAL(TRUE));
  }
  // Initialise TEMP variables

  if ((__GET_VAR(data__->V_PWR,) > __GET_VAR(data__->V_PWR_MAX_IN,))) {
    __SET_VAR(data__->,V_PWR_MAX,,__GET_VAR(data__->V_PWR,));
  };
  if ((__GET_VAR(data__->V_PWR,) < __GET_VAR(data__->V_PWR_MIN_IN,))) {
    __SET_VAR(data__->,V_PWR_MIN,,__GET_VAR(data__->V_PWR,));
  };
  if ((__GET_VAR(data__->V_BAT,) > __GET_VAR(data__->V_BAT_MAX_IN,))) {
    __SET_VAR(data__->,V_BAT_MAX,,__GET_VAR(data__->V_BAT,));
  };
  if ((__GET_VAR(data__->V_BAT,) < __GET_VAR(data__->V_BAT_MIN_IN,))) {
    __SET_VAR(data__->,V_BAT_MIN,,__GET_VAR(data__->V_BAT,));
  };

  goto __end;

__end:
  return;
} // FUNCTIONBLOCK_body__() 





void PROGRAM0_init__(PROGRAM0 *data__, BOOL retain) {
  READ_PWR_init__(&data__->READ_PWR0,retain);
  __INIT_EXTERNAL(REAL,V_PWR_MIN,data__->V_PWR_MIN,retain)
  __INIT_EXTERNAL(REAL,V_PWR_MAX,data__->V_PWR_MAX,retain)
  __INIT_EXTERNAL(REAL,V_BAT_MIN,data__->V_BAT_MIN,retain)
  __INIT_EXTERNAL(REAL,V_BAT_MAX,data__->V_BAT_MAX,retain)
  __INIT_EXTERNAL(REAL,V_BAT_0,data__->V_BAT_0,retain)
  __INIT_EXTERNAL(REAL,V_PWR_0,data__->V_PWR_0,retain)
  FUNCTIONBLOCK_init__(&data__->FUNCTIONBLOCK0,retain);
}

// Code part
void PROGRAM0_body__(PROGRAM0 *data__) {
  // Initialise TEMP variables

  READ_PWR_body__(&data__->READ_PWR0);
  __SET_VAR(data__->FUNCTIONBLOCK0.,V_PWR,,__GET_VAR(data__->READ_PWR0.V_PWR,));
  __SET_VAR(data__->FUNCTIONBLOCK0.,V_BAT,,__GET_VAR(data__->READ_PWR0.V_BAT,));
  __SET_VAR(data__->FUNCTIONBLOCK0.,V_PWR_MIN_IN,,__GET_EXTERNAL(data__->V_PWR_MIN,));
  __SET_VAR(data__->FUNCTIONBLOCK0.,V_PWR_MAX_IN,,__GET_EXTERNAL(data__->V_PWR_MAX,));
  __SET_VAR(data__->FUNCTIONBLOCK0.,V_BAT_MIN_IN,,__GET_EXTERNAL(data__->V_BAT_MIN,));
  __SET_VAR(data__->FUNCTIONBLOCK0.,V_BAT_MAX_IN,,__GET_EXTERNAL(data__->V_BAT_MAX,));
  FUNCTIONBLOCK_body__(&data__->FUNCTIONBLOCK0);
  __SET_EXTERNAL(data__->,V_PWR_MIN,,__GET_VAR(data__->FUNCTIONBLOCK0.V_PWR_MIN,));
  __SET_EXTERNAL(data__->,V_PWR_0,,__GET_VAR(data__->READ_PWR0.V_PWR,));
  __SET_EXTERNAL(data__->,V_PWR_MAX,,__GET_VAR(data__->FUNCTIONBLOCK0.V_PWR_MAX,));
  __SET_EXTERNAL(data__->,V_BAT_0,,__GET_VAR(data__->READ_PWR0.V_BAT,));
  __SET_EXTERNAL(data__->,V_BAT_MIN,,__GET_VAR(data__->FUNCTIONBLOCK0.V_BAT_MIN,));
  __SET_EXTERNAL(data__->,V_BAT_MAX,,__GET_VAR(data__->FUNCTIONBLOCK0.V_BAT_MAX,));

  goto __end;

__end:
  return;
} // PROGRAM0_body__() 





