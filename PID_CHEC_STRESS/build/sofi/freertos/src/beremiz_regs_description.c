/**
 * @file beremiz_regs_description.c
 * @author Shoma Gane <shomagan@gmail.com>
 *         Ayrat Girfanov <girfanov.ayrat@yandex.ru>
 * @defgroup beremiz
 * @ingroup beremiz
 * @version 0.1 
 * @brief  TODO!!! write brief in 
 */
/*
 * Copyright (c) 2018 Snema Service
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
 * SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY
 * OF SUCH DAMAGE.
 *
 * This file is part of the sofi PLC.
 *
 * Author: Shoma Gane <shomagan@gmail.com>
 *         Ayrat Girfanov <girfanov.ayrat@yandex.ru>
 */
#ifndef BEREMIZ_REGS_DESCRIPTION_C
#define BEREMIZ_REGS_DESCRIPTION_C 1
#include "beremiz_regs_description.h"
#include "regs_description.h"
#include "sofi_config.h"
#include "beremiz_task.h"
#include "bkram_access.h"
#include "matiec/iec_types_all.h"


extern u8 mdb_address_space[];
extern u8 mdb_array_address_space[];



extern __IEC_DINT_t CONFIG__TEST_CONFIG;
extern __IEC_UDINT_t CONFIG__TIME_START;
extern __IEC_BOOL_t CONFIG__AUTO_MAN;
extern __IEC_BOOL_t CONFIG__AUTO_MAN0;
extern __IEC_BOOL_t CONFIG__AUTO_MAN1;
extern __IEC_BOOL_t CONFIG__AUTO_MAN2;
extern __IEC_BOOL_t CONFIG__AUTO_MAN3;
extern __IEC_BOOL_t CONFIG__AUTO_MAN4;
extern __IEC_REAL_t CONFIG__ER;
extern __IEC_REAL_t CONFIG__ER0;
extern __IEC_REAL_t CONFIG__ER1;
extern __IEC_REAL_t CONFIG__ER2;
extern __IEC_REAL_t CONFIG__ER3;
extern __IEC_REAL_t CONFIG__ER4;
extern __IEC_REAL_t CONFIG__ER5;
extern __IEC_REAL_t CONFIG__ER6;
extern __IEC_REAL_t CONFIG__ER7;
extern __IEC_REAL_t CONFIG__ER8;
extern __IEC_REAL_t CONFIG__ER9;
extern __IEC_REAL_t CONFIG__ER10;
extern __IEC_REAL_t CONFIG__ER11;
extern __IEC_REAL_t CONFIG__ER12;
extern __IEC_REAL_t CONFIG__ER13;
extern __IEC_REAL_t CONFIG__ER14;
extern __IEC_REAL_t CONFIG__ER15;
extern __IEC_REAL_t CONFIG__ER16;
extern __IEC_REAL_t CONFIG__ER17;
extern __IEC_REAL_t CONFIG__ER18;
extern __IEC_REAL_t CONFIG__ER19;
extern __IEC_REAL_t CONFIG__ER20;
extern __IEC_REAL_t CONFIG__ER21;
extern __IEC_REAL_t CONFIG__ER22;
extern __IEC_REAL_t CONFIG__ER23;
extern __IEC_REAL_t CONFIG__ER24;
extern __IEC_REAL_t CONFIG__ER25;
extern __IEC_REAL_t CONFIG__ER26;
extern __IEC_REAL_t CONFIG__ER27;
extern __IEC_REAL_t CONFIG__ER28;
extern __IEC_REAL_t CONFIG__ER29;
extern __IEC_REAL_t CONFIG__ER30;
extern __IEC_REAL_t CONFIG__ER31;
extern __IEC_REAL_t CONFIG__ER32;
extern __IEC_REAL_t CONFIG__ER33;
extern __IEC_REAL_t CONFIG__ER34;
extern __IEC_REAL_t CONFIG__ER35;
extern __IEC_REAL_t CONFIG__ER36;
extern __IEC_REAL_t CONFIG__ER37;
extern __IEC_REAL_t CONFIG__ER38;
extern __IEC_REAL_t CONFIG__ER39;
extern __IEC_REAL_t CONFIG__ER40;
extern __IEC_REAL_t CONFIG__ER41;
extern __IEC_REAL_t CONFIG__ER42;
extern __IEC_REAL_t CONFIG__ER43;
extern __IEC_REAL_t CONFIG__ER44;
extern __IEC_REAL_t CONFIG__ER45;
extern __IEC_REAL_t CONFIG__ER46;
extern __IEC_REAL_t CONFIG__ER47;
extern __IEC_REAL_t CONFIG__ER48;
extern __IEC_REAL_t CONFIG__ER49;
extern __IEC_REAL_t CONFIG__ER50;
extern __IEC_REAL_t CONFIG__ER51;
extern __IEC_REAL_t CONFIG__ER52;
extern __IEC_REAL_t CONFIG__ER53;
extern __IEC_REAL_t CONFIG__ER54;
extern __IEC_REAL_t CONFIG__ER55;
extern __IEC_REAL_t CONFIG__ER56;
extern __IEC_REAL_t CONFIG__ER57;
extern __IEC_REAL_t CONFIG__ER58;
extern __IEC_REAL_t CONFIG__ER59;
extern __IEC_REAL_t CONFIG__ER60;
extern __IEC_REAL_t CONFIG__ER61;
extern __IEC_REAL_t CONFIG__ER62;
extern __IEC_REAL_t CONFIG__ER63;
extern __IEC_REAL_t CONFIG__ER64;
extern __IEC_REAL_t CONFIG__ER65;
extern __IEC_REAL_t CONFIG__ER66;
extern __IEC_REAL_t CONFIG__ER67;
extern __IEC_REAL_t CONFIG__ER68;
extern __IEC_REAL_t CONFIG__ER69;
extern __IEC_REAL_t CONFIG__ER70;
extern __IEC_REAL_t CONFIG__ER71;
extern __IEC_REAL_t CONFIG__ER72;
extern __IEC_REAL_t CONFIG__ER73;
extern __IEC_REAL_t CONFIG__ER74;
extern __IEC_REAL_t CONFIG__ER75;
extern __IEC_REAL_t CONFIG__ER76;
extern __IEC_REAL_t CONFIG__ER77;
extern __IEC_REAL_t CONFIG__ER78;
extern __IEC_REAL_t CONFIG__ER79;
extern __IEC_REAL_t CONFIG__ER80;
extern __IEC_REAL_t CONFIG__ER81;
extern __IEC_REAL_t CONFIG__ER82;
extern __IEC_REAL_t CONFIG__ER83;
extern __IEC_REAL_t CONFIG__ER84;
extern __IEC_REAL_t CONFIG__ER85;
extern __IEC_REAL_t CONFIG__ER86;
extern __IEC_REAL_t CONFIG__ER87;
extern __IEC_REAL_t CONFIG__ER88;
extern __IEC_REAL_t CONFIG__ER89;
extern __IEC_REAL_t CONFIG__ER90;
extern __IEC_REAL_t CONFIG__ER91;
extern __IEC_REAL_t CONFIG__ER92;
extern __IEC_REAL_t CONFIG__ER93;
extern __IEC_REAL_t CONFIG__ER94;
extern __IEC_REAL_t CONFIG__ER95;
extern __IEC_REAL_t CONFIG__ER96;
extern __IEC_REAL_t CONFIG__ER97;
extern __IEC_REAL_t CONFIG__ER98;
extern __IEC_REAL_t CONFIG__ER99;
extern __IEC_REAL_t CONFIG__ER100;
extern __IEC_REAL_t CONFIG__ER101;
extern __IEC_REAL_t CONFIG__ER102;
extern __IEC_REAL_t CONFIG__ER103;
extern __IEC_REAL_t CONFIG__ER104;
extern __IEC_REAL_t CONFIG__ER105;
extern __IEC_REAL_t CONFIG__ER106;
extern __IEC_REAL_t CONFIG__ER107;
extern __IEC_REAL_t CONFIG__ER108;
extern __IEC_REAL_t CONFIG__ER109;
extern __IEC_REAL_t CONFIG__ER110;
extern __IEC_REAL_t CONFIG__ER111;
extern __IEC_REAL_t CONFIG__ER112;
extern __IEC_REAL_t CONFIG__ER113;
extern __IEC_REAL_t CONFIG__ER114;
extern __IEC_REAL_t CONFIG__ER115;
extern __IEC_REAL_t CONFIG__ER116;
extern __IEC_REAL_t CONFIG__ER117;
extern __IEC_REAL_t CONFIG__ER118;
extern __IEC_REAL_t CONFIG__ER119;
extern __IEC_REAL_t CONFIG__ER120;
extern __IEC_REAL_t CONFIG__ER121;
extern __IEC_REAL_t CONFIG__ER122;
extern __IEC_REAL_t CONFIG__ER123;
extern __IEC_REAL_t CONFIG__ER124;
extern __IEC_REAL_t CONFIG__ER125;
extern __IEC_REAL_t CONFIG__ER126;
extern __IEC_REAL_t CONFIG__ER127;
extern __IEC_REAL_t CONFIG__ER128;
extern __IEC_REAL_t CONFIG__ER129;
extern __IEC_REAL_t CONFIG__ER130;
extern __IEC_REAL_t CONFIG__ER131;
extern __IEC_REAL_t CONFIG__ER132;
extern __IEC_REAL_t CONFIG__ER133;
extern __IEC_REAL_t CONFIG__ER134;
extern __IEC_REAL_t CONFIG__ER135;
extern __IEC_REAL_t CONFIG__ER136;
extern __IEC_REAL_t CONFIG__ER137;
extern __IEC_REAL_t CONFIG__ER138;
extern __IEC_REAL_t CONFIG__ER139;
extern __IEC_REAL_t CONFIG__ER140;
extern __IEC_REAL_t CONFIG__ER141;
extern __IEC_REAL_t CONFIG__ER142;
extern __IEC_REAL_t CONFIG__ER143;
extern __IEC_REAL_t CONFIG__ER144;
extern __IEC_REAL_t CONFIG__ER145;
extern __IEC_REAL_t CONFIG__ER146;
extern __IEC_REAL_t CONFIG__ER147;
extern __IEC_REAL_t CONFIG__ER148;
extern __IEC_REAL_t CONFIG__ER149;
extern __IEC_REAL_t CONFIG__ER150;
extern __IEC_REAL_t CONFIG__ER151;
extern __IEC_REAL_t CONFIG__ER152;
extern __IEC_REAL_t CONFIG__ER153;
extern __IEC_REAL_t CONFIG__ER154;
extern __IEC_REAL_t CONFIG__ER155;
extern __IEC_REAL_t CONFIG__ER156;
extern __IEC_REAL_t CONFIG__ER157;
extern __IEC_REAL_t CONFIG__ER158;
extern __IEC_REAL_t CONFIG__ER159;
extern __IEC_REAL_t CONFIG__PROPORTIONAL;
extern __IEC_REAL_t CONFIG__INTEGRAL_0;
extern __IEC_REAL_t CONFIG__DIFFERENTIAL;
extern __IEC_REAL_t CONFIG__TASK_MANUAL;
extern __IEC_REAL_t CONFIG__TASK_AUTO;
extern __IEC_UDINT_t CONFIG__OUT_TIME_MAX;
extern __IEC_UDINT_t CONFIG__OUT_TIME_MAX0;
extern __IEC_UDINT_t CONFIG__OUT_TIME_MAX1;
extern __IEC_UDINT_t CONFIG__OUT_TIME_MAX2;
extern __IEC_UDINT_t CONFIG__OUT_TIME_MAX3;
extern __IEC_UDINT_t CONFIG__OUT_TIME_MAX4;
extern __IEC_UDINT_t CONFIG__OUT_TIME_MIN;
extern __IEC_UDINT_t CONFIG__OUT_TIME_MIN0;
extern __IEC_UDINT_t CONFIG__OUT_TIME_MIN1;
extern __IEC_UDINT_t CONFIG__OUT_TIME_MIN2;
extern __IEC_UDINT_t CONFIG__OUT_TIME_MIN3;
extern __IEC_UDINT_t CONFIG__OUT_TIME_MIN4;
extern __IEC_REAL_t CONFIG__OUT_MIN;
extern __IEC_REAL_t CONFIG__OUT_MIN0;
extern __IEC_REAL_t CONFIG__OUT_MIN1;
extern __IEC_REAL_t CONFIG__OUT_MIN2;
extern __IEC_REAL_t CONFIG__OUT_MIN3;
extern __IEC_REAL_t CONFIG__OUT_MIN4;
extern __IEC_REAL_t CONFIG__OUT_MAX;
extern __IEC_REAL_t CONFIG__OUT_MAX0;
extern __IEC_REAL_t CONFIG__OUT_MAX1;
extern __IEC_REAL_t CONFIG__OUT_MAX2;
extern __IEC_REAL_t CONFIG__OUT_MAX3;
extern __IEC_REAL_t CONFIG__OUT_MAX4;
extern __IEC_REAL_t CONFIG__K1;
extern __IEC_REAL_t CONFIG__K10;
extern __IEC_REAL_t CONFIG__K11;
extern __IEC_REAL_t CONFIG__K12;
extern __IEC_REAL_t CONFIG__K13;
extern __IEC_REAL_t CONFIG__K14;
extern __IEC_REAL_t CONFIG__K15;
extern __IEC_REAL_t CONFIG__K16;
extern __IEC_REAL_t CONFIG__K17;
extern __IEC_REAL_t CONFIG__K18;
extern __IEC_REAL_t CONFIG__K19;
extern __IEC_REAL_t CONFIG__K110;
extern __IEC_REAL_t CONFIG__K111;
extern __IEC_REAL_t CONFIG__K112;
extern __IEC_REAL_t CONFIG__K113;
extern __IEC_REAL_t CONFIG__K114;
extern __IEC_REAL_t CONFIG__K115;
extern __IEC_REAL_t CONFIG__K116;
extern __IEC_REAL_t CONFIG__K117;
extern __IEC_REAL_t CONFIG__K118;
extern __IEC_REAL_t CONFIG__K119;
extern __IEC_REAL_t CONFIG__K120;
extern __IEC_REAL_t CONFIG__K121;
extern __IEC_REAL_t CONFIG__K122;
extern __IEC_REAL_t CONFIG__K123;
extern __IEC_REAL_t CONFIG__K124;
extern __IEC_REAL_t CONFIG__K125;
extern __IEC_REAL_t CONFIG__K126;
extern __IEC_REAL_t CONFIG__K127;
extern __IEC_REAL_t CONFIG__K128;
extern __IEC_REAL_t CONFIG__K129;
extern __IEC_REAL_t CONFIG__K130;
extern __IEC_REAL_t CONFIG__K131;
extern __IEC_REAL_t CONFIG__K132;
extern __IEC_REAL_t CONFIG__K133;
extern __IEC_REAL_t CONFIG__K134;
extern __IEC_REAL_t CONFIG__K135;
extern __IEC_REAL_t CONFIG__K136;
extern __IEC_REAL_t CONFIG__K137;
extern __IEC_REAL_t CONFIG__K138;
extern __IEC_REAL_t CONFIG__K139;
extern __IEC_REAL_t CONFIG__K140;
extern __IEC_REAL_t CONFIG__K141;
extern __IEC_REAL_t CONFIG__K142;
extern __IEC_REAL_t CONFIG__K143;
extern __IEC_REAL_t CONFIG__K144;
extern __IEC_REAL_t CONFIG__K145;
extern __IEC_REAL_t CONFIG__K146;
extern __IEC_REAL_t CONFIG__K147;
extern __IEC_REAL_t CONFIG__K148;
extern __IEC_REAL_t CONFIG__K149;
extern __IEC_REAL_t CONFIG__K150;
extern __IEC_REAL_t CONFIG__K151;
extern __IEC_REAL_t CONFIG__K152;
extern __IEC_REAL_t CONFIG__K153;
extern __IEC_REAL_t CONFIG__K154;
extern __IEC_REAL_t CONFIG__K155;
extern __IEC_REAL_t CONFIG__K156;
extern __IEC_REAL_t CONFIG__K157;
extern __IEC_REAL_t CONFIG__K158;
extern __IEC_REAL_t CONFIG__K159;
extern __IEC_REAL_t CONFIG__K160;
extern __IEC_REAL_t CONFIG__K161;
extern __IEC_REAL_t CONFIG__K162;
extern __IEC_REAL_t CONFIG__K163;
extern __IEC_REAL_t CONFIG__K164;
extern __IEC_REAL_t CONFIG__K165;
extern __IEC_REAL_t CONFIG__K166;
extern __IEC_REAL_t CONFIG__K167;
extern __IEC_REAL_t CONFIG__K168;
extern __IEC_REAL_t CONFIG__K169;
extern __IEC_REAL_t CONFIG__K170;
extern __IEC_REAL_t CONFIG__K171;
extern __IEC_REAL_t CONFIG__K172;
extern __IEC_REAL_t CONFIG__K173;
extern __IEC_REAL_t CONFIG__K174;
extern __IEC_REAL_t CONFIG__K175;
extern __IEC_REAL_t CONFIG__K176;
extern __IEC_REAL_t CONFIG__K177;
extern __IEC_REAL_t CONFIG__K178;
extern __IEC_REAL_t CONFIG__K179;
extern __IEC_REAL_t CONFIG__K180;
extern __IEC_REAL_t CONFIG__K181;
extern __IEC_REAL_t CONFIG__K182;
extern __IEC_REAL_t CONFIG__K183;
extern __IEC_REAL_t CONFIG__K184;
extern __IEC_REAL_t CONFIG__K185;
extern __IEC_REAL_t CONFIG__K186;
extern __IEC_REAL_t CONFIG__K187;
extern __IEC_REAL_t CONFIG__K188;
extern __IEC_REAL_t CONFIG__K189;
extern __IEC_REAL_t CONFIG__K190;
extern __IEC_REAL_t CONFIG__K191;
extern __IEC_REAL_t CONFIG__K192;
extern __IEC_REAL_t CONFIG__K193;
extern __IEC_REAL_t CONFIG__K194;
extern __IEC_REAL_t CONFIG__K195;
extern __IEC_REAL_t CONFIG__K196;
extern __IEC_REAL_t CONFIG__K197;
extern __IEC_REAL_t CONFIG__K198;
extern __IEC_REAL_t CONFIG__K199;
extern __IEC_REAL_t CONFIG__K1000;
extern __IEC_REAL_t CONFIG__K1001;
extern __IEC_REAL_t CONFIG__K1002;
extern __IEC_REAL_t CONFIG__K1003;
extern __IEC_REAL_t CONFIG__K1004;
extern __IEC_REAL_t CONFIG__K1005;
extern __IEC_REAL_t CONFIG__K1006;
extern __IEC_REAL_t CONFIG__K1007;
extern __IEC_REAL_t CONFIG__K1008;
extern __IEC_REAL_t CONFIG__K1009;
extern __IEC_REAL_t CONFIG__K1010;
extern __IEC_REAL_t CONFIG__K1011;
extern __IEC_REAL_t CONFIG__K1012;
extern __IEC_REAL_t CONFIG__K1013;
extern __IEC_REAL_t CONFIG__K1014;
extern __IEC_REAL_t CONFIG__K1015;
extern __IEC_REAL_t CONFIG__K1016;
extern __IEC_REAL_t CONFIG__K1017;
extern __IEC_REAL_t CONFIG__K1018;
extern __IEC_REAL_t CONFIG__K1019;
extern __IEC_REAL_t CONFIG__K1020;
extern __IEC_REAL_t CONFIG__K1021;
extern __IEC_REAL_t CONFIG__K1022;
extern __IEC_REAL_t CONFIG__K1023;
extern __IEC_REAL_t CONFIG__K1024;
extern __IEC_REAL_t CONFIG__K1025;
extern __IEC_REAL_t CONFIG__K1026;
extern __IEC_REAL_t CONFIG__K1027;
extern __IEC_REAL_t CONFIG__K1028;
extern __IEC_REAL_t CONFIG__K1029;
extern __IEC_REAL_t CONFIG__K1030;
extern __IEC_REAL_t CONFIG__K1031;
extern __IEC_REAL_t CONFIG__K1032;
extern __IEC_REAL_t CONFIG__K1033;
extern __IEC_REAL_t CONFIG__K1034;
extern __IEC_REAL_t CONFIG__K1035;
extern __IEC_REAL_t CONFIG__K1036;
extern __IEC_REAL_t CONFIG__K1037;
extern __IEC_REAL_t CONFIG__K1038;
extern __IEC_REAL_t CONFIG__K1039;
extern __IEC_REAL_t CONFIG__K1040;
extern __IEC_REAL_t CONFIG__K1041;
extern __IEC_REAL_t CONFIG__K1042;
extern __IEC_REAL_t CONFIG__K1043;
extern __IEC_REAL_t CONFIG__K1044;
extern __IEC_REAL_t CONFIG__K1045;
extern __IEC_REAL_t CONFIG__K1046;
extern __IEC_REAL_t CONFIG__K1047;
extern __IEC_REAL_t CONFIG__K1048;
extern __IEC_REAL_t CONFIG__K1049;
extern __IEC_REAL_t CONFIG__K2;
extern __IEC_REAL_t CONFIG__K20;
extern __IEC_REAL_t CONFIG__K21;
extern __IEC_REAL_t CONFIG__K22;
extern __IEC_REAL_t CONFIG__K23;
extern __IEC_REAL_t CONFIG__K24;
extern __IEC_REAL_t CONFIG__K25;
extern __IEC_REAL_t CONFIG__K26;
extern __IEC_REAL_t CONFIG__K27;
extern __IEC_REAL_t CONFIG__K28;
extern __IEC_REAL_t CONFIG__K29;
extern __IEC_REAL_t CONFIG__K210;
extern __IEC_REAL_t CONFIG__K211;
extern __IEC_REAL_t CONFIG__K212;
extern __IEC_REAL_t CONFIG__K213;
extern __IEC_REAL_t CONFIG__K214;
extern __IEC_REAL_t CONFIG__K215;
extern __IEC_REAL_t CONFIG__K216;
extern __IEC_REAL_t CONFIG__K217;
extern __IEC_REAL_t CONFIG__K218;
extern __IEC_REAL_t CONFIG__K219;
extern __IEC_REAL_t CONFIG__K220;
extern __IEC_REAL_t CONFIG__K221;
extern __IEC_REAL_t CONFIG__K222;
extern __IEC_REAL_t CONFIG__K223;
extern __IEC_REAL_t CONFIG__K224;
extern __IEC_REAL_t CONFIG__K225;
extern __IEC_REAL_t CONFIG__K226;
extern __IEC_REAL_t CONFIG__K227;
extern __IEC_REAL_t CONFIG__K228;
extern __IEC_REAL_t CONFIG__K229;
extern __IEC_REAL_t CONFIG__K230;
extern __IEC_REAL_t CONFIG__K231;
extern __IEC_REAL_t CONFIG__K232;
extern __IEC_REAL_t CONFIG__K233;
extern __IEC_REAL_t CONFIG__K234;
extern __IEC_REAL_t CONFIG__K235;
extern __IEC_REAL_t CONFIG__K236;
extern __IEC_REAL_t CONFIG__K237;
extern __IEC_REAL_t CONFIG__K238;
extern __IEC_REAL_t CONFIG__K239;
extern __IEC_REAL_t CONFIG__K240;
extern __IEC_REAL_t CONFIG__K241;
extern __IEC_REAL_t CONFIG__K242;
extern __IEC_REAL_t CONFIG__K243;
extern __IEC_REAL_t CONFIG__K244;
extern __IEC_REAL_t CONFIG__K245;
extern __IEC_REAL_t CONFIG__K246;
extern __IEC_REAL_t CONFIG__K247;
extern __IEC_REAL_t CONFIG__K248;
extern __IEC_REAL_t CONFIG__K249;
extern __IEC_REAL_t CONFIG__K250;
extern __IEC_REAL_t CONFIG__K251;
extern __IEC_REAL_t CONFIG__K252;
extern __IEC_REAL_t CONFIG__K253;
extern __IEC_REAL_t CONFIG__K254;
extern __IEC_REAL_t CONFIG__K255;
extern __IEC_REAL_t CONFIG__K256;
extern __IEC_REAL_t CONFIG__K257;
extern __IEC_REAL_t CONFIG__K258;
extern __IEC_REAL_t CONFIG__K259;
extern __IEC_REAL_t CONFIG__K260;
extern __IEC_REAL_t CONFIG__K261;
extern __IEC_REAL_t CONFIG__K262;
extern __IEC_REAL_t CONFIG__K263;
extern __IEC_REAL_t CONFIG__K264;
extern __IEC_REAL_t CONFIG__K265;
extern __IEC_REAL_t CONFIG__K266;
extern __IEC_REAL_t CONFIG__K267;
extern __IEC_REAL_t CONFIG__K268;
extern __IEC_REAL_t CONFIG__K269;
extern __IEC_REAL_t CONFIG__K270;
extern __IEC_REAL_t CONFIG__K271;
extern __IEC_REAL_t CONFIG__K272;
extern __IEC_REAL_t CONFIG__K273;
extern __IEC_REAL_t CONFIG__K274;
extern __IEC_REAL_t CONFIG__K275;
extern __IEC_REAL_t CONFIG__K276;
extern __IEC_REAL_t CONFIG__K277;
extern __IEC_REAL_t CONFIG__K278;
extern __IEC_REAL_t CONFIG__K279;
extern __IEC_REAL_t CONFIG__K280;
extern __IEC_REAL_t CONFIG__K281;
extern __IEC_REAL_t CONFIG__K282;
extern __IEC_REAL_t CONFIG__K283;
extern __IEC_REAL_t CONFIG__K284;
extern __IEC_REAL_t CONFIG__K285;
extern __IEC_REAL_t CONFIG__K286;
extern __IEC_REAL_t CONFIG__K287;
extern __IEC_REAL_t CONFIG__K288;
extern __IEC_REAL_t CONFIG__K289;
extern __IEC_REAL_t CONFIG__K290;
extern __IEC_REAL_t CONFIG__K291;
extern __IEC_REAL_t CONFIG__K292;
extern __IEC_REAL_t CONFIG__K293;
extern __IEC_REAL_t CONFIG__K294;
extern __IEC_REAL_t CONFIG__K295;
extern __IEC_REAL_t CONFIG__K296;
extern __IEC_REAL_t CONFIG__K297;
extern __IEC_REAL_t CONFIG__K298;
extern __IEC_REAL_t CONFIG__K299;
extern __IEC_REAL_t CONFIG__K2000;
extern __IEC_REAL_t CONFIG__K2001;
extern __IEC_REAL_t CONFIG__K2002;
extern __IEC_REAL_t CONFIG__K2003;
extern __IEC_REAL_t CONFIG__K2004;
extern __IEC_REAL_t CONFIG__K2005;
extern __IEC_REAL_t CONFIG__K2006;
extern __IEC_REAL_t CONFIG__K2007;
extern __IEC_REAL_t CONFIG__K2008;
extern __IEC_REAL_t CONFIG__K2009;
extern __IEC_REAL_t CONFIG__K2010;
extern __IEC_REAL_t CONFIG__K2011;
extern __IEC_REAL_t CONFIG__K2012;
extern __IEC_REAL_t CONFIG__K2013;
extern __IEC_REAL_t CONFIG__K2014;
extern __IEC_REAL_t CONFIG__K2015;
extern __IEC_REAL_t CONFIG__K2016;
extern __IEC_REAL_t CONFIG__K2017;
extern __IEC_REAL_t CONFIG__K2018;
extern __IEC_REAL_t CONFIG__K2019;
extern __IEC_REAL_t CONFIG__K2020;
extern __IEC_REAL_t CONFIG__K2021;
extern __IEC_REAL_t CONFIG__K2022;
extern __IEC_REAL_t CONFIG__K2023;
extern __IEC_REAL_t CONFIG__K2024;
extern __IEC_REAL_t CONFIG__K2025;
extern __IEC_REAL_t CONFIG__K2026;
extern __IEC_REAL_t CONFIG__K2027;
extern __IEC_REAL_t CONFIG__K2028;
extern __IEC_REAL_t CONFIG__K2029;
extern __IEC_REAL_t CONFIG__K2030;
extern __IEC_REAL_t CONFIG__K2031;
extern __IEC_REAL_t CONFIG__K2032;
extern __IEC_REAL_t CONFIG__K2033;
extern __IEC_REAL_t CONFIG__K2034;
extern __IEC_REAL_t CONFIG__K2035;
extern __IEC_REAL_t CONFIG__K2036;
extern __IEC_REAL_t CONFIG__K2037;
extern __IEC_REAL_t CONFIG__K2038;
extern __IEC_REAL_t CONFIG__K2039;
extern __IEC_REAL_t CONFIG__K2040;
extern __IEC_REAL_t CONFIG__K2041;
extern __IEC_REAL_t CONFIG__K2042;
extern __IEC_REAL_t CONFIG__K2043;
extern __IEC_REAL_t CONFIG__K2044;
extern __IEC_REAL_t CONFIG__K2045;
extern __IEC_REAL_t CONFIG__K2046;
extern __IEC_REAL_t CONFIG__K2047;
extern __IEC_REAL_t CONFIG__K2048;
extern __IEC_REAL_t CONFIG__K2049;
extern __IEC_REAL_t CONFIG__K2050;


regs_description_t const beremiz_regs_description[BEREMIZ_REGS_VAR_NUM]={
    
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_DINT_t *)(&CONFIG__TEST_CONFIG))->value,/*saves address*/0,/*description*/"TEST_CONFIG",/*name*/"TEST_CONFIG",/*type*/S32_REGS_FLAG,/*ind*/0  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_UDINT_t *)(&CONFIG__TIME_START))->value,/*saves address*/0,/*description*/"TIME_START",/*name*/"TIME_START",/*type*/U32_REGS_FLAG,/*ind*/1  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_BOOL_t *)(&CONFIG__AUTO_MAN))->value,/*saves address*/0,/*description*/"AUTO_MAN",/*name*/"AUTO_MAN",/*type*/U8_REGS_FLAG,/*ind*/2  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_BOOL_t *)(&CONFIG__AUTO_MAN0))->value,/*saves address*/0,/*description*/"AUTO_MAN0",/*name*/"AUTO_MAN0",/*type*/U8_REGS_FLAG,/*ind*/3  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_BOOL_t *)(&CONFIG__AUTO_MAN1))->value,/*saves address*/0,/*description*/"AUTO_MAN1",/*name*/"AUTO_MAN1",/*type*/U8_REGS_FLAG,/*ind*/4  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_BOOL_t *)(&CONFIG__AUTO_MAN2))->value,/*saves address*/0,/*description*/"AUTO_MAN2",/*name*/"AUTO_MAN2",/*type*/U8_REGS_FLAG,/*ind*/5  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_BOOL_t *)(&CONFIG__AUTO_MAN3))->value,/*saves address*/0,/*description*/"AUTO_MAN3",/*name*/"AUTO_MAN3",/*type*/U8_REGS_FLAG,/*ind*/6  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_BOOL_t *)(&CONFIG__AUTO_MAN4))->value,/*saves address*/0,/*description*/"AUTO_MAN4",/*name*/"AUTO_MAN4",/*type*/U8_REGS_FLAG,/*ind*/7  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__ER))->value,/*saves address*/0,/*description*/"ER",/*name*/"ER",/*type*/FLOAT_REGS_FLAG,/*ind*/8  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__ER0))->value,/*saves address*/0,/*description*/"ER0",/*name*/"ER0",/*type*/FLOAT_REGS_FLAG,/*ind*/9  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__ER1))->value,/*saves address*/0,/*description*/"ER1",/*name*/"ER1",/*type*/FLOAT_REGS_FLAG,/*ind*/10  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__ER2))->value,/*saves address*/0,/*description*/"ER2",/*name*/"ER2",/*type*/FLOAT_REGS_FLAG,/*ind*/11  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__ER3))->value,/*saves address*/0,/*description*/"ER3",/*name*/"ER3",/*type*/FLOAT_REGS_FLAG,/*ind*/12  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__ER4))->value,/*saves address*/0,/*description*/"ER4",/*name*/"ER4",/*type*/FLOAT_REGS_FLAG,/*ind*/13  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__ER5))->value,/*saves address*/0,/*description*/"ER5",/*name*/"ER5",/*type*/FLOAT_REGS_FLAG,/*ind*/14  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__ER6))->value,/*saves address*/0,/*description*/"ER6",/*name*/"ER6",/*type*/FLOAT_REGS_FLAG,/*ind*/15  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__ER7))->value,/*saves address*/0,/*description*/"ER7",/*name*/"ER7",/*type*/FLOAT_REGS_FLAG,/*ind*/16  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__ER8))->value,/*saves address*/0,/*description*/"ER8",/*name*/"ER8",/*type*/FLOAT_REGS_FLAG,/*ind*/17  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__ER9))->value,/*saves address*/0,/*description*/"ER9",/*name*/"ER9",/*type*/FLOAT_REGS_FLAG,/*ind*/18  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__ER10))->value,/*saves address*/0,/*description*/"ER10",/*name*/"ER10",/*type*/FLOAT_REGS_FLAG,/*ind*/19  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__ER11))->value,/*saves address*/0,/*description*/"ER11",/*name*/"ER11",/*type*/FLOAT_REGS_FLAG,/*ind*/20  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__ER12))->value,/*saves address*/0,/*description*/"ER12",/*name*/"ER12",/*type*/FLOAT_REGS_FLAG,/*ind*/21  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__ER13))->value,/*saves address*/0,/*description*/"ER13",/*name*/"ER13",/*type*/FLOAT_REGS_FLAG,/*ind*/22  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__ER14))->value,/*saves address*/0,/*description*/"ER14",/*name*/"ER14",/*type*/FLOAT_REGS_FLAG,/*ind*/23  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__ER15))->value,/*saves address*/0,/*description*/"ER15",/*name*/"ER15",/*type*/FLOAT_REGS_FLAG,/*ind*/24  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__ER16))->value,/*saves address*/0,/*description*/"ER16",/*name*/"ER16",/*type*/FLOAT_REGS_FLAG,/*ind*/25  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__ER17))->value,/*saves address*/0,/*description*/"ER17",/*name*/"ER17",/*type*/FLOAT_REGS_FLAG,/*ind*/26  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__ER18))->value,/*saves address*/0,/*description*/"ER18",/*name*/"ER18",/*type*/FLOAT_REGS_FLAG,/*ind*/27  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__ER19))->value,/*saves address*/0,/*description*/"ER19",/*name*/"ER19",/*type*/FLOAT_REGS_FLAG,/*ind*/28  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__ER20))->value,/*saves address*/0,/*description*/"ER20",/*name*/"ER20",/*type*/FLOAT_REGS_FLAG,/*ind*/29  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__ER21))->value,/*saves address*/0,/*description*/"ER21",/*name*/"ER21",/*type*/FLOAT_REGS_FLAG,/*ind*/30  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__ER22))->value,/*saves address*/0,/*description*/"ER22",/*name*/"ER22",/*type*/FLOAT_REGS_FLAG,/*ind*/31  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__ER23))->value,/*saves address*/0,/*description*/"ER23",/*name*/"ER23",/*type*/FLOAT_REGS_FLAG,/*ind*/32  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__ER24))->value,/*saves address*/0,/*description*/"ER24",/*name*/"ER24",/*type*/FLOAT_REGS_FLAG,/*ind*/33  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__ER25))->value,/*saves address*/0,/*description*/"ER25",/*name*/"ER25",/*type*/FLOAT_REGS_FLAG,/*ind*/34  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__ER26))->value,/*saves address*/0,/*description*/"ER26",/*name*/"ER26",/*type*/FLOAT_REGS_FLAG,/*ind*/35  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__ER27))->value,/*saves address*/0,/*description*/"ER27",/*name*/"ER27",/*type*/FLOAT_REGS_FLAG,/*ind*/36  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__ER28))->value,/*saves address*/0,/*description*/"ER28",/*name*/"ER28",/*type*/FLOAT_REGS_FLAG,/*ind*/37  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__ER29))->value,/*saves address*/0,/*description*/"ER29",/*name*/"ER29",/*type*/FLOAT_REGS_FLAG,/*ind*/38  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__ER30))->value,/*saves address*/0,/*description*/"ER30",/*name*/"ER30",/*type*/FLOAT_REGS_FLAG,/*ind*/39  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__ER31))->value,/*saves address*/0,/*description*/"ER31",/*name*/"ER31",/*type*/FLOAT_REGS_FLAG,/*ind*/40  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__ER32))->value,/*saves address*/0,/*description*/"ER32",/*name*/"ER32",/*type*/FLOAT_REGS_FLAG,/*ind*/41  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__ER33))->value,/*saves address*/0,/*description*/"ER33",/*name*/"ER33",/*type*/FLOAT_REGS_FLAG,/*ind*/42  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__ER34))->value,/*saves address*/0,/*description*/"ER34",/*name*/"ER34",/*type*/FLOAT_REGS_FLAG,/*ind*/43  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__ER35))->value,/*saves address*/0,/*description*/"ER35",/*name*/"ER35",/*type*/FLOAT_REGS_FLAG,/*ind*/44  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__ER36))->value,/*saves address*/0,/*description*/"ER36",/*name*/"ER36",/*type*/FLOAT_REGS_FLAG,/*ind*/45  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__ER37))->value,/*saves address*/0,/*description*/"ER37",/*name*/"ER37",/*type*/FLOAT_REGS_FLAG,/*ind*/46  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__ER38))->value,/*saves address*/0,/*description*/"ER38",/*name*/"ER38",/*type*/FLOAT_REGS_FLAG,/*ind*/47  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__ER39))->value,/*saves address*/0,/*description*/"ER39",/*name*/"ER39",/*type*/FLOAT_REGS_FLAG,/*ind*/48  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__ER40))->value,/*saves address*/0,/*description*/"ER40",/*name*/"ER40",/*type*/FLOAT_REGS_FLAG,/*ind*/49  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__ER41))->value,/*saves address*/0,/*description*/"ER41",/*name*/"ER41",/*type*/FLOAT_REGS_FLAG,/*ind*/50  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__ER42))->value,/*saves address*/0,/*description*/"ER42",/*name*/"ER42",/*type*/FLOAT_REGS_FLAG,/*ind*/51  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__ER43))->value,/*saves address*/0,/*description*/"ER43",/*name*/"ER43",/*type*/FLOAT_REGS_FLAG,/*ind*/52  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__ER44))->value,/*saves address*/0,/*description*/"ER44",/*name*/"ER44",/*type*/FLOAT_REGS_FLAG,/*ind*/53  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__ER45))->value,/*saves address*/0,/*description*/"ER45",/*name*/"ER45",/*type*/FLOAT_REGS_FLAG,/*ind*/54  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__ER46))->value,/*saves address*/0,/*description*/"ER46",/*name*/"ER46",/*type*/FLOAT_REGS_FLAG,/*ind*/55  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__ER47))->value,/*saves address*/0,/*description*/"ER47",/*name*/"ER47",/*type*/FLOAT_REGS_FLAG,/*ind*/56  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__ER48))->value,/*saves address*/0,/*description*/"ER48",/*name*/"ER48",/*type*/FLOAT_REGS_FLAG,/*ind*/57  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__ER49))->value,/*saves address*/0,/*description*/"ER49",/*name*/"ER49",/*type*/FLOAT_REGS_FLAG,/*ind*/58  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__ER50))->value,/*saves address*/0,/*description*/"ER50",/*name*/"ER50",/*type*/FLOAT_REGS_FLAG,/*ind*/59  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__ER51))->value,/*saves address*/0,/*description*/"ER51",/*name*/"ER51",/*type*/FLOAT_REGS_FLAG,/*ind*/60  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__ER52))->value,/*saves address*/0,/*description*/"ER52",/*name*/"ER52",/*type*/FLOAT_REGS_FLAG,/*ind*/61  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__ER53))->value,/*saves address*/0,/*description*/"ER53",/*name*/"ER53",/*type*/FLOAT_REGS_FLAG,/*ind*/62  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__ER54))->value,/*saves address*/0,/*description*/"ER54",/*name*/"ER54",/*type*/FLOAT_REGS_FLAG,/*ind*/63  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__ER55))->value,/*saves address*/0,/*description*/"ER55",/*name*/"ER55",/*type*/FLOAT_REGS_FLAG,/*ind*/64  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__ER56))->value,/*saves address*/0,/*description*/"ER56",/*name*/"ER56",/*type*/FLOAT_REGS_FLAG,/*ind*/65  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__ER57))->value,/*saves address*/0,/*description*/"ER57",/*name*/"ER57",/*type*/FLOAT_REGS_FLAG,/*ind*/66  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__ER58))->value,/*saves address*/0,/*description*/"ER58",/*name*/"ER58",/*type*/FLOAT_REGS_FLAG,/*ind*/67  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__ER59))->value,/*saves address*/0,/*description*/"ER59",/*name*/"ER59",/*type*/FLOAT_REGS_FLAG,/*ind*/68  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__ER60))->value,/*saves address*/0,/*description*/"ER60",/*name*/"ER60",/*type*/FLOAT_REGS_FLAG,/*ind*/69  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__ER61))->value,/*saves address*/0,/*description*/"ER61",/*name*/"ER61",/*type*/FLOAT_REGS_FLAG,/*ind*/70  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__ER62))->value,/*saves address*/0,/*description*/"ER62",/*name*/"ER62",/*type*/FLOAT_REGS_FLAG,/*ind*/71  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__ER63))->value,/*saves address*/0,/*description*/"ER63",/*name*/"ER63",/*type*/FLOAT_REGS_FLAG,/*ind*/72  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__ER64))->value,/*saves address*/0,/*description*/"ER64",/*name*/"ER64",/*type*/FLOAT_REGS_FLAG,/*ind*/73  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__ER65))->value,/*saves address*/0,/*description*/"ER65",/*name*/"ER65",/*type*/FLOAT_REGS_FLAG,/*ind*/74  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__ER66))->value,/*saves address*/0,/*description*/"ER66",/*name*/"ER66",/*type*/FLOAT_REGS_FLAG,/*ind*/75  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__ER67))->value,/*saves address*/0,/*description*/"ER67",/*name*/"ER67",/*type*/FLOAT_REGS_FLAG,/*ind*/76  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__ER68))->value,/*saves address*/0,/*description*/"ER68",/*name*/"ER68",/*type*/FLOAT_REGS_FLAG,/*ind*/77  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__ER69))->value,/*saves address*/0,/*description*/"ER69",/*name*/"ER69",/*type*/FLOAT_REGS_FLAG,/*ind*/78  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__ER70))->value,/*saves address*/0,/*description*/"ER70",/*name*/"ER70",/*type*/FLOAT_REGS_FLAG,/*ind*/79  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__ER71))->value,/*saves address*/0,/*description*/"ER71",/*name*/"ER71",/*type*/FLOAT_REGS_FLAG,/*ind*/80  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__ER72))->value,/*saves address*/0,/*description*/"ER72",/*name*/"ER72",/*type*/FLOAT_REGS_FLAG,/*ind*/81  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__ER73))->value,/*saves address*/0,/*description*/"ER73",/*name*/"ER73",/*type*/FLOAT_REGS_FLAG,/*ind*/82  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__ER74))->value,/*saves address*/0,/*description*/"ER74",/*name*/"ER74",/*type*/FLOAT_REGS_FLAG,/*ind*/83  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__ER75))->value,/*saves address*/0,/*description*/"ER75",/*name*/"ER75",/*type*/FLOAT_REGS_FLAG,/*ind*/84  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__ER76))->value,/*saves address*/0,/*description*/"ER76",/*name*/"ER76",/*type*/FLOAT_REGS_FLAG,/*ind*/85  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__ER77))->value,/*saves address*/0,/*description*/"ER77",/*name*/"ER77",/*type*/FLOAT_REGS_FLAG,/*ind*/86  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__ER78))->value,/*saves address*/0,/*description*/"ER78",/*name*/"ER78",/*type*/FLOAT_REGS_FLAG,/*ind*/87  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__ER79))->value,/*saves address*/0,/*description*/"ER79",/*name*/"ER79",/*type*/FLOAT_REGS_FLAG,/*ind*/88  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__ER80))->value,/*saves address*/0,/*description*/"ER80",/*name*/"ER80",/*type*/FLOAT_REGS_FLAG,/*ind*/89  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__ER81))->value,/*saves address*/0,/*description*/"ER81",/*name*/"ER81",/*type*/FLOAT_REGS_FLAG,/*ind*/90  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__ER82))->value,/*saves address*/0,/*description*/"ER82",/*name*/"ER82",/*type*/FLOAT_REGS_FLAG,/*ind*/91  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__ER83))->value,/*saves address*/0,/*description*/"ER83",/*name*/"ER83",/*type*/FLOAT_REGS_FLAG,/*ind*/92  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__ER84))->value,/*saves address*/0,/*description*/"ER84",/*name*/"ER84",/*type*/FLOAT_REGS_FLAG,/*ind*/93  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__ER85))->value,/*saves address*/0,/*description*/"ER85",/*name*/"ER85",/*type*/FLOAT_REGS_FLAG,/*ind*/94  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__ER86))->value,/*saves address*/0,/*description*/"ER86",/*name*/"ER86",/*type*/FLOAT_REGS_FLAG,/*ind*/95  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__ER87))->value,/*saves address*/0,/*description*/"ER87",/*name*/"ER87",/*type*/FLOAT_REGS_FLAG,/*ind*/96  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__ER88))->value,/*saves address*/0,/*description*/"ER88",/*name*/"ER88",/*type*/FLOAT_REGS_FLAG,/*ind*/97  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__ER89))->value,/*saves address*/0,/*description*/"ER89",/*name*/"ER89",/*type*/FLOAT_REGS_FLAG,/*ind*/98  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__ER90))->value,/*saves address*/0,/*description*/"ER90",/*name*/"ER90",/*type*/FLOAT_REGS_FLAG,/*ind*/99  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__ER91))->value,/*saves address*/0,/*description*/"ER91",/*name*/"ER91",/*type*/FLOAT_REGS_FLAG,/*ind*/100  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__ER92))->value,/*saves address*/0,/*description*/"ER92",/*name*/"ER92",/*type*/FLOAT_REGS_FLAG,/*ind*/101  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__ER93))->value,/*saves address*/0,/*description*/"ER93",/*name*/"ER93",/*type*/FLOAT_REGS_FLAG,/*ind*/102  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__ER94))->value,/*saves address*/0,/*description*/"ER94",/*name*/"ER94",/*type*/FLOAT_REGS_FLAG,/*ind*/103  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__ER95))->value,/*saves address*/0,/*description*/"ER95",/*name*/"ER95",/*type*/FLOAT_REGS_FLAG,/*ind*/104  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__ER96))->value,/*saves address*/0,/*description*/"ER96",/*name*/"ER96",/*type*/FLOAT_REGS_FLAG,/*ind*/105  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__ER97))->value,/*saves address*/0,/*description*/"ER97",/*name*/"ER97",/*type*/FLOAT_REGS_FLAG,/*ind*/106  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__ER98))->value,/*saves address*/0,/*description*/"ER98",/*name*/"ER98",/*type*/FLOAT_REGS_FLAG,/*ind*/107  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__ER99))->value,/*saves address*/0,/*description*/"ER99",/*name*/"ER99",/*type*/FLOAT_REGS_FLAG,/*ind*/108  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__ER100))->value,/*saves address*/0,/*description*/"ER100",/*name*/"ER100",/*type*/FLOAT_REGS_FLAG,/*ind*/109  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__ER101))->value,/*saves address*/0,/*description*/"ER101",/*name*/"ER101",/*type*/FLOAT_REGS_FLAG,/*ind*/110  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__ER102))->value,/*saves address*/0,/*description*/"ER102",/*name*/"ER102",/*type*/FLOAT_REGS_FLAG,/*ind*/111  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__ER103))->value,/*saves address*/0,/*description*/"ER103",/*name*/"ER103",/*type*/FLOAT_REGS_FLAG,/*ind*/112  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__ER104))->value,/*saves address*/0,/*description*/"ER104",/*name*/"ER104",/*type*/FLOAT_REGS_FLAG,/*ind*/113  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__ER105))->value,/*saves address*/0,/*description*/"ER105",/*name*/"ER105",/*type*/FLOAT_REGS_FLAG,/*ind*/114  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__ER106))->value,/*saves address*/0,/*description*/"ER106",/*name*/"ER106",/*type*/FLOAT_REGS_FLAG,/*ind*/115  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__ER107))->value,/*saves address*/0,/*description*/"ER107",/*name*/"ER107",/*type*/FLOAT_REGS_FLAG,/*ind*/116  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__ER108))->value,/*saves address*/0,/*description*/"ER108",/*name*/"ER108",/*type*/FLOAT_REGS_FLAG,/*ind*/117  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__ER109))->value,/*saves address*/0,/*description*/"ER109",/*name*/"ER109",/*type*/FLOAT_REGS_FLAG,/*ind*/118  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__ER110))->value,/*saves address*/0,/*description*/"ER110",/*name*/"ER110",/*type*/FLOAT_REGS_FLAG,/*ind*/119  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__ER111))->value,/*saves address*/0,/*description*/"ER111",/*name*/"ER111",/*type*/FLOAT_REGS_FLAG,/*ind*/120  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__ER112))->value,/*saves address*/0,/*description*/"ER112",/*name*/"ER112",/*type*/FLOAT_REGS_FLAG,/*ind*/121  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__ER113))->value,/*saves address*/0,/*description*/"ER113",/*name*/"ER113",/*type*/FLOAT_REGS_FLAG,/*ind*/122  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__ER114))->value,/*saves address*/0,/*description*/"ER114",/*name*/"ER114",/*type*/FLOAT_REGS_FLAG,/*ind*/123  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__ER115))->value,/*saves address*/0,/*description*/"ER115",/*name*/"ER115",/*type*/FLOAT_REGS_FLAG,/*ind*/124  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__ER116))->value,/*saves address*/0,/*description*/"ER116",/*name*/"ER116",/*type*/FLOAT_REGS_FLAG,/*ind*/125  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__ER117))->value,/*saves address*/0,/*description*/"ER117",/*name*/"ER117",/*type*/FLOAT_REGS_FLAG,/*ind*/126  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__ER118))->value,/*saves address*/0,/*description*/"ER118",/*name*/"ER118",/*type*/FLOAT_REGS_FLAG,/*ind*/127  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__ER119))->value,/*saves address*/0,/*description*/"ER119",/*name*/"ER119",/*type*/FLOAT_REGS_FLAG,/*ind*/128  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__ER120))->value,/*saves address*/0,/*description*/"ER120",/*name*/"ER120",/*type*/FLOAT_REGS_FLAG,/*ind*/129  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__ER121))->value,/*saves address*/0,/*description*/"ER121",/*name*/"ER121",/*type*/FLOAT_REGS_FLAG,/*ind*/130  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__ER122))->value,/*saves address*/0,/*description*/"ER122",/*name*/"ER122",/*type*/FLOAT_REGS_FLAG,/*ind*/131  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__ER123))->value,/*saves address*/0,/*description*/"ER123",/*name*/"ER123",/*type*/FLOAT_REGS_FLAG,/*ind*/132  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__ER124))->value,/*saves address*/0,/*description*/"ER124",/*name*/"ER124",/*type*/FLOAT_REGS_FLAG,/*ind*/133  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__ER125))->value,/*saves address*/0,/*description*/"ER125",/*name*/"ER125",/*type*/FLOAT_REGS_FLAG,/*ind*/134  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__ER126))->value,/*saves address*/0,/*description*/"ER126",/*name*/"ER126",/*type*/FLOAT_REGS_FLAG,/*ind*/135  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__ER127))->value,/*saves address*/0,/*description*/"ER127",/*name*/"ER127",/*type*/FLOAT_REGS_FLAG,/*ind*/136  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__ER128))->value,/*saves address*/0,/*description*/"ER128",/*name*/"ER128",/*type*/FLOAT_REGS_FLAG,/*ind*/137  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__ER129))->value,/*saves address*/0,/*description*/"ER129",/*name*/"ER129",/*type*/FLOAT_REGS_FLAG,/*ind*/138  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__ER130))->value,/*saves address*/0,/*description*/"ER130",/*name*/"ER130",/*type*/FLOAT_REGS_FLAG,/*ind*/139  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__ER131))->value,/*saves address*/0,/*description*/"ER131",/*name*/"ER131",/*type*/FLOAT_REGS_FLAG,/*ind*/140  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__ER132))->value,/*saves address*/0,/*description*/"ER132",/*name*/"ER132",/*type*/FLOAT_REGS_FLAG,/*ind*/141  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__ER133))->value,/*saves address*/0,/*description*/"ER133",/*name*/"ER133",/*type*/FLOAT_REGS_FLAG,/*ind*/142  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__ER134))->value,/*saves address*/0,/*description*/"ER134",/*name*/"ER134",/*type*/FLOAT_REGS_FLAG,/*ind*/143  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__ER135))->value,/*saves address*/0,/*description*/"ER135",/*name*/"ER135",/*type*/FLOAT_REGS_FLAG,/*ind*/144  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__ER136))->value,/*saves address*/0,/*description*/"ER136",/*name*/"ER136",/*type*/FLOAT_REGS_FLAG,/*ind*/145  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__ER137))->value,/*saves address*/0,/*description*/"ER137",/*name*/"ER137",/*type*/FLOAT_REGS_FLAG,/*ind*/146  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__ER138))->value,/*saves address*/0,/*description*/"ER138",/*name*/"ER138",/*type*/FLOAT_REGS_FLAG,/*ind*/147  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__ER139))->value,/*saves address*/0,/*description*/"ER139",/*name*/"ER139",/*type*/FLOAT_REGS_FLAG,/*ind*/148  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__ER140))->value,/*saves address*/0,/*description*/"ER140",/*name*/"ER140",/*type*/FLOAT_REGS_FLAG,/*ind*/149  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__ER141))->value,/*saves address*/0,/*description*/"ER141",/*name*/"ER141",/*type*/FLOAT_REGS_FLAG,/*ind*/150  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__ER142))->value,/*saves address*/0,/*description*/"ER142",/*name*/"ER142",/*type*/FLOAT_REGS_FLAG,/*ind*/151  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__ER143))->value,/*saves address*/0,/*description*/"ER143",/*name*/"ER143",/*type*/FLOAT_REGS_FLAG,/*ind*/152  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__ER144))->value,/*saves address*/0,/*description*/"ER144",/*name*/"ER144",/*type*/FLOAT_REGS_FLAG,/*ind*/153  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__ER145))->value,/*saves address*/0,/*description*/"ER145",/*name*/"ER145",/*type*/FLOAT_REGS_FLAG,/*ind*/154  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__ER146))->value,/*saves address*/0,/*description*/"ER146",/*name*/"ER146",/*type*/FLOAT_REGS_FLAG,/*ind*/155  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__ER147))->value,/*saves address*/0,/*description*/"ER147",/*name*/"ER147",/*type*/FLOAT_REGS_FLAG,/*ind*/156  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__ER148))->value,/*saves address*/0,/*description*/"ER148",/*name*/"ER148",/*type*/FLOAT_REGS_FLAG,/*ind*/157  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__ER149))->value,/*saves address*/0,/*description*/"ER149",/*name*/"ER149",/*type*/FLOAT_REGS_FLAG,/*ind*/158  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__ER150))->value,/*saves address*/0,/*description*/"ER150",/*name*/"ER150",/*type*/FLOAT_REGS_FLAG,/*ind*/159  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__ER151))->value,/*saves address*/0,/*description*/"ER151",/*name*/"ER151",/*type*/FLOAT_REGS_FLAG,/*ind*/160  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__ER152))->value,/*saves address*/0,/*description*/"ER152",/*name*/"ER152",/*type*/FLOAT_REGS_FLAG,/*ind*/161  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__ER153))->value,/*saves address*/0,/*description*/"ER153",/*name*/"ER153",/*type*/FLOAT_REGS_FLAG,/*ind*/162  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__ER154))->value,/*saves address*/0,/*description*/"ER154",/*name*/"ER154",/*type*/FLOAT_REGS_FLAG,/*ind*/163  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__ER155))->value,/*saves address*/0,/*description*/"ER155",/*name*/"ER155",/*type*/FLOAT_REGS_FLAG,/*ind*/164  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__ER156))->value,/*saves address*/0,/*description*/"ER156",/*name*/"ER156",/*type*/FLOAT_REGS_FLAG,/*ind*/165  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__ER157))->value,/*saves address*/0,/*description*/"ER157",/*name*/"ER157",/*type*/FLOAT_REGS_FLAG,/*ind*/166  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__ER158))->value,/*saves address*/0,/*description*/"ER158",/*name*/"ER158",/*type*/FLOAT_REGS_FLAG,/*ind*/167  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__ER159))->value,/*saves address*/0,/*description*/"ER159",/*name*/"ER159",/*type*/FLOAT_REGS_FLAG,/*ind*/168  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__PROPORTIONAL))->value,/*saves address*/0,/*description*/"PROPORTIONAL",/*name*/"PROPORTIONAL",/*type*/FLOAT_REGS_FLAG,/*ind*/169  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__INTEGRAL_0))->value,/*saves address*/0,/*description*/"INTEGRAL_0",/*name*/"INTEGRAL_0",/*type*/FLOAT_REGS_FLAG,/*ind*/170  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__DIFFERENTIAL))->value,/*saves address*/0,/*description*/"DIFFERENTIAL",/*name*/"DIFFERENTIAL",/*type*/FLOAT_REGS_FLAG,/*ind*/171  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__TASK_MANUAL))->value,/*saves address*/0,/*description*/"TASK_MANUAL",/*name*/"TASK_MANUAL",/*type*/FLOAT_REGS_FLAG,/*ind*/172  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__TASK_AUTO))->value,/*saves address*/0,/*description*/"TASK_AUTO",/*name*/"TASK_AUTO",/*type*/FLOAT_REGS_FLAG,/*ind*/173  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_UDINT_t *)(&CONFIG__OUT_TIME_MAX))->value,/*saves address*/0,/*description*/"OUT_TIME_MAX",/*name*/"OUT_TIME_MAX",/*type*/U32_REGS_FLAG,/*ind*/174  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_UDINT_t *)(&CONFIG__OUT_TIME_MAX0))->value,/*saves address*/0,/*description*/"OUT_TIME_MAX0",/*name*/"OUT_TIME_MAX0",/*type*/U32_REGS_FLAG,/*ind*/175  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_UDINT_t *)(&CONFIG__OUT_TIME_MAX1))->value,/*saves address*/0,/*description*/"OUT_TIME_MAX1",/*name*/"OUT_TIME_MAX1",/*type*/U32_REGS_FLAG,/*ind*/176  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_UDINT_t *)(&CONFIG__OUT_TIME_MAX2))->value,/*saves address*/0,/*description*/"OUT_TIME_MAX2",/*name*/"OUT_TIME_MAX2",/*type*/U32_REGS_FLAG,/*ind*/177  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_UDINT_t *)(&CONFIG__OUT_TIME_MAX3))->value,/*saves address*/0,/*description*/"OUT_TIME_MAX3",/*name*/"OUT_TIME_MAX3",/*type*/U32_REGS_FLAG,/*ind*/178  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_UDINT_t *)(&CONFIG__OUT_TIME_MAX4))->value,/*saves address*/0,/*description*/"OUT_TIME_MAX4",/*name*/"OUT_TIME_MAX4",/*type*/U32_REGS_FLAG,/*ind*/179  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_UDINT_t *)(&CONFIG__OUT_TIME_MIN))->value,/*saves address*/0,/*description*/"OUT_TIME_MIN",/*name*/"OUT_TIME_MIN",/*type*/U32_REGS_FLAG,/*ind*/180  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_UDINT_t *)(&CONFIG__OUT_TIME_MIN0))->value,/*saves address*/0,/*description*/"OUT_TIME_MIN0",/*name*/"OUT_TIME_MIN0",/*type*/U32_REGS_FLAG,/*ind*/181  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_UDINT_t *)(&CONFIG__OUT_TIME_MIN1))->value,/*saves address*/0,/*description*/"OUT_TIME_MIN1",/*name*/"OUT_TIME_MIN1",/*type*/U32_REGS_FLAG,/*ind*/182  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_UDINT_t *)(&CONFIG__OUT_TIME_MIN2))->value,/*saves address*/0,/*description*/"OUT_TIME_MIN2",/*name*/"OUT_TIME_MIN2",/*type*/U32_REGS_FLAG,/*ind*/183  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_UDINT_t *)(&CONFIG__OUT_TIME_MIN3))->value,/*saves address*/0,/*description*/"OUT_TIME_MIN3",/*name*/"OUT_TIME_MIN3",/*type*/U32_REGS_FLAG,/*ind*/184  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_UDINT_t *)(&CONFIG__OUT_TIME_MIN4))->value,/*saves address*/0,/*description*/"OUT_TIME_MIN4",/*name*/"OUT_TIME_MIN4",/*type*/U32_REGS_FLAG,/*ind*/185  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__OUT_MIN))->value,/*saves address*/0,/*description*/"OUT_MIN",/*name*/"OUT_MIN",/*type*/FLOAT_REGS_FLAG,/*ind*/186  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__OUT_MIN0))->value,/*saves address*/0,/*description*/"OUT_MIN0",/*name*/"OUT_MIN0",/*type*/FLOAT_REGS_FLAG,/*ind*/187  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__OUT_MIN1))->value,/*saves address*/0,/*description*/"OUT_MIN1",/*name*/"OUT_MIN1",/*type*/FLOAT_REGS_FLAG,/*ind*/188  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__OUT_MIN2))->value,/*saves address*/0,/*description*/"OUT_MIN2",/*name*/"OUT_MIN2",/*type*/FLOAT_REGS_FLAG,/*ind*/189  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__OUT_MIN3))->value,/*saves address*/0,/*description*/"OUT_MIN3",/*name*/"OUT_MIN3",/*type*/FLOAT_REGS_FLAG,/*ind*/190  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__OUT_MIN4))->value,/*saves address*/0,/*description*/"OUT_MIN4",/*name*/"OUT_MIN4",/*type*/FLOAT_REGS_FLAG,/*ind*/191  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__OUT_MAX))->value,/*saves address*/0,/*description*/"OUT_MAX",/*name*/"OUT_MAX",/*type*/FLOAT_REGS_FLAG,/*ind*/192  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__OUT_MAX0))->value,/*saves address*/0,/*description*/"OUT_MAX0",/*name*/"OUT_MAX0",/*type*/FLOAT_REGS_FLAG,/*ind*/193  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__OUT_MAX1))->value,/*saves address*/0,/*description*/"OUT_MAX1",/*name*/"OUT_MAX1",/*type*/FLOAT_REGS_FLAG,/*ind*/194  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__OUT_MAX2))->value,/*saves address*/0,/*description*/"OUT_MAX2",/*name*/"OUT_MAX2",/*type*/FLOAT_REGS_FLAG,/*ind*/195  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__OUT_MAX3))->value,/*saves address*/0,/*description*/"OUT_MAX3",/*name*/"OUT_MAX3",/*type*/FLOAT_REGS_FLAG,/*ind*/196  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__OUT_MAX4))->value,/*saves address*/0,/*description*/"OUT_MAX4",/*name*/"OUT_MAX4",/*type*/FLOAT_REGS_FLAG,/*ind*/197  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K1))->value,/*saves address*/0,/*description*/"K1",/*name*/"K1",/*type*/FLOAT_REGS_FLAG,/*ind*/198  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K10))->value,/*saves address*/0,/*description*/"K10",/*name*/"K10",/*type*/FLOAT_REGS_FLAG,/*ind*/199  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K11))->value,/*saves address*/0,/*description*/"K11",/*name*/"K11",/*type*/FLOAT_REGS_FLAG,/*ind*/200  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K12))->value,/*saves address*/0,/*description*/"K12",/*name*/"K12",/*type*/FLOAT_REGS_FLAG,/*ind*/201  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K13))->value,/*saves address*/0,/*description*/"K13",/*name*/"K13",/*type*/FLOAT_REGS_FLAG,/*ind*/202  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K14))->value,/*saves address*/0,/*description*/"K14",/*name*/"K14",/*type*/FLOAT_REGS_FLAG,/*ind*/203  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K15))->value,/*saves address*/0,/*description*/"K15",/*name*/"K15",/*type*/FLOAT_REGS_FLAG,/*ind*/204  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K16))->value,/*saves address*/0,/*description*/"K16",/*name*/"K16",/*type*/FLOAT_REGS_FLAG,/*ind*/205  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K17))->value,/*saves address*/0,/*description*/"K17",/*name*/"K17",/*type*/FLOAT_REGS_FLAG,/*ind*/206  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K18))->value,/*saves address*/0,/*description*/"K18",/*name*/"K18",/*type*/FLOAT_REGS_FLAG,/*ind*/207  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K19))->value,/*saves address*/0,/*description*/"K19",/*name*/"K19",/*type*/FLOAT_REGS_FLAG,/*ind*/208  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K110))->value,/*saves address*/0,/*description*/"K110",/*name*/"K110",/*type*/FLOAT_REGS_FLAG,/*ind*/209  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K111))->value,/*saves address*/0,/*description*/"K111",/*name*/"K111",/*type*/FLOAT_REGS_FLAG,/*ind*/210  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K112))->value,/*saves address*/0,/*description*/"K112",/*name*/"K112",/*type*/FLOAT_REGS_FLAG,/*ind*/211  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K113))->value,/*saves address*/0,/*description*/"K113",/*name*/"K113",/*type*/FLOAT_REGS_FLAG,/*ind*/212  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K114))->value,/*saves address*/0,/*description*/"K114",/*name*/"K114",/*type*/FLOAT_REGS_FLAG,/*ind*/213  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K115))->value,/*saves address*/0,/*description*/"K115",/*name*/"K115",/*type*/FLOAT_REGS_FLAG,/*ind*/214  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K116))->value,/*saves address*/0,/*description*/"K116",/*name*/"K116",/*type*/FLOAT_REGS_FLAG,/*ind*/215  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K117))->value,/*saves address*/0,/*description*/"K117",/*name*/"K117",/*type*/FLOAT_REGS_FLAG,/*ind*/216  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K118))->value,/*saves address*/0,/*description*/"K118",/*name*/"K118",/*type*/FLOAT_REGS_FLAG,/*ind*/217  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K119))->value,/*saves address*/0,/*description*/"K119",/*name*/"K119",/*type*/FLOAT_REGS_FLAG,/*ind*/218  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K120))->value,/*saves address*/0,/*description*/"K120",/*name*/"K120",/*type*/FLOAT_REGS_FLAG,/*ind*/219  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K121))->value,/*saves address*/0,/*description*/"K121",/*name*/"K121",/*type*/FLOAT_REGS_FLAG,/*ind*/220  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K122))->value,/*saves address*/0,/*description*/"K122",/*name*/"K122",/*type*/FLOAT_REGS_FLAG,/*ind*/221  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K123))->value,/*saves address*/0,/*description*/"K123",/*name*/"K123",/*type*/FLOAT_REGS_FLAG,/*ind*/222  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K124))->value,/*saves address*/0,/*description*/"K124",/*name*/"K124",/*type*/FLOAT_REGS_FLAG,/*ind*/223  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K125))->value,/*saves address*/0,/*description*/"K125",/*name*/"K125",/*type*/FLOAT_REGS_FLAG,/*ind*/224  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K126))->value,/*saves address*/0,/*description*/"K126",/*name*/"K126",/*type*/FLOAT_REGS_FLAG,/*ind*/225  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K127))->value,/*saves address*/0,/*description*/"K127",/*name*/"K127",/*type*/FLOAT_REGS_FLAG,/*ind*/226  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K128))->value,/*saves address*/0,/*description*/"K128",/*name*/"K128",/*type*/FLOAT_REGS_FLAG,/*ind*/227  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K129))->value,/*saves address*/0,/*description*/"K129",/*name*/"K129",/*type*/FLOAT_REGS_FLAG,/*ind*/228  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K130))->value,/*saves address*/0,/*description*/"K130",/*name*/"K130",/*type*/FLOAT_REGS_FLAG,/*ind*/229  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K131))->value,/*saves address*/0,/*description*/"K131",/*name*/"K131",/*type*/FLOAT_REGS_FLAG,/*ind*/230  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K132))->value,/*saves address*/0,/*description*/"K132",/*name*/"K132",/*type*/FLOAT_REGS_FLAG,/*ind*/231  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K133))->value,/*saves address*/0,/*description*/"K133",/*name*/"K133",/*type*/FLOAT_REGS_FLAG,/*ind*/232  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K134))->value,/*saves address*/0,/*description*/"K134",/*name*/"K134",/*type*/FLOAT_REGS_FLAG,/*ind*/233  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K135))->value,/*saves address*/0,/*description*/"K135",/*name*/"K135",/*type*/FLOAT_REGS_FLAG,/*ind*/234  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K136))->value,/*saves address*/0,/*description*/"K136",/*name*/"K136",/*type*/FLOAT_REGS_FLAG,/*ind*/235  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K137))->value,/*saves address*/0,/*description*/"K137",/*name*/"K137",/*type*/FLOAT_REGS_FLAG,/*ind*/236  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K138))->value,/*saves address*/0,/*description*/"K138",/*name*/"K138",/*type*/FLOAT_REGS_FLAG,/*ind*/237  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K139))->value,/*saves address*/0,/*description*/"K139",/*name*/"K139",/*type*/FLOAT_REGS_FLAG,/*ind*/238  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K140))->value,/*saves address*/0,/*description*/"K140",/*name*/"K140",/*type*/FLOAT_REGS_FLAG,/*ind*/239  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K141))->value,/*saves address*/0,/*description*/"K141",/*name*/"K141",/*type*/FLOAT_REGS_FLAG,/*ind*/240  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K142))->value,/*saves address*/0,/*description*/"K142",/*name*/"K142",/*type*/FLOAT_REGS_FLAG,/*ind*/241  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K143))->value,/*saves address*/0,/*description*/"K143",/*name*/"K143",/*type*/FLOAT_REGS_FLAG,/*ind*/242  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K144))->value,/*saves address*/0,/*description*/"K144",/*name*/"K144",/*type*/FLOAT_REGS_FLAG,/*ind*/243  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K145))->value,/*saves address*/0,/*description*/"K145",/*name*/"K145",/*type*/FLOAT_REGS_FLAG,/*ind*/244  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K146))->value,/*saves address*/0,/*description*/"K146",/*name*/"K146",/*type*/FLOAT_REGS_FLAG,/*ind*/245  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K147))->value,/*saves address*/0,/*description*/"K147",/*name*/"K147",/*type*/FLOAT_REGS_FLAG,/*ind*/246  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K148))->value,/*saves address*/0,/*description*/"K148",/*name*/"K148",/*type*/FLOAT_REGS_FLAG,/*ind*/247  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K149))->value,/*saves address*/0,/*description*/"K149",/*name*/"K149",/*type*/FLOAT_REGS_FLAG,/*ind*/248  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K150))->value,/*saves address*/0,/*description*/"K150",/*name*/"K150",/*type*/FLOAT_REGS_FLAG,/*ind*/249  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K151))->value,/*saves address*/0,/*description*/"K151",/*name*/"K151",/*type*/FLOAT_REGS_FLAG,/*ind*/250  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K152))->value,/*saves address*/0,/*description*/"K152",/*name*/"K152",/*type*/FLOAT_REGS_FLAG,/*ind*/251  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K153))->value,/*saves address*/0,/*description*/"K153",/*name*/"K153",/*type*/FLOAT_REGS_FLAG,/*ind*/252  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K154))->value,/*saves address*/0,/*description*/"K154",/*name*/"K154",/*type*/FLOAT_REGS_FLAG,/*ind*/253  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K155))->value,/*saves address*/0,/*description*/"K155",/*name*/"K155",/*type*/FLOAT_REGS_FLAG,/*ind*/254  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K156))->value,/*saves address*/0,/*description*/"K156",/*name*/"K156",/*type*/FLOAT_REGS_FLAG,/*ind*/255  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K157))->value,/*saves address*/0,/*description*/"K157",/*name*/"K157",/*type*/FLOAT_REGS_FLAG,/*ind*/256  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K158))->value,/*saves address*/0,/*description*/"K158",/*name*/"K158",/*type*/FLOAT_REGS_FLAG,/*ind*/257  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K159))->value,/*saves address*/0,/*description*/"K159",/*name*/"K159",/*type*/FLOAT_REGS_FLAG,/*ind*/258  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K160))->value,/*saves address*/0,/*description*/"K160",/*name*/"K160",/*type*/FLOAT_REGS_FLAG,/*ind*/259  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K161))->value,/*saves address*/0,/*description*/"K161",/*name*/"K161",/*type*/FLOAT_REGS_FLAG,/*ind*/260  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K162))->value,/*saves address*/0,/*description*/"K162",/*name*/"K162",/*type*/FLOAT_REGS_FLAG,/*ind*/261  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K163))->value,/*saves address*/0,/*description*/"K163",/*name*/"K163",/*type*/FLOAT_REGS_FLAG,/*ind*/262  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K164))->value,/*saves address*/0,/*description*/"K164",/*name*/"K164",/*type*/FLOAT_REGS_FLAG,/*ind*/263  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K165))->value,/*saves address*/0,/*description*/"K165",/*name*/"K165",/*type*/FLOAT_REGS_FLAG,/*ind*/264  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K166))->value,/*saves address*/0,/*description*/"K166",/*name*/"K166",/*type*/FLOAT_REGS_FLAG,/*ind*/265  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K167))->value,/*saves address*/0,/*description*/"K167",/*name*/"K167",/*type*/FLOAT_REGS_FLAG,/*ind*/266  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K168))->value,/*saves address*/0,/*description*/"K168",/*name*/"K168",/*type*/FLOAT_REGS_FLAG,/*ind*/267  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K169))->value,/*saves address*/0,/*description*/"K169",/*name*/"K169",/*type*/FLOAT_REGS_FLAG,/*ind*/268  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K170))->value,/*saves address*/0,/*description*/"K170",/*name*/"K170",/*type*/FLOAT_REGS_FLAG,/*ind*/269  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K171))->value,/*saves address*/0,/*description*/"K171",/*name*/"K171",/*type*/FLOAT_REGS_FLAG,/*ind*/270  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K172))->value,/*saves address*/0,/*description*/"K172",/*name*/"K172",/*type*/FLOAT_REGS_FLAG,/*ind*/271  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K173))->value,/*saves address*/0,/*description*/"K173",/*name*/"K173",/*type*/FLOAT_REGS_FLAG,/*ind*/272  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K174))->value,/*saves address*/0,/*description*/"K174",/*name*/"K174",/*type*/FLOAT_REGS_FLAG,/*ind*/273  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K175))->value,/*saves address*/0,/*description*/"K175",/*name*/"K175",/*type*/FLOAT_REGS_FLAG,/*ind*/274  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K176))->value,/*saves address*/0,/*description*/"K176",/*name*/"K176",/*type*/FLOAT_REGS_FLAG,/*ind*/275  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K177))->value,/*saves address*/0,/*description*/"K177",/*name*/"K177",/*type*/FLOAT_REGS_FLAG,/*ind*/276  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K178))->value,/*saves address*/0,/*description*/"K178",/*name*/"K178",/*type*/FLOAT_REGS_FLAG,/*ind*/277  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K179))->value,/*saves address*/0,/*description*/"K179",/*name*/"K179",/*type*/FLOAT_REGS_FLAG,/*ind*/278  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K180))->value,/*saves address*/0,/*description*/"K180",/*name*/"K180",/*type*/FLOAT_REGS_FLAG,/*ind*/279  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K181))->value,/*saves address*/0,/*description*/"K181",/*name*/"K181",/*type*/FLOAT_REGS_FLAG,/*ind*/280  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K182))->value,/*saves address*/0,/*description*/"K182",/*name*/"K182",/*type*/FLOAT_REGS_FLAG,/*ind*/281  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K183))->value,/*saves address*/0,/*description*/"K183",/*name*/"K183",/*type*/FLOAT_REGS_FLAG,/*ind*/282  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K184))->value,/*saves address*/0,/*description*/"K184",/*name*/"K184",/*type*/FLOAT_REGS_FLAG,/*ind*/283  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K185))->value,/*saves address*/0,/*description*/"K185",/*name*/"K185",/*type*/FLOAT_REGS_FLAG,/*ind*/284  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K186))->value,/*saves address*/0,/*description*/"K186",/*name*/"K186",/*type*/FLOAT_REGS_FLAG,/*ind*/285  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K187))->value,/*saves address*/0,/*description*/"K187",/*name*/"K187",/*type*/FLOAT_REGS_FLAG,/*ind*/286  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K188))->value,/*saves address*/0,/*description*/"K188",/*name*/"K188",/*type*/FLOAT_REGS_FLAG,/*ind*/287  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K189))->value,/*saves address*/0,/*description*/"K189",/*name*/"K189",/*type*/FLOAT_REGS_FLAG,/*ind*/288  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K190))->value,/*saves address*/0,/*description*/"K190",/*name*/"K190",/*type*/FLOAT_REGS_FLAG,/*ind*/289  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K191))->value,/*saves address*/0,/*description*/"K191",/*name*/"K191",/*type*/FLOAT_REGS_FLAG,/*ind*/290  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K192))->value,/*saves address*/0,/*description*/"K192",/*name*/"K192",/*type*/FLOAT_REGS_FLAG,/*ind*/291  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K193))->value,/*saves address*/0,/*description*/"K193",/*name*/"K193",/*type*/FLOAT_REGS_FLAG,/*ind*/292  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K194))->value,/*saves address*/0,/*description*/"K194",/*name*/"K194",/*type*/FLOAT_REGS_FLAG,/*ind*/293  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K195))->value,/*saves address*/0,/*description*/"K195",/*name*/"K195",/*type*/FLOAT_REGS_FLAG,/*ind*/294  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K196))->value,/*saves address*/0,/*description*/"K196",/*name*/"K196",/*type*/FLOAT_REGS_FLAG,/*ind*/295  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K197))->value,/*saves address*/0,/*description*/"K197",/*name*/"K197",/*type*/FLOAT_REGS_FLAG,/*ind*/296  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K198))->value,/*saves address*/0,/*description*/"K198",/*name*/"K198",/*type*/FLOAT_REGS_FLAG,/*ind*/297  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K199))->value,/*saves address*/0,/*description*/"K199",/*name*/"K199",/*type*/FLOAT_REGS_FLAG,/*ind*/298  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K1000))->value,/*saves address*/0,/*description*/"K1000",/*name*/"K1000",/*type*/FLOAT_REGS_FLAG,/*ind*/299  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K1001))->value,/*saves address*/0,/*description*/"K1001",/*name*/"K1001",/*type*/FLOAT_REGS_FLAG,/*ind*/300  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K1002))->value,/*saves address*/0,/*description*/"K1002",/*name*/"K1002",/*type*/FLOAT_REGS_FLAG,/*ind*/301  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K1003))->value,/*saves address*/0,/*description*/"K1003",/*name*/"K1003",/*type*/FLOAT_REGS_FLAG,/*ind*/302  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K1004))->value,/*saves address*/0,/*description*/"K1004",/*name*/"K1004",/*type*/FLOAT_REGS_FLAG,/*ind*/303  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K1005))->value,/*saves address*/0,/*description*/"K1005",/*name*/"K1005",/*type*/FLOAT_REGS_FLAG,/*ind*/304  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K1006))->value,/*saves address*/0,/*description*/"K1006",/*name*/"K1006",/*type*/FLOAT_REGS_FLAG,/*ind*/305  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K1007))->value,/*saves address*/0,/*description*/"K1007",/*name*/"K1007",/*type*/FLOAT_REGS_FLAG,/*ind*/306  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K1008))->value,/*saves address*/0,/*description*/"K1008",/*name*/"K1008",/*type*/FLOAT_REGS_FLAG,/*ind*/307  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K1009))->value,/*saves address*/0,/*description*/"K1009",/*name*/"K1009",/*type*/FLOAT_REGS_FLAG,/*ind*/308  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K1010))->value,/*saves address*/0,/*description*/"K1010",/*name*/"K1010",/*type*/FLOAT_REGS_FLAG,/*ind*/309  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K1011))->value,/*saves address*/0,/*description*/"K1011",/*name*/"K1011",/*type*/FLOAT_REGS_FLAG,/*ind*/310  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K1012))->value,/*saves address*/0,/*description*/"K1012",/*name*/"K1012",/*type*/FLOAT_REGS_FLAG,/*ind*/311  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K1013))->value,/*saves address*/0,/*description*/"K1013",/*name*/"K1013",/*type*/FLOAT_REGS_FLAG,/*ind*/312  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K1014))->value,/*saves address*/0,/*description*/"K1014",/*name*/"K1014",/*type*/FLOAT_REGS_FLAG,/*ind*/313  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K1015))->value,/*saves address*/0,/*description*/"K1015",/*name*/"K1015",/*type*/FLOAT_REGS_FLAG,/*ind*/314  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K1016))->value,/*saves address*/0,/*description*/"K1016",/*name*/"K1016",/*type*/FLOAT_REGS_FLAG,/*ind*/315  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K1017))->value,/*saves address*/0,/*description*/"K1017",/*name*/"K1017",/*type*/FLOAT_REGS_FLAG,/*ind*/316  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K1018))->value,/*saves address*/0,/*description*/"K1018",/*name*/"K1018",/*type*/FLOAT_REGS_FLAG,/*ind*/317  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K1019))->value,/*saves address*/0,/*description*/"K1019",/*name*/"K1019",/*type*/FLOAT_REGS_FLAG,/*ind*/318  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K1020))->value,/*saves address*/0,/*description*/"K1020",/*name*/"K1020",/*type*/FLOAT_REGS_FLAG,/*ind*/319  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K1021))->value,/*saves address*/0,/*description*/"K1021",/*name*/"K1021",/*type*/FLOAT_REGS_FLAG,/*ind*/320  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K1022))->value,/*saves address*/0,/*description*/"K1022",/*name*/"K1022",/*type*/FLOAT_REGS_FLAG,/*ind*/321  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K1023))->value,/*saves address*/0,/*description*/"K1023",/*name*/"K1023",/*type*/FLOAT_REGS_FLAG,/*ind*/322  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K1024))->value,/*saves address*/0,/*description*/"K1024",/*name*/"K1024",/*type*/FLOAT_REGS_FLAG,/*ind*/323  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K1025))->value,/*saves address*/0,/*description*/"K1025",/*name*/"K1025",/*type*/FLOAT_REGS_FLAG,/*ind*/324  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K1026))->value,/*saves address*/0,/*description*/"K1026",/*name*/"K1026",/*type*/FLOAT_REGS_FLAG,/*ind*/325  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K1027))->value,/*saves address*/0,/*description*/"K1027",/*name*/"K1027",/*type*/FLOAT_REGS_FLAG,/*ind*/326  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K1028))->value,/*saves address*/0,/*description*/"K1028",/*name*/"K1028",/*type*/FLOAT_REGS_FLAG,/*ind*/327  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K1029))->value,/*saves address*/0,/*description*/"K1029",/*name*/"K1029",/*type*/FLOAT_REGS_FLAG,/*ind*/328  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K1030))->value,/*saves address*/0,/*description*/"K1030",/*name*/"K1030",/*type*/FLOAT_REGS_FLAG,/*ind*/329  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K1031))->value,/*saves address*/0,/*description*/"K1031",/*name*/"K1031",/*type*/FLOAT_REGS_FLAG,/*ind*/330  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K1032))->value,/*saves address*/0,/*description*/"K1032",/*name*/"K1032",/*type*/FLOAT_REGS_FLAG,/*ind*/331  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K1033))->value,/*saves address*/0,/*description*/"K1033",/*name*/"K1033",/*type*/FLOAT_REGS_FLAG,/*ind*/332  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K1034))->value,/*saves address*/0,/*description*/"K1034",/*name*/"K1034",/*type*/FLOAT_REGS_FLAG,/*ind*/333  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K1035))->value,/*saves address*/0,/*description*/"K1035",/*name*/"K1035",/*type*/FLOAT_REGS_FLAG,/*ind*/334  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K1036))->value,/*saves address*/0,/*description*/"K1036",/*name*/"K1036",/*type*/FLOAT_REGS_FLAG,/*ind*/335  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K1037))->value,/*saves address*/0,/*description*/"K1037",/*name*/"K1037",/*type*/FLOAT_REGS_FLAG,/*ind*/336  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K1038))->value,/*saves address*/0,/*description*/"K1038",/*name*/"K1038",/*type*/FLOAT_REGS_FLAG,/*ind*/337  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K1039))->value,/*saves address*/0,/*description*/"K1039",/*name*/"K1039",/*type*/FLOAT_REGS_FLAG,/*ind*/338  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K1040))->value,/*saves address*/0,/*description*/"K1040",/*name*/"K1040",/*type*/FLOAT_REGS_FLAG,/*ind*/339  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K1041))->value,/*saves address*/0,/*description*/"K1041",/*name*/"K1041",/*type*/FLOAT_REGS_FLAG,/*ind*/340  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K1042))->value,/*saves address*/0,/*description*/"K1042",/*name*/"K1042",/*type*/FLOAT_REGS_FLAG,/*ind*/341  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K1043))->value,/*saves address*/0,/*description*/"K1043",/*name*/"K1043",/*type*/FLOAT_REGS_FLAG,/*ind*/342  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K1044))->value,/*saves address*/0,/*description*/"K1044",/*name*/"K1044",/*type*/FLOAT_REGS_FLAG,/*ind*/343  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K1045))->value,/*saves address*/0,/*description*/"K1045",/*name*/"K1045",/*type*/FLOAT_REGS_FLAG,/*ind*/344  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K1046))->value,/*saves address*/0,/*description*/"K1046",/*name*/"K1046",/*type*/FLOAT_REGS_FLAG,/*ind*/345  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K1047))->value,/*saves address*/0,/*description*/"K1047",/*name*/"K1047",/*type*/FLOAT_REGS_FLAG,/*ind*/346  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K1048))->value,/*saves address*/0,/*description*/"K1048",/*name*/"K1048",/*type*/FLOAT_REGS_FLAG,/*ind*/347  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K1049))->value,/*saves address*/0,/*description*/"K1049",/*name*/"K1049",/*type*/FLOAT_REGS_FLAG,/*ind*/348  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K2))->value,/*saves address*/0,/*description*/"K2",/*name*/"K2",/*type*/FLOAT_REGS_FLAG,/*ind*/349  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K20))->value,/*saves address*/0,/*description*/"K20",/*name*/"K20",/*type*/FLOAT_REGS_FLAG,/*ind*/350  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K21))->value,/*saves address*/0,/*description*/"K21",/*name*/"K21",/*type*/FLOAT_REGS_FLAG,/*ind*/351  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K22))->value,/*saves address*/0,/*description*/"K22",/*name*/"K22",/*type*/FLOAT_REGS_FLAG,/*ind*/352  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K23))->value,/*saves address*/0,/*description*/"K23",/*name*/"K23",/*type*/FLOAT_REGS_FLAG,/*ind*/353  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K24))->value,/*saves address*/0,/*description*/"K24",/*name*/"K24",/*type*/FLOAT_REGS_FLAG,/*ind*/354  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K25))->value,/*saves address*/0,/*description*/"K25",/*name*/"K25",/*type*/FLOAT_REGS_FLAG,/*ind*/355  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K26))->value,/*saves address*/0,/*description*/"K26",/*name*/"K26",/*type*/FLOAT_REGS_FLAG,/*ind*/356  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K27))->value,/*saves address*/0,/*description*/"K27",/*name*/"K27",/*type*/FLOAT_REGS_FLAG,/*ind*/357  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K28))->value,/*saves address*/0,/*description*/"K28",/*name*/"K28",/*type*/FLOAT_REGS_FLAG,/*ind*/358  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K29))->value,/*saves address*/0,/*description*/"K29",/*name*/"K29",/*type*/FLOAT_REGS_FLAG,/*ind*/359  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K210))->value,/*saves address*/0,/*description*/"K210",/*name*/"K210",/*type*/FLOAT_REGS_FLAG,/*ind*/360  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K211))->value,/*saves address*/0,/*description*/"K211",/*name*/"K211",/*type*/FLOAT_REGS_FLAG,/*ind*/361  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K212))->value,/*saves address*/0,/*description*/"K212",/*name*/"K212",/*type*/FLOAT_REGS_FLAG,/*ind*/362  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K213))->value,/*saves address*/0,/*description*/"K213",/*name*/"K213",/*type*/FLOAT_REGS_FLAG,/*ind*/363  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K214))->value,/*saves address*/0,/*description*/"K214",/*name*/"K214",/*type*/FLOAT_REGS_FLAG,/*ind*/364  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K215))->value,/*saves address*/0,/*description*/"K215",/*name*/"K215",/*type*/FLOAT_REGS_FLAG,/*ind*/365  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K216))->value,/*saves address*/0,/*description*/"K216",/*name*/"K216",/*type*/FLOAT_REGS_FLAG,/*ind*/366  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K217))->value,/*saves address*/0,/*description*/"K217",/*name*/"K217",/*type*/FLOAT_REGS_FLAG,/*ind*/367  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K218))->value,/*saves address*/0,/*description*/"K218",/*name*/"K218",/*type*/FLOAT_REGS_FLAG,/*ind*/368  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K219))->value,/*saves address*/0,/*description*/"K219",/*name*/"K219",/*type*/FLOAT_REGS_FLAG,/*ind*/369  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K220))->value,/*saves address*/0,/*description*/"K220",/*name*/"K220",/*type*/FLOAT_REGS_FLAG,/*ind*/370  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K221))->value,/*saves address*/0,/*description*/"K221",/*name*/"K221",/*type*/FLOAT_REGS_FLAG,/*ind*/371  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K222))->value,/*saves address*/0,/*description*/"K222",/*name*/"K222",/*type*/FLOAT_REGS_FLAG,/*ind*/372  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K223))->value,/*saves address*/0,/*description*/"K223",/*name*/"K223",/*type*/FLOAT_REGS_FLAG,/*ind*/373  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K224))->value,/*saves address*/0,/*description*/"K224",/*name*/"K224",/*type*/FLOAT_REGS_FLAG,/*ind*/374  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K225))->value,/*saves address*/0,/*description*/"K225",/*name*/"K225",/*type*/FLOAT_REGS_FLAG,/*ind*/375  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K226))->value,/*saves address*/0,/*description*/"K226",/*name*/"K226",/*type*/FLOAT_REGS_FLAG,/*ind*/376  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K227))->value,/*saves address*/0,/*description*/"K227",/*name*/"K227",/*type*/FLOAT_REGS_FLAG,/*ind*/377  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K228))->value,/*saves address*/0,/*description*/"K228",/*name*/"K228",/*type*/FLOAT_REGS_FLAG,/*ind*/378  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K229))->value,/*saves address*/0,/*description*/"K229",/*name*/"K229",/*type*/FLOAT_REGS_FLAG,/*ind*/379  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K230))->value,/*saves address*/0,/*description*/"K230",/*name*/"K230",/*type*/FLOAT_REGS_FLAG,/*ind*/380  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K231))->value,/*saves address*/0,/*description*/"K231",/*name*/"K231",/*type*/FLOAT_REGS_FLAG,/*ind*/381  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K232))->value,/*saves address*/0,/*description*/"K232",/*name*/"K232",/*type*/FLOAT_REGS_FLAG,/*ind*/382  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K233))->value,/*saves address*/0,/*description*/"K233",/*name*/"K233",/*type*/FLOAT_REGS_FLAG,/*ind*/383  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K234))->value,/*saves address*/0,/*description*/"K234",/*name*/"K234",/*type*/FLOAT_REGS_FLAG,/*ind*/384  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K235))->value,/*saves address*/0,/*description*/"K235",/*name*/"K235",/*type*/FLOAT_REGS_FLAG,/*ind*/385  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K236))->value,/*saves address*/0,/*description*/"K236",/*name*/"K236",/*type*/FLOAT_REGS_FLAG,/*ind*/386  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K237))->value,/*saves address*/0,/*description*/"K237",/*name*/"K237",/*type*/FLOAT_REGS_FLAG,/*ind*/387  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K238))->value,/*saves address*/0,/*description*/"K238",/*name*/"K238",/*type*/FLOAT_REGS_FLAG,/*ind*/388  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K239))->value,/*saves address*/0,/*description*/"K239",/*name*/"K239",/*type*/FLOAT_REGS_FLAG,/*ind*/389  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K240))->value,/*saves address*/0,/*description*/"K240",/*name*/"K240",/*type*/FLOAT_REGS_FLAG,/*ind*/390  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K241))->value,/*saves address*/0,/*description*/"K241",/*name*/"K241",/*type*/FLOAT_REGS_FLAG,/*ind*/391  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K242))->value,/*saves address*/0,/*description*/"K242",/*name*/"K242",/*type*/FLOAT_REGS_FLAG,/*ind*/392  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K243))->value,/*saves address*/0,/*description*/"K243",/*name*/"K243",/*type*/FLOAT_REGS_FLAG,/*ind*/393  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K244))->value,/*saves address*/0,/*description*/"K244",/*name*/"K244",/*type*/FLOAT_REGS_FLAG,/*ind*/394  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K245))->value,/*saves address*/0,/*description*/"K245",/*name*/"K245",/*type*/FLOAT_REGS_FLAG,/*ind*/395  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K246))->value,/*saves address*/0,/*description*/"K246",/*name*/"K246",/*type*/FLOAT_REGS_FLAG,/*ind*/396  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K247))->value,/*saves address*/0,/*description*/"K247",/*name*/"K247",/*type*/FLOAT_REGS_FLAG,/*ind*/397  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K248))->value,/*saves address*/0,/*description*/"K248",/*name*/"K248",/*type*/FLOAT_REGS_FLAG,/*ind*/398  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K249))->value,/*saves address*/0,/*description*/"K249",/*name*/"K249",/*type*/FLOAT_REGS_FLAG,/*ind*/399  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K250))->value,/*saves address*/0,/*description*/"K250",/*name*/"K250",/*type*/FLOAT_REGS_FLAG,/*ind*/400  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K251))->value,/*saves address*/0,/*description*/"K251",/*name*/"K251",/*type*/FLOAT_REGS_FLAG,/*ind*/401  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K252))->value,/*saves address*/0,/*description*/"K252",/*name*/"K252",/*type*/FLOAT_REGS_FLAG,/*ind*/402  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K253))->value,/*saves address*/0,/*description*/"K253",/*name*/"K253",/*type*/FLOAT_REGS_FLAG,/*ind*/403  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K254))->value,/*saves address*/0,/*description*/"K254",/*name*/"K254",/*type*/FLOAT_REGS_FLAG,/*ind*/404  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K255))->value,/*saves address*/0,/*description*/"K255",/*name*/"K255",/*type*/FLOAT_REGS_FLAG,/*ind*/405  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K256))->value,/*saves address*/0,/*description*/"K256",/*name*/"K256",/*type*/FLOAT_REGS_FLAG,/*ind*/406  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K257))->value,/*saves address*/0,/*description*/"K257",/*name*/"K257",/*type*/FLOAT_REGS_FLAG,/*ind*/407  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K258))->value,/*saves address*/0,/*description*/"K258",/*name*/"K258",/*type*/FLOAT_REGS_FLAG,/*ind*/408  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K259))->value,/*saves address*/0,/*description*/"K259",/*name*/"K259",/*type*/FLOAT_REGS_FLAG,/*ind*/409  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K260))->value,/*saves address*/0,/*description*/"K260",/*name*/"K260",/*type*/FLOAT_REGS_FLAG,/*ind*/410  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K261))->value,/*saves address*/0,/*description*/"K261",/*name*/"K261",/*type*/FLOAT_REGS_FLAG,/*ind*/411  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K262))->value,/*saves address*/0,/*description*/"K262",/*name*/"K262",/*type*/FLOAT_REGS_FLAG,/*ind*/412  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K263))->value,/*saves address*/0,/*description*/"K263",/*name*/"K263",/*type*/FLOAT_REGS_FLAG,/*ind*/413  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K264))->value,/*saves address*/0,/*description*/"K264",/*name*/"K264",/*type*/FLOAT_REGS_FLAG,/*ind*/414  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K265))->value,/*saves address*/0,/*description*/"K265",/*name*/"K265",/*type*/FLOAT_REGS_FLAG,/*ind*/415  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K266))->value,/*saves address*/0,/*description*/"K266",/*name*/"K266",/*type*/FLOAT_REGS_FLAG,/*ind*/416  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K267))->value,/*saves address*/0,/*description*/"K267",/*name*/"K267",/*type*/FLOAT_REGS_FLAG,/*ind*/417  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K268))->value,/*saves address*/0,/*description*/"K268",/*name*/"K268",/*type*/FLOAT_REGS_FLAG,/*ind*/418  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K269))->value,/*saves address*/0,/*description*/"K269",/*name*/"K269",/*type*/FLOAT_REGS_FLAG,/*ind*/419  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K270))->value,/*saves address*/0,/*description*/"K270",/*name*/"K270",/*type*/FLOAT_REGS_FLAG,/*ind*/420  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K271))->value,/*saves address*/0,/*description*/"K271",/*name*/"K271",/*type*/FLOAT_REGS_FLAG,/*ind*/421  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K272))->value,/*saves address*/0,/*description*/"K272",/*name*/"K272",/*type*/FLOAT_REGS_FLAG,/*ind*/422  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K273))->value,/*saves address*/0,/*description*/"K273",/*name*/"K273",/*type*/FLOAT_REGS_FLAG,/*ind*/423  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K274))->value,/*saves address*/0,/*description*/"K274",/*name*/"K274",/*type*/FLOAT_REGS_FLAG,/*ind*/424  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K275))->value,/*saves address*/0,/*description*/"K275",/*name*/"K275",/*type*/FLOAT_REGS_FLAG,/*ind*/425  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K276))->value,/*saves address*/0,/*description*/"K276",/*name*/"K276",/*type*/FLOAT_REGS_FLAG,/*ind*/426  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K277))->value,/*saves address*/0,/*description*/"K277",/*name*/"K277",/*type*/FLOAT_REGS_FLAG,/*ind*/427  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K278))->value,/*saves address*/0,/*description*/"K278",/*name*/"K278",/*type*/FLOAT_REGS_FLAG,/*ind*/428  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K279))->value,/*saves address*/0,/*description*/"K279",/*name*/"K279",/*type*/FLOAT_REGS_FLAG,/*ind*/429  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K280))->value,/*saves address*/0,/*description*/"K280",/*name*/"K280",/*type*/FLOAT_REGS_FLAG,/*ind*/430  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K281))->value,/*saves address*/0,/*description*/"K281",/*name*/"K281",/*type*/FLOAT_REGS_FLAG,/*ind*/431  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K282))->value,/*saves address*/0,/*description*/"K282",/*name*/"K282",/*type*/FLOAT_REGS_FLAG,/*ind*/432  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K283))->value,/*saves address*/0,/*description*/"K283",/*name*/"K283",/*type*/FLOAT_REGS_FLAG,/*ind*/433  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K284))->value,/*saves address*/0,/*description*/"K284",/*name*/"K284",/*type*/FLOAT_REGS_FLAG,/*ind*/434  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K285))->value,/*saves address*/0,/*description*/"K285",/*name*/"K285",/*type*/FLOAT_REGS_FLAG,/*ind*/435  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K286))->value,/*saves address*/0,/*description*/"K286",/*name*/"K286",/*type*/FLOAT_REGS_FLAG,/*ind*/436  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K287))->value,/*saves address*/0,/*description*/"K287",/*name*/"K287",/*type*/FLOAT_REGS_FLAG,/*ind*/437  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K288))->value,/*saves address*/0,/*description*/"K288",/*name*/"K288",/*type*/FLOAT_REGS_FLAG,/*ind*/438  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K289))->value,/*saves address*/0,/*description*/"K289",/*name*/"K289",/*type*/FLOAT_REGS_FLAG,/*ind*/439  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K290))->value,/*saves address*/0,/*description*/"K290",/*name*/"K290",/*type*/FLOAT_REGS_FLAG,/*ind*/440  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K291))->value,/*saves address*/0,/*description*/"K291",/*name*/"K291",/*type*/FLOAT_REGS_FLAG,/*ind*/441  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K292))->value,/*saves address*/0,/*description*/"K292",/*name*/"K292",/*type*/FLOAT_REGS_FLAG,/*ind*/442  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K293))->value,/*saves address*/0,/*description*/"K293",/*name*/"K293",/*type*/FLOAT_REGS_FLAG,/*ind*/443  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K294))->value,/*saves address*/0,/*description*/"K294",/*name*/"K294",/*type*/FLOAT_REGS_FLAG,/*ind*/444  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K295))->value,/*saves address*/0,/*description*/"K295",/*name*/"K295",/*type*/FLOAT_REGS_FLAG,/*ind*/445  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K296))->value,/*saves address*/0,/*description*/"K296",/*name*/"K296",/*type*/FLOAT_REGS_FLAG,/*ind*/446  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K297))->value,/*saves address*/0,/*description*/"K297",/*name*/"K297",/*type*/FLOAT_REGS_FLAG,/*ind*/447  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K298))->value,/*saves address*/0,/*description*/"K298",/*name*/"K298",/*type*/FLOAT_REGS_FLAG,/*ind*/448  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K299))->value,/*saves address*/0,/*description*/"K299",/*name*/"K299",/*type*/FLOAT_REGS_FLAG,/*ind*/449  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K2000))->value,/*saves address*/0,/*description*/"K2000",/*name*/"K2000",/*type*/FLOAT_REGS_FLAG,/*ind*/450  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K2001))->value,/*saves address*/0,/*description*/"K2001",/*name*/"K2001",/*type*/FLOAT_REGS_FLAG,/*ind*/451  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K2002))->value,/*saves address*/0,/*description*/"K2002",/*name*/"K2002",/*type*/FLOAT_REGS_FLAG,/*ind*/452  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K2003))->value,/*saves address*/0,/*description*/"K2003",/*name*/"K2003",/*type*/FLOAT_REGS_FLAG,/*ind*/453  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K2004))->value,/*saves address*/0,/*description*/"K2004",/*name*/"K2004",/*type*/FLOAT_REGS_FLAG,/*ind*/454  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K2005))->value,/*saves address*/0,/*description*/"K2005",/*name*/"K2005",/*type*/FLOAT_REGS_FLAG,/*ind*/455  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K2006))->value,/*saves address*/0,/*description*/"K2006",/*name*/"K2006",/*type*/FLOAT_REGS_FLAG,/*ind*/456  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K2007))->value,/*saves address*/0,/*description*/"K2007",/*name*/"K2007",/*type*/FLOAT_REGS_FLAG,/*ind*/457  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K2008))->value,/*saves address*/0,/*description*/"K2008",/*name*/"K2008",/*type*/FLOAT_REGS_FLAG,/*ind*/458  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K2009))->value,/*saves address*/0,/*description*/"K2009",/*name*/"K2009",/*type*/FLOAT_REGS_FLAG,/*ind*/459  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K2010))->value,/*saves address*/0,/*description*/"K2010",/*name*/"K2010",/*type*/FLOAT_REGS_FLAG,/*ind*/460  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K2011))->value,/*saves address*/0,/*description*/"K2011",/*name*/"K2011",/*type*/FLOAT_REGS_FLAG,/*ind*/461  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K2012))->value,/*saves address*/0,/*description*/"K2012",/*name*/"K2012",/*type*/FLOAT_REGS_FLAG,/*ind*/462  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K2013))->value,/*saves address*/0,/*description*/"K2013",/*name*/"K2013",/*type*/FLOAT_REGS_FLAG,/*ind*/463  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K2014))->value,/*saves address*/0,/*description*/"K2014",/*name*/"K2014",/*type*/FLOAT_REGS_FLAG,/*ind*/464  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K2015))->value,/*saves address*/0,/*description*/"K2015",/*name*/"K2015",/*type*/FLOAT_REGS_FLAG,/*ind*/465  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K2016))->value,/*saves address*/0,/*description*/"K2016",/*name*/"K2016",/*type*/FLOAT_REGS_FLAG,/*ind*/466  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K2017))->value,/*saves address*/0,/*description*/"K2017",/*name*/"K2017",/*type*/FLOAT_REGS_FLAG,/*ind*/467  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K2018))->value,/*saves address*/0,/*description*/"K2018",/*name*/"K2018",/*type*/FLOAT_REGS_FLAG,/*ind*/468  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K2019))->value,/*saves address*/0,/*description*/"K2019",/*name*/"K2019",/*type*/FLOAT_REGS_FLAG,/*ind*/469  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K2020))->value,/*saves address*/0,/*description*/"K2020",/*name*/"K2020",/*type*/FLOAT_REGS_FLAG,/*ind*/470  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K2021))->value,/*saves address*/0,/*description*/"K2021",/*name*/"K2021",/*type*/FLOAT_REGS_FLAG,/*ind*/471  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K2022))->value,/*saves address*/0,/*description*/"K2022",/*name*/"K2022",/*type*/FLOAT_REGS_FLAG,/*ind*/472  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K2023))->value,/*saves address*/0,/*description*/"K2023",/*name*/"K2023",/*type*/FLOAT_REGS_FLAG,/*ind*/473  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K2024))->value,/*saves address*/0,/*description*/"K2024",/*name*/"K2024",/*type*/FLOAT_REGS_FLAG,/*ind*/474  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K2025))->value,/*saves address*/0,/*description*/"K2025",/*name*/"K2025",/*type*/FLOAT_REGS_FLAG,/*ind*/475  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K2026))->value,/*saves address*/0,/*description*/"K2026",/*name*/"K2026",/*type*/FLOAT_REGS_FLAG,/*ind*/476  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K2027))->value,/*saves address*/0,/*description*/"K2027",/*name*/"K2027",/*type*/FLOAT_REGS_FLAG,/*ind*/477  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K2028))->value,/*saves address*/0,/*description*/"K2028",/*name*/"K2028",/*type*/FLOAT_REGS_FLAG,/*ind*/478  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K2029))->value,/*saves address*/0,/*description*/"K2029",/*name*/"K2029",/*type*/FLOAT_REGS_FLAG,/*ind*/479  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K2030))->value,/*saves address*/0,/*description*/"K2030",/*name*/"K2030",/*type*/FLOAT_REGS_FLAG,/*ind*/480  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K2031))->value,/*saves address*/0,/*description*/"K2031",/*name*/"K2031",/*type*/FLOAT_REGS_FLAG,/*ind*/481  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K2032))->value,/*saves address*/0,/*description*/"K2032",/*name*/"K2032",/*type*/FLOAT_REGS_FLAG,/*ind*/482  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K2033))->value,/*saves address*/0,/*description*/"K2033",/*name*/"K2033",/*type*/FLOAT_REGS_FLAG,/*ind*/483  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K2034))->value,/*saves address*/0,/*description*/"K2034",/*name*/"K2034",/*type*/FLOAT_REGS_FLAG,/*ind*/484  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K2035))->value,/*saves address*/0,/*description*/"K2035",/*name*/"K2035",/*type*/FLOAT_REGS_FLAG,/*ind*/485  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K2036))->value,/*saves address*/0,/*description*/"K2036",/*name*/"K2036",/*type*/FLOAT_REGS_FLAG,/*ind*/486  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K2037))->value,/*saves address*/0,/*description*/"K2037",/*name*/"K2037",/*type*/FLOAT_REGS_FLAG,/*ind*/487  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K2038))->value,/*saves address*/0,/*description*/"K2038",/*name*/"K2038",/*type*/FLOAT_REGS_FLAG,/*ind*/488  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K2039))->value,/*saves address*/0,/*description*/"K2039",/*name*/"K2039",/*type*/FLOAT_REGS_FLAG,/*ind*/489  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K2040))->value,/*saves address*/0,/*description*/"K2040",/*name*/"K2040",/*type*/FLOAT_REGS_FLAG,/*ind*/490  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K2041))->value,/*saves address*/0,/*description*/"K2041",/*name*/"K2041",/*type*/FLOAT_REGS_FLAG,/*ind*/491  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K2042))->value,/*saves address*/0,/*description*/"K2042",/*name*/"K2042",/*type*/FLOAT_REGS_FLAG,/*ind*/492  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K2043))->value,/*saves address*/0,/*description*/"K2043",/*name*/"K2043",/*type*/FLOAT_REGS_FLAG,/*ind*/493  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K2044))->value,/*saves address*/0,/*description*/"K2044",/*name*/"K2044",/*type*/FLOAT_REGS_FLAG,/*ind*/494  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K2045))->value,/*saves address*/0,/*description*/"K2045",/*name*/"K2045",/*type*/FLOAT_REGS_FLAG,/*ind*/495  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K2046))->value,/*saves address*/0,/*description*/"K2046",/*name*/"K2046",/*type*/FLOAT_REGS_FLAG,/*ind*/496  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K2047))->value,/*saves address*/0,/*description*/"K2047",/*name*/"K2047",/*type*/FLOAT_REGS_FLAG,/*ind*/497  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K2048))->value,/*saves address*/0,/*description*/"K2048",/*name*/"K2048",/*type*/FLOAT_REGS_FLAG,/*ind*/498  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K2049))->value,/*saves address*/0,/*description*/"K2049",/*name*/"K2049",/*type*/FLOAT_REGS_FLAG,/*ind*/499  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__K2050))->value,/*saves address*/0,/*description*/"K2050",/*name*/"K2050",/*type*/FLOAT_REGS_FLAG,/*ind*/500  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},

};
#endif//BEREMIZ_REGS_DESCRIPTION_C
