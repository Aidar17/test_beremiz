/**
 * @file C:\Beremiz\beremiz_test_projects\PID_CHEC_STRESS\build//sofi/freertos/src/sofi_beremiz.c
 * @author Shoma Gane <shomagan@gmail.com>
 *         Ayrat Girfanov <girfanov.ayrat@yandex.ru>
 * @defgroup beremiz 
 * @ingroup beremiz 
 * @version 0.1 
 * @brief  TODO!!! write brief in 
 */
    /*
     * Copyright (c) 2018 Snema Service
     * All rights reserved.
     *
     * Redistribution and use in source and binary forms, with or without modification,
     * are permitted provided that the following conditions are met:
     *
     * 1. Redistributions of source code must retain the above copyright notice,
     *    this list of conditions and the following disclaimer.
     * 2. Redistributions in binary form must reproduce the above copyright notice,
     *    this list of conditions and the following disclaimer in the documentation
     *    and/or other materials provided with the distribution.
     * 3. The name of the author may not be used to endorse or promote products
     *    derived from this software without specific prior written permission.
     *
     * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
     * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
     * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
     * SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
     * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
     * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
     * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
     * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
     * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY
     * OF SUCH DAMAGE.
     *
     * This file is part of the sofi PLC.
     *
     * Author: Shoma Gane <shomagan@gmail.com>
     *         Ayrat Girfanov <girfanov.ayrat@yandex.ru>
     */
#ifndef SOFI_BEREMIZ_C
#define SOFI_BEREMIZ_C
#include "regs.h"
#include "sofi_dev.h"
#include "sofi_beremiz.h"

#include "link_functions.h"
#include "os_service.h"

extern link_functions_t * p_link_functions;
void READ_PARAM_UDINT_init__(READ_PARAM_UDINT *data__, BOOL retain) {
    __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
    __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
    __INIT_VAR(data__->ENABLE,0,retain)
    __INIT_VAR(data__->ADDRESS,0,retain)
    __INIT_VAR(data__->CHECK,0,retain)
    __INIT_VAR(data__->VALUE,0,retain)
}

// Code part
void READ_PARAM_UDINT_body__(READ_PARAM_UDINT *data) {
    // Control execution
    __SET_VAR(data->,CHECK,,0);
    if (!__GET_VAR(data->EN)) {
    __SET_VAR(data->,ENO,,__BOOL_LITERAL(FALSE));
    return;
    }  else {
    __SET_VAR(data->,ENO,,__BOOL_LITERAL(TRUE));
    }
    // Initialise TEMP variables
    if (__GET_VAR(data->ENABLE,)) {
        int res;
        u64 value;
        regs_flag_t flag = U32_REGS_FLAG;
        res = get_value_by_address((u32)__GET_VAR(data->ADDRESS,),&value,flag);
        if(res!=0){
            __SET_VAR(data->,CHECK,,0);
        }else{
            __SET_VAR(data->,VALUE,,(IEC_DINT)value);
            __SET_VAR(data->,CHECK,,1);
        }
    } else {
        __SET_VAR(data->,VALUE,,0);
        __SET_VAR(data->,CHECK,,0);
    };
    return;
} // READ_PARAM_body__() 
void SET_PARAM_UDINT_init__(SET_PARAM_UDINT *data__, BOOL retain) {
    __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
    __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
    __INIT_VAR(data__->ENABLE,0,retain)
    __INIT_VAR(data__->ADDRESS,0,retain)
    __INIT_VAR(data__->VALUE,0,retain)
    __INIT_VAR(data__->CHECK,0,retain)
}

// Code part
void SET_PARAM_UDINT_body__(SET_PARAM_UDINT *data) {
    // Control execution
    __SET_VAR(data->,CHECK,,0);
    if (!__GET_VAR(data->EN)) {
    __SET_VAR(data->,ENO,,__BOOL_LITERAL(FALSE));
    return;
    }  else {
    __SET_VAR(data->,ENO,,__BOOL_LITERAL(TRUE));
    }
    // Initialise TEMP variables
    if (__GET_VAR(data->ENABLE,)) {
        int res;
        regs_flag_t flag = U32_REGS_FLAG;
        res = set_value_by_address((u32)__GET_VAR(data->ADDRESS,),(u64)__GET_VAR(data->VALUE,),flag);
        if(res!=0){
            __SET_VAR(data->,CHECK,,0);
        }else{
            __SET_VAR(data->,CHECK,,1);
        }
    } else {
        __SET_VAR(data->,CHECK,,0);
    };
    return;
} // SET_PARAM_body__()
void READ_PARAM_UINT_init__(READ_PARAM_UINT *data__, BOOL retain) {
    __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
    __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
    __INIT_VAR(data__->ENABLE,0,retain)
    __INIT_VAR(data__->ADDRESS,0,retain)
    __INIT_VAR(data__->CHECK,0,retain)
    __INIT_VAR(data__->VALUE,0,retain)
}

void READ_PARAM_UINT_body__(READ_PARAM_UINT *data) {
    // Control execution
    __SET_VAR(data->,CHECK,,0);
    if (!__GET_VAR(data->EN)) {
    __SET_VAR(data->,ENO,,__BOOL_LITERAL(FALSE));
    return;
    }  else {
    __SET_VAR(data->,ENO,,__BOOL_LITERAL(TRUE));
    }
    // Initialise TEMP variables
    if (__GET_VAR(data->ENABLE,)) {
        int res;
        u64 value;
        regs_flag_t flag = U16_REGS_FLAG;
        res = get_value_by_address((u32)__GET_VAR(data->ADDRESS,),&value,flag);
        if(res!=0){
            __SET_VAR(data->,CHECK,,0);
        }else{
            __SET_VAR(data->,VALUE,,(IEC_INT)value);
            __SET_VAR(data->,CHECK,,1);
        }
    } else {
        __SET_VAR(data->,VALUE,,0);
        __SET_VAR(data->,CHECK,,0);
    };
    return;
} // READ_PARAM_body__() 
void SET_PARAM_UINT_init__(SET_PARAM_UINT *data__, BOOL retain) {
    __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
    __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
    __INIT_VAR(data__->ENABLE,0,retain)
    __INIT_VAR(data__->ADDRESS,0,retain)
    __INIT_VAR(data__->VALUE,0,retain)
    __INIT_VAR(data__->CHECK,0,retain)
}

// Code part
void SET_PARAM_UINT_body__(SET_PARAM_UINT *data) {
    // Control execution
    __SET_VAR(data->,CHECK,,0);
    if (!__GET_VAR(data->EN)) {
    __SET_VAR(data->,ENO,,__BOOL_LITERAL(FALSE));
    return;
    }  else {
    __SET_VAR(data->,ENO,,__BOOL_LITERAL(TRUE));
    }
    // Initialise TEMP variables
    if (__GET_VAR(data->ENABLE,)) {
        int res;
        regs_flag_t flag = U16_REGS_FLAG;
        res = set_value_by_address((u32)__GET_VAR(data->ADDRESS,),(u64)__GET_VAR(data->VALUE,),flag);
        if(res!=0){
            __SET_VAR(data->,CHECK,,0);
        }else{
            __SET_VAR(data->,CHECK,,1);
        }
    } else {
        __SET_VAR(data->,CHECK,,0);
    };
    return;
} // SET_PARAM_body__()
void READ_PARAM_USINT_init__(READ_PARAM_USINT *data__, BOOL retain) {
    __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
    __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
    __INIT_VAR(data__->ENABLE,0,retain)
    __INIT_VAR(data__->ADDRESS,0,retain)
    __INIT_VAR(data__->CHECK,0,retain)
    __INIT_VAR(data__->VALUE,0,retain)
}

void READ_PARAM_USINT_body__(READ_PARAM_USINT *data) {
    // Control execution
    __SET_VAR(data->,CHECK,,0);
    if (!__GET_VAR(data->EN)) {
    __SET_VAR(data->,ENO,,__BOOL_LITERAL(FALSE));
    return;
    }  else {
    __SET_VAR(data->,ENO,,__BOOL_LITERAL(TRUE));
    }
    // Initialise TEMP variables
    if (__GET_VAR(data->ENABLE,)) {
        int res;
        u64 value;
        regs_flag_t flag = U8_REGS_FLAG;
        res = get_value_by_address((u32)__GET_VAR(data->ADDRESS,),&value,flag);
        if(res!=0){
            __SET_VAR(data->,CHECK,,0);
        }else{
            __SET_VAR(data->,VALUE,,(IEC_SINT)value);
            __SET_VAR(data->,CHECK,,1);
        }
    } else {
        __SET_VAR(data->,VALUE,,0);
        __SET_VAR(data->,CHECK,,0);
    };
    return;
} // READ_PARAM_body__() 
void SET_PARAM_USINT_init__(SET_PARAM_USINT *data__, BOOL retain) {
    __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
    __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
    __INIT_VAR(data__->ENABLE,0,retain)
    __INIT_VAR(data__->ADDRESS,0,retain)
    __INIT_VAR(data__->VALUE,0,retain)
    __INIT_VAR(data__->CHECK,0,retain)
}

// Code part
void SET_PARAM_USINT_body__(SET_PARAM_USINT *data) {
    // Control execution
    __SET_VAR(data->,CHECK,,0);
    if (!__GET_VAR(data->EN)) {
    __SET_VAR(data->,ENO,,__BOOL_LITERAL(FALSE));
    return;
    }  else {
    __SET_VAR(data->,ENO,,__BOOL_LITERAL(TRUE));
    }
    // Initialise TEMP variables
    if (__GET_VAR(data->ENABLE,)) {
        int res;
        regs_flag_t flag = U8_REGS_FLAG;
        res = set_value_by_address((u32)__GET_VAR(data->ADDRESS,),(u64)__GET_VAR(data->VALUE,),flag);
        if(res!=0){
            __SET_VAR(data->,CHECK,,0);
        }else{
            __SET_VAR(data->,CHECK,,1);
        }
    } else {
        __SET_VAR(data->,CHECK,,0);
    };
    return;
} // SET_PARAM_body__()

void READ_DI_init__(READ_DI *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->DI_OUT,0,retain)
}

// Code part
void READ_DI_body__(READ_DI *data__) {
  // Control execution
  if (!__GET_VAR(data__->EN)) {
    __SET_VAR(data__->,ENO,,__BOOL_LITERAL(FALSE));
    return;
  }
  __SET_VAR(data__->,ENO,,__BOOL_LITERAL(FALSE));
  // Initialise TEMP variables
  char di_name[] = "di_state";
  regs_template_t regs_template;
  regs_template.name = di_name;
  if(p_link_functions->regs_description_get_by_name(&regs_template)==0){
      p_link_functions->task_enter_critical();
      u32 * value = (u32*)regs_template.p_value;
      __SET_VAR(data__->,DI_OUT,,*value);
      __SET_VAR(data__->,ENO,,__BOOL_LITERAL(TRUE));
      p_link_functions->task_exit_critical();
  }

  return;
} // READ_DI_body__()

void READ_DI_CNT_init__(READ_DI_CNT *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->DI_NUMBER,0,retain)
  __INIT_VAR(data__->DI_CNT_VALUE,0,retain)
}
// Code part
void READ_DI_CNT_body__(READ_DI_CNT *data__) {
    // Control execution
    if (!__GET_VAR(data__->EN)) {
        __SET_VAR(data__->,ENO,,__BOOL_LITERAL(FALSE));
      return;
    }  else {
        __SET_VAR(data__->,ENO,,__BOOL_LITERAL(TRUE));
    }
    // Initialise TEMP variables
    char cnt_name[] = "di_cnt";
    regs_template_t regs_template;
    regs_template.name = cnt_name;
    if(p_link_functions->regs_description_get_by_name(&regs_template)==0){
        regs_access_t reg;
        reg.value.op_u64 = 0;
        reg.flag = regs_template.type;
        u16 address = regs_template.guid & GUID_ADDRESS_MASK;
        u16 shift = p_link_functions->regs_size_in_byte(regs_template.type) * __GET_VAR(data__->DI_NUMBER,);
        address +=  shift;
        if(p_link_functions->regs_get(address,&reg)==0){
              __SET_VAR(data__->,DI_CNT_VALUE,,(u64)reg.value.op_u64);
        }
    }
    return;
} // READ_DI_CNT_body__()

void READ_DI_FREQ_init__(READ_DI_FREQ *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->DI_NUMBER,0,retain)
  __INIT_VAR(data__->DI_FREQ_VALUE,0,retain)
}
// Code part
void READ_DI_FREQ_body__(READ_DI_FREQ *data__) {
    // Control execution
    if (!__GET_VAR(data__->EN)) {
        __SET_VAR(data__->,ENO,,__BOOL_LITERAL(FALSE));
      return;
    }  else {
        __SET_VAR(data__->,ENO,,__BOOL_LITERAL(TRUE));
    }
    // Initialise TEMP variables
    char freq_name[] = "di_freq";
    regs_template_t regs_template;
    regs_template.name = freq_name;
    if(p_link_functions->regs_description_get_by_name(&regs_template)==0){
        regs_access_t reg;
        reg.value.op_f = 0.0;
        reg.flag = regs_template.type;
        u16 address = regs_template.guid & GUID_ADDRESS_MASK;
        u16 shift = p_link_functions->regs_size_in_byte(regs_template.type) * __GET_VAR(data__->DI_NUMBER,);
        address +=  shift;
        if(p_link_functions->regs_get(address,&reg)==0){
              __SET_VAR(data__->,DI_FREQ_VALUE,,reg.value.op_f);
        }
    }
    return;
} // DI_FREQ_body__()

void READ_AI_STATE_init__(READ_AI_STATE *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->AI_STATE_VALUE,0,retain)
}

// Code part
void READ_AI_STATE_body__(READ_AI_STATE *data__) {
  // Control execution
  if (!__GET_VAR(data__->EN)) {
    __SET_VAR(data__->,ENO,,__BOOL_LITERAL(FALSE));
    return;
  }
  __SET_VAR(data__->,ENO,,__BOOL_LITERAL(FALSE));
  // Initialise TEMP variables
  char ai_state_name[] = "ai_state";
  regs_template_t regs_template;
  regs_template.name = ai_state_name;
  if(p_link_functions->regs_description_get_by_name(&regs_template)==0){
      u16  value;
      p_link_functions->task_enter_critical();
      memcpy(&value,regs_template.p_value,sizeof(u16));
      __SET_VAR(data__->,AI_STATE_VALUE,,value);
      __SET_VAR(data__->,ENO,,__BOOL_LITERAL(TRUE));
      p_link_functions->task_exit_critical();
  }

  return;
} // READ_AI_STATE_body__()

void READ_AI_init__(READ_AI *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->AI_NUMBER,0,retain)
  __INIT_VAR(data__->AI_VALUE,0,retain)
}

// Code part
void READ_AI_body__(READ_AI *data__) {
  // Control execution
  if (!__GET_VAR(data__->EN)) {
    __SET_VAR(data__->,ENO,,__BOOL_LITERAL(FALSE));
    return;
  }  else {
    __SET_VAR(data__->,ENO,,__BOOL_LITERAL(TRUE));
  }
  // Initialise TEMP variables

  char ai_name[] = "ai_unit";
  regs_template_t regs_template;
  regs_template.name = ai_name;
  /*@todo add control ai_number*/
  if(p_link_functions->regs_description_get_by_name(&regs_template)==0){
      regs_access_t reg;
      reg.value.op_u64 = 0;
      reg.flag = regs_template.type;
      u16 address = regs_template.guid & GUID_ADDRESS_MASK;
      u16 shift = p_link_functions->regs_size_in_byte(regs_template.type) * __GET_VAR(data__->AI_NUMBER,);
      address +=  shift;
      if(p_link_functions->regs_get((u16)address,&reg)==0){
            __SET_VAR(data__->,AI_VALUE,,(u16)reg.value.op_u32);
      }
  }
  return;
} // READ_AI_body__()

void READ_DO_SC_init__(READ_DO_SC *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->DO_SC_FLAG,0,retain)
  __INIT_VAR(data__->DO_SC_EN,0,retain)
}

// Code part
void READ_DO_SC_body__(READ_DO_SC *data__) {
  // Control execution
  if (!__GET_VAR(data__->EN)) {
    __SET_VAR(data__->,ENO,,__BOOL_LITERAL(FALSE));
    return;
  }
  __SET_VAR(data__->,ENO,,__BOOL_LITERAL(FALSE));
  // Initialise TEMP variables
  char do_name[] = "do_sc_ctrl";
  regs_template_t regs_template;
  regs_template.name = do_name;
  if(p_link_functions->regs_description_get_by_name(&regs_template)==0){
      p_link_functions->task_enter_critical();
	u8 value = *(u8*)regs_template.p_value;
      __SET_VAR(data__->,DO_SC_FLAG,,(value>>4));   
      __SET_VAR(data__->,DO_SC_EN,,(value&0x0F));
      __SET_VAR(data__->,ENO,,__BOOL_LITERAL(TRUE));
      p_link_functions->task_exit_critical();
  }
  return;
} // READ_DO_SC_body__()

void READ_DO_init__(READ_DO *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->DO_OUT,0,retain)
}

// Code part
void READ_DO_body__(READ_DO *data__) {
  // Control execution
  if (!__GET_VAR(data__->EN)) {
    __SET_VAR(data__->,ENO,,__BOOL_LITERAL(FALSE));
    return;
  }
  __SET_VAR(data__->,ENO,,__BOOL_LITERAL(FALSE));
  // Initialise TEMP variables
  char do_name[] = "do_state";
  regs_template_t regs_template;
  regs_template.name = do_name;
  if(p_link_functions->regs_description_get_by_name(&regs_template)==0){
      p_link_functions->task_enter_critical();
      u8 * value = (u8*)regs_template.p_value;
      __SET_VAR(data__->,DO_OUT,,*value);
      __SET_VAR(data__->,ENO,,__BOOL_LITERAL(TRUE));
      p_link_functions->task_exit_critical();
  }

  return;
} // READ_DO_body__()

void READ_RESET_init__(READ_RESET *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->RESET_NUM,0,retain)
  __INIT_VAR(data__->LAST_RESET,0,retain)
}

// Code part
void READ_RESET_body__(READ_RESET *data__) {
  // Control execution
  if (!__GET_VAR(data__->EN)) {
    __SET_VAR(data__->,ENO,,__BOOL_LITERAL(FALSE));
    return;
  }
  __SET_VAR(data__->,ENO,,__BOOL_LITERAL(FALSE));
  // Initialise TEMP variables
  char num_name[] = "reset_num";
  char last_name[] = "last_reset";
  regs_template_t regs_template;
  regs_template.name = num_name;
  if(p_link_functions->regs_description_get_by_name(&regs_template)==0){
      p_link_functions->task_enter_critical();
      u16 * value = (u16*)regs_template.p_value;
      __SET_VAR(data__->,RESET_NUM,,*value);
      __SET_VAR(data__->,ENO,,__BOOL_LITERAL(TRUE));
      p_link_functions->task_exit_critical();
  }
  regs_template.name = last_name;
  if(p_link_functions->regs_description_get_by_name(&regs_template)==0){
      p_link_functions->task_enter_critical();
      u16 * value = (u16*)regs_template.p_value;
      __SET_VAR(data__->,LAST_RESET,,*value);
      __SET_VAR(data__->,ENO,,__BOOL_LITERAL(TRUE));
      p_link_functions->task_exit_critical();
  }

  return;
} // READ_RESET_body__()

void READ_PWR_init__(READ_PWR *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->V_PWR,0,retain)
  __INIT_VAR(data__->V_BAT,0,retain)
}

// Code part
void READ_PWR_body__(READ_PWR *data__) {
  // Control execution
  if (!__GET_VAR(data__->EN)) {
    __SET_VAR(data__->,ENO,,__BOOL_LITERAL(FALSE));
    return;
  }
  __SET_VAR(data__->,ENO,,__BOOL_LITERAL(FALSE));
  // Initialise TEMP variables
  char bat_name[] = "v_bat";
  char external_pwr_name[] = "v_pwr";
  regs_template_t regs_template;
  regs_template.name = bat_name;
  if(p_link_functions->regs_description_get_by_name(&regs_template)==0){
      p_link_functions->task_enter_critical();
      float * value = (float*)regs_template.p_value;
      __SET_VAR(data__->,V_BAT,,*value);
      __SET_VAR(data__->,ENO,,__BOOL_LITERAL(TRUE));
      p_link_functions->task_exit_critical();
  }
  regs_template.name = external_pwr_name;
  if(p_link_functions->regs_description_get_by_name(&regs_template)==0){
      p_link_functions->task_enter_critical();
      float * value = (float*)regs_template.p_value;
      __SET_VAR(data__->,V_PWR,,*value);
      __SET_VAR(data__->,ENO,,__BOOL_LITERAL(TRUE));
      p_link_functions->task_exit_critical();
  }

  return;
} // READ_PWR_body__()

void READ_INTERNAL_TEMP_init__(READ_INTERNAL_TEMP *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->INTERNAL_TEMP_OUT,0,retain)
}

// Code part
void READ_INTERNAL_TEMP_body__(READ_INTERNAL_TEMP *data__) {
  // Control execution
  if (!__GET_VAR(data__->EN)) {
    __SET_VAR(data__->,ENO,,__BOOL_LITERAL(FALSE));
    return;
  }
  __SET_VAR(data__->,ENO,,__BOOL_LITERAL(FALSE));
  // Initialise TEMP variables
  char int_temp_name[] = "internal_temp";
  regs_template_t regs_template;
  regs_template.name = int_temp_name;
  if(p_link_functions->regs_description_get_by_name(&regs_template)==0){
      p_link_functions->task_enter_critical();
      float * value = (float*)regs_template.p_value;
      __SET_VAR(data__->,INTERNAL_TEMP_OUT,,*value);
      __SET_VAR(data__->,ENO,,__BOOL_LITERAL(TRUE));
      p_link_functions->task_exit_critical();
  }

  return;
} // READ_INTERNAL_TEMP_body__()

void READ_SYS_TICK_COUNTER_init__(READ_SYS_TICK_COUNTER *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->SYS_TICK_COUNTER_VALUE,0,retain)
}
// Code part
void READ_SYS_TICK_COUNTER_body__(READ_SYS_TICK_COUNTER *data__) {
  // Control execution
  if (!__GET_VAR(data__->EN)) {
    __SET_VAR(data__->,ENO,,__BOOL_LITERAL(FALSE));
    return;
  }
  __SET_VAR(data__->,ENO,,__BOOL_LITERAL(FALSE));
  // Initialise TEMP variables
  char tick_name[] = "sys_tick_counter";
  regs_template_t regs_template;
  regs_template.name = tick_name;
  if(p_link_functions->regs_description_get_by_name(&regs_template)==0){
      u64  value;
      p_link_functions->task_enter_critical();
      memcpy(&value,regs_template.p_value,sizeof(u64));
      __SET_VAR(data__->,SYS_TICK_COUNTER_VALUE,,value);
      __SET_VAR(data__->,ENO,,__BOOL_LITERAL(TRUE));
      p_link_functions->task_exit_critical();
  }
  return;
} // READ_SYS_TICK_COUNTER_body__()

void WRITE_MDB_ADDRESS_init__(WRITE_MDB_ADDRESS *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->MDB_ADDR,0,retain)
}

// Code part
void WRITE_MDB_ADDRESS_body__(WRITE_MDB_ADDRESS *data__) {
  // Control execution
  if (!__GET_VAR(data__->EN)) {
    __SET_VAR(data__->,ENO,,__BOOL_LITERAL(FALSE));
    return;
  }
  // Initialise TEMP variables
  char mdb_name[] = "mdb_addr";
  regs_template_t regs_template;
  regs_template.name = mdb_name;
  if(p_link_functions->regs_description_get_by_name(&regs_template)==0){
      regs_access_t reg;
      reg.value.op_u16 = (u16)__GET_VAR(data__->MDB_ADDR,);
      reg.flag = regs_template.type;
      u16 address = regs_template.guid & GUID_ADDRESS_MASK;
      if(p_link_functions->regs_set(address,reg)==0){
          __SET_VAR(data__->,ENO,,__BOOL_LITERAL(TRUE));
      }else{
          __SET_VAR(data__->,ENO,,__BOOL_LITERAL(FALSE));
      }
  }
  return;
} // WRITE_MDB_ADDRESS_body__()

void WRITE_UART_SETS_init__(WRITE_UART_SETS *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->MESO_UART,0,retain)
  __INIT_VAR(data__->SET_RS_485_2,0,retain)
  __INIT_VAR(data__->SET_RS_232,0,retain)
  __INIT_VAR(data__->SET_RS_485_1,0,retain)
  __INIT_VAR(data__->SET_RS_485_IMMO,0,retain)
  __INIT_VAR(data__->SET_HART,0,retain)
}

// Code part
void WRITE_UART_SETS_body__(WRITE_UART_SETS *data__) {
  // Control execution
  if (!__GET_VAR(data__->EN)) {
    __SET_VAR(data__->,ENO,,__BOOL_LITERAL(FALSE));
    return;
  }
  // Initialise TEMP variables

  char uart1_name[] = "uart1_sets";
  regs_template_t regs_template;
  regs_template.name = uart1_name;
  if(p_link_functions->regs_description_get_by_name(&regs_template)==0){
      regs_access_t reg;
      reg.value.op_u16 = (u16)__GET_VAR(data__->MESO_UART,);
      reg.flag = regs_template.type;
      u16 address = regs_template.guid & GUID_ADDRESS_MASK;
      if(p_link_functions->regs_set(address,reg)==0){
          __SET_VAR(data__->,ENO,,__BOOL_LITERAL(TRUE));
      }else{
          __SET_VAR(data__->,ENO,,__BOOL_LITERAL(FALSE));
      }
  }

  char uart2_name[] = "uart2_sets";
  regs_template.name = uart2_name;
  if(p_link_functions->regs_description_get_by_name(&regs_template)==0){
      regs_access_t reg;
      reg.value.op_u16 = (u16)__GET_VAR(data__->SET_RS_485_2,);
      reg.flag = regs_template.type;
      u16 address = regs_template.guid & GUID_ADDRESS_MASK;
      if(p_link_functions->regs_set(address,reg)==0){
          __SET_VAR(data__->,ENO,,__BOOL_LITERAL(TRUE));
      }else{
          __SET_VAR(data__->,ENO,,__BOOL_LITERAL(FALSE));
      }
  }

  char uart3_name[] = "uart3_sets";
  regs_template.name = uart3_name;
  if(p_link_functions->regs_description_get_by_name(&regs_template)==0){
      regs_access_t reg;
      reg.value.op_u16 = (u16)__GET_VAR(data__->SET_RS_232,);
      reg.flag = regs_template.type;
      u16 address = regs_template.guid & GUID_ADDRESS_MASK;
      if(p_link_functions->regs_set(address,reg)==0){
          __SET_VAR(data__->,ENO,,__BOOL_LITERAL(TRUE));
      }else{
          __SET_VAR(data__->,ENO,,__BOOL_LITERAL(FALSE));
      }
  }

  char uart5_name[] = "uart5_sets";
  regs_template.name = uart5_name;
  if(p_link_functions->regs_description_get_by_name(&regs_template)==0){
      regs_access_t reg;
      reg.value.op_u16 = (u16)__GET_VAR(data__->SET_RS_485_1,);
      reg.flag = regs_template.type;
      u16 address = regs_template.guid & GUID_ADDRESS_MASK;
      if(p_link_functions->regs_set(address,reg)==0){
          __SET_VAR(data__->,ENO,,__BOOL_LITERAL(TRUE));
      }else{
          __SET_VAR(data__->,ENO,,__BOOL_LITERAL(FALSE));
      }
  }

  char uart6_name[] = "uart6_sets";
  regs_template.name = uart6_name;
  if(p_link_functions->regs_description_get_by_name(&regs_template)==0){
      regs_access_t reg;
      reg.value.op_u16 = (u16)__GET_VAR(data__->SET_RS_485_IMMO,);
      reg.flag = regs_template.type;
      u16 address = regs_template.guid & GUID_ADDRESS_MASK;
      if(p_link_functions->regs_set(address,reg)==0){
          __SET_VAR(data__->,ENO,,__BOOL_LITERAL(TRUE));
      }else{
          __SET_VAR(data__->,ENO,,__BOOL_LITERAL(FALSE));
      }
  }

  char uart7_name[] = "uart7_sets";
  regs_template.name = uart7_name;
  if(p_link_functions->regs_description_get_by_name(&regs_template)==0){
      regs_access_t reg;
      reg.value.op_u16 = (u16)__GET_VAR(data__->SET_HART,);
      reg.flag = regs_template.type;
      u16 address = regs_template.guid & GUID_ADDRESS_MASK;
      if(p_link_functions->regs_set(address,reg)==0){
          __SET_VAR(data__->,ENO,,__BOOL_LITERAL(TRUE));
      }else{
          __SET_VAR(data__->,ENO,,__BOOL_LITERAL(FALSE));
      }
  }
  return;
} // WRITE_UART_SETS_body__()

void WRITE_CH_TIMEOUT_init__(WRITE_CH_TIMEOUT *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->CH_NUMBER,0,retain)
  __INIT_VAR(data__->CHANNEL_TIMEOUT,0,retain)
}
// Code part
void WRITE_CH_TIMEOUT_body__(WRITE_CH_TIMEOUT *data__) {
    // Control execution
    if (!__GET_VAR(data__->EN)) {
        __SET_VAR(data__->,ENO,,__BOOL_LITERAL(FALSE));
      return;
    }
    // Initialise TEMP variables
    char ch_timeout_name[] = "channels_timeout";
    regs_template_t regs_template;
    regs_template.name = ch_timeout_name;
    if(p_link_functions->regs_description_get_by_name(&regs_template)==0){
        regs_access_t reg;
        reg.flag = regs_template.type;
        u8 address = regs_template.guid & GUID_ADDRESS_MASK;
        u8 shift = p_link_functions->regs_size_in_byte(regs_template.type) * __GET_VAR(data__->CH_NUMBER,);
        address +=  shift;
        reg.value.op_u32 = __GET_VAR(data__->CHANNEL_TIMEOUT,);
        if(p_link_functions->regs_set(address,reg)==0){
            __SET_VAR(data__->,ENO,,__BOOL_LITERAL(TRUE));
        }else{
            __SET_VAR(data__->,ENO,,__BOOL_LITERAL(FALSE));
        }
    }
    return;
} // WRITE_CH_TIMEOUT_body__()

void WRITE_DI_NOISE_FLTR_US_init__(WRITE_DI_NOISE_FLTR_US *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->DI_NUMBER,0,retain)
  __INIT_VAR(data__->DI_NOISE_FLTR_VALUE,0,retain)
}
// Code part
void WRITE_DI_NOISE_FLTR_US_body__(WRITE_DI_NOISE_FLTR_US *data__) {
    // Control execution
    if (!__GET_VAR(data__->EN)) {
        __SET_VAR(data__->,ENO,,__BOOL_LITERAL(FALSE));
      return;
    }
    // Initialise TEMP variables
    char noise_name[] = "di_noise_fltr_us";
    regs_template_t regs_template;
    regs_template.name = noise_name;
    if(p_link_functions->regs_description_get_by_name(&regs_template)==0){
        regs_access_t reg;
        reg.flag = regs_template.type;
        u16 address = regs_template.guid & GUID_ADDRESS_MASK;
        u16 shift = p_link_functions->regs_size_in_byte(regs_template.type) * __GET_VAR(data__->DI_NUMBER,);
        address +=  shift;
        reg.value.op_u16 = __GET_VAR(data__->DI_NOISE_FLTR_VALUE,);
        if(p_link_functions->regs_set(address,reg)==0){
            __SET_VAR(data__->,ENO,,__BOOL_LITERAL(TRUE));
        }else{
            __SET_VAR(data__->,ENO,,__BOOL_LITERAL(FALSE));
        }
    }
    return;
} // DI_PULSELESS_body__()

void WRITE_DI_PULSELESS_init__(WRITE_DI_PULSELESS *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->DI_NUMBER,0,retain)
  __INIT_VAR(data__->DI_PULSELESS_VALUE,0,retain)
}
// Code part
void WRITE_DI_PULSELESS_body__(WRITE_DI_PULSELESS *data__) {
    // Control execution
    if (!__GET_VAR(data__->EN)) {
        __SET_VAR(data__->,ENO,,__BOOL_LITERAL(FALSE));
      return;
    }
    // Initialise TEMP variables
    char pulseless_name[] = "di_pulseless";
    regs_template_t regs_template;
    regs_template.name = pulseless_name;
    if(p_link_functions->regs_description_get_by_name(&regs_template)==0){
        regs_access_t reg;
        reg.flag = regs_template.type;
        u16 address = regs_template.guid & GUID_ADDRESS_MASK;
        u16 shift = p_link_functions->regs_size_in_byte(regs_template.type) * __GET_VAR(data__->DI_NUMBER,);
        address +=  shift;
        reg.value.op_u32 = __GET_VAR(data__->DI_PULSELESS_VALUE,);
        if(p_link_functions->regs_set(address,reg)==0){
            __SET_VAR(data__->,ENO,,__BOOL_LITERAL(TRUE));
        }else{
            __SET_VAR(data__->,ENO,,__BOOL_LITERAL(FALSE));
        }
    }
    return;
} // WRITE_DI_PULSELESS_body__()

void WRITE_DI_MODE_init__(WRITE_DI_MODE *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->DI_NUMBER,0,retain)
  __INIT_VAR(data__->DI_MODE_VALUE,0,retain)
}
// Code part
void WRITE_DI_MODE_body__(WRITE_DI_MODE *data__) {
    // Control execution
    if (!__GET_VAR(data__->EN)) {
        __SET_VAR(data__->,ENO,,__BOOL_LITERAL(FALSE));
      return;
    }
    // Initialise TEMP variables
    char mode_name[] = "di_mode";
    regs_template_t regs_template;
    regs_template.name = mode_name;
    if(p_link_functions->regs_description_get_by_name(&regs_template)==0){
        regs_access_t reg;
        reg.flag = regs_template.type;
        u16 address = regs_template.guid & GUID_ADDRESS_MASK;
        u16 shift = p_link_functions->regs_size_in_byte(regs_template.type) * __GET_VAR(data__->DI_NUMBER,);
        address +=  shift;
        reg.value.op_u16 = __GET_VAR(data__->DI_MODE_VALUE,);
        if(p_link_functions->regs_set(address,reg)==0){
            __SET_VAR(data__->,ENO,,__BOOL_LITERAL(TRUE));
        }else{
            __SET_VAR(data__->,ENO,,__BOOL_LITERAL(FALSE));
        }
    }
    return;
} // DI_MODE_body__()

void WRITE_DO_init__(WRITE_DO *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->DO_VALUE,0,retain)
  __INIT_VAR(data__->DO_MASK,0,retain)
}

// Code part
void WRITE_DO_body__(WRITE_DO *data__) {
  // Control execution
  if (!__GET_VAR(data__->EN)) {
    __SET_VAR(data__->,ENO,,__BOOL_LITERAL(FALSE));
    return;
  }
  // Initialise TEMP variables
  char do_name[] = "do_ctrl";
  regs_template_t regs_template;
  regs_template.name = do_name;
  if(p_link_functions->regs_description_get_by_name(&regs_template)==0){
      regs_access_t reg;
      reg.value.op_u16 = (u16)__GET_VAR(data__->DO_VALUE,) | (((u16)__GET_VAR(data__->DO_MASK,))<<4);
      reg.flag = regs_template.type;
      u16 address = regs_template.guid & GUID_ADDRESS_MASK;
      if(p_link_functions->regs_set(address,reg)==0){
          __SET_VAR(data__->,ENO,,__BOOL_LITERAL(TRUE));
      }else{
          __SET_VAR(data__->,ENO,,__BOOL_LITERAL(FALSE));
      }
  }
  return;
} // WRITE_DO_body__()

void WRITE_DO_SC_init__(WRITE_DO_SC *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->DO_SC_FLAG,0,retain)
  __INIT_VAR(data__->DO_SC_EN,0,retain)
}

// Code part
void WRITE_DO_SC_body__(WRITE_DO_SC *data__) {
  // Control execution
  if (!__GET_VAR(data__->EN)) {
    __SET_VAR(data__->,ENO,,__BOOL_LITERAL(FALSE));
    return;
  }
  // Initialise TEMP variables
  char do_sc_name[] = "do_sc_ctrl";
  regs_template_t regs_template;
  regs_template.name = do_sc_name;
  if(p_link_functions->regs_description_get_by_name(&regs_template)==0){
      regs_access_t reg;
      reg.value.op_u16 = (u16)__GET_VAR(data__->DO_SC_FLAG,) | (((u16)__GET_VAR(data__->DO_SC_EN,))<<4);
      reg.flag = regs_template.type;
      u16 address = regs_template.guid & GUID_ADDRESS_MASK;
      if(p_link_functions->regs_set(address,reg)==0){
          __SET_VAR(data__->,ENO,,__BOOL_LITERAL(TRUE));
      }else{
          __SET_VAR(data__->,ENO,,__BOOL_LITERAL(FALSE));
      }
  }
  return;
} // WRITE_DO_SC_body__()

void WRITE_DO_PWM_FREQ_init__(WRITE_DO_PWM_FREQ *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->DO_PWM_FREQ,0,retain)
}

// Code part
void WRITE_DO_PWM_FREQ_body__(WRITE_DO_PWM_FREQ *data__) {
  // Control execution
  if (!__GET_VAR(data__->EN)) {
    __SET_VAR(data__->,ENO,,__BOOL_LITERAL(FALSE));
    return;
  }
  // Initialise TEMP variables
  char do_freq_name[] = "do_pwm_freq";
  regs_template_t regs_template;
  regs_template.name = do_freq_name;
  if(p_link_functions->regs_description_get_by_name(&regs_template)==0){
      regs_access_t reg;
      reg.value.op_u16 = (u16)__GET_VAR(data__->DO_PWM_FREQ,);
      reg.flag = regs_template.type;
      u16 address = regs_template.guid & GUID_ADDRESS_MASK;
      if(p_link_functions->regs_set(address,reg)==0){
          __SET_VAR(data__->,ENO,,__BOOL_LITERAL(TRUE));
      }else{
          __SET_VAR(data__->,ENO,,__BOOL_LITERAL(FALSE));
      }
  }
  return;
} // WRITE_DO_PWM_FREQ_body__()

void WRITE_DO_PWM_CTRL_init__(WRITE_DO_PWM_CTRL *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->DO_NUMBER,0,retain)
  __INIT_VAR(data__->DO_PWM_CTRL,0,retain)
}
// Code part
void WRITE_DO_PWM_CTRL_body__(WRITE_DO_PWM_CTRL *data__) {
    // Control execution
    if (!__GET_VAR(data__->EN)) {
        __SET_VAR(data__->,ENO,,__BOOL_LITERAL(FALSE));
      return;
    }
    // Initialise TEMP variables
    char ctrl_name[] = "do_pwm_ctrl";
    regs_template_t regs_template;
    regs_template.name = ctrl_name;
    if(p_link_functions->regs_description_get_by_name(&regs_template)==0){
        regs_access_t reg;
        reg.flag = regs_template.type;
        u8 address = regs_template.guid & GUID_ADDRESS_MASK;
        u8 shift = p_link_functions->regs_size_in_byte(regs_template.type) * __GET_VAR(data__->DO_NUMBER,);
        address +=  shift;
        reg.value.op_u16 = __GET_VAR(data__->DO_PWM_CTRL,);
        if(p_link_functions->regs_set(address,reg)==0){
            __SET_VAR(data__->,ENO,,__BOOL_LITERAL(TRUE));
        }else{
            __SET_VAR(data__->,ENO,,__BOOL_LITERAL(FALSE));
        }
    }
    return;
} // WRITE_DO_PWM_CTRL_body__()

void STRUCT_REAL_TIME_init__(STRUCT_REAL_TIME *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->HOUR_TIME,0,retain)
  __INIT_VAR(data__->MINUTE_TIME,0,retain)
  __INIT_VAR(data__->SEC_TIME,0,retain)
  __INIT_VAR(data__->SUB_SEC_TIME,0,retain)
  __INIT_VAR(data__->WEEK_DAY_TIME,0,retain)
  __INIT_VAR(data__->MONTH_TIME,0,retain)
  __INIT_VAR(data__->DATE_TIME,0,retain)
  __INIT_VAR(data__->YEAR_TIME,0,retain)
  __INIT_VAR(data__->YEAR_DAY_TIME,0,retain)
}

// Code part
void STRUCT_REAL_TIME_body__(STRUCT_REAL_TIME *data__) {
  // Control execution
  if (!__GET_VAR(data__->EN)) {
    __SET_VAR(data__->,ENO,,__BOOL_LITERAL(FALSE));
    return;
  }
  __SET_VAR(data__->,ENO,,__BOOL_LITERAL(FALSE));
  // Initialise TEMP variables
  char time_name[] = "time_hms";
  regs_template_t regs_template;
  regs_template.name = time_name;
  if(p_link_functions->regs_description_get_by_name(&regs_template)==0){
      sofi_time_r time_temp;
      u16 address = regs_template.guid & GUID_ADDRESS_MASK;
      if(p_link_functions->regs_get_buffer(address,(u8*)(void*)&time_temp,sizeof(sofi_time_r))==0){
            __SET_VAR(data__->,HOUR_TIME,,time_temp.hour);
            __SET_VAR(data__->,MINUTE_TIME,,time_temp.min);
            __SET_VAR(data__->,SEC_TIME,,time_temp.sec);
            __SET_VAR(data__->,SUB_SEC_TIME,,time_temp.sub_sec);
            __SET_VAR(data__->,WEEK_DAY_TIME,,time_temp.week_day);
            __SET_VAR(data__->,MONTH_TIME,,time_temp.month);
            __SET_VAR(data__->,DATE_TIME,,time_temp.date);
            __SET_VAR(data__->,YEAR_TIME,,time_temp.year);
            __SET_VAR(data__->,YEAR_DAY_TIME,,time_temp.year_day);
            __SET_VAR(data__->,ENO,,__BOOL_LITERAL(TRUE));
      }
  }
  return;
} // STRUCT_REAL_TIME_body__()
#endif //SOFI_BEREMIZ_C
