/**
 * @file beremiz_regs_description.c
 * @author Shoma Gane <shomagan@gmail.com>
 *         Ayrat Girfanov <girfanov.ayrat@yandex.ru>
 * @defgroup beremiz
 * @ingroup beremiz
 * @version 0.1 
 * @brief  TODO!!! write brief in 
 */
/*
 * Copyright (c) 2018 Snema Service
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
 * SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY
 * OF SUCH DAMAGE.
 *
 * This file is part of the sofi PLC.
 *
 * Author: Shoma Gane <shomagan@gmail.com>
 *         Ayrat Girfanov <girfanov.ayrat@yandex.ru>
 */
#ifndef BEREMIZ_REGS_DESCRIPTION_C
#define BEREMIZ_REGS_DESCRIPTION_C 1
#include "beremiz_regs_description.h"
#include "regs_description.h"
#include "sofi_config.h"
#include "beremiz_task.h"
#include "bkram_access.h"
#include "matiec/iec_types_all.h"


extern u8 mdb_address_space[];
extern u8 mdb_array_address_space[];



extern __IEC_DINT_t CONFIG__TEST_CONFIG;
extern __IEC_BOOL_t CONFIG__ERROR_TEST1;
extern __IEC_BOOL_t CONFIG__ERROR_TEST2;
extern __IEC_DINT_t CONFIG__NUMBERERROR11;
extern __IEC_DINT_t CONFIG__NUMBERERROR12;
extern __IEC_DINT_t CONFIG__NUMBERERROR21;
extern __IEC_DINT_t CONFIG__NUMBERERROR22;
extern __IEC_DINT_t CONFIG__A0DINT;
extern __IEC_INT_t CONFIG__A0INT;
extern __IEC_UINT_t CONFIG__A0UINT;
extern __IEC_ULINT_t CONFIG__A0ULINT;
extern __IEC_USINT_t CONFIG__A0USINT;
extern __IEC_UDINT_t CONFIG__A0UDINT;
extern __IEC_REAL_t CONFIG__A0REAL;
extern __IEC_LREAL_t CONFIG__A0LREAL;
extern __IEC_BYTE_t CONFIG__A0BYTE;
extern __IEC_WORD_t CONFIG__A0WORD;
extern __IEC_LWORD_t CONFIG__A0LWORD;
extern __IEC_LINT_t CONFIG__A0LINT;


regs_description_t const beremiz_regs_description[BEREMIZ_REGS_VAR_NUM]={
    
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_DINT_t *)(&CONFIG__TEST_CONFIG))->value,/*saves address*/0,/*description*/"TEST_CONFIG",/*name*/"TEST_CONFIG",/*type*/S32_REGS_FLAG,/*ind*/0  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_BOOL_t *)(&CONFIG__ERROR_TEST1))->value,/*saves address*/0,/*description*/"ERROR_TEST1",/*name*/"ERROR_TEST1",/*type*/U8_REGS_FLAG,/*ind*/1  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_BOOL_t *)(&CONFIG__ERROR_TEST2))->value,/*saves address*/0,/*description*/"ERROR_TEST2",/*name*/"ERROR_TEST2",/*type*/U8_REGS_FLAG,/*ind*/2  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_DINT_t *)(&CONFIG__NUMBERERROR11))->value,/*saves address*/0,/*description*/"NUMBERERROR11",/*name*/"NUMBERERROR11",/*type*/S32_REGS_FLAG,/*ind*/3  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_DINT_t *)(&CONFIG__NUMBERERROR12))->value,/*saves address*/0,/*description*/"NUMBERERROR12",/*name*/"NUMBERERROR12",/*type*/S32_REGS_FLAG,/*ind*/4  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_DINT_t *)(&CONFIG__NUMBERERROR21))->value,/*saves address*/0,/*description*/"NUMBERERROR21",/*name*/"NUMBERERROR21",/*type*/S32_REGS_FLAG,/*ind*/5  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_DINT_t *)(&CONFIG__NUMBERERROR22))->value,/*saves address*/0,/*description*/"NUMBERERROR22",/*name*/"NUMBERERROR22",/*type*/S32_REGS_FLAG,/*ind*/6  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_DINT_t *)(&CONFIG__A0DINT))->value,/*saves address*/0,/*description*/"A0DINT",/*name*/"A0DINT",/*type*/S32_REGS_FLAG,/*ind*/7  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_INT_t *)(&CONFIG__A0INT))->value,/*saves address*/0,/*description*/"A0INT",/*name*/"A0INT",/*type*/S16_REGS_FLAG,/*ind*/8  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_UINT_t *)(&CONFIG__A0UINT))->value,/*saves address*/0,/*description*/"A0UINT",/*name*/"A0UINT",/*type*/U16_REGS_FLAG,/*ind*/9  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_ULINT_t *)(&CONFIG__A0ULINT))->value,/*saves address*/0,/*description*/"A0ULINT",/*name*/"A0ULINT",/*type*/U64_REGS_FLAG,/*ind*/10  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_USINT_t *)(&CONFIG__A0USINT))->value,/*saves address*/0,/*description*/"A0USINT",/*name*/"A0USINT",/*type*/U8_REGS_FLAG,/*ind*/11  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_UDINT_t *)(&CONFIG__A0UDINT))->value,/*saves address*/0,/*description*/"A0UDINT",/*name*/"A0UDINT",/*type*/U32_REGS_FLAG,/*ind*/12  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__A0REAL))->value,/*saves address*/0,/*description*/"A0REAL",/*name*/"A0REAL",/*type*/FLOAT_REGS_FLAG,/*ind*/13  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_LREAL_t *)(&CONFIG__A0LREAL))->value,/*saves address*/0,/*description*/"A0LREAL",/*name*/"A0LREAL",/*type*/DOUBLE_REGS_FLAG,/*ind*/14  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_BYTE_t *)(&CONFIG__A0BYTE))->value,/*saves address*/0,/*description*/"A0BYTE",/*name*/"A0BYTE",/*type*/U8_REGS_FLAG,/*ind*/15  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_WORD_t *)(&CONFIG__A0WORD))->value,/*saves address*/0,/*description*/"A0WORD",/*name*/"A0WORD",/*type*/U16_REGS_FLAG,/*ind*/16  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_LWORD_t *)(&CONFIG__A0LWORD))->value,/*saves address*/0,/*description*/"A0LWORD",/*name*/"A0LWORD",/*type*/U64_REGS_FLAG,/*ind*/17  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_LINT_t *)(&CONFIG__A0LINT))->value,/*saves address*/0,/*description*/"A0LINT",/*name*/"A0LINT",/*type*/S64_REGS_FLAG,/*ind*/18  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},

};
#endif//BEREMIZ_REGS_DESCRIPTION_C
