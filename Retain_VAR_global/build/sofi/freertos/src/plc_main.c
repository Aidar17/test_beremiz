/**
 * @file C:\Beremiz\beremiz_test_projects\Retain_VAR_global\build//sofi/freertos/src/plc_main.c
 * @author Shoma Gane <shomagan@gmail.com>
 *         Ayrat Girfanov <girfanov.ayrat@yandex.ru>
 * @defgroup beremiz 
 * @ingroup beremiz 
 * @version 0.1 
 * @brief  TODO!!! write brief in 
 */
    /*
     * Copyright (c) 2018 Snema Service
     * All rights reserved.
     *
     * Redistribution and use in source and binary forms, with or without modification,
     * are permitted provided that the following conditions are met:
     *
     * 1. Redistributions of source code must retain the above copyright notice,
     *    this list of conditions and the following disclaimer.
     * 2. Redistributions in binary form must reproduce the above copyright notice,
     *    this list of conditions and the following disclaimer in the documentation
     *    and/or other materials provided with the distribution.
     * 3. The name of the author may not be used to endorse or promote products
     *    derived from this software without specific prior written permission.
     *
     * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
     * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
     * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
     * SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
     * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
     * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
     * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
     * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
     * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY
     * OF SUCH DAMAGE.
     *
     * This file is part of the sofi PLC.
     *
     * Author: Shoma Gane <shomagan@gmail.com>
     *         Ayrat Girfanov <girfanov.ayrat@yandex.ru>
     */
#ifndef PLC_MAIN_C
#define PLC_MAIN_C
/**
 * Head of code common to all C targets
 **/


#include "plc_main.h"
#include "POUS.h"
#include "config_task.h"
#include "os_service.h"
#include "link_functions.h"
#include "beremiz_task.h"


/*
 * Prototypes of functions provided by generated C softPLC
 **/
void __init_debug(void);
void __publish_debug(void);

/*
 * Prototypes of functions provided by generated target C code
 * */
/*
 *  Variables used by generated C softPLC and plugins
 **/
IEC_TIME __CURRENT_TIME;
IEC_BOOL __DEBUG = 0;
unsigned long long __tick;
char *PLC_ID = NULL;


/* Help to quit cleanly when init fail at a certain level */
static int init_level = 0;


/*
 * Initialize variables according to PLC's default values,
 * and then init plugins with that values
 **/
int __init()
{
    init_level = 0;
    __tick = 0;
    config_init__();
    __init_debug();
    return 0;
}


/**
 * No platform specific code for "Generic" target
 **/
void PLC_GetTime(IEC_TIME *CURRENT_TIME)
{
    //time in second from 1970 year
    char name[] = "time";

    regs_template_t  regs_template;
    regs_template.name = name;
    if(!p_link_functions->regs_description_get_by_name(&regs_template)){
        sofi_time_r * time;
        time = (sofi_time_r *)(void*)regs_template.p_value;//copy pointer to
        (*CURRENT_TIME).tv_sec = time->sec;
        //part in milliseconds fractional (ex. sec.millisec)
        char name_100us[] = "tick100us";
        regs_template.name = name_100us;
        if(!p_link_functions->regs_description_get_by_name(&regs_template)){
            u64 tick100us;
            memcpy(&tick100us,regs_template.p_value,sizeof(u64));
            (*CURRENT_TIME).tv_nsec = (tick100us%10) * 100000;
        }else{
            //@todo add error messege
        }
    }else{
        //@todo add error messege
    }
}

static void PLC_timer_notify(void);

void PLC_timer_notify()
{
    PLC_GetTime(&__CURRENT_TIME);
    __tick++;
    config_run__(__tick);
    __publish_debug();
}
/* Variable used to stop plcloop thread */
void* PlcLoop(void * args)
{
    (void)args;
    PLC_timer_notify();
    return NULL;
}
#endif //PLC_MAIN_C
