/**
 * @file beremiz_regs_description.c
 * @author Shoma Gane <shomagan@gmail.com>
 *         Ayrat Girfanov <girfanov.ayrat@yandex.ru>
 * @defgroup beremiz
 * @ingroup beremiz
 * @version 0.1 
 * @brief  TODO!!! write brief in 
 */
/*
 * Copyright (c) 2018 Snema Service
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
 * SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY
 * OF SUCH DAMAGE.
 *
 * This file is part of the sofi PLC.
 *
 * Author: Shoma Gane <shomagan@gmail.com>
 *         Ayrat Girfanov <girfanov.ayrat@yandex.ru>
 */
#ifndef BEREMIZ_REGS_DESCRIPTION_C
#define BEREMIZ_REGS_DESCRIPTION_C 1
#include "beremiz_regs_description.h"
#include "regs_description.h"
#include "sofi_config.h"
#include "beremiz_task.h"
#include "bkram_access.h"
#include "matiec/iec_types_all.h"
#include "modbus_master.h"
extern u16 com_buffer_req_0[] ;
extern u16 com_buffer_req_1[] ;
extern u16 com_buffer_req_2[] ;
extern u16 com_buffer_req_3[] ;
extern u16 com_buffer_req_4[] ;
extern u16 com_buffer_req_5[] ;
extern u16 com_buffer_req_6[] ;
extern u16 com_buffer_req_7[] ;
extern u16 com_buffer_req_8[] ;
extern u16 com_buffer_req_9[] ;
extern u16 com_buffer_req_10[] ;
extern u16 com_buffer_req_11[] ;

extern u8 mdb_address_space[];
extern u8 mdb_array_address_space[];



extern __IEC_UDINT_t CONFIG__START_TEST;
extern __IEC_UDINT_p CONFIG__DO_TEST_W;
extern __IEC_UDINT_p CONFIG__AO_TEST_W;
extern __IEC_UDINT_p CONFIG__AI_TEST_W;
extern __IEC_UDINT_p CONFIG__DI_TEST_W;
extern __IEC_UDINT_p CONFIG__DO_TEST_R;
extern __IEC_UDINT_p CONFIG__AO_TEST_R;
extern __IEC_UDINT_p CONFIG__AI_TEST_R;
extern __IEC_UDINT_p CONFIG__DI_TEST_R;
extern __IEC_UDINT_p CONFIG__DO_FREE_HEAP;
extern __IEC_UDINT_p CONFIG__AO_FREE_HEAP;
extern __IEC_UDINT_p CONFIG__AI_FREE_HEAP;
extern __IEC_UDINT_p CONFIG__DI_FREE_HEAP;
extern __IEC_UINT_p CONFIG__DO_IP3;
extern __IEC_UINT_p CONFIG__AO_IP3;
extern __IEC_UINT_p CONFIG__AI_IP3;
extern __IEC_UINT_p CONFIG__DI_IP3;
extern __IEC_UINT_t CONFIG__BRIC_1_MDB_ADDR0;
extern __IEC_UINT_t CONFIG__DO_1_MDB_ADDR0;
extern __IEC_UINT_t CONFIG__AO_1_MDB_ADDR0;
extern __IEC_UINT_t CONFIG__AI_1_MDB_ADDR0;
extern __IEC_UINT_t CONFIG__DI_1_MDB_ADDR0;
extern __IEC_UINT_t CONFIG__BRIC_1_MODULE_NUMBER0;
extern __IEC_UINT_t CONFIG__DO_1_MODULE_NUMBER0;
extern __IEC_UINT_t CONFIG__AO_1_MODULE_NUMBER0;
extern __IEC_UINT_t CONFIG__AI_1_MODULE_NUMBER0;
extern __IEC_UINT_t CONFIG__DI_1_MODULE_NUMBER0;
extern __IEC_UINT_t CONFIG__AO_CH_0;
extern __IEC_UINT_t CONFIG__AO_CH_1;
extern __IEC_UINT_t CONFIG__AO_CH_2;
extern __IEC_UINT_t CONFIG__AO_CH_3;
extern __IEC_UINT_t CONFIG__DO_CH;
extern __IEC_UINT_t CONFIG__DO_PLC_CH;
extern __IEC_REAL_t CONFIG__BRIC_1_AI_PHYSICAL0;
extern __IEC_REAL_t CONFIG__BRIC_1_AI_PHYSICAL1;
extern __IEC_UINT_t CONFIG__BRIC_1_AI_UNIT0;
extern __IEC_UINT_t CONFIG__BRIC_1_AI_UNIT1;
extern __IEC_USINT_t CONFIG__BRIC_1_DO_SC_CTRL0;
extern __IEC_UDINT_t CONFIG__DI_1_DI_STATE0;
extern __IEC_UDINT_t CONFIG__BRIC_1_DI_STATE0;
extern __IEC_UINT_t CONFIG__DO_1_DO_SC_CTRL0;
extern __IEC_UINT_t CONFIG__AI_1_AI_UNIT0;
extern __IEC_UINT_t CONFIG__AI_1_AI_UNIT1;
extern __IEC_UINT_t CONFIG__ADR_MOD;
extern __IEC_UINT_t CONFIG__RES_N;
extern __IEC_UINT_t CONFIG__LAS_RES;
extern __IEC_UINT_t CONFIG__ADR_M;
extern __IEC_UINT_t CONFIG__DI_N;
extern __IEC_ULINT_t CONFIG__DI_V;
extern __IEC_REAL_t CONFIG__DI_F;
extern __IEC_UDINT_t CONFIG__DI_O;
extern __IEC_USINT_t CONFIG__DO_O;
extern __IEC_UINT_t CONFIG__W_DO_SC_F;
extern __IEC_UINT_t CONFIG__W_DO_SC_E;
extern __IEC_UINT_t CONFIG__DO_W;
extern __IEC_UINT_t CONFIG__DO_F;
extern __IEC_ULINT_t CONFIG__TEST_RESULT_MODULE;
extern __IEC_ULINT_t CONFIG__TEST_RESULT_MODULE_2;
extern __IEC_ULINT_t CONFIG__TEST_RESULT_PLC;
extern __IEC_BOOL_t CONFIG__TEST_2;
extern __IEC_BOOL_t CONFIG__TEST_3;


regs_description_t const beremiz_regs_description[BEREMIZ_REGS_VAR_NUM]={
    
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_UDINT_t *)(&CONFIG__START_TEST))->value,/*saves address*/0,/*description*/"START_TEST",/*name*/"START_TEST",/*type*/U32_REGS_FLAG,/*ind*/0  ,/*guid*/GUID_USER_HEAD | 131072 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&com_buffer_req_4[0],/*saves address*/0,/*description*/"DO_TEST_W",/*name*/"DO_TEST_W",/*type*/U32_REGS_FLAG,/*ind*/1  ,/*guid*/GUID_USER_HEAD|(131073 )  , /*size*/1 , /*flags*/USER_VARS},
{/*default value*/NULL,/*pointer to value*/(u8*)&com_buffer_req_5[0],/*saves address*/0,/*description*/"AO_TEST_W",/*name*/"AO_TEST_W",/*type*/U32_REGS_FLAG,/*ind*/2  ,/*guid*/GUID_USER_HEAD|(131074 )  , /*size*/1 , /*flags*/USER_VARS},
{/*default value*/NULL,/*pointer to value*/(u8*)&com_buffer_req_6[0],/*saves address*/0,/*description*/"AI_TEST_W",/*name*/"AI_TEST_W",/*type*/U32_REGS_FLAG,/*ind*/3  ,/*guid*/GUID_USER_HEAD|(131075 )  , /*size*/1 , /*flags*/USER_VARS},
{/*default value*/NULL,/*pointer to value*/(u8*)&com_buffer_req_7[0],/*saves address*/0,/*description*/"DI_TEST_W",/*name*/"DI_TEST_W",/*type*/U32_REGS_FLAG,/*ind*/4  ,/*guid*/GUID_USER_HEAD|(131076 )  , /*size*/1 , /*flags*/USER_VARS},
{/*default value*/NULL,/*pointer to value*/(u8*)&com_buffer_req_0[3],/*saves address*/0,/*description*/"DO_TEST_R",/*name*/"DO_TEST_R",/*type*/U32_REGS_FLAG,/*ind*/5  ,/*guid*/GUID_USER_HEAD|(131077 )  , /*size*/1 , /*flags*/USER_VARS},
{/*default value*/NULL,/*pointer to value*/(u8*)&com_buffer_req_1[3],/*saves address*/0,/*description*/"AO_TEST_R",/*name*/"AO_TEST_R",/*type*/U32_REGS_FLAG,/*ind*/6  ,/*guid*/GUID_USER_HEAD|(131078 )  , /*size*/1 , /*flags*/USER_VARS},
{/*default value*/NULL,/*pointer to value*/(u8*)&com_buffer_req_2[3],/*saves address*/0,/*description*/"AI_TEST_R",/*name*/"AI_TEST_R",/*type*/U32_REGS_FLAG,/*ind*/7  ,/*guid*/GUID_USER_HEAD|(131079 )  , /*size*/1 , /*flags*/USER_VARS},
{/*default value*/NULL,/*pointer to value*/(u8*)&com_buffer_req_3[3],/*saves address*/0,/*description*/"DI_TEST_R",/*name*/"DI_TEST_R",/*type*/U32_REGS_FLAG,/*ind*/8  ,/*guid*/GUID_USER_HEAD|(131080 )  , /*size*/1 , /*flags*/USER_VARS},
{/*default value*/NULL,/*pointer to value*/(u8*)&com_buffer_req_0[0],/*saves address*/0,/*description*/"DO_FREE_HEAP",/*name*/"DO_FREE_HEAP",/*type*/U32_REGS_FLAG,/*ind*/9  ,/*guid*/GUID_USER_HEAD|(131081 )  , /*size*/1 , /*flags*/USER_VARS},
{/*default value*/NULL,/*pointer to value*/(u8*)&com_buffer_req_1[0],/*saves address*/0,/*description*/"AO_FREE_HEAP",/*name*/"AO_FREE_HEAP",/*type*/U32_REGS_FLAG,/*ind*/10  ,/*guid*/GUID_USER_HEAD|(131082 )  , /*size*/1 , /*flags*/USER_VARS},
{/*default value*/NULL,/*pointer to value*/(u8*)&com_buffer_req_2[0],/*saves address*/0,/*description*/"AI_FREE_HEAP",/*name*/"AI_FREE_HEAP",/*type*/U32_REGS_FLAG,/*ind*/11  ,/*guid*/GUID_USER_HEAD|(131083 )  , /*size*/1 , /*flags*/USER_VARS},
{/*default value*/NULL,/*pointer to value*/(u8*)&com_buffer_req_3[0],/*saves address*/0,/*description*/"DI_FREE_HEAP",/*name*/"DI_FREE_HEAP",/*type*/U32_REGS_FLAG,/*ind*/12  ,/*guid*/GUID_USER_HEAD|(131084 )  , /*size*/1 , /*flags*/USER_VARS},
{/*default value*/NULL,/*pointer to value*/(u8*)&com_buffer_req_8[1],/*saves address*/0,/*description*/"change ip",/*name*/"DO_IP3",/*type*/U16_REGS_FLAG,/*ind*/13  ,/*guid*/GUID_USER_HEAD|(131085 )  , /*size*/1 , /*flags*/USER_VARS},
{/*default value*/NULL,/*pointer to value*/(u8*)&com_buffer_req_9[1],/*saves address*/0,/*description*/"change ip",/*name*/"AO_IP3",/*type*/U16_REGS_FLAG,/*ind*/14  ,/*guid*/GUID_USER_HEAD|(131086 )  , /*size*/1 , /*flags*/USER_VARS},
{/*default value*/NULL,/*pointer to value*/(u8*)&com_buffer_req_10[1],/*saves address*/0,/*description*/"change ip",/*name*/"AI_IP3",/*type*/U16_REGS_FLAG,/*ind*/15  ,/*guid*/GUID_USER_HEAD|(131087 )  , /*size*/1 , /*flags*/USER_VARS},
{/*default value*/NULL,/*pointer to value*/(u8*)&com_buffer_req_11[1],/*saves address*/0,/*description*/"change ip",/*name*/"DI_IP3",/*type*/U16_REGS_FLAG,/*ind*/16  ,/*guid*/GUID_USER_HEAD|(131088 )  , /*size*/1 , /*flags*/USER_VARS},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_UINT_t *)(&CONFIG__BRIC_1_MDB_ADDR0))->value,/*saves address*/0,/*description*/"setting the address on the slave",/*name*/"BRIC_1_MDB_ADDR0",/*type*/U16_REGS_FLAG,/*ind*/17  ,/*guid*/GUID_USER_HEAD | 131089 , /*size*/1 , /*flags*/USER_VARS},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_UINT_t *)(&CONFIG__DO_1_MDB_ADDR0))->value,/*saves address*/0,/*description*/"setting the address on the slave",/*name*/"DO_1_MDB_ADDR0",/*type*/U16_REGS_FLAG,/*ind*/18  ,/*guid*/GUID_USER_HEAD | 131090 , /*size*/1 , /*flags*/USER_VARS},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_UINT_t *)(&CONFIG__AO_1_MDB_ADDR0))->value,/*saves address*/0,/*description*/"setting the address on the slave",/*name*/"AO_1_MDB_ADDR0",/*type*/U16_REGS_FLAG,/*ind*/19  ,/*guid*/GUID_USER_HEAD | 131091 , /*size*/1 , /*flags*/USER_VARS},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_UINT_t *)(&CONFIG__AI_1_MDB_ADDR0))->value,/*saves address*/0,/*description*/"setting the address on the slave",/*name*/"AI_1_MDB_ADDR0",/*type*/U16_REGS_FLAG,/*ind*/20  ,/*guid*/GUID_USER_HEAD | 131092 , /*size*/1 , /*flags*/USER_VARS},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_UINT_t *)(&CONFIG__DI_1_MDB_ADDR0))->value,/*saves address*/0,/*description*/"setting the address on the slave",/*name*/"DI_1_MDB_ADDR0",/*type*/U16_REGS_FLAG,/*ind*/21  ,/*guid*/GUID_USER_HEAD | 131093 , /*size*/1 , /*flags*/USER_VARS},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_UINT_t *)(&CONFIG__BRIC_1_MODULE_NUMBER0))->value,/*saves address*/0,/*description*/"BRIC_1_MODULE_NUMBER0",/*name*/"BRIC_1_MODULE_NUMBER0",/*type*/U16_REGS_FLAG,/*ind*/22  ,/*guid*/GUID_USER_HEAD | 131094 , /*size*/1 , /*flags*/USER_VARS},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_UINT_t *)(&CONFIG__DO_1_MODULE_NUMBER0))->value,/*saves address*/0,/*description*/"DO_1_MODULE_NUMBER0",/*name*/"DO_1_MODULE_NUMBER0",/*type*/U16_REGS_FLAG,/*ind*/23  ,/*guid*/GUID_USER_HEAD | 131095 , /*size*/1 , /*flags*/USER_VARS},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_UINT_t *)(&CONFIG__AO_1_MODULE_NUMBER0))->value,/*saves address*/0,/*description*/"AO_1_MODULE_NUMBER0",/*name*/"AO_1_MODULE_NUMBER0",/*type*/U16_REGS_FLAG,/*ind*/24  ,/*guid*/GUID_USER_HEAD | 131096 , /*size*/1 , /*flags*/USER_VARS},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_UINT_t *)(&CONFIG__AI_1_MODULE_NUMBER0))->value,/*saves address*/0,/*description*/"AI_1_MODULE_NUMBER0",/*name*/"AI_1_MODULE_NUMBER0",/*type*/U16_REGS_FLAG,/*ind*/25  ,/*guid*/GUID_USER_HEAD | 131097 , /*size*/1 , /*flags*/USER_VARS},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_UINT_t *)(&CONFIG__DI_1_MODULE_NUMBER0))->value,/*saves address*/0,/*description*/"DI_1_MODULE_NUMBER0",/*name*/"DI_1_MODULE_NUMBER0",/*type*/U16_REGS_FLAG,/*ind*/26  ,/*guid*/GUID_USER_HEAD | 131098 , /*size*/1 , /*flags*/USER_VARS},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_UINT_t *)(&CONFIG__AO_CH_0))->value,/*saves address*/0,/*description*/"setting ai level",/*name*/"AO_CH_0",/*type*/U16_REGS_FLAG,/*ind*/27  ,/*guid*/GUID_USER_HEAD | 131099 , /*size*/1 , /*flags*/USER_VARS},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_UINT_t *)(&CONFIG__AO_CH_1))->value,/*saves address*/0,/*description*/"setting ai level",/*name*/"AO_CH_1",/*type*/U16_REGS_FLAG,/*ind*/28  ,/*guid*/GUID_USER_HEAD | 131100 , /*size*/1 , /*flags*/USER_VARS},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_UINT_t *)(&CONFIG__AO_CH_2))->value,/*saves address*/0,/*description*/"setting ai level",/*name*/"AO_CH_2",/*type*/U16_REGS_FLAG,/*ind*/29  ,/*guid*/GUID_USER_HEAD | 131101 , /*size*/1 , /*flags*/USER_VARS},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_UINT_t *)(&CONFIG__AO_CH_3))->value,/*saves address*/0,/*description*/"setting ai level",/*name*/"AO_CH_3",/*type*/U16_REGS_FLAG,/*ind*/30  ,/*guid*/GUID_USER_HEAD | 131102 , /*size*/1 , /*flags*/USER_VARS},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_UINT_t *)(&CONFIG__DO_CH))->value,/*saves address*/0,/*description*/"enable do on a module",/*name*/"DO_CH",/*type*/U16_REGS_FLAG,/*ind*/31  ,/*guid*/GUID_USER_HEAD | 131103 , /*size*/1 , /*flags*/USER_VARS},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_UINT_t *)(&CONFIG__DO_PLC_CH))->value,/*saves address*/0,/*description*/"enable do on a plc",/*name*/"DO_PLC_CH",/*type*/U16_REGS_FLAG,/*ind*/32  ,/*guid*/GUID_USER_HEAD | 131104 , /*size*/1 , /*flags*/USER_VARS},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__BRIC_1_AI_PHYSICAL0))->value,/*saves address*/0,/*description*/"BRIC_1_AI_PHYSICAL0",/*name*/"BRIC_1_AI_PHYSICAL0",/*type*/FLOAT_REGS_FLAG,/*ind*/33  ,/*guid*/GUID_USER_HEAD | 131105 , /*size*/1 , /*flags*/USER_VARS},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__BRIC_1_AI_PHYSICAL1))->value,/*saves address*/0,/*description*/"BRIC_1_AI_PHYSICAL1",/*name*/"BRIC_1_AI_PHYSICAL1",/*type*/FLOAT_REGS_FLAG,/*ind*/34  ,/*guid*/GUID_USER_HEAD | 131106 , /*size*/1 , /*flags*/USER_VARS},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_UINT_t *)(&CONFIG__BRIC_1_AI_UNIT0))->value,/*saves address*/0,/*description*/"BRIC_1_AI_UNIT0",/*name*/"BRIC_1_AI_UNIT0",/*type*/U16_REGS_FLAG,/*ind*/35  ,/*guid*/GUID_USER_HEAD | 131107 , /*size*/1 , /*flags*/USER_VARS},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_UINT_t *)(&CONFIG__BRIC_1_AI_UNIT1))->value,/*saves address*/0,/*description*/"BRIC_1_AI_UNIT1",/*name*/"BRIC_1_AI_UNIT1",/*type*/U16_REGS_FLAG,/*ind*/36  ,/*guid*/GUID_USER_HEAD | 131108 , /*size*/1 , /*flags*/USER_VARS},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_USINT_t *)(&CONFIG__BRIC_1_DO_SC_CTRL0))->value,/*saves address*/0,/*description*/"BRIC_1_DO_SC_CTRL0",/*name*/"BRIC_1_DO_SC_CTRL0",/*type*/U8_REGS_FLAG,/*ind*/37  ,/*guid*/GUID_USER_HEAD | 131109 , /*size*/1 , /*flags*/USER_VARS},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_UDINT_t *)(&CONFIG__DI_1_DI_STATE0))->value,/*saves address*/0,/*description*/"DI_1_DI_STATE0",/*name*/"DI_1_DI_STATE0",/*type*/U32_REGS_FLAG,/*ind*/38  ,/*guid*/GUID_USER_HEAD | 131110 , /*size*/1 , /*flags*/USER_VARS},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_UDINT_t *)(&CONFIG__BRIC_1_DI_STATE0))->value,/*saves address*/0,/*description*/"BRIC_1_DI_STATE0",/*name*/"BRIC_1_DI_STATE0",/*type*/U32_REGS_FLAG,/*ind*/39  ,/*guid*/GUID_USER_HEAD | 131111 , /*size*/1 , /*flags*/USER_VARS},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_UINT_t *)(&CONFIG__DO_1_DO_SC_CTRL0))->value,/*saves address*/0,/*description*/"DO_1_DO_SC_CTRL0",/*name*/"DO_1_DO_SC_CTRL0",/*type*/U16_REGS_FLAG,/*ind*/40  ,/*guid*/GUID_USER_HEAD | 131112 , /*size*/1 , /*flags*/USER_VARS},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_UINT_t *)(&CONFIG__AI_1_AI_UNIT0))->value,/*saves address*/0,/*description*/"AI_1_AI_UNIT0",/*name*/"AI_1_AI_UNIT0",/*type*/U16_REGS_FLAG,/*ind*/41  ,/*guid*/GUID_USER_HEAD | 131113 , /*size*/1 , /*flags*/USER_VARS},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_UINT_t *)(&CONFIG__AI_1_AI_UNIT1))->value,/*saves address*/0,/*description*/"AI_1_AI_UNIT1",/*name*/"AI_1_AI_UNIT1",/*type*/U16_REGS_FLAG,/*ind*/42  ,/*guid*/GUID_USER_HEAD | 131114 , /*size*/1 , /*flags*/USER_VARS},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_UINT_t *)(&CONFIG__ADR_MOD))->value,/*saves address*/0,/*description*/"read address",/*name*/"ADR_MOD",/*type*/U16_REGS_FLAG,/*ind*/43  ,/*guid*/GUID_USER_HEAD | 131115 , /*size*/1 , /*flags*/USER_VARS},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_UINT_t *)(&CONFIG__RES_N))->value,/*saves address*/0,/*description*/"RES_N",/*name*/"RES_N",/*type*/U16_REGS_FLAG,/*ind*/44  ,/*guid*/GUID_USER_HEAD | 131116 , /*size*/1 , /*flags*/USER_VARS},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_UINT_t *)(&CONFIG__LAS_RES))->value,/*saves address*/0,/*description*/"LAS_RES",/*name*/"LAS_RES",/*type*/U16_REGS_FLAG,/*ind*/45  ,/*guid*/GUID_USER_HEAD | 131117 , /*size*/1 , /*flags*/USER_VARS},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_UINT_t *)(&CONFIG__ADR_M))->value,/*saves address*/0,/*description*/"modbus address to change",/*name*/"ADR_M",/*type*/U16_REGS_FLAG,/*ind*/46  ,/*guid*/GUID_USER_HEAD | 131118 , /*size*/1 , /*flags*/USER_VARS},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_UINT_t *)(&CONFIG__DI_N))->value,/*saves address*/0,/*description*/"checked channel number",/*name*/"DI_N",/*type*/U16_REGS_FLAG,/*ind*/47  ,/*guid*/GUID_USER_HEAD | 131119 , /*size*/1 , /*flags*/USER_VARS},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_ULINT_t *)(&CONFIG__DI_V))->value,/*saves address*/0,/*description*/"DI_V",/*name*/"DI_V",/*type*/U64_REGS_FLAG,/*ind*/48  ,/*guid*/GUID_USER_HEAD | 131120 , /*size*/1 , /*flags*/USER_VARS},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__DI_F))->value,/*saves address*/0,/*description*/"DI_F",/*name*/"DI_F",/*type*/FLOAT_REGS_FLAG,/*ind*/49  ,/*guid*/GUID_USER_HEAD | 131121 , /*size*/1 , /*flags*/USER_VARS},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_UDINT_t *)(&CONFIG__DI_O))->value,/*saves address*/0,/*description*/"DI_O",/*name*/"DI_O",/*type*/U32_REGS_FLAG,/*ind*/50  ,/*guid*/GUID_USER_HEAD | 131122 , /*size*/1 , /*flags*/USER_VARS},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_USINT_t *)(&CONFIG__DO_O))->value,/*saves address*/0,/*description*/"DO_O",/*name*/"DO_O",/*type*/U8_REGS_FLAG,/*ind*/51  ,/*guid*/GUID_USER_HEAD | 131123 , /*size*/1 , /*flags*/USER_VARS},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_UINT_t *)(&CONFIG__W_DO_SC_F))->value,/*saves address*/0,/*description*/"security do",/*name*/"W_DO_SC_F",/*type*/U16_REGS_FLAG,/*ind*/52  ,/*guid*/GUID_USER_HEAD | 131124 , /*size*/1 , /*flags*/USER_VARS},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_UINT_t *)(&CONFIG__W_DO_SC_E))->value,/*saves address*/0,/*description*/"security do",/*name*/"W_DO_SC_E",/*type*/U16_REGS_FLAG,/*ind*/53  ,/*guid*/GUID_USER_HEAD | 131125 , /*size*/1 , /*flags*/USER_VARS},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_UINT_t *)(&CONFIG__DO_W))->value,/*saves address*/0,/*description*/"value do",/*name*/"DO_W",/*type*/U16_REGS_FLAG,/*ind*/54  ,/*guid*/GUID_USER_HEAD | 131126 , /*size*/1 , /*flags*/USER_VARS},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_UINT_t *)(&CONFIG__DO_F))->value,/*saves address*/0,/*description*/"frequency do",/*name*/"DO_F",/*type*/U16_REGS_FLAG,/*ind*/55  ,/*guid*/GUID_USER_HEAD | 131127 , /*size*/1 , /*flags*/USER_VARS},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_ULINT_t *)(&CONFIG__TEST_RESULT_MODULE))->value,/*saves address*/0,/*description*/"TEST_RESULT_MODULE",/*name*/"TEST_RESULT_MODULE",/*type*/U64_REGS_FLAG,/*ind*/56  ,/*guid*/GUID_USER_HEAD | 131128 , /*size*/1 , /*flags*/USER_VARS},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_ULINT_t *)(&CONFIG__TEST_RESULT_MODULE_2))->value,/*saves address*/0,/*description*/"TEST_RESULT_MODULE_2",/*name*/"TEST_RESULT_MODULE_2",/*type*/U64_REGS_FLAG,/*ind*/57  ,/*guid*/GUID_USER_HEAD | 131129 , /*size*/1 , /*flags*/USER_VARS},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_ULINT_t *)(&CONFIG__TEST_RESULT_PLC))->value,/*saves address*/0,/*description*/"TEST_RESULT_PLC",/*name*/"TEST_RESULT_PLC",/*type*/U64_REGS_FLAG,/*ind*/58  ,/*guid*/GUID_USER_HEAD | 131130 , /*size*/1 , /*flags*/USER_VARS},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_BOOL_t *)(&CONFIG__TEST_2))->value,/*saves address*/0,/*description*/"start of channels",/*name*/"TEST_2",/*type*/U8_REGS_FLAG,/*ind*/59  ,/*guid*/GUID_USER_HEAD | 131131 , /*size*/1 , /*flags*/USER_VARS},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_BOOL_t *)(&CONFIG__TEST_3))->value,/*saves address*/0,/*description*/"start of frequency",/*name*/"TEST_3",/*type*/U8_REGS_FLAG,/*ind*/60  ,/*guid*/GUID_USER_HEAD | 131132 , /*size*/1 , /*flags*/USER_VARS},

};
#endif//BEREMIZ_REGS_DESCRIPTION_C
