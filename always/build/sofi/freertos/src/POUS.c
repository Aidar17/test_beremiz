/**
 * @file D:\test\always\build//sofi/freertos/src/POUS.c
 * @author Shoma Gane <shomagan@gmail.com>
 *         Ayrat Girfanov <girfanov.ayrat@yandex.ru>
 * @defgroup beremiz 
 * @ingroup beremiz 
 * @version 0.1 
 * @brief  TODO!!! write brief in 
 */
    /*
     * Copyright (c) 2018 Snema Service
     * All rights reserved.
     *
     * Redistribution and use in source and binary forms, with or without modification,
     * are permitted provided that the following conditions are met:
     *
     * 1. Redistributions of source code must retain the above copyright notice,
     *    this list of conditions and the following disclaimer.
     * 2. Redistributions in binary form must reproduce the above copyright notice,
     *    this list of conditions and the following disclaimer in the documentation
     *    and/or other materials provided with the distribution.
     * 3. The name of the author may not be used to endorse or promote products
     *    derived from this software without specific prior written permission.
     *
     * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
     * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
     * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
     * SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
     * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
     * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
     * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
     * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
     * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY
     * OF SUCH DAMAGE.
     *
     * This file is part of the sofi PLC.
     *
     * Author: Shoma Gane <shomagan@gmail.com>
     *         Ayrat Girfanov <girfanov.ayrat@yandex.ru>
     */
#ifndef POUS_C
#define POUS_C
#include "POUS.h"
#include "config_task.h"
#include "plc_main.h"
void CONDITION_init__(CONDITION *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->LOCALVAR0,0,retain)
  __INIT_VAR(data__->LOCALVAR1,0,retain)
}

// Code part
void CONDITION_body__(CONDITION *data__) {
  // Control execution
  if (!__GET_VAR(data__->EN)) {
    __SET_VAR(data__->,ENO,,__BOOL_LITERAL(FALSE));
    return;
  }
  else {
    __SET_VAR(data__->,ENO,,__BOOL_LITERAL(TRUE));
  }
  // Initialise TEMP variables

  if ((__GET_VAR(data__->LOCALVAR0,) == 0.0)) {
    __SET_VAR(data__->,LOCALVAR1,,0);
  } else {
    __SET_VAR(data__->,LOCALVAR1,,1);
  };

  goto __end;

__end:
  return;
} // CONDITION_body__() 





void CONDITION_1_init__(CONDITION_1 *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->LOCALVAR0,0,retain)
  __INIT_VAR(data__->LOCALVAR1,__BOOL_LITERAL(FALSE),retain)
}

// Code part
void CONDITION_1_body__(CONDITION_1 *data__) {
  // Control execution
  if (!__GET_VAR(data__->EN)) {
    __SET_VAR(data__->,ENO,,__BOOL_LITERAL(FALSE));
    return;
  }
  else {
    __SET_VAR(data__->,ENO,,__BOOL_LITERAL(TRUE));
  }
  // Initialise TEMP variables

  if ((__GET_VAR(data__->LOCALVAR0,) == 1)) {
    __SET_VAR(data__->,LOCALVAR1,,1);
  };

  goto __end;

__end:
  return;
} // CONDITION_1_body__() 





void CONDITION_2_init__(CONDITION_2 *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->LOCALVAR0,0,retain)
  __INIT_VAR(data__->LOCALVAR1,0,retain)
}

// Code part
void CONDITION_2_body__(CONDITION_2 *data__) {
  // Control execution
  if (!__GET_VAR(data__->EN)) {
    __SET_VAR(data__->,ENO,,__BOOL_LITERAL(FALSE));
    return;
  }
  else {
    __SET_VAR(data__->,ENO,,__BOOL_LITERAL(TRUE));
  }
  // Initialise TEMP variables

  if ((__GET_VAR(data__->LOCALVAR0,) == 0)) {
    __SET_VAR(data__->,LOCALVAR1,,1);
  };

  goto __end;

__end:
  return;
} // CONDITION_2_body__() 





void CONDITION_3_init__(CONDITION_3 *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->LOCALVAR0,0,retain)
  __INIT_VAR(data__->LOCALVAR1,0,retain)
}

// Code part
void CONDITION_3_body__(CONDITION_3 *data__) {
  // Control execution
  if (!__GET_VAR(data__->EN)) {
    __SET_VAR(data__->,ENO,,__BOOL_LITERAL(FALSE));
    return;
  }
  else {
    __SET_VAR(data__->,ENO,,__BOOL_LITERAL(TRUE));
  }
  // Initialise TEMP variables

  if ((__GET_VAR(data__->LOCALVAR0,) == 0)) {
    __SET_VAR(data__->,LOCALVAR1,,0);
  } else {
    __SET_VAR(data__->,LOCALVAR1,,1);
  };

  goto __end;

__end:
  return;
} // CONDITION_3_body__() 





void AI_SIGNAL_init__(AI_SIGNAL *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->LOCALVAR0,0,retain)
  __INIT_VAR(data__->LOCALVAR1,0,retain)
}

// Code part
void AI_SIGNAL_body__(AI_SIGNAL *data__) {
  // Control execution
  if (!__GET_VAR(data__->EN)) {
    __SET_VAR(data__->,ENO,,__BOOL_LITERAL(FALSE));
    return;
  }
  else {
    __SET_VAR(data__->,ENO,,__BOOL_LITERAL(TRUE));
  }
  // Initialise TEMP variables

  if ((__GET_VAR(data__->LOCALVAR0,) > 100)) {
    __SET_VAR(data__->,LOCALVAR1,,1);
  } else {
    __SET_VAR(data__->,LOCALVAR1,,0);
  };

  goto __end;

__end:
  return;
} // AI_SIGNAL_body__() 





void DI_SIGNAL_init__(DI_SIGNAL *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->LOCALVAR0,0,retain)
  __INIT_VAR(data__->LOCALVAR1,__BOOL_LITERAL(FALSE),retain)
}

// Code part
void DI_SIGNAL_body__(DI_SIGNAL *data__) {
  // Control execution
  if (!__GET_VAR(data__->EN)) {
    __SET_VAR(data__->,ENO,,__BOOL_LITERAL(FALSE));
    return;
  }
  else {
    __SET_VAR(data__->,ENO,,__BOOL_LITERAL(TRUE));
  }
  // Initialise TEMP variables

  if ((__GET_VAR(data__->LOCALVAR0,) == 11)) {
    __SET_VAR(data__->,LOCALVAR1,,1);
  } else {
    __SET_VAR(data__->,LOCALVAR1,,0);
  };

  goto __end;

__end:
  return;
} // DI_SIGNAL_body__() 





void CONDITION_4_init__(CONDITION_4 *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->LOCALVAR0,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->LOCALVAR1,0,retain)
}

// Code part
void CONDITION_4_body__(CONDITION_4 *data__) {
  // Control execution
  if (!__GET_VAR(data__->EN)) {
    __SET_VAR(data__->,ENO,,__BOOL_LITERAL(FALSE));
    return;
  }
  else {
    __SET_VAR(data__->,ENO,,__BOOL_LITERAL(TRUE));
  }
  // Initialise TEMP variables

  if ((__GET_VAR(data__->LOCALVAR0,) == 1)) {
    __SET_VAR(data__->,LOCALVAR1,,1);
  } else {
    __SET_VAR(data__->,LOCALVAR1,,0);
  };

  goto __end;

__end:
  return;
} // CONDITION_4_body__() 





void DI_CNT_HJ_init__(DI_CNT_HJ *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->LOCALVAR0,0,retain)
  __INIT_VAR(data__->LOCALVAR1,0,retain)
}

// Code part
void DI_CNT_HJ_body__(DI_CNT_HJ *data__) {
  // Control execution
  if (!__GET_VAR(data__->EN)) {
    __SET_VAR(data__->,ENO,,__BOOL_LITERAL(FALSE));
    return;
  }
  else {
    __SET_VAR(data__->,ENO,,__BOOL_LITERAL(TRUE));
  }
  // Initialise TEMP variables

  if ((__GET_VAR(data__->LOCALVAR0,) == 10)) {
    __SET_VAR(data__->,LOCALVAR1,,1);
  } else {
    __SET_VAR(data__->,LOCALVAR1,,0);
  };

  goto __end;

__end:
  return;
} // DI_CNT_HJ_body__() 





void DI_FREQ_init__(DI_FREQ *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->LOCALVAR0,0,retain)
  __INIT_VAR(data__->LOCALVAR1,0,retain)
}

// Code part
void DI_FREQ_body__(DI_FREQ *data__) {
  // Control execution
  if (!__GET_VAR(data__->EN)) {
    __SET_VAR(data__->,ENO,,__BOOL_LITERAL(FALSE));
    return;
  }
  else {
    __SET_VAR(data__->,ENO,,__BOOL_LITERAL(TRUE));
  }
  // Initialise TEMP variables

  if ((__GET_VAR(data__->LOCALVAR0,) == 0.0)) {
    __SET_VAR(data__->,LOCALVAR1,,0);
  } else {
    __SET_VAR(data__->,LOCALVAR1,,1);
  };

  goto __end;

__end:
  return;
} // DI_FREQ_body__() 





void PROGRAM0_init__(PROGRAM0 *data__, BOOL retain) {
  __INIT_EXTERNAL(REAL,BRIC_1_AI_PHYSICAL_0,data__->BRIC_1_AI_PHYSICAL_0,retain)
  __INIT_EXTERNAL(REAL,BRIC_1_AI_PHYSICAL_1,data__->BRIC_1_AI_PHYSICAL_1,retain)
  __INIT_EXTERNAL(REAL,BRIC_1_AI_PHYSICAL0,data__->BRIC_1_AI_PHYSICAL0,retain)
  __INIT_EXTERNAL(REAL,BRIC_1_AI_PHYSICAL1,data__->BRIC_1_AI_PHYSICAL1,retain)
  __INIT_EXTERNAL(UINT,BRIC_1_AI_UNIT_0,data__->BRIC_1_AI_UNIT_0,retain)
  __INIT_EXTERNAL(UINT,BRIC_1_AI_UNIT_1,data__->BRIC_1_AI_UNIT_1,retain)
  __INIT_EXTERNAL(UINT,BRIC_1_AI_UNIT0,data__->BRIC_1_AI_UNIT0,retain)
  __INIT_EXTERNAL(UINT,BRIC_1_AI_UNIT1,data__->BRIC_1_AI_UNIT1,retain)
  __INIT_EXTERNAL(UDINT,BRIC_1_DI_STATE,data__->BRIC_1_DI_STATE,retain)
  __INIT_EXTERNAL(UDINT,BRIC_1_DI_STATE0,data__->BRIC_1_DI_STATE0,retain)
  __INIT_EXTERNAL(USINT,BRIC_1_DO_SC_CTRL,data__->BRIC_1_DO_SC_CTRL,retain)
  __INIT_EXTERNAL(USINT,BRIC_1_DO_SC_CTRL0,data__->BRIC_1_DO_SC_CTRL0,retain)
  __INIT_EXTERNAL(UINT,BRIC_1_DO_CTRL,data__->BRIC_1_DO_CTRL,retain)
  __INIT_EXTERNAL(UINT,BRIC_1_MDB_ADDR,data__->BRIC_1_MDB_ADDR,retain)
  __INIT_EXTERNAL(UINT,BRIC_1_MDB_ADDR0,data__->BRIC_1_MDB_ADDR0,retain)
  __INIT_EXTERNAL(UINT,BRIC_1_MODULE_NUMBER,data__->BRIC_1_MODULE_NUMBER,retain)
  __INIT_EXTERNAL(UINT,BRIC_1_MODULE_NUMBER0,data__->BRIC_1_MODULE_NUMBER0,retain)
  __INIT_EXTERNAL(REAL,BRIC_1_INTERNAL_TEMP,data__->BRIC_1_INTERNAL_TEMP,retain)
  __INIT_EXTERNAL(UINT,DO_1_DO_SC_CTRL,data__->DO_1_DO_SC_CTRL,retain)
  __INIT_EXTERNAL(UINT,DO_1_DO_SC_CTRL0,data__->DO_1_DO_SC_CTRL0,retain)
  __INIT_EXTERNAL(UINT,DO_1_DO_CTRL,data__->DO_1_DO_CTRL,retain)
  __INIT_EXTERNAL(UINT,DO_1_MDB_ADDR,data__->DO_1_MDB_ADDR,retain)
  __INIT_EXTERNAL(UINT,DO_1_MDB_ADDR0,data__->DO_1_MDB_ADDR0,retain)
  __INIT_EXTERNAL(UINT,DO_1_MODULE_NUMBER,data__->DO_1_MODULE_NUMBER,retain)
  __INIT_EXTERNAL(UINT,DO_1_MODULE_NUMBER0,data__->DO_1_MODULE_NUMBER0,retain)
  __INIT_EXTERNAL(REAL,DO_1_INTERNAL_TEMP,data__->DO_1_INTERNAL_TEMP,retain)
  __INIT_EXTERNAL(USINT,DO_1_IP_3,data__->DO_1_IP_3,retain)
  __INIT_EXTERNAL(UINT,AO_1_AO_VAL_0,data__->AO_1_AO_VAL_0,retain)
  __INIT_EXTERNAL(UINT,AO_1_AO_VAL_1,data__->AO_1_AO_VAL_1,retain)
  __INIT_EXTERNAL(UINT,AO_1_AO_VAL_2,data__->AO_1_AO_VAL_2,retain)
  __INIT_EXTERNAL(UINT,AO_1_AO_VAL_3,data__->AO_1_AO_VAL_3,retain)
  __INIT_EXTERNAL(UINT,AO_CH_0,data__->AO_CH_0,retain)
  __INIT_EXTERNAL(UINT,AO_CH_1,data__->AO_CH_1,retain)
  __INIT_EXTERNAL(UINT,AO_CH_2,data__->AO_CH_2,retain)
  __INIT_EXTERNAL(UINT,AO_CH_3,data__->AO_CH_3,retain)
  __INIT_EXTERNAL(UINT,AO_1_MDB_ADDR,data__->AO_1_MDB_ADDR,retain)
  __INIT_EXTERNAL(UINT,AO_1_MDB_ADDR0,data__->AO_1_MDB_ADDR0,retain)
  __INIT_EXTERNAL(UINT,AO_1_MODULE_NUMBER,data__->AO_1_MODULE_NUMBER,retain)
  __INIT_EXTERNAL(UINT,AO_1_MODULE_NUMBER0,data__->AO_1_MODULE_NUMBER0,retain)
  __INIT_EXTERNAL(REAL,AO_1_INTERNAL_TEMP,data__->AO_1_INTERNAL_TEMP,retain)
  __INIT_EXTERNAL(USINT,AO_1_IP_3,data__->AO_1_IP_3,retain)
  __INIT_EXTERNAL(UINT,AI_1_AI_UNIT_0,data__->AI_1_AI_UNIT_0,retain)
  __INIT_EXTERNAL(UINT,AI_1_AI_UNIT_1,data__->AI_1_AI_UNIT_1,retain)
  __INIT_EXTERNAL(UINT,AI_1_AI_UNIT0,data__->AI_1_AI_UNIT0,retain)
  __INIT_EXTERNAL(UINT,AI_1_AI_UNIT1,data__->AI_1_AI_UNIT1,retain)
  __INIT_EXTERNAL(UINT,AI_1_MDB_ADDR,data__->AI_1_MDB_ADDR,retain)
  __INIT_EXTERNAL(UINT,AI_1_MDB_ADDR0,data__->AI_1_MDB_ADDR0,retain)
  __INIT_EXTERNAL(UINT,AI_1_MODULE_NUMBER,data__->AI_1_MODULE_NUMBER,retain)
  __INIT_EXTERNAL(UINT,AI_1_MODULE_NUMBER0,data__->AI_1_MODULE_NUMBER0,retain)
  __INIT_EXTERNAL(REAL,AI_1_INTERNAL_TEMP,data__->AI_1_INTERNAL_TEMP,retain)
  __INIT_EXTERNAL(USINT,AI_1_IP_3,data__->AI_1_IP_3,retain)
  __INIT_EXTERNAL(UDINT,DI_1_DI_STATE,data__->DI_1_DI_STATE,retain)
  __INIT_EXTERNAL(UDINT,DI_1_DI_STATE0,data__->DI_1_DI_STATE0,retain)
  __INIT_EXTERNAL(UINT,DI_1_MDB_ADDR,data__->DI_1_MDB_ADDR,retain)
  __INIT_EXTERNAL(UINT,DI_1_MDB_ADDR0,data__->DI_1_MDB_ADDR0,retain)
  __INIT_EXTERNAL(UINT,DI_1_MODULE_NUMBER,data__->DI_1_MODULE_NUMBER,retain)
  __INIT_EXTERNAL(UINT,DI_1_MODULE_NUMBER0,data__->DI_1_MODULE_NUMBER0,retain)
  __INIT_EXTERNAL(REAL,DI_1_INTERNAL_TEMP,data__->DI_1_INTERNAL_TEMP,retain)
  __INIT_EXTERNAL(USINT,DI_1_IP_3,data__->DI_1_IP_3,retain)
  __INIT_EXTERNAL(UDINT,START_TEST,data__->START_TEST,retain)
  __INIT_VAR(data__->Z_ERO,0,retain)
  __INIT_VAR(data__->Z_ERO0,0,retain)
  __INIT_VAR(data__->Z_ERO1,0,retain)
  __INIT_VAR(data__->ST_TEST,1,retain)
  __INIT_EXTERNAL(UDINT,DO_TEST_W,data__->DO_TEST_W,retain)
  __INIT_EXTERNAL(UDINT,DI_TEST_W,data__->DI_TEST_W,retain)
  __INIT_EXTERNAL(UDINT,AI_TEST_W,data__->AI_TEST_W,retain)
  __INIT_EXTERNAL(UDINT,AO_TEST_W,data__->AO_TEST_W,retain)
  __INIT_EXTERNAL(UINT,DO_CH,data__->DO_CH,retain)
  __INIT_EXTERNAL(UINT,DO_PLC_CH,data__->DO_PLC_CH,retain)
  READ_RESET_init__(&data__->READ_RESET0,retain);
  READ_PWR_init__(&data__->READ_PWR0,retain);
  READ_TEMP_init__(&data__->READ_TEMP0,retain);
  WRITE_MDB_ADDRESS_init__(&data__->WRITE_MDB_ADDRESS0,retain);
  WRITE_DO_PWM_FREQ_init__(&data__->WRITE_DO_PWM_FREQ0,retain);
  WRITE_DO_init__(&data__->WRITE_DO0,retain);
  READ_DI_init__(&data__->READ_DI0,retain);
  READ_DI_CNT_init__(&data__->READ_DI_CNT0,retain);
  READ_DI_FREQ_init__(&data__->READ_DI_FREQ0,retain);
  READ_DO_SC_init__(&data__->READ_DO_SC0,retain);
  READ_DO_init__(&data__->READ_DO0,retain);
  WRITE_DO_SC_init__(&data__->WRITE_DO_SC0,retain);
  READ_PARAM_UINT_init__(&data__->READ_PARAM_UINT0,retain);
  __INIT_VAR(data__->B_1,1,retain)
  __INIT_VAR(data__->B_2,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->NUN_ADR_MOD,0,retain)
  __INIT_EXTERNAL(UINT,ADR_MOD,data__->ADR_MOD,retain)
  __INIT_EXTERNAL(UINT,RES_N,data__->RES_N,retain)
  __INIT_EXTERNAL(UINT,LAS_RES,data__->LAS_RES,retain)
  __INIT_EXTERNAL(UINT,ADR_M,data__->ADR_M,retain)
  __INIT_EXTERNAL(UINT,DI_N,data__->DI_N,retain)
  __INIT_EXTERNAL(ULINT,DI_V,data__->DI_V,retain)
  __INIT_EXTERNAL(REAL,DI_F,data__->DI_F,retain)
  __INIT_EXTERNAL(UDINT,DI_O,data__->DI_O,retain)
  __INIT_EXTERNAL(USINT,DO_O,data__->DO_O,retain)
  __INIT_EXTERNAL(UINT,DO_F,data__->DO_F,retain)
  __INIT_VAR(data__->R_DO_SC_F,0,retain)
  __INIT_EXTERNAL(UINT,W_DO_SC_F,data__->W_DO_SC_F,retain)
  __INIT_VAR(data__->R_DO_SC_E,0,retain)
  __INIT_EXTERNAL(UINT,W_DO_SC_E,data__->W_DO_SC_E,retain)
  __INIT_EXTERNAL(UINT,DO_W,data__->DO_W,retain)
  CONDITION_init__(&data__->CONDITION1,retain);
  CONDITION_init__(&data__->CONDITION2,retain);
  CONDITION_1_init__(&data__->CONDITION_10,retain);
  CONDITION_2_init__(&data__->CONDITION_20,retain);
  __INIT_EXTERNAL(ULINT,TEST_RESULT_MODULE,data__->TEST_RESULT_MODULE,retain)
  __INIT_EXTERNAL(ULINT,TEST_RESULT_MODULE_2,data__->TEST_RESULT_MODULE_2,retain)
  __INIT_EXTERNAL(ULINT,TEST_RESULT_PLC,data__->TEST_RESULT_PLC,retain)
  CONDITION_3_init__(&data__->CONDITION_30,retain);
  SR_init__(&data__->SR0,retain);
  TON_init__(&data__->TON0,retain);
  __INIT_VAR(data__->LOCALVAR2,1,retain)
  __INIT_VAR(data__->LOCALVAR4,70.0,retain)
  __INIT_VAR(data__->LOCALVAR6,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->LOCALVAR5,0,retain)
  CONDITION_3_init__(&data__->CONDITION_31,retain);
  CONDITION_3_init__(&data__->CONDITION_32,retain);
  __INIT_EXTERNAL(BOOL,TEST_2,data__->TEST_2,retain)
  __INIT_EXTERNAL(BOOL,TEST_3,data__->TEST_3,retain)
  AI_SIGNAL_init__(&data__->AI_SIGNAL0,retain);
  AI_SIGNAL_init__(&data__->AI_SIGNAL1,retain);
  DI_SIGNAL_init__(&data__->DI_SIGNAL0,retain);
  CONDITION_4_init__(&data__->CONDITION_40,retain);
  DI_SIGNAL_init__(&data__->DI_SIGNAL1,retain);
  AI_SIGNAL_init__(&data__->AI_SIGNAL2,retain);
  AI_SIGNAL_init__(&data__->AI_SIGNAL3,retain);
  CONDITION_4_init__(&data__->CONDITION_41,retain);
  SR_init__(&data__->SR1,retain);
  SR_init__(&data__->SR2,retain);
  DI_SIGNAL_init__(&data__->DI_SIGNAL2,retain);
  CONDITION_4_init__(&data__->CONDITION_42,retain);
  SR_init__(&data__->SR3,retain);
  CONDITION_2_init__(&data__->CONDITION_21,retain);
  DI_CNT_HJ_init__(&data__->DI_CNT_HJ0,retain);
  DI_FREQ_init__(&data__->DI_FREQ0,retain);
  __INIT_VAR(data__->SEL69_OUT,0,retain)
  __INIT_VAR(data__->SEL226_OUT,0,retain)
  __INIT_VAR(data__->SEL93_OUT,0,retain)
  __INIT_VAR(data__->SEL13_OUT,0,retain)
  __INIT_VAR(data__->SEL57_OUT,0,retain)
  __INIT_VAR(data__->SUB163_OUT,0,retain)
  __INIT_VAR(data__->SUB161_OUT,0,retain)
  __INIT_VAR(data__->SUB165_OUT,0,retain)
  __INIT_VAR(data__->SUB167_OUT,0,retain)
  __INIT_VAR(data__->ADD168_OUT,0,retain)
  __INIT_VAR(data__->MUL76_OUT,0,retain)
  __INIT_VAR(data__->MUL174_OUT,0,retain)
  __INIT_VAR(data__->MUL155_OUT,0,retain)
  __INIT_VAR(data__->MUL188_OUT,0,retain)
  __INIT_VAR(data__->ADD171_OUT,0,retain)
  __INIT_VAR(data__->SEL227_OUT,0,retain)
  __INIT_VAR(data__->SEL5_OUT,0,retain)
  __INIT_VAR(data__->SEL26_OUT,0,retain)
  __INIT_VAR(data__->SEL149_OUT,0,retain)
  __INIT_VAR(data__->SEL28_OUT,0,retain)
  __INIT_VAR(data__->SEL30_OUT,0,retain)
  __INIT_VAR(data__->MUL35_OUT,0,retain)
  __INIT_VAR(data__->MUL212_OUT,0,retain)
  __INIT_VAR(data__->MUL192_OUT,0,retain)
  __INIT_VAR(data__->MUL220_OUT,0,retain)
  __INIT_VAR(data__->MUL224_OUT,0,retain)
  __INIT_VAR(data__->MUL276_OUT,0,retain)
  __INIT_VAR(data__->SUB286_OUT,0,retain)
  __INIT_VAR(data__->SUB119_OUT,0,retain)
  __INIT_VAR(data__->ADD289_OUT,0,retain)
  __INIT_VAR(data__->MUL292_OUT,0,retain)
  __INIT_VAR(data__->MUL297_OUT,0,retain)
  __INIT_VAR(data__->MUL302_OUT,0,retain)
  __INIT_VAR(data__->BOOL_TO_ULINT202_OUT,0,retain)
  __INIT_VAR(data__->MUL204_OUT,0,retain)
  __INIT_VAR(data__->ADD209_OUT,0,retain)
  __INIT_VAR(data__->SEL92_OUT,0,retain)
  __INIT_VAR(data__->REAL_TO_TIME196_OUT,__time_to_timespec(1, 0, 0, 0, 0, 0),retain)
  __INIT_VAR(data__->SEL4_OUT,0,retain)
  __INIT_VAR(data__->MUL241_OUT,0,retain)
  __INIT_VAR(data__->MUL246_OUT,0,retain)
  __INIT_VAR(data__->MUL259_OUT,0,retain)
  __INIT_VAR(data__->MUL249_OUT,0,retain)
  __INIT_VAR(data__->MUL252_OUT,0,retain)
  __INIT_VAR(data__->ADD244_OUT,0,retain)
  __INIT_VAR(data__->SEL146_OUT,0,retain)
  __INIT_VAR(data__->SEL147_OUT,0,retain)
  __INIT_VAR(data__->SEL39_OUT,0,retain)
  __INIT_VAR(data__->SEL278_OUT,0,retain)
  __INIT_VAR(data__->SEL282_OUT,0,retain)
}

// Code part
void PROGRAM0_body__(PROGRAM0 *data__) {
  // Initialise TEMP variables

  __SET_EXTERNAL(data__->,BRIC_1_AI_PHYSICAL0,,__GET_EXTERNAL(data__->BRIC_1_AI_PHYSICAL_0,));
  __SET_EXTERNAL(data__->,DO_1_MDB_ADDR,,__GET_EXTERNAL(data__->DO_1_MDB_ADDR0,));
  __SET_EXTERNAL(data__->,AO_1_MDB_ADDR,,__GET_EXTERNAL(data__->AO_1_MDB_ADDR0,));
  __SET_EXTERNAL(data__->,BRIC_1_AI_PHYSICAL1,,__GET_EXTERNAL(data__->BRIC_1_AI_PHYSICAL_1,));
  __SET_EXTERNAL(data__->,DO_1_MODULE_NUMBER0,,__GET_EXTERNAL(data__->DO_1_MODULE_NUMBER,));
  __SET_EXTERNAL(data__->,AO_1_MODULE_NUMBER0,,__GET_EXTERNAL(data__->AO_1_MODULE_NUMBER,));
  __SET_EXTERNAL(data__->,DI_1_MDB_ADDR,,__GET_EXTERNAL(data__->DI_1_MDB_ADDR0,));
  __SET_VAR(data__->READ_DI_CNT0.,DI_NUMBER,,__GET_EXTERNAL(data__->DI_N,));
  READ_DI_CNT_body__(&data__->READ_DI_CNT0);
  __SET_EXTERNAL(data__->,DI_V,,__GET_VAR(data__->READ_DI_CNT0.DI_CNT_VALUE,));
  __SET_EXTERNAL(data__->,AI_1_MDB_ADDR,,__GET_EXTERNAL(data__->AI_1_MDB_ADDR0,));
  __SET_EXTERNAL(data__->,DI_1_MODULE_NUMBER0,,__GET_EXTERNAL(data__->DI_1_MODULE_NUMBER,));
  __SET_VAR(data__->CONDITION_10.,LOCALVAR0,,__GET_EXTERNAL(data__->START_TEST,));
  CONDITION_1_body__(&data__->CONDITION_10);
  __SET_VAR(data__->,SEL69_OUT,,SEL__UDINT__BOOL__UDINT__UDINT(
    (BOOL)__BOOL_LITERAL(TRUE),
    NULL,
    (BOOL)__GET_VAR(data__->CONDITION_10.LOCALVAR1,),
    (UDINT)__GET_VAR(data__->Z_ERO,),
    (UDINT)__GET_VAR(data__->ST_TEST,)));
  __SET_EXTERNAL(data__->,DO_TEST_W,,__GET_VAR(data__->SEL69_OUT,));
  __SET_EXTERNAL(data__->,START_TEST,,__GET_EXTERNAL(data__->DO_TEST_W,));
  __SET_VAR(data__->READ_DI_FREQ0.,DI_NUMBER,,__GET_EXTERNAL(data__->DI_N,));
  READ_DI_FREQ_body__(&data__->READ_DI_FREQ0);
  __SET_EXTERNAL(data__->,DI_F,,__GET_VAR(data__->READ_DI_FREQ0.DI_FREQ_VALUE,));
  __SET_VAR(data__->,SEL226_OUT,,SEL__UINT__BOOL__UINT__UINT(
    (BOOL)__BOOL_LITERAL(TRUE),
    NULL,
    (BOOL)__GET_EXTERNAL(data__->TEST_2,),
    (UINT)0,
    (UINT)__GET_EXTERNAL(data__->BRIC_1_AI_UNIT_0,)));
  __SET_EXTERNAL(data__->,BRIC_1_AI_UNIT0,,__GET_VAR(data__->SEL226_OUT,));
  __SET_EXTERNAL(data__->,AI_1_MODULE_NUMBER0,,__GET_EXTERNAL(data__->AI_1_MODULE_NUMBER,));
  __SET_EXTERNAL(data__->,DI_1_DI_STATE0,,__GET_EXTERNAL(data__->DI_1_DI_STATE,));
  __SET_EXTERNAL(data__->,DI_TEST_W,,__GET_VAR(data__->SEL69_OUT,));
  READ_RESET_body__(&data__->READ_RESET0);
  __SET_EXTERNAL(data__->,RES_N,,__GET_VAR(data__->READ_RESET0.RESET_NUM,));
  __SET_VAR(data__->,SEL93_OUT,,SEL__UINT__BOOL__UINT__UINT(
    (BOOL)__BOOL_LITERAL(TRUE),
    NULL,
    (BOOL)__GET_EXTERNAL(data__->TEST_2,),
    (UINT)0,
    (UINT)__GET_EXTERNAL(data__->DO_CH,)));
  __SET_EXTERNAL(data__->,DO_1_DO_CTRL,,__GET_VAR(data__->SEL93_OUT,));
  __SET_VAR(data__->,SEL13_OUT,,SEL__UINT__BOOL__UINT__UINT(
    (BOOL)__BOOL_LITERAL(TRUE),
    NULL,
    (BOOL)__GET_EXTERNAL(data__->TEST_2,),
    (UINT)0,
    (UINT)__GET_EXTERNAL(data__->AO_CH_0,)));
  __SET_EXTERNAL(data__->,AO_1_AO_VAL_0,,__GET_VAR(data__->SEL13_OUT,));
  __SET_EXTERNAL(data__->,AI_TEST_W,,__GET_VAR(data__->SEL69_OUT,));
  __SET_EXTERNAL(data__->,LAS_RES,,__GET_VAR(data__->READ_RESET0.LAST_RESET,));
  __SET_VAR(data__->,SEL57_OUT,,SEL__UINT__BOOL__UINT__UINT(
    (BOOL)__BOOL_LITERAL(TRUE),
    NULL,
    (BOOL)__GET_EXTERNAL(data__->TEST_2,),
    (UINT)0,
    (UINT)__GET_EXTERNAL(data__->AI_1_AI_UNIT_0,)));
  __SET_EXTERNAL(data__->,AI_1_AI_UNIT0,,__GET_VAR(data__->SEL57_OUT,));
  __SET_VAR(data__->,SUB163_OUT,,SUB__USINT__USINT__USINT(
    (BOOL)__BOOL_LITERAL(TRUE),
    NULL,
    (USINT)__GET_EXTERNAL(data__->DO_1_IP_3,),
    (USINT)7));
  __SET_VAR(data__->,SUB161_OUT,,SUB__USINT__USINT__USINT(
    (BOOL)__BOOL_LITERAL(TRUE),
    NULL,
    (USINT)__GET_EXTERNAL(data__->AO_1_IP_3,),
    (USINT)7));
  __SET_VAR(data__->,SUB165_OUT,,SUB__USINT__USINT__USINT(
    (BOOL)__BOOL_LITERAL(TRUE),
    NULL,
    (USINT)__GET_EXTERNAL(data__->DI_1_IP_3,),
    (USINT)7));
  __SET_VAR(data__->,SUB167_OUT,,SUB__USINT__USINT__USINT(
    (BOOL)__BOOL_LITERAL(TRUE),
    NULL,
    (USINT)__GET_EXTERNAL(data__->AI_1_IP_3,),
    (USINT)7));
  __SET_VAR(data__->,ADD168_OUT,,ADD__USINT__USINT(
    (BOOL)__BOOL_LITERAL(TRUE),
    NULL,
    (UINT)4,
    (USINT)__GET_VAR(data__->SUB163_OUT,),
    (USINT)__GET_VAR(data__->SUB161_OUT,),
    (USINT)__GET_VAR(data__->SUB165_OUT,),
    (USINT)__GET_VAR(data__->SUB167_OUT,)));
  __SET_VAR(data__->CONDITION_20.,LOCALVAR0,,__GET_VAR(data__->ADD168_OUT,));
  CONDITION_2_body__(&data__->CONDITION_20);
  __SET_VAR(data__->,MUL76_OUT,,MUL__REAL__REAL(
    (BOOL)__BOOL_LITERAL(TRUE),
    NULL,
    (UINT)5,
    (REAL)__GET_EXTERNAL(data__->BRIC_1_INTERNAL_TEMP,),
    (REAL)__GET_EXTERNAL(data__->DO_1_INTERNAL_TEMP,),
    (REAL)__GET_EXTERNAL(data__->AO_1_INTERNAL_TEMP,),
    (REAL)__GET_EXTERNAL(data__->AI_1_INTERNAL_TEMP,),
    (REAL)__GET_EXTERNAL(data__->DI_1_INTERNAL_TEMP,)));
  __SET_VAR(data__->CONDITION1.,LOCALVAR0,,__GET_VAR(data__->MUL76_OUT,));
  CONDITION_body__(&data__->CONDITION1);
  __SET_VAR(data__->,MUL174_OUT,,MUL__ULINT__ULINT(
    (BOOL)__BOOL_LITERAL(TRUE),
    NULL,
    (UINT)2,
    (ULINT)__GET_VAR(data__->CONDITION1.LOCALVAR1,),
    (ULINT)2));
  __SET_VAR(data__->,MUL155_OUT,,MUL__UINT__UINT(
    (BOOL)__BOOL_LITERAL(TRUE),
    NULL,
    (UINT)5,
    (UINT)__GET_EXTERNAL(data__->BRIC_1_MODULE_NUMBER0,),
    (UINT)__GET_EXTERNAL(data__->DO_1_MODULE_NUMBER0,),
    (UINT)__GET_EXTERNAL(data__->AO_1_MODULE_NUMBER0,),
    (UINT)__GET_EXTERNAL(data__->AI_1_MODULE_NUMBER0,),
    (UINT)__GET_EXTERNAL(data__->DI_1_MODULE_NUMBER0,)));
  __SET_VAR(data__->CONDITION_30.,LOCALVAR0,,__GET_VAR(data__->MUL155_OUT,));
  CONDITION_3_body__(&data__->CONDITION_30);
  __SET_VAR(data__->,MUL188_OUT,,MUL__ULINT__ULINT(
    (BOOL)__BOOL_LITERAL(TRUE),
    NULL,
    (UINT)2,
    (ULINT)__GET_VAR(data__->CONDITION_30.LOCALVAR1,),
    (ULINT)4));
  __SET_VAR(data__->,ADD171_OUT,,ADD__ULINT__ULINT(
    (BOOL)__BOOL_LITERAL(TRUE),
    NULL,
    (UINT)3,
    (ULINT)__GET_VAR(data__->CONDITION_20.LOCALVAR1,),
    (ULINT)__GET_VAR(data__->MUL174_OUT,),
    (ULINT)__GET_VAR(data__->MUL188_OUT,)));
  __SET_EXTERNAL(data__->,TEST_RESULT_MODULE,,__GET_VAR(data__->ADD171_OUT,));
  __SET_EXTERNAL(data__->,AO_TEST_W,,__GET_VAR(data__->SEL69_OUT,));
  __SET_VAR(data__->,SEL227_OUT,,SEL__UINT__BOOL__UINT__UINT(
    (BOOL)__BOOL_LITERAL(TRUE),
    NULL,
    (BOOL)__GET_EXTERNAL(data__->TEST_2,),
    (UINT)0,
    (UINT)__GET_EXTERNAL(data__->BRIC_1_AI_UNIT_1,)));
  __SET_EXTERNAL(data__->,BRIC_1_AI_UNIT1,,__GET_VAR(data__->SEL227_OUT,));
  __SET_VAR(data__->,SEL5_OUT,,SEL__UINT__BOOL__UINT__UINT(
    (BOOL)__BOOL_LITERAL(TRUE),
    NULL,
    (BOOL)__GET_EXTERNAL(data__->TEST_3,),
    (UINT)0,
    (UINT)__GET_EXTERNAL(data__->DO_1_DO_SC_CTRL0,)));
  __SET_EXTERNAL(data__->,DO_1_DO_SC_CTRL,,__GET_VAR(data__->SEL5_OUT,));
  __SET_VAR(data__->,SEL26_OUT,,SEL__UINT__BOOL__UINT__UINT(
    (BOOL)__BOOL_LITERAL(TRUE),
    NULL,
    (BOOL)__GET_EXTERNAL(data__->TEST_2,),
    (UINT)0,
    (UINT)__GET_EXTERNAL(data__->AO_CH_1,)));
  __SET_EXTERNAL(data__->,AO_1_AO_VAL_1,,__GET_VAR(data__->SEL26_OUT,));
  __SET_VAR(data__->,SEL149_OUT,,SEL__UINT__BOOL__UINT__UINT(
    (BOOL)__BOOL_LITERAL(TRUE),
    NULL,
    (BOOL)__GET_EXTERNAL(data__->TEST_2,),
    (UINT)0,
    (UINT)__GET_EXTERNAL(data__->AI_1_AI_UNIT_1,)));
  __SET_EXTERNAL(data__->,AI_1_AI_UNIT1,,__GET_VAR(data__->SEL149_OUT,));
  __SET_VAR(data__->,SEL28_OUT,,SEL__UINT__BOOL__UINT__UINT(
    (BOOL)__BOOL_LITERAL(TRUE),
    NULL,
    (BOOL)__GET_EXTERNAL(data__->TEST_2,),
    (UINT)0,
    (UINT)__GET_EXTERNAL(data__->AO_CH_2,)));
  __SET_EXTERNAL(data__->,AO_1_AO_VAL_2,,__GET_VAR(data__->SEL28_OUT,));
  __SET_EXTERNAL(data__->,BRIC_1_DI_STATE0,,__GET_EXTERNAL(data__->BRIC_1_DI_STATE,));
  __SET_VAR(data__->,SEL30_OUT,,SEL__UINT__BOOL__UINT__UINT(
    (BOOL)__BOOL_LITERAL(TRUE),
    NULL,
    (BOOL)__GET_EXTERNAL(data__->TEST_2,),
    (UINT)0,
    (UINT)__GET_EXTERNAL(data__->AO_CH_3,)));
  __SET_EXTERNAL(data__->,AO_1_AO_VAL_3,,__GET_VAR(data__->SEL30_OUT,));
  READ_DI_body__(&data__->READ_DI0);
  __SET_EXTERNAL(data__->,DI_O,,__GET_VAR(data__->READ_DI0.DI_OUT,));
  __SET_EXTERNAL(data__->,DI_1_DI_STATE0,,__GET_EXTERNAL(data__->DI_O,));
  __SET_EXTERNAL(data__->,BRIC_1_MDB_ADDR,,__GET_EXTERNAL(data__->BRIC_1_MDB_ADDR0,));
  __SET_VAR(data__->READ_PARAM_UINT0.,ENABLE,,__GET_VAR(data__->B_1,));
  __SET_VAR(data__->READ_PARAM_UINT0.,ADDRESS,,__GET_VAR(data__->NUN_ADR_MOD,));
  READ_PARAM_UINT_body__(&data__->READ_PARAM_UINT0);
  __SET_VAR(data__->,B_2,,__GET_VAR(data__->READ_PARAM_UINT0.CHECK,));
  __SET_EXTERNAL(data__->,BRIC_1_MODULE_NUMBER0,,__GET_EXTERNAL(data__->BRIC_1_MODULE_NUMBER,));
  __SET_EXTERNAL(data__->,ADR_MOD,,__GET_VAR(data__->READ_PARAM_UINT0.VALUE,));
  READ_PWR_body__(&data__->READ_PWR0);
  READ_TEMP_body__(&data__->READ_TEMP0);
  __SET_VAR(data__->,MUL35_OUT,,MUL__REAL__REAL(
    (BOOL)__BOOL_LITERAL(TRUE),
    NULL,
    (UINT)4,
    (REAL)__GET_VAR(data__->READ_PWR0.V_PWR,),
    (REAL)__GET_VAR(data__->READ_PWR0.V_BAT,),
    (REAL)__GET_VAR(data__->READ_TEMP0.MCU_TEMP,),
    (REAL)__GET_VAR(data__->READ_TEMP0.ADC_TEMP,)));
  __SET_VAR(data__->CONDITION2.,LOCALVAR0,,__GET_VAR(data__->MUL35_OUT,));
  CONDITION_body__(&data__->CONDITION2);
  __SET_VAR(data__->,MUL212_OUT,,MUL__UINT__UINT(
    (BOOL)__BOOL_LITERAL(TRUE),
    NULL,
    (UINT)2,
    (UINT)__GET_EXTERNAL(data__->RES_N,),
    (UINT)__GET_EXTERNAL(data__->LAS_RES,)));
  __SET_VAR(data__->CONDITION_31.,LOCALVAR0,,__GET_VAR(data__->MUL212_OUT,));
  CONDITION_3_body__(&data__->CONDITION_31);
  __SET_VAR(data__->,MUL192_OUT,,MUL__ULINT__ULINT(
    (BOOL)__BOOL_LITERAL(TRUE),
    NULL,
    (UINT)2,
    (ULINT)__GET_VAR(data__->CONDITION_31.LOCALVAR1,),
    (ULINT)2));
  __SET_VAR(data__->,MUL220_OUT,,MUL__UINT__UINT(
    (BOOL)__BOOL_LITERAL(TRUE),
    NULL,
    (UINT)2,
    (UINT)__GET_EXTERNAL(data__->ADR_M,),
    (UINT)__GET_EXTERNAL(data__->ADR_MOD,)));
  __SET_VAR(data__->CONDITION_32.,LOCALVAR0,,__GET_VAR(data__->MUL220_OUT,));
  CONDITION_3_body__(&data__->CONDITION_32);
  __SET_VAR(data__->,MUL224_OUT,,MUL__ULINT__ULINT(
    (BOOL)__BOOL_LITERAL(TRUE),
    NULL,
    (UINT)2,
    (ULINT)__GET_VAR(data__->CONDITION_32.LOCALVAR1,),
    (ULINT)4));
  __SET_VAR(data__->DI_SIGNAL2.,LOCALVAR0,,__GET_EXTERNAL(data__->DI_1_DI_STATE0,));
  DI_SIGNAL_body__(&data__->DI_SIGNAL2);
  __SET_VAR(data__->SR3.,S1,,__GET_VAR(data__->DI_SIGNAL2.LOCALVAR1,));
  __SET_VAR(data__->SR3.,R,,0);
  SR_body__(&data__->SR3);
  __SET_VAR(data__->CONDITION_42.,LOCALVAR0,,__GET_VAR(data__->SR3.Q1,));
  CONDITION_4_body__(&data__->CONDITION_42);
  __SET_VAR(data__->,MUL276_OUT,,MUL__ULINT__ULINT(
    (BOOL)__BOOL_LITERAL(TRUE),
    NULL,
    (UINT)2,
    (ULINT)__GET_VAR(data__->CONDITION_42.LOCALVAR1,),
    (ULINT)8));
  __SET_VAR(data__->,SUB286_OUT,,SUB__USINT__USINT__USINT(
    (BOOL)__BOOL_LITERAL(TRUE),
    NULL,
    (USINT)__GET_VAR(data__->R_DO_SC_F,),
    (USINT)8));
  __SET_VAR(data__->,SUB119_OUT,,SUB__USINT__USINT__USINT(
    (BOOL)__BOOL_LITERAL(TRUE),
    NULL,
    (USINT)__GET_VAR(data__->R_DO_SC_E,),
    (USINT)6));
  __SET_VAR(data__->,ADD289_OUT,,ADD__USINT__USINT(
    (BOOL)__BOOL_LITERAL(TRUE),
    NULL,
    (UINT)2,
    (USINT)__GET_VAR(data__->SUB286_OUT,),
    (USINT)__GET_VAR(data__->SUB119_OUT,)));
  __SET_VAR(data__->CONDITION_21.,LOCALVAR0,,__GET_VAR(data__->ADD289_OUT,));
  CONDITION_2_body__(&data__->CONDITION_21);
  __SET_VAR(data__->,MUL292_OUT,,MUL__ULINT__ULINT(
    (BOOL)__BOOL_LITERAL(TRUE),
    NULL,
    (UINT)2,
    (ULINT)__GET_VAR(data__->CONDITION_21.LOCALVAR1,),
    (ULINT)16));
  __SET_VAR(data__->DI_CNT_HJ0.,LOCALVAR0,,__GET_EXTERNAL(data__->DI_V,));
  DI_CNT_HJ_body__(&data__->DI_CNT_HJ0);
  __SET_VAR(data__->,MUL297_OUT,,MUL__ULINT__ULINT(
    (BOOL)__BOOL_LITERAL(TRUE),
    NULL,
    (UINT)2,
    (ULINT)__GET_VAR(data__->DI_CNT_HJ0.LOCALVAR1,),
    (ULINT)32));
  __SET_VAR(data__->DI_FREQ0.,LOCALVAR0,,__GET_EXTERNAL(data__->DI_F,));
  DI_FREQ_body__(&data__->DI_FREQ0);
  __SET_VAR(data__->,MUL302_OUT,,MUL__ULINT__ULINT(
    (BOOL)__BOOL_LITERAL(TRUE),
    NULL,
    (UINT)2,
    (ULINT)__GET_VAR(data__->DI_FREQ0.LOCALVAR1,),
    (ULINT)64));
  __SET_VAR(data__->,BOOL_TO_ULINT202_OUT,,BOOL_TO_ULINT(
    (BOOL)__BOOL_LITERAL(TRUE),
    NULL,
    (BOOL)__GET_VAR(data__->LOCALVAR6,)));
  __SET_VAR(data__->,MUL204_OUT,,MUL__ULINT__ULINT(
    (BOOL)__BOOL_LITERAL(TRUE),
    NULL,
    (UINT)2,
    (ULINT)__GET_VAR(data__->BOOL_TO_ULINT202_OUT,),
    (ULINT)8));
  __SET_VAR(data__->,ADD209_OUT,,ADD__ULINT__ULINT(
    (BOOL)__BOOL_LITERAL(TRUE),
    NULL,
    (UINT)8,
    (ULINT)__GET_VAR(data__->CONDITION2.LOCALVAR1,),
    (ULINT)__GET_VAR(data__->MUL192_OUT,),
    (ULINT)__GET_VAR(data__->MUL224_OUT,),
    (ULINT)__GET_VAR(data__->MUL276_OUT,),
    (ULINT)__GET_VAR(data__->MUL292_OUT,),
    (ULINT)__GET_VAR(data__->MUL297_OUT,),
    (ULINT)__GET_VAR(data__->MUL302_OUT,),
    (ULINT)__GET_VAR(data__->MUL204_OUT,)));
  __SET_EXTERNAL(data__->,TEST_RESULT_PLC,,__GET_VAR(data__->ADD209_OUT,));
  __SET_VAR(data__->,SEL92_OUT,,SEL__UINT__BOOL__UINT__UINT(
    (BOOL)__BOOL_LITERAL(TRUE),
    NULL,
    (BOOL)__GET_EXTERNAL(data__->TEST_2,),
    (UINT)0,
    (UINT)__GET_EXTERNAL(data__->DO_PLC_CH,)));
  __SET_EXTERNAL(data__->,BRIC_1_DO_CTRL,,__GET_VAR(data__->SEL92_OUT,));
  __SET_VAR(data__->,REAL_TO_TIME196_OUT,,REAL_TO_TIME(
    (BOOL)__BOOL_LITERAL(TRUE),
    NULL,
    (REAL)__GET_VAR(data__->LOCALVAR4,)));
  __SET_VAR(data__->TON0.,IN,,__GET_VAR(data__->LOCALVAR2,));
  __SET_VAR(data__->TON0.,PT,,__GET_VAR(data__->REAL_TO_TIME196_OUT,));
  TON_body__(&data__->TON0);
  __SET_VAR(data__->SR0.,S1,,__GET_VAR(data__->TON0.Q,));
  __SET_VAR(data__->SR0.,R,,__GET_VAR(data__->LOCALVAR5,));
  SR_body__(&data__->SR0);
  __SET_VAR(data__->,LOCALVAR6,,__GET_VAR(data__->SR0.Q1,));
  __SET_VAR(data__->,SEL4_OUT,,SEL__USINT__BOOL__USINT__USINT(
    (BOOL)__BOOL_LITERAL(TRUE),
    NULL,
    (BOOL)__GET_EXTERNAL(data__->TEST_3,),
    (USINT)0,
    (USINT)__GET_EXTERNAL(data__->BRIC_1_DO_SC_CTRL,)));
  __SET_EXTERNAL(data__->,BRIC_1_DO_SC_CTRL0,,__GET_VAR(data__->SEL4_OUT,));
  READ_DO_SC_body__(&data__->READ_DO_SC0);
  __SET_VAR(data__->,R_DO_SC_F,,__GET_VAR(data__->READ_DO_SC0.DO_SC_FLAG,));
  __SET_VAR(data__->,R_DO_SC_E,,__GET_VAR(data__->READ_DO_SC0.DO_SC_EN,));
  READ_DO_body__(&data__->READ_DO0);
  __SET_EXTERNAL(data__->,DO_O,,__GET_VAR(data__->READ_DO0.DO_OUT,));
  __SET_VAR(data__->AI_SIGNAL0.,LOCALVAR0,,__GET_EXTERNAL(data__->BRIC_1_AI_UNIT0,));
  AI_SIGNAL_body__(&data__->AI_SIGNAL0);
  __SET_VAR(data__->AI_SIGNAL1.,LOCALVAR0,,__GET_EXTERNAL(data__->BRIC_1_AI_UNIT1,));
  AI_SIGNAL_body__(&data__->AI_SIGNAL1);
  __SET_VAR(data__->,MUL241_OUT,,MUL__ULINT__ULINT(
    (BOOL)__BOOL_LITERAL(TRUE),
    NULL,
    (UINT)2,
    (ULINT)__GET_VAR(data__->AI_SIGNAL0.LOCALVAR1,),
    (ULINT)__GET_VAR(data__->AI_SIGNAL1.LOCALVAR1,)));
  __SET_VAR(data__->DI_SIGNAL0.,LOCALVAR0,,__GET_EXTERNAL(data__->BRIC_1_DI_STATE0,));
  DI_SIGNAL_body__(&data__->DI_SIGNAL0);
  __SET_VAR(data__->SR1.,S1,,__GET_VAR(data__->DI_SIGNAL0.LOCALVAR1,));
  __SET_VAR(data__->SR1.,R,,0);
  SR_body__(&data__->SR1);
  __SET_VAR(data__->CONDITION_40.,LOCALVAR0,,__GET_VAR(data__->SR1.Q1,));
  CONDITION_4_body__(&data__->CONDITION_40);
  __SET_VAR(data__->,MUL246_OUT,,MUL__ULINT__ULINT(
    (BOOL)__BOOL_LITERAL(TRUE),
    NULL,
    (UINT)2,
    (ULINT)__GET_VAR(data__->CONDITION_40.LOCALVAR1,),
    (ULINT)2));
  __SET_VAR(data__->AI_SIGNAL2.,LOCALVAR0,,__GET_EXTERNAL(data__->AI_1_AI_UNIT0,));
  AI_SIGNAL_body__(&data__->AI_SIGNAL2);
  __SET_VAR(data__->AI_SIGNAL3.,LOCALVAR0,,__GET_EXTERNAL(data__->AI_1_AI_UNIT1,));
  AI_SIGNAL_body__(&data__->AI_SIGNAL3);
  __SET_VAR(data__->,MUL259_OUT,,MUL__ULINT__ULINT(
    (BOOL)__BOOL_LITERAL(TRUE),
    NULL,
    (UINT)2,
    (ULINT)__GET_VAR(data__->AI_SIGNAL2.LOCALVAR1,),
    (ULINT)__GET_VAR(data__->AI_SIGNAL3.LOCALVAR1,)));
  __SET_VAR(data__->,MUL249_OUT,,MUL__ULINT__ULINT(
    (BOOL)__BOOL_LITERAL(TRUE),
    NULL,
    (UINT)2,
    (ULINT)__GET_VAR(data__->MUL259_OUT,),
    (ULINT)4));
  __SET_VAR(data__->DI_SIGNAL1.,LOCALVAR0,,__GET_EXTERNAL(data__->DI_1_DI_STATE0,));
  DI_SIGNAL_body__(&data__->DI_SIGNAL1);
  __SET_VAR(data__->SR2.,S1,,__GET_VAR(data__->DI_SIGNAL1.LOCALVAR1,));
  __SET_VAR(data__->SR2.,R,,0);
  SR_body__(&data__->SR2);
  __SET_VAR(data__->CONDITION_41.,LOCALVAR0,,__GET_VAR(data__->SR2.Q1,));
  CONDITION_4_body__(&data__->CONDITION_41);
  __SET_VAR(data__->,MUL252_OUT,,MUL__ULINT__ULINT(
    (BOOL)__BOOL_LITERAL(TRUE),
    NULL,
    (UINT)2,
    (ULINT)__GET_VAR(data__->CONDITION_41.LOCALVAR1,),
    (ULINT)8));
  __SET_VAR(data__->,ADD244_OUT,,ADD__ULINT__ULINT(
    (BOOL)__BOOL_LITERAL(TRUE),
    NULL,
    (UINT)4,
    (ULINT)__GET_VAR(data__->MUL241_OUT,),
    (ULINT)__GET_VAR(data__->MUL246_OUT,),
    (ULINT)__GET_VAR(data__->MUL249_OUT,),
    (ULINT)__GET_VAR(data__->MUL252_OUT,)));
  __SET_EXTERNAL(data__->,TEST_RESULT_MODULE_2,,__GET_VAR(data__->ADD244_OUT,));
  __SET_VAR(data__->,SEL146_OUT,,SEL__UINT__BOOL__UINT__UINT(
    (BOOL)__BOOL_LITERAL(TRUE),
    NULL,
    (BOOL)__GET_EXTERNAL(data__->TEST_3,),
    (UINT)0,
    (UINT)__GET_EXTERNAL(data__->DO_F,)));
  __SET_VAR(data__->WRITE_DO_PWM_FREQ0.,DO_PWM_FREQ,,__GET_VAR(data__->SEL146_OUT,));
  WRITE_DO_PWM_FREQ_body__(&data__->WRITE_DO_PWM_FREQ0);
  __SET_VAR(data__->,SEL147_OUT,,SEL__UINT__BOOL__UINT__UINT(
    (BOOL)__BOOL_LITERAL(TRUE),
    NULL,
    (BOOL)__GET_EXTERNAL(data__->TEST_3,),
    (UINT)0,
    (UINT)__GET_EXTERNAL(data__->W_DO_SC_F,)));
  __SET_VAR(data__->,SEL39_OUT,,SEL__UINT__BOOL__UINT__UINT(
    (BOOL)__BOOL_LITERAL(TRUE),
    NULL,
    (BOOL)__GET_EXTERNAL(data__->TEST_3,),
    (UINT)0,
    (UINT)__GET_EXTERNAL(data__->W_DO_SC_E,)));
  __SET_VAR(data__->WRITE_DO_SC0.,DO_SC_FLAG,,__GET_VAR(data__->SEL147_OUT,));
  __SET_VAR(data__->WRITE_DO_SC0.,DO_SC_EN,,__GET_VAR(data__->SEL39_OUT,));
  WRITE_DO_SC_body__(&data__->WRITE_DO_SC0);
  __SET_VAR(data__->WRITE_MDB_ADDRESS0.,MDB_ADDR,,__GET_EXTERNAL(data__->ADR_M,));
  WRITE_MDB_ADDRESS_body__(&data__->WRITE_MDB_ADDRESS0);
  __SET_VAR(data__->,SEL278_OUT,,SEL__UINT__BOOL__UINT__UINT(
    (BOOL)__BOOL_LITERAL(TRUE),
    NULL,
    (BOOL)__GET_EXTERNAL(data__->TEST_2,),
    (UINT)0,
    (UINT)__GET_EXTERNAL(data__->DO_W,)));
  __SET_VAR(data__->,SEL282_OUT,,SEL__UINT__BOOL__UINT__UINT(
    (BOOL)__BOOL_LITERAL(TRUE),
    NULL,
    (BOOL)__GET_EXTERNAL(data__->TEST_2,),
    (UINT)0,
    (UINT)__GET_EXTERNAL(data__->DO_W,)));
  __SET_VAR(data__->WRITE_DO0.,DO_VALUE,,__GET_VAR(data__->SEL278_OUT,));
  __SET_VAR(data__->WRITE_DO0.,DO_MASK,,__GET_VAR(data__->SEL282_OUT,));
  WRITE_DO_body__(&data__->WRITE_DO0);

  goto __end;

__end:
  return;
} // PROGRAM0_body__() 





#endif //POUS_C
