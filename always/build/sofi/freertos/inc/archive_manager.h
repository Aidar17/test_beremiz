/*
 * Copyright (c) 2018 Snema Service
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
 * SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY
 * OF SUCH DAMAGE.
 *
 * This file is part of the sofi PLC.
 *
 * Author: Shoma Gane <shomagan@gmail.com>
 *         Ayrat Girfanov <girfanov.ayrat@yandex.ru>
 */
#ifndef ARCHIVE_MANAGER_H
#define ARCHIVE_MANAGER_H 1
#include "link_functions.h"
#include "beremiz_task.h"

#define ARCHIVES_MANAGERS_NUMBER 0
#define MAX_ARCHIVES_MANAGERS 14
#define ARCHIVE_MANAGER_THREAD_SIZE 200
#define ARCHIVE_MANAGER_ADD_GET_TIMEOUT 5000
#define ARCHIVE_MANAGER_STRUCTURE_SIZE (sizeof(archive_manager_t))
#define ARCHIVE_MANAGER_SIGNAL              (1<<27)
/*__DECLARE_GLOBAL_PROTOTYPE start*/
/*__DECLARE_GLOBAL_PROTOTYPE stop*/
typedef struct MCU_PACK{
    UINT save_arc; /*!<catch rising edge*/
    UDINT arc_for_read; /*!<arc number stored in buffer*/
    UINT id_number;    /*!<uniq arc number [0:10] (uniq only inside plc)*/
    UINT body_len; /*!<len of arc header + data*/
    UDINT unix_time_last_arc; /*!<time for last arc writed*/
    UDINT arcs_number;  /*!<number of arcs for this type*/
    UDINT last_readed;  /*!<last arc read 0 if didt read after start*/
    UDINT first_available;  /*!<first arc available in plc*/
}archive_manager_t;

typedef struct MCU_PACK{
    __IEC_UINT_p *const save_arc; /*!<catch rising edge*/
    __IEC_UDINT_p *const arc_for_read; /*!<arc number stored in buffer*/
    __IEC_UINT_p *const id_number;    /*!<uniq arc number [0:10] (uniq only inside plc)*/
    __IEC_UINT_p *const body_len; /*!<len of arc header + data*/
    __IEC_UDINT_p *const unix_time_last_arc; /*!<time for last arc writed*/
    __IEC_UDINT_p *const arcs_number;  /*!<number of arcs for this type*/
    __IEC_UDINT_p *const last_readed;  /*!<last arc read 0 if didt read after start*/
    __IEC_UDINT_p *const first_available;  /*!<first arc available in plc*/
}archive_manager_access_t;

typedef struct MCU_PACK{
    __IEC_UINT_p *const id_number;                  /*!< uniq arc number [0:10] (uniq only inside plc)*/
    __IEC_UINT_p *const header_len;                 /*!< len of header - sizeof(arc_header_t)*/
    __IEC_UINT_p *const body_len;                   /*!< full len data and header*/
    __IEC_USINT_p *const sec_data;  /*!<RTC Time Seconds [0;59]*/
    __IEC_USINT_p *const min_data;  /*!<RTC Time Minutes [0;59]*/
    __IEC_USINT_p *const hour_data; /*!<RTC Time Hour [0;59]*/
    __IEC_USINT_p *const date_data; /*!<RTC Date[1;31]*/
    __IEC_USINT_p *const month_data;/*!<RTC Date Month (in BCD format)*/
    __IEC_USINT_p *const year_data;/*!<RTC Date Year[0;99]*/
    __IEC_UDINT_p *const unix_time;                  /*!< unix time of saving   */
    __IEC_UDINT_p *const flags;                /*!< state flags of arc*/
    __IEC_UDINT_p *const id_crc;                  /*!< calculated from user data type*/
    __IEC_UDINT_p *const number;                     /*!< arc number by in order*/
}archive_header_access_t;
/*USERs STRUCT TYPES START*/
/*USERs STRUCT TYPES STOP*/
/*COMPLITE STRUCT TYPES START*/
typedef struct MCU_PACK{
    const archive_manager_access_t archive_manager_access;
    const archive_header_access_t archive_header_access;
}archive_complete_t;
/*COMPLITE STRUCT TYPES STOP*/
/*functions declarations start*/
int archive_manager_stop(void);
int archive_manager_start(void);
int archive_manager_control_check(void);
void archive_manager_thread(const void * param);
/*functions declarations stop*/
#endif //ARCHIVE_MANAGER_H