/* The total number of nodes, needed to support _all_ instances of the modbus plugin */
#define TOTAL_TCPNODE_COUNT       0
#define TOTAL_RTUNODE_COUNT       1
#define TOTAL_ASCNODE_COUNT       0
#define TOTAL_ROUT_NODE_COUNT     0
#define TOTAL_MODBUS_AREA_COUNT   0
/* Values for instance 1 of the modbus plugin */
#define MAX_NUMBER_OF_TCPCLIENTS  0
#define NUMBER_OF_TCPSERVER_NODES 0
#define NUMBER_OF_TCPCLIENT_NODES 0
#define NUMBER_OF_TCPCLIENT_REQTS 0

#define NUMBER_OF_RTUSERVER_NODES 0
#define NUMBER_OF_RTUCLIENT_NODES 1
#define NUMBER_OF_RTUCLIENT_REQTS 12

#define NUMBER_OF_ASCIISERVER_NODES 0
#define NUMBER_OF_ASCIICLIENT_NODES 0
#define NUMBER_OF_ASCIICLIENT_REQTS 0

#define NUMBER_OF_SERVER_NODES (NUMBER_OF_TCPSERVER_NODES + \
                                NUMBER_OF_RTUSERVER_NODES + \
                               NUMBER_OF_ASCIISERVER_NODES)

#define NUMBER_OF_CLIENT_NODES (NUMBER_OF_TCPCLIENT_NODES + \
                                NUMBER_OF_RTUCLIENT_NODES + \
                                NUMBER_OF_ASCIICLIENT_NODES)

#define NUMBER_OF_CLIENT_REQTS (NUMBER_OF_TCPCLIENT_REQTS + \
                                NUMBER_OF_RTUCLIENT_REQTS + \
                                NUMBER_OF_ASCIICLIENT_REQTS)

#define MAX_READ_BITS 254
#define MAX_WORD_NUM 127
#define MAX_PACKET_LEN (MAX_WORD_NUM*2 + 7)

#define COILS_01 1
#define INPUT_DISCRETES_02 2
#define HOLDING_REGISTERS_03 3
#define INPUT_REGISTERS_04 4
client_node_t		client_nodes[NUMBER_OF_CLIENT_NODES] = {
{"2.0"/*location*/, {naf_rtu, {.rtu = {RS_485_IMMO_UART, 115200 /*baud*/, 0 /*parity*/, 8 /*data bits*/, 1 /*stop bits*/, 0 /* ignore echo */}}}/*node_addr_t*/, -1 /* mb_nd */, 0 /* init_state */, 250 /* communication period */,0,NULL},
};

client_request_t	client_requests[NUMBER_OF_CLIENT_REQTS] = {
{"2_0_0"/*location*/,RS_485_IMMO_UART/*channel*/, 0/*client_node_id*/, 11/*slave_id*/, req_input/*req_type*/, 3/*mb_function*/, 49 /*first reg address*/,
10/*reg number*/,DEF_REQ_SEND_RETRIES/*retries*/, 0 /* error_code */, 0 /* prev_code */, 100/* timeout */,
NULL, NULL},
{"2_0_1"/*location*/,RS_485_IMMO_UART/*channel*/, 0/*client_node_id*/, 12/*slave_id*/, req_input/*req_type*/, 3/*mb_function*/, 49 /*first reg address*/,
10/*reg number*/,DEF_REQ_SEND_RETRIES/*retries*/, 0 /* error_code */, 0 /* prev_code */, 100/* timeout */,
NULL, NULL},
{"2_0_2"/*location*/,RS_485_IMMO_UART/*channel*/, 0/*client_node_id*/, 13/*slave_id*/, req_input/*req_type*/, 3/*mb_function*/, 49 /*first reg address*/,
10/*reg number*/,DEF_REQ_SEND_RETRIES/*retries*/, 0 /* error_code */, 0 /* prev_code */, 100/* timeout */,
NULL, NULL},
{"2_0_3"/*location*/,RS_485_IMMO_UART/*channel*/, 0/*client_node_id*/, 14/*slave_id*/, req_input/*req_type*/, 3/*mb_function*/, 49 /*first reg address*/,
10/*reg number*/,DEF_REQ_SEND_RETRIES/*retries*/, 0 /* error_code */, 0 /* prev_code */, 100/* timeout */,
NULL, NULL},
{"2_0_4"/*location*/,RS_485_IMMO_UART/*channel*/, 0/*client_node_id*/, 11/*slave_id*/, req_output/*req_type*/, 16/*mb_function*/, 55 /*first reg address*/,
2/*reg number*/,DEF_REQ_SEND_RETRIES/*retries*/, 0 /* error_code */, 0 /* prev_code */, 100/* timeout */,
NULL, NULL},
{"2_0_5"/*location*/,RS_485_IMMO_UART/*channel*/, 0/*client_node_id*/, 12/*slave_id*/, req_output/*req_type*/, 16/*mb_function*/, 55 /*first reg address*/,
2/*reg number*/,DEF_REQ_SEND_RETRIES/*retries*/, 0 /* error_code */, 0 /* prev_code */, 100/* timeout */,
NULL, NULL},
{"2_0_6"/*location*/,RS_485_IMMO_UART/*channel*/, 0/*client_node_id*/, 13/*slave_id*/, req_output/*req_type*/, 16/*mb_function*/, 55 /*first reg address*/,
2/*reg number*/,DEF_REQ_SEND_RETRIES/*retries*/, 0 /* error_code */, 0 /* prev_code */, 100/* timeout */,
NULL, NULL},
{"2_0_7"/*location*/,RS_485_IMMO_UART/*channel*/, 0/*client_node_id*/, 14/*slave_id*/, req_output/*req_type*/, 16/*mb_function*/, 55 /*first reg address*/,
2/*reg number*/,DEF_REQ_SEND_RETRIES/*retries*/, 0 /* error_code */, 0 /* prev_code */, 100/* timeout */,
NULL, NULL},
{"2_0_8"/*location*/,RS_485_IMMO_UART/*channel*/, 0/*client_node_id*/, 11/*slave_id*/, req_output/*req_type*/, 16/*mb_function*/, 4 /*first reg address*/,
2/*reg number*/,DEF_REQ_SEND_RETRIES/*retries*/, 0 /* error_code */, 0 /* prev_code */, 100/* timeout */,
NULL, NULL},
{"2_0_9"/*location*/,RS_485_IMMO_UART/*channel*/, 0/*client_node_id*/, 12/*slave_id*/, req_output/*req_type*/, 16/*mb_function*/, 4 /*first reg address*/,
2/*reg number*/,DEF_REQ_SEND_RETRIES/*retries*/, 0 /* error_code */, 0 /* prev_code */, 100/* timeout */,
NULL, NULL},
{"2_0_10"/*location*/,RS_485_IMMO_UART/*channel*/, 0/*client_node_id*/, 13/*slave_id*/, req_output/*req_type*/, 16/*mb_function*/, 4 /*first reg address*/,
2/*reg number*/,DEF_REQ_SEND_RETRIES/*retries*/, 0 /* error_code */, 0 /* prev_code */, 100/* timeout */,
NULL, NULL},
{"2_0_11"/*location*/,RS_485_IMMO_UART/*channel*/, 0/*client_node_id*/, 14/*slave_id*/, req_output/*req_type*/, 16/*mb_function*/, 4 /*first reg address*/,
2/*reg number*/,DEF_REQ_SEND_RETRIES/*retries*/, 0 /* error_code */, 0 /* prev_code */, 100/* timeout */,
NULL, NULL},
};

static route_node_t	route_nodes[TOTAL_ROUT_NODE_COUNT] = {
};

static area_node_t	area_nodes[TOTAL_MODBUS_AREA_COUNT] = {
};

UDINT *__QD2_0_4_0;
UDINT *__QD2_0_5_0;
UDINT *__QD2_0_6_0;
UDINT *__QD2_0_7_0;
UDINT *__ID2_0_0_3;
UDINT *__ID2_0_1_3;
UDINT *__ID2_0_2_3;
UDINT *__ID2_0_3_3;
UDINT *__ID2_0_0_0;
UDINT *__ID2_0_1_0;
UDINT *__ID2_0_2_0;
UDINT *__ID2_0_3_0;
UINT *__QW2_0_8_1;
UINT *__QW2_0_9_1;
UINT *__QW2_0_10_1;
UINT *__QW2_0_11_1;
u16 plcv_buffer_req_0[1] = {0}; // now it's not used 
u16 com_buffer_req_0[10] = {0,0,0,0,0,0,0,0,0,0,};
u16 plcv_buffer_req_1[1] = {0}; // now it's not used 
u16 com_buffer_req_1[10] = {0,0,0,0,0,0,0,0,0,0,};
u16 plcv_buffer_req_2[1] = {0}; // now it's not used 
u16 com_buffer_req_2[10] = {0,0,0,0,0,0,0,0,0,0,};
u16 plcv_buffer_req_3[1] = {0}; // now it's not used 
u16 com_buffer_req_3[10] = {0,0,0,0,0,0,0,0,0,0,};
u16 plcv_buffer_req_4[1] = {0}; // now it's not used 
u16 com_buffer_req_4[2] = {0,0,};
u16 plcv_buffer_req_5[1] = {0}; // now it's not used 
u16 com_buffer_req_5[2] = {0,0,};
u16 plcv_buffer_req_6[1] = {0}; // now it's not used 
u16 com_buffer_req_6[2] = {0,0,};
u16 plcv_buffer_req_7[1] = {0}; // now it's not used 
u16 com_buffer_req_7[2] = {0,0,};
u16 plcv_buffer_req_8[1] = {0}; // now it's not used 
u16 com_buffer_req_8[2] = {0,0,};
u16 plcv_buffer_req_9[1] = {0}; // now it's not used 
u16 com_buffer_req_9[2] = {0,0,};
u16 plcv_buffer_req_10[1] = {0}; // now it's not used 
u16 com_buffer_req_10[2] = {0,0,};
u16 plcv_buffer_req_11[1] = {0}; // now it's not used 
u16 com_buffer_req_11[2] = {0,0,};
