# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "C"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_C
  "D:/test/always/build/sofi/freertos/src/POUS.c" "D:/test/always/build/sofi/freertos/cmake_arm/CMakeFiles/sofi_task.elf.dir/src/POUS.c.obj"
  "D:/test/always/build/sofi/freertos/src/archive_manager.c" "D:/test/always/build/sofi/freertos/cmake_arm/CMakeFiles/sofi_task.elf.dir/src/archive_manager.c.obj"
  "D:/test/always/build/sofi/freertos/src/beremiz_regs_description.c" "D:/test/always/build/sofi/freertos/cmake_arm/CMakeFiles/sofi_task.elf.dir/src/beremiz_regs_description.c.obj"
  "D:/test/always/build/sofi/freertos/src/beremiz_task.c" "D:/test/always/build/sofi/freertos/cmake_arm/CMakeFiles/sofi_task.elf.dir/src/beremiz_task.c.obj"
  "D:/test/always/build/sofi/freertos/src/can_open_dict.c" "D:/test/always/build/sofi/freertos/cmake_arm/CMakeFiles/sofi_task.elf.dir/src/can_open_dict.c.obj"
  "D:/test/always/build/sofi/freertos/src/config_task.c" "D:/test/always/build/sofi/freertos/cmake_arm/CMakeFiles/sofi_task.elf.dir/src/config_task.c.obj"
  "D:/test/always/build/sofi/freertos/src/modbus_master.c" "D:/test/always/build/sofi/freertos/cmake_arm/CMakeFiles/sofi_task.elf.dir/src/modbus_master.c.obj"
  "D:/test/always/build/sofi/freertos/src/os_service.c" "D:/test/always/build/sofi/freertos/cmake_arm/CMakeFiles/sofi_task.elf.dir/src/os_service.c.obj"
  "D:/test/always/build/sofi/freertos/src/plc_debugger.c" "D:/test/always/build/sofi/freertos/cmake_arm/CMakeFiles/sofi_task.elf.dir/src/plc_debugger.c.obj"
  "D:/test/always/build/sofi/freertos/src/plc_main.c" "D:/test/always/build/sofi/freertos/cmake_arm/CMakeFiles/sofi_task.elf.dir/src/plc_main.c.obj"
  "D:/test/always/build/sofi/freertos/src/resource1.c" "D:/test/always/build/sofi/freertos/cmake_arm/CMakeFiles/sofi_task.elf.dir/src/resource1.c.obj"
  "D:/test/always/build/sofi/freertos/src/sofi_beremiz.c" "D:/test/always/build/sofi/freertos/cmake_arm/CMakeFiles/sofi_task.elf.dir/src/sofi_beremiz.c.obj"
  "D:/test/always/build/sofi/freertos/src/sofi_dev.c" "D:/test/always/build/sofi/freertos/cmake_arm/CMakeFiles/sofi_task.elf.dir/src/sofi_dev.c.obj"
  )
set(CMAKE_C_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_C
  "STM32F767xx"
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "C:/Beremiz/sofi/generator/template/freertos/lib/stm32"
  "C:/Beremiz/sofi/generator/template/freertos/lib/sofi"
  "C:/Beremiz/sofi/generator/template/freertos/lib/lwip/system"
  "C:/Beremiz/sofi/generator/template/freertos/lib/lwip/src/include"
  "C:/Beremiz/sofi/generator/template/freertos/lib/freertos"
  "C:/Beremiz/sofi/generator/template/freertos/lib/usb"
  "C:/Beremiz/sofi/generator/template/freertos/lib/can_festival"
  "../inc"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
