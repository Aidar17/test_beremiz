FUNCTION_BLOCK condition
  VAR_INPUT
    LocalVar0 : REAL;
  END_VAR
  VAR_OUTPUT
    LocalVar1 : ULINT;
  END_VAR

  IF LocalVar0=0.0 
  THEN LocalVar1 := 0;
  ELSE LocalVar1 := 1;
  END_IF;
END_FUNCTION_BLOCK

FUNCTION_BLOCK condition_1
  VAR_INPUT
    LocalVar0 : UDINT;
  END_VAR
  VAR_OUTPUT
    LocalVar1 : BOOL;
  END_VAR

  IF LocalVar0=1 
  THEN LocalVar1 := 1;
  END_IF;
END_FUNCTION_BLOCK

FUNCTION_BLOCK condition_2
  VAR_INPUT
    LocalVar0 : USINT;
  END_VAR
  VAR_OUTPUT
    LocalVar1 : ULINT;
  END_VAR

  IF LocalVar0=0 
  THEN LocalVar1 := 1;
  END_IF;
END_FUNCTION_BLOCK

FUNCTION_BLOCK condition_3
  VAR_INPUT
    LocalVar0 : UINT;
  END_VAR
  VAR_OUTPUT
    LocalVar1 : ULINT;
  END_VAR

  IF LocalVar0=0 
  THEN LocalVar1 := 0;
  ELSE LocalVar1 := 1;
  END_IF;
END_FUNCTION_BLOCK

FUNCTION_BLOCK ai_signal
  VAR_INPUT
    LocalVar0 : UINT;
  END_VAR
  VAR_OUTPUT
    LocalVar1 : ULINT;
  END_VAR

  IF LocalVar0>100
  THEN LocalVar1 := 1;
  ELSE LocalVar1 := 0;
  END_IF;
END_FUNCTION_BLOCK

FUNCTION_BLOCK di_signal
  VAR_INPUT
    LocalVar0 : UDINT;
  END_VAR
  VAR_OUTPUT
    LocalVar1 : BOOL;
  END_VAR

  IF LocalVar0=11
  THEN LocalVar1 := 1;
  ELSE LocalVar1 := 0;
  END_IF;
END_FUNCTION_BLOCK

FUNCTION_BLOCK condition_4
  VAR_INPUT
    LocalVar0 : BOOL;
  END_VAR
  VAR_OUTPUT
    LocalVar1 : ULINT;
  END_VAR

  IF LocalVar0=1
  THEN LocalVar1 := 1;
  ELSE LocalVar1 := 0;
  END_IF;
END_FUNCTION_BLOCK

FUNCTION_BLOCK di_cnt_hj
  VAR_INPUT
    LocalVar0 : ULINT;
  END_VAR
  VAR_OUTPUT
    LocalVar1 : ULINT;
  END_VAR

  IF LocalVar0=10
  THEN LocalVar1 := 1;
  ELSE LocalVar1 := 0;
  END_IF;
END_FUNCTION_BLOCK

FUNCTION_BLOCK di_freq
  VAR_INPUT
    LocalVar0 : REAL;
  END_VAR
  VAR_OUTPUT
    LocalVar1 : ULINT;
  END_VAR

  IF LocalVar0=0.0
  THEN LocalVar1 := 0;
  ELSE LocalVar1 := 1;
  END_IF;
END_FUNCTION_BLOCK

PROGRAM program0
  VAR_EXTERNAL
    BRIC_1_ai_physical_0 : REAL;
    BRIC_1_ai_physical_1 : REAL;
    BRIC_1_ai_physical0 : REAL;
    BRIC_1_ai_physical1 : REAL;
    BRIC_1_ai_unit_0 : UINT;
    BRIC_1_ai_unit_1 : UINT;
    BRIC_1_ai_unit0 : UINT;
    BRIC_1_ai_unit1 : UINT;
    BRIC_1_di_state : UDINT;
    BRIC_1_di_state0 : UDINT;
    BRIC_1_do_sc_ctrl : USINT;
    BRIC_1_do_sc_ctrl0 : USINT;
    BRIC_1_do_ctrl : UINT;
    BRIC_1_mdb_addr : UINT;
    BRIC_1_mdb_addr0 : UINT;
    BRIC_1_module_number : UINT;
    BRIC_1_module_number0 : UINT;
    BRIC_1_internal_temp : REAL;
    DO_1_do_sc_ctrl : UINT;
    DO_1_do_sc_ctrl0 : UINT;
    DO_1_do_ctrl : UINT;
    DO_1_mdb_addr : UINT;
    DO_1_mdb_addr0 : UINT;
    DO_1_module_number : UINT;
    DO_1_module_number0 : UINT;
    DO_1_internal_temp : REAL;
    DO_1_ip_3 : USINT;
    AO_1_ao_val_0 : UINT;
    AO_1_ao_val_1 : UINT;
    AO_1_ao_val_2 : UINT;
    AO_1_ao_val_3 : UINT;
    AO_ch_0 : UINT;
    AO_ch_1 : UINT;
    AO_ch_2 : UINT;
    AO_ch_3 : UINT;
    AO_1_mdb_addr : UINT;
    AO_1_mdb_addr0 : UINT;
    AO_1_module_number : UINT;
    AO_1_module_number0 : UINT;
    AO_1_internal_temp : REAL;
    AO_1_ip_3 : USINT;
    AI_1_ai_unit_0 : UINT;
    AI_1_ai_unit_1 : UINT;
    AI_1_ai_unit0 : UINT;
    AI_1_ai_unit1 : UINT;
    AI_1_mdb_addr : UINT;
    AI_1_mdb_addr0 : UINT;
    AI_1_module_number : UINT;
    AI_1_module_number0 : UINT;
    AI_1_internal_temp : REAL;
    AI_1_ip_3 : USINT;
    DI_1_di_state : UDINT;
    DI_1_di_state0 : UDINT;
    DI_1_mdb_addr : UINT;
    DI_1_mdb_addr0 : UINT;
    DI_1_module_number : UINT;
    DI_1_module_number0 : UINT;
    DI_1_internal_temp : REAL;
    DI_1_ip_3 : USINT;
    start_test : UDINT;
  END_VAR
  VAR
    z_ero : UDINT := 0;
    z_ero0 : UINT := 0;
    z_ero1 : USINT := 0;
    st_test : UDINT := 1;
  END_VAR
  VAR_EXTERNAL
    DO_test_w : UDINT;
    DI_test_w : UDINT;
    AI_test_w : UDINT;
    AO_test_w : UDINT;
    DO_ch : UINT;
    DO_plc_ch : UINT;
  END_VAR
  VAR
    READ_RESET0 : READ_RESET;
    READ_PWR0 : READ_PWR;
    READ_TEMP0 : READ_TEMP;
    WRITE_MDB_ADDRESS0 : WRITE_MDB_ADDRESS;
    WRITE_DO_PWM_FREQ0 : WRITE_DO_PWM_FREQ;
    WRITE_DO0 : WRITE_DO;
    READ_DI0 : READ_DI;
    READ_DI_CNT0 : READ_DI_CNT;
    READ_DI_FREQ0 : READ_DI_FREQ;
    READ_DO_SC0 : READ_DO_SC;
    READ_DO0 : READ_DO;
    WRITE_DO_SC0 : WRITE_DO_SC;
    READ_PARAM_UINT0 : READ_PARAM_UINT;
    B_1 : BOOL := 1;
    B_2 : BOOL;
    NUN_ADR_MOD : UDINT := 0;
  END_VAR
  VAR_EXTERNAL
    ADR_MOD : UINT;
    RES_N : UINT;
    LAS_RES : UINT;
    ADR_M : UINT;
    DI_N : UINT;
    DI_V : ULINT;
    DI_F : REAL;
    DI_O : UDINT;
    DO_O : USINT;
    DO_F : UINT;
  END_VAR
  VAR
    R_DO_SC_F : USINT;
  END_VAR
  VAR_EXTERNAL
    W_DO_SC_F : UINT;
  END_VAR
  VAR
    R_DO_SC_E : USINT;
  END_VAR
  VAR_EXTERNAL
    W_DO_SC_E : UINT;
    DO_W : UINT;
  END_VAR
  VAR
    condition1 : condition;
    condition2 : condition;
    condition_10 : condition_1;
    condition_20 : condition_2;
  END_VAR
  VAR_EXTERNAL
    test_result_module : ULINT;
    test_result_module_2 : ULINT;
    test_result_plc : ULINT;
  END_VAR
  VAR
    condition_30 : condition_3;
    SR0 : SR;
    TON0 : TON;
    LocalVar2 : BOOL := 1;
    LocalVar4 : REAL := 70.0;
    LocalVar6 : BOOL;
    LocalVar5 : BOOL := 0;
    condition_31 : condition_3;
    condition_32 : condition_3;
  END_VAR
  VAR_EXTERNAL
    test_2 : BOOL;
    test_3 : BOOL;
  END_VAR
  VAR
    ai_signal0 : ai_signal;
    ai_signal1 : ai_signal;
    di_signal0 : di_signal;
    condition_40 : condition_4;
    di_signal1 : di_signal;
    ai_signal2 : ai_signal;
    ai_signal3 : ai_signal;
    condition_41 : condition_4;
    SR1 : SR;
    SR2 : SR;
    di_signal2 : di_signal;
    condition_42 : condition_4;
    SR3 : SR;
    condition_21 : condition_2;
    di_cnt_hj0 : di_cnt_hj;
    di_freq0 : di_freq;
    SEL69_OUT : UDINT;
    SEL226_OUT : UINT;
    SEL93_OUT : UINT;
    SEL13_OUT : UINT;
    SEL57_OUT : UINT;
    SUB163_OUT : USINT;
    SUB161_OUT : USINT;
    SUB165_OUT : USINT;
    SUB167_OUT : USINT;
    ADD168_OUT : USINT;
    MUL76_OUT : REAL;
    MUL174_OUT : ULINT;
    MUL155_OUT : UINT;
    MUL188_OUT : ULINT;
    ADD171_OUT : ULINT;
    SEL227_OUT : UINT;
    SEL5_OUT : UINT;
    SEL26_OUT : UINT;
    SEL149_OUT : UINT;
    SEL28_OUT : UINT;
    SEL30_OUT : UINT;
    MUL35_OUT : REAL;
    MUL212_OUT : UINT;
    MUL192_OUT : ULINT;
    MUL220_OUT : UINT;
    MUL224_OUT : ULINT;
    MUL276_OUT : ULINT;
    SUB286_OUT : USINT;
    SUB119_OUT : USINT;
    ADD289_OUT : USINT;
    MUL292_OUT : ULINT;
    MUL297_OUT : ULINT;
    MUL302_OUT : ULINT;
    BOOL_TO_ULINT202_OUT : ULINT;
    MUL204_OUT : ULINT;
    ADD209_OUT : ULINT;
    SEL92_OUT : UINT;
    REAL_TO_TIME196_OUT : TIME;
    SEL4_OUT : USINT;
    MUL241_OUT : ULINT;
    MUL246_OUT : ULINT;
    MUL259_OUT : ULINT;
    MUL249_OUT : ULINT;
    MUL252_OUT : ULINT;
    ADD244_OUT : ULINT;
    SEL146_OUT : UINT;
    SEL147_OUT : UINT;
    SEL39_OUT : UINT;
    SEL278_OUT : UINT;
    SEL282_OUT : UINT;
  END_VAR

  BRIC_1_ai_physical0 := BRIC_1_ai_physical_0;
  DO_1_mdb_addr := DO_1_mdb_addr0;
  AO_1_mdb_addr := AO_1_mdb_addr0;
  BRIC_1_ai_physical1 := BRIC_1_ai_physical_1;
  DO_1_module_number0 := DO_1_module_number;
  AO_1_module_number0 := AO_1_module_number;
  DI_1_mdb_addr := DI_1_mdb_addr0;
  READ_DI_CNT0(DI_NUMBER := DI_N);
  DI_V := READ_DI_CNT0.DI_CNT_VALUE;
  AI_1_mdb_addr := AI_1_mdb_addr0;
  DI_1_module_number0 := DI_1_module_number;
  condition_10(LocalVar0 := start_test);
  SEL69_OUT := SEL(condition_10.LocalVar1, z_ero, st_test);
  DO_test_w := SEL69_OUT;
  start_test := DO_test_w;
  READ_DI_FREQ0(DI_NUMBER := DI_N);
  DI_F := READ_DI_FREQ0.DI_FREQ_VALUE;
  SEL226_OUT := SEL(test_2, 0, BRIC_1_ai_unit_0);
  BRIC_1_ai_unit0 := SEL226_OUT;
  AI_1_module_number0 := AI_1_module_number;
  DI_1_di_state0 := DI_1_di_state;
  DI_test_w := SEL69_OUT;
  READ_RESET0();
  RES_N := READ_RESET0.RESET_NUM;
  SEL93_OUT := SEL(test_2, 0, DO_ch);
  DO_1_do_ctrl := SEL93_OUT;
  SEL13_OUT := SEL(test_2, 0, AO_ch_0);
  AO_1_ao_val_0 := SEL13_OUT;
  AI_test_w := SEL69_OUT;
  LAS_RES := READ_RESET0.LAST_RESET;
  SEL57_OUT := SEL(test_2, 0, AI_1_ai_unit_0);
  AI_1_ai_unit0 := SEL57_OUT;
  SUB163_OUT := SUB(DO_1_ip_3, 7);
  SUB161_OUT := SUB(AO_1_ip_3, 7);
  SUB165_OUT := SUB(DI_1_ip_3, 7);
  SUB167_OUT := SUB(AI_1_ip_3, 7);
  ADD168_OUT := ADD(SUB163_OUT, SUB161_OUT, SUB165_OUT, SUB167_OUT);
  condition_20(LocalVar0 := ADD168_OUT);
  MUL76_OUT := MUL(BRIC_1_internal_temp, DO_1_internal_temp, AO_1_internal_temp, AI_1_internal_temp, DI_1_internal_temp);
  condition1(LocalVar0 := MUL76_OUT);
  MUL174_OUT := MUL(condition1.LocalVar1, 2);
  MUL155_OUT := MUL(BRIC_1_module_number0, DO_1_module_number0, AO_1_module_number0, AI_1_module_number0, DI_1_module_number0);
  condition_30(LocalVar0 := MUL155_OUT);
  MUL188_OUT := MUL(condition_30.LocalVar1, 4);
  ADD171_OUT := ADD(condition_20.LocalVar1, MUL174_OUT, MUL188_OUT);
  test_result_module := ADD171_OUT;
  AO_test_w := SEL69_OUT;
  SEL227_OUT := SEL(test_2, 0, BRIC_1_ai_unit_1);
  BRIC_1_ai_unit1 := SEL227_OUT;
  SEL5_OUT := SEL(test_3, 0, DO_1_do_sc_ctrl0);
  DO_1_do_sc_ctrl := SEL5_OUT;
  SEL26_OUT := SEL(test_2, 0, AO_ch_1);
  AO_1_ao_val_1 := SEL26_OUT;
  SEL149_OUT := SEL(test_2, 0, AI_1_ai_unit_1);
  AI_1_ai_unit1 := SEL149_OUT;
  SEL28_OUT := SEL(test_2, 0, AO_ch_2);
  AO_1_ao_val_2 := SEL28_OUT;
  BRIC_1_di_state0 := BRIC_1_di_state;
  SEL30_OUT := SEL(test_2, 0, AO_ch_3);
  AO_1_ao_val_3 := SEL30_OUT;
  READ_DI0();
  DI_O := READ_DI0.DI_OUT;
  DI_1_di_state0 := DI_O;
  BRIC_1_mdb_addr := BRIC_1_mdb_addr0;
  READ_PARAM_UINT0(ENABLE := B_1, ADDRESS := NUN_ADR_MOD);
  B_2 := READ_PARAM_UINT0.CHECK;
  BRIC_1_module_number0 := BRIC_1_module_number;
  ADR_MOD := READ_PARAM_UINT0.VALUE;
  READ_PWR0();
  READ_TEMP0();
  MUL35_OUT := MUL(READ_PWR0.V_PWR, READ_PWR0.V_BAT, READ_TEMP0.MCU_TEMP, READ_TEMP0.ADC_TEMP);
  condition2(LocalVar0 := MUL35_OUT);
  MUL212_OUT := MUL(RES_N, LAS_RES);
  condition_31(LocalVar0 := MUL212_OUT);
  MUL192_OUT := MUL(condition_31.LocalVar1, 2);
  MUL220_OUT := MUL(ADR_M, ADR_MOD);
  condition_32(LocalVar0 := MUL220_OUT);
  MUL224_OUT := MUL(condition_32.LocalVar1, 4);
  di_signal2(LocalVar0 := DI_1_di_state0);
  SR3(S1 := di_signal2.LocalVar1, R := 0);
  condition_42(LocalVar0 := SR3.Q1);
  MUL276_OUT := MUL(condition_42.LocalVar1, 8);
  SUB286_OUT := SUB(R_DO_SC_F, 8);
  SUB119_OUT := SUB(R_DO_SC_E, 6);
  ADD289_OUT := ADD(SUB286_OUT, SUB119_OUT);
  condition_21(LocalVar0 := ADD289_OUT);
  MUL292_OUT := MUL(condition_21.LocalVar1, 16);
  di_cnt_hj0(LocalVar0 := DI_V);
  MUL297_OUT := MUL(di_cnt_hj0.LocalVar1, 32);
  di_freq0(LocalVar0 := DI_F);
  MUL302_OUT := MUL(di_freq0.LocalVar1, 64);
  BOOL_TO_ULINT202_OUT := BOOL_TO_ULINT(LocalVar6);
  MUL204_OUT := MUL(BOOL_TO_ULINT202_OUT, 8);
  ADD209_OUT := ADD(condition2.LocalVar1, MUL192_OUT, MUL224_OUT, MUL276_OUT, MUL292_OUT, MUL297_OUT, MUL302_OUT, MUL204_OUT);
  test_result_plc := ADD209_OUT;
  SEL92_OUT := SEL(test_2, 0, DO_plc_ch);
  BRIC_1_do_ctrl := SEL92_OUT;
  REAL_TO_TIME196_OUT := REAL_TO_TIME(LocalVar4);
  TON0(IN := LocalVar2, PT := REAL_TO_TIME196_OUT);
  SR0(S1 := TON0.Q, R := LocalVar5);
  LocalVar6 := SR0.Q1;
  SEL4_OUT := SEL(test_3, 0, BRIC_1_do_sc_ctrl);
  BRIC_1_do_sc_ctrl0 := SEL4_OUT;
  READ_DO_SC0();
  R_DO_SC_F := READ_DO_SC0.DO_SC_FLAG;
  R_DO_SC_E := READ_DO_SC0.DO_SC_EN;
  READ_DO0();
  DO_O := READ_DO0.DO_OUT;
  ai_signal0(LocalVar0 := BRIC_1_ai_unit0);
  ai_signal1(LocalVar0 := BRIC_1_ai_unit1);
  MUL241_OUT := MUL(ai_signal0.LocalVar1, ai_signal1.LocalVar1);
  di_signal0(LocalVar0 := BRIC_1_di_state0);
  SR1(S1 := di_signal0.LocalVar1, R := 0);
  condition_40(LocalVar0 := SR1.Q1);
  MUL246_OUT := MUL(condition_40.LocalVar1, 2);
  ai_signal2(LocalVar0 := AI_1_ai_unit0);
  ai_signal3(LocalVar0 := AI_1_ai_unit1);
  MUL259_OUT := MUL(ai_signal2.LocalVar1, ai_signal3.LocalVar1);
  MUL249_OUT := MUL(MUL259_OUT, 4);
  di_signal1(LocalVar0 := DI_1_di_state0);
  SR2(S1 := di_signal1.LocalVar1, R := 0);
  condition_41(LocalVar0 := SR2.Q1);
  MUL252_OUT := MUL(condition_41.LocalVar1, 8);
  ADD244_OUT := ADD(MUL241_OUT, MUL246_OUT, MUL249_OUT, MUL252_OUT);
  test_result_module_2 := ADD244_OUT;
  SEL146_OUT := SEL(test_3, 0, DO_F);
  WRITE_DO_PWM_FREQ0(DO_PWM_FREQ := SEL146_OUT);
  SEL147_OUT := SEL(test_3, 0, W_DO_SC_F);
  SEL39_OUT := SEL(test_3, 0, W_DO_SC_E);
  WRITE_DO_SC0(DO_SC_FLAG := SEL147_OUT, DO_SC_EN := SEL39_OUT);
  WRITE_MDB_ADDRESS0(MDB_ADDR := ADR_M);
  SEL278_OUT := SEL(test_2, 0, DO_W);
  SEL282_OUT := SEL(test_2, 0, DO_W);
  WRITE_DO0(DO_VALUE := SEL278_OUT, DO_MASK := SEL282_OUT);
END_PROGRAM


CONFIGURATION config
  VAR_GLOBAL RETAIN
    start_test : UDINT := 1;
  END_VAR
  VAR_GLOBAL
    DO_test_w AT %QD2.0.4.0 : UDINT;
    AO_test_w AT %QD2.0.5.0 : UDINT;
    AI_test_w AT %QD2.0.6.0 : UDINT;
    DI_test_w AT %QD2.0.7.0 : UDINT;
    DO_test_r AT %ID2.0.0.3 : UDINT;
    AO_test_r AT %ID2.0.1.3 : UDINT;
    AI_test_r AT %ID2.0.2.3 : UDINT;
    DI_test_r AT %ID2.0.3.3 : UDINT;
    DO_free_heap AT %ID2.0.0.0 : UDINT;
    AO_free_heap AT %ID2.0.1.0 : UDINT;
    AI_free_heap AT %ID2.0.2.0 : UDINT;
    DI_free_heap AT %ID2.0.3.0 : UDINT;
    DO_IP3 AT %QW2.0.8.1 : UINT := 7;
    AO_IP3 AT %QW2.0.9.1 : UINT := 7;
    AI_IP3 AT %QW2.0.10.1 : UINT := 7;
    DI_IP3 AT %QW2.0.11.1 : UINT := 7;
    BRIC_1_mdb_addr0 : UINT := 10;
    DO_1_mdb_addr0 : UINT := 11;
    AO_1_mdb_addr0 : UINT := 12;
    AI_1_mdb_addr0 : UINT := 13;
    DI_1_mdb_addr0 : UINT := 14;
    BRIC_1_module_number0 : UINT;
    DO_1_module_number0 : UINT;
    AO_1_module_number0 : UINT;
    AI_1_module_number0 : UINT;
    DI_1_module_number0 : UINT;
    AO_ch_0 : UINT := 10000;
    AO_ch_1 : UINT := 10000;
    AO_ch_2 : UINT := 10000;
    AO_ch_3 : UINT := 10000;
    DO_ch : UINT := 187;
    DO_plc_ch : UINT := 11;
    BRIC_1_ai_physical0 : REAL;
    BRIC_1_ai_physical1 : REAL;
    BRIC_1_ai_unit0 : UINT;
    BRIC_1_ai_unit1 : UINT;
    BRIC_1_do_sc_ctrl0 : USINT;
    DI_1_di_state0 : UDINT;
    BRIC_1_di_state0 : UDINT;
    DO_1_do_sc_ctrl0 : UINT := 26231;
    AI_1_ai_unit0 : UINT;
    AI_1_ai_unit1 : UINT;
    ADR_MOD : UINT;
    RES_N : UINT;
    LAS_RES : UINT;
    ADR_M : UINT := 5;
    DI_N : UINT := 3;
    DI_V : ULINT;
    DI_F : REAL;
    DI_O : UDINT;
    DO_O : USINT;
    W_DO_SC_F : UINT := 8;
    W_DO_SC_E : UINT := 6;
    DO_W : UINT := 11;
    DO_F : UINT := 100;
    test_result_module : ULINT;
    test_result_module_2 : ULINT;
    test_result_plc : ULINT;
    test_2 : BOOL := 0;
    test_3 : BOOL := 0;
    BRIC_1_mdb_addr : UINT := 0;
    BRIC_1_last_reset : UINT := 0;
    BRIC_1_uart1_sets : UINT := 0;
    BRIC_1_uart2_sets : UINT := 0;
    BRIC_1_uart3_sets : UINT := 0;
    BRIC_1_uart5_sets : UINT := 0;
    BRIC_1_uart6_sets : UINT := 0;
    BRIC_1_uart7_sets : UINT := 0;
    BRIC_1_do_state : USINT := 0;
    BRIC_1_do_sc_ctrl : USINT := 0;
    BRIC_1_do_ctrl : UINT := 0;
    BRIC_1_do_pwm_freq : UINT := 0;
    BRIC_1_do_pwm_ctrl_0 : UINT := 0;
    BRIC_1_do_pwm_ctrl_1 : UINT := 0;
    BRIC_1_do_pwm_ctrl_2 : UINT := 0;
    BRIC_1_do_pwm_ctrl_3 : UINT := 0;
    BRIC_1_di_noise_fltr_us_0 : UINT := 0;
    BRIC_1_di_noise_fltr_us_1 : UINT := 0;
    BRIC_1_di_noise_fltr_us_2 : UINT := 0;
    BRIC_1_di_noise_fltr_us_3 : UINT := 0;
    BRIC_1_di_noise_fltr_us_4 : UINT := 0;
    BRIC_1_di_noise_fltr_us_5 : UINT := 0;
    BRIC_1_di_noise_fltr_us_6 : UINT := 0;
    BRIC_1_di_noise_fltr_us_7 : UINT := 0;
    BRIC_1_di_noise_fltr_us_8 : UINT := 0;
    BRIC_1_di_noise_fltr_us_9 : UINT := 0;
    BRIC_1_di_noise_fltr_us_10 : UINT := 0;
    BRIC_1_di_noise_fltr_us_11 : UINT := 0;
    BRIC_1_di_noise_fltr_us_12 : UINT := 0;
    BRIC_1_di_noise_fltr_us_13 : UINT := 0;
    BRIC_1_di_noise_fltr_us_14 : UINT := 0;
    BRIC_1_di_noise_fltr_us_15 : UINT := 0;
    BRIC_1_di_pulseless_ms_0 : UDINT := 0;
    BRIC_1_di_pulseless_ms_1 : UDINT := 0;
    BRIC_1_di_pulseless_ms_2 : UDINT := 0;
    BRIC_1_di_pulseless_ms_3 : UDINT := 0;
    BRIC_1_di_pulseless_ms_4 : UDINT := 0;
    BRIC_1_di_pulseless_ms_5 : UDINT := 0;
    BRIC_1_di_pulseless_ms_6 : UDINT := 0;
    BRIC_1_di_pulseless_ms_7 : UDINT := 0;
    BRIC_1_di_pulseless_ms_8 : UDINT := 0;
    BRIC_1_di_pulseless_ms_9 : UDINT := 0;
    BRIC_1_di_pulseless_ms_10 : UDINT := 0;
    BRIC_1_di_pulseless_ms_11 : UDINT := 0;
    BRIC_1_di_pulseless_ms_12 : UDINT := 0;
    BRIC_1_di_pulseless_ms_13 : UDINT := 0;
    BRIC_1_di_pulseless_ms_14 : UDINT := 0;
    BRIC_1_di_pulseless_ms_15 : UDINT := 0;
    BRIC_1_di_mode_0 : UINT := 0;
    BRIC_1_di_mode_1 : UINT := 0;
    BRIC_1_di_mode_2 : UINT := 0;
    BRIC_1_di_mode_3 : UINT := 0;
    BRIC_1_di_mode_4 : UINT := 0;
    BRIC_1_di_mode_5 : UINT := 0;
    BRIC_1_di_mode_6 : UINT := 0;
    BRIC_1_di_mode_7 : UINT := 0;
    BRIC_1_di_mode_8 : UINT := 0;
    BRIC_1_di_mode_9 : UINT := 0;
    BRIC_1_di_mode_10 : UINT := 0;
    BRIC_1_di_mode_11 : UINT := 0;
    BRIC_1_di_mode_12 : UINT := 0;
    BRIC_1_di_mode_13 : UINT := 0;
    BRIC_1_di_mode_14 : UINT := 0;
    BRIC_1_di_mode_15 : UINT := 0;
    BRIC_1_di_state : UDINT := 0;
    BRIC_1_di_cnt_0 : ULINT := 0;
    BRIC_1_di_cnt_1 : ULINT := 0;
    BRIC_1_di_cnt_2 : ULINT := 0;
    BRIC_1_di_cnt_3 : ULINT := 0;
    BRIC_1_di_cnt_4 : ULINT := 0;
    BRIC_1_di_cnt_5 : ULINT := 0;
    BRIC_1_di_cnt_6 : ULINT := 0;
    BRIC_1_di_cnt_7 : ULINT := 0;
    BRIC_1_di_cnt_8 : ULINT := 0;
    BRIC_1_di_cnt_9 : ULINT := 0;
    BRIC_1_di_cnt_10 : ULINT := 0;
    BRIC_1_di_cnt_11 : ULINT := 0;
    BRIC_1_di_cnt_12 : ULINT := 0;
    BRIC_1_di_cnt_13 : ULINT := 0;
    BRIC_1_di_cnt_14 : ULINT := 0;
    BRIC_1_di_cnt_15 : ULINT := 0;
    BRIC_1_di_freq_0 : REAL := 0;
    BRIC_1_di_freq_1 : REAL := 0;
    BRIC_1_di_freq_2 : REAL := 0;
    BRIC_1_di_freq_3 : REAL := 0;
    BRIC_1_di_freq_4 : REAL := 0;
    BRIC_1_di_freq_5 : REAL := 0;
    BRIC_1_di_freq_6 : REAL := 0;
    BRIC_1_di_freq_7 : REAL := 0;
    BRIC_1_di_freq_8 : REAL := 0;
    BRIC_1_di_freq_9 : REAL := 0;
    BRIC_1_di_freq_10 : REAL := 0;
    BRIC_1_di_freq_11 : REAL := 0;
    BRIC_1_di_freq_12 : REAL := 0;
    BRIC_1_di_freq_13 : REAL := 0;
    BRIC_1_di_freq_14 : REAL := 0;
    BRIC_1_di_freq_15 : REAL := 0;
    BRIC_1_ai_unit_0 : UINT := 0;
    BRIC_1_ai_unit_1 : UINT := 0;
    BRIC_1_ai_unit_2 : UINT := 0;
    BRIC_1_ai_unit_3 : UINT := 0;
    BRIC_1_ai_unit_4 : UINT := 0;
    BRIC_1_ai_unit_5 : UINT := 0;
    BRIC_1_ai_unit_6 : UINT := 0;
    BRIC_1_ai_unit_7 : UINT := 0;
    BRIC_1_ai_state : UINT := 0;
    BRIC_1_internal_temp : REAL := 0;
    BRIC_1_external_temp : REAL := 0;
    BRIC_1_v_pwr : REAL := 0;
    BRIC_1_v_bat : REAL := 0;
    BRIC_1_sys_tick_counter : ULINT := 0;
    BRIC_1_unix_time_sec : DINT := 0;
    BRIC_1_isol_pwr_state : UINT := 0;
    BRIC_1_command : UINT := 0;
    BRIC_1_num_of_vars : UINT := 0;
    BRIC_1_current_os : UINT := 0;
    BRIC_1_module_number : UINT := 0;
    BRIC_1_local_ip_0 : USINT := 0;
    BRIC_1_local_ip_1 : USINT := 0;
    BRIC_1_local_ip_2 : USINT := 0;
    BRIC_1_local_ip_3 : USINT := 0;
    BRIC_1_local_netmask_0 : USINT := 0;
    BRIC_1_local_netmask_1 : USINT := 0;
    BRIC_1_local_netmask_2 : USINT := 0;
    BRIC_1_local_netmask_3 : USINT := 0;
    BRIC_1_local_gateaway_0 : USINT := 0;
    BRIC_1_local_gateaway_1 : USINT := 0;
    BRIC_1_local_gateaway_2 : USINT := 0;
    BRIC_1_local_gateaway_3 : USINT := 0;
    BRIC_1_ai_calib_a_0 : REAL := 0;
    BRIC_1_ai_calib_a_1 : REAL := 0;
    BRIC_1_ai_calib_a_2 : REAL := 0;
    BRIC_1_ai_calib_a_3 : REAL := 0;
    BRIC_1_ai_calib_a_4 : REAL := 0;
    BRIC_1_ai_calib_a_5 : REAL := 0;
    BRIC_1_ai_calib_a_6 : REAL := 0;
    BRIC_1_ai_calib_a_7 : REAL := 0;
    BRIC_1_ai_calib_b_0 : REAL := 0;
    BRIC_1_ai_calib_b_1 : REAL := 0;
    BRIC_1_ai_calib_b_2 : REAL := 0;
    BRIC_1_ai_calib_b_3 : REAL := 0;
    BRIC_1_ai_calib_b_4 : REAL := 0;
    BRIC_1_ai_calib_b_5 : REAL := 0;
    BRIC_1_ai_calib_b_6 : REAL := 0;
    BRIC_1_ai_calib_b_7 : REAL := 0;
    BRIC_1_ai_physical_0 : REAL := 0;
    BRIC_1_ai_physical_1 : REAL := 0;
    BRIC_1_ai_physical_2 : REAL := 0;
    BRIC_1_ai_physical_3 : REAL := 0;
    BRIC_1_ai_physical_4 : REAL := 0;
    BRIC_1_ai_physical_5 : REAL := 0;
    BRIC_1_ai_physical_6 : REAL := 0;
    BRIC_1_ai_physical_7 : REAL := 0;
    DO_1_mdb_addr : UINT := 0;
    DO_1_device_type : USINT := 0;
    DO_1_board_ver : USINT := 0;
    DO_1_module_number : UINT := 0;
    DO_1_num_of_vars : UINT := 0;
    DO_1_ip_0 : USINT := 0;
    DO_1_ip_1 : USINT := 0;
    DO_1_ip_2 : USINT := 0;
    DO_1_ip_3 : USINT := 0;
    DO_1_netmask_0 : USINT := 0;
    DO_1_netmask_1 : USINT := 0;
    DO_1_netmask_2 : USINT := 0;
    DO_1_netmask_3 : USINT := 0;
    DO_1_gateaway_0 : USINT := 0;
    DO_1_gateaway_1 : USINT := 0;
    DO_1_gateaway_2 : USINT := 0;
    DO_1_gateaway_3 : USINT := 0;
    DO_1_usb_local_ip_0 : USINT := 0;
    DO_1_usb_local_ip_1 : USINT := 0;
    DO_1_usb_local_ip_2 : USINT := 0;
    DO_1_usb_local_ip_3 : USINT := 0;
    DO_1_reset_num : UINT := 0;
    DO_1_last_reset : UINT := 0;
    DO_1_sys_tick_counter : ULINT := 0;
    DO_1_unix_time_sec : DINT := 0;
    DO_1_os_version_0 : USINT := 0;
    DO_1_os_version_1 : USINT := 0;
    DO_1_os_version_2 : USINT := 0;
    DO_1_os_version_3 : USINT := 0;
    DO_1_mac_addr_0 : USINT := 0;
    DO_1_mac_addr_1 : USINT := 0;
    DO_1_mac_addr_2 : USINT := 0;
    DO_1_mac_addr_3 : USINT := 0;
    DO_1_mac_addr_4 : USINT := 0;
    DO_1_mac_addr_5 : USINT := 0;
    DO_1_internal_temp : REAL := 0;
    DO_1_v_pwr : REAL := 0;
    DO_1_v_bat : REAL := 0;
    DO_1_do_test_result : UDINT := 0;
    DO_1_sofi_test_result : UDINT := 0;
    DO_1_sofi_test_blocks : UDINT := 0;
    DO_1_run_test : UDINT := 0;
    DO_1_state : UDINT := 0;
    DO_1_command : UINT := 0;
    DO_1_uart1_sets : UINT := 0;
    DO_1_uart3_sets : UINT := 0;
    DO_1_do_state : UINT := 0;
    DO_1_do_sc_ctrl : UINT := 0;
    DO_1_do_ctrl : UINT := 0;
    DO_1_do_pwm_freq : UINT := 0;
    DO_1_do_pwm_ctrl_0 : UINT := 0;
    DO_1_do_pwm_ctrl_1 : UINT := 0;
    DO_1_do_pwm_ctrl_2 : UINT := 0;
    DO_1_do_pwm_ctrl_3 : UINT := 0;
    DO_1_do_pwm_ctrl_4 : UINT := 0;
    DO_1_do_pwm_ctrl_5 : UINT := 0;
    DO_1_do_pwm_ctrl_6 : UINT := 0;
    DO_1_do_pwm_ctrl_7 : UINT := 0;
    DO_1_isol_pwr_state : UINT := 0;
    AO_1_mdb_addr : UINT := 0;
    AO_1_device_type : USINT := 0;
    AO_1_board_ver : USINT := 0;
    AO_1_module_number : UINT := 0;
    AO_1_num_of_vars : UINT := 0;
    AO_1_ip_0 : USINT := 0;
    AO_1_ip_1 : USINT := 0;
    AO_1_ip_2 : USINT := 0;
    AO_1_ip_3 : USINT := 0;
    AO_1_netmask_0 : USINT := 0;
    AO_1_netmask_1 : USINT := 0;
    AO_1_netmask_2 : USINT := 0;
    AO_1_netmask_3 : USINT := 0;
    AO_1_gateaway_0 : USINT := 0;
    AO_1_gateaway_1 : USINT := 0;
    AO_1_gateaway_2 : USINT := 0;
    AO_1_gateaway_3 : USINT := 0;
    AO_1_usb_local_ip_0 : USINT := 0;
    AO_1_usb_local_ip_1 : USINT := 0;
    AO_1_usb_local_ip_2 : USINT := 0;
    AO_1_usb_local_ip_3 : USINT := 0;
    AO_1_reset_num : UINT := 0;
    AO_1_last_reset : UINT := 0;
    AO_1_sys_tick_counter : ULINT := 0;
    AO_1_unix_time_sec : DINT := 0;
    AO_1_os_version_0 : USINT := 0;
    AO_1_os_version_1 : USINT := 0;
    AO_1_os_version_2 : USINT := 0;
    AO_1_os_version_3 : USINT := 0;
    AO_1_mac_addr_0 : USINT := 0;
    AO_1_mac_addr_1 : USINT := 0;
    AO_1_mac_addr_2 : USINT := 0;
    AO_1_mac_addr_3 : USINT := 0;
    AO_1_mac_addr_4 : USINT := 0;
    AO_1_mac_addr_5 : USINT := 0;
    AO_1_internal_temp : REAL := 0;
    AO_1_v_pwr : REAL := 0;
    AO_1_v_bat : REAL := 0;
    AO_1_ao_test_result : UDINT := 0;
    AO_1_sofi_test_result : UDINT := 0;
    AO_1_sofi_test_blocks : UDINT := 0;
    AO_1_run_test : UDINT := 0;
    AO_1_state : UDINT := 0;
    AO_1_command : UINT := 0;
    AO_1_uart1_sets : UINT := 0;
    AO_1_uart3_sets : UINT := 0;
    AO_1_ao_val_0 : UINT := 0;
    AO_1_ao_val_1 : UINT := 0;
    AO_1_ao_val_2 : UINT := 0;
    AO_1_ao_val_3 : UINT := 0;
    AO_1_ao_state_0 : USINT := 0;
    AO_1_ao_state_1 : USINT := 0;
    AO_1_ao_state_2 : USINT := 0;
    AO_1_ao_state_3 : USINT := 0;
    AO_1_ao_config_0 : USINT := 0;
    AO_1_ao_config_1 : USINT := 0;
    AO_1_ao_config_2 : USINT := 0;
    AO_1_ao_config_3 : USINT := 0;
    AO_1_ao_calib_a_0 : REAL := 0;
    AO_1_ao_calib_a_1 : REAL := 0;
    AO_1_ao_calib_a_2 : REAL := 0;
    AO_1_ao_calib_a_3 : REAL := 0;
    AO_1_ao_calib_b_0 : REAL := 0;
    AO_1_ao_calib_b_1 : REAL := 0;
    AO_1_ao_calib_b_2 : REAL := 0;
    AO_1_ao_calib_b_3 : REAL := 0;
    AO_1_ao_physical_0 : REAL := 0;
    AO_1_ao_physical_1 : REAL := 0;
    AO_1_ao_physical_2 : REAL := 0;
    AO_1_ao_physical_3 : REAL := 0;
    AO_1_isol_pwr_state : UINT := 0;
    AI_1_mdb_addr : UINT := 0;
    AI_1_device_type : USINT := 0;
    AI_1_board_ver : USINT := 0;
    AI_1_module_number : UINT := 0;
    AI_1_num_of_vars : UINT := 0;
    AI_1_ip_0 : USINT := 0;
    AI_1_ip_1 : USINT := 0;
    AI_1_ip_2 : USINT := 0;
    AI_1_ip_3 : USINT := 0;
    AI_1_netmask_0 : USINT := 0;
    AI_1_netmask_1 : USINT := 0;
    AI_1_netmask_2 : USINT := 0;
    AI_1_netmask_3 : USINT := 0;
    AI_1_gateaway_0 : USINT := 0;
    AI_1_gateaway_1 : USINT := 0;
    AI_1_gateaway_2 : USINT := 0;
    AI_1_gateaway_3 : USINT := 0;
    AI_1_usb_local_ip_0 : USINT := 0;
    AI_1_usb_local_ip_1 : USINT := 0;
    AI_1_usb_local_ip_2 : USINT := 0;
    AI_1_usb_local_ip_3 : USINT := 0;
    AI_1_reset_num : UINT := 0;
    AI_1_last_reset : UINT := 0;
    AI_1_sys_tick_counter : ULINT := 0;
    AI_1_unix_time_sec : DINT := 0;
    AI_1_os_version_0 : USINT := 0;
    AI_1_os_version_1 : USINT := 0;
    AI_1_os_version_2 : USINT := 0;
    AI_1_os_version_3 : USINT := 0;
    AI_1_mac_addr_0 : USINT := 0;
    AI_1_mac_addr_1 : USINT := 0;
    AI_1_mac_addr_2 : USINT := 0;
    AI_1_mac_addr_3 : USINT := 0;
    AI_1_mac_addr_4 : USINT := 0;
    AI_1_mac_addr_5 : USINT := 0;
    AI_1_internal_temp : REAL := 0;
    AI_1_v_pwr : REAL := 0;
    AI_1_v_bat : REAL := 0;
    AI_1_ai_test_result : UDINT := 0;
    AI_1_sofi_test_result : UDINT := 0;
    AI_1_sofi_test_blocks : UDINT := 0;
    AI_1_run_test : UDINT := 0;
    AI_1_state : UDINT := 0;
    AI_1_command : UINT := 0;
    AI_1_uart1_sets : UINT := 0;
    AI_1_uart3_sets : UINT := 0;
    AI_1_uart6_sets : UINT := 0;
    AI_1_ai_unit_0 : UINT := 0;
    AI_1_ai_unit_1 : UINT := 0;
    AI_1_ai_unit_2 : UINT := 0;
    AI_1_ai_unit_3 : UINT := 0;
    AI_1_ai_unit_4 : UINT := 0;
    AI_1_ai_unit_5 : UINT := 0;
    AI_1_ai_unit_6 : UINT := 0;
    AI_1_ai_unit_7 : UINT := 0;
    AI_1_ai_unit_8 : UINT := 0;
    AI_1_ai_unit_9 : UINT := 0;
    AI_1_ai_unit_10 : UINT := 0;
    AI_1_ai_unit_11 : UINT := 0;
    AI_1_ai_unit_12 : UINT := 0;
    AI_1_ai_unit_13 : UINT := 0;
    AI_1_ai_unit_14 : UINT := 0;
    AI_1_ai_unit_15 : UINT := 0;
    AI_1_ai_state : UINT := 0;
    AI_1_ai_calib_a_0 : REAL := 0;
    AI_1_ai_calib_a_1 : REAL := 0;
    AI_1_ai_calib_a_2 : REAL := 0;
    AI_1_ai_calib_a_3 : REAL := 0;
    AI_1_ai_calib_a_4 : REAL := 0;
    AI_1_ai_calib_a_5 : REAL := 0;
    AI_1_ai_calib_a_6 : REAL := 0;
    AI_1_ai_calib_a_7 : REAL := 0;
    AI_1_ai_calib_a_8 : REAL := 0;
    AI_1_ai_calib_a_9 : REAL := 0;
    AI_1_ai_calib_a_10 : REAL := 0;
    AI_1_ai_calib_a_11 : REAL := 0;
    AI_1_ai_calib_a_12 : REAL := 0;
    AI_1_ai_calib_a_13 : REAL := 0;
    AI_1_ai_calib_a_14 : REAL := 0;
    AI_1_ai_calib_a_15 : REAL := 0;
    AI_1_ai_calib_b_0 : REAL := 0;
    AI_1_ai_calib_b_1 : REAL := 0;
    AI_1_ai_calib_b_2 : REAL := 0;
    AI_1_ai_calib_b_3 : REAL := 0;
    AI_1_ai_calib_b_4 : REAL := 0;
    AI_1_ai_calib_b_5 : REAL := 0;
    AI_1_ai_calib_b_6 : REAL := 0;
    AI_1_ai_calib_b_7 : REAL := 0;
    AI_1_ai_calib_b_8 : REAL := 0;
    AI_1_ai_calib_b_9 : REAL := 0;
    AI_1_ai_calib_b_10 : REAL := 0;
    AI_1_ai_calib_b_11 : REAL := 0;
    AI_1_ai_calib_b_12 : REAL := 0;
    AI_1_ai_calib_b_13 : REAL := 0;
    AI_1_ai_calib_b_14 : REAL := 0;
    AI_1_ai_calib_b_15 : REAL := 0;
    AI_1_ai_physical_0 : REAL := 0;
    AI_1_ai_physical_1 : REAL := 0;
    AI_1_ai_physical_2 : REAL := 0;
    AI_1_ai_physical_3 : REAL := 0;
    AI_1_ai_physical_4 : REAL := 0;
    AI_1_ai_physical_5 : REAL := 0;
    AI_1_ai_physical_6 : REAL := 0;
    AI_1_ai_physical_7 : REAL := 0;
    AI_1_ai_physical_8 : REAL := 0;
    AI_1_ai_physical_9 : REAL := 0;
    AI_1_ai_physical_10 : REAL := 0;
    AI_1_ai_physical_11 : REAL := 0;
    AI_1_ai_physical_12 : REAL := 0;
    AI_1_ai_physical_13 : REAL := 0;
    AI_1_ai_physical_14 : REAL := 0;
    AI_1_ai_physical_15 : REAL := 0;
    AI_1_isol_pwr_state : UINT := 0;
    DI_1_mdb_addr : UINT := 0;
    DI_1_device_type : USINT := 0;
    DI_1_board_ver : USINT := 0;
    DI_1_module_number : UINT := 0;
    DI_1_num_of_vars : UINT := 0;
    DI_1_ip_0 : USINT := 0;
    DI_1_ip_1 : USINT := 0;
    DI_1_ip_2 : USINT := 0;
    DI_1_ip_3 : USINT := 0;
    DI_1_netmask_0 : USINT := 0;
    DI_1_netmask_1 : USINT := 0;
    DI_1_netmask_2 : USINT := 0;
    DI_1_netmask_3 : USINT := 0;
    DI_1_gateaway_0 : USINT := 0;
    DI_1_gateaway_1 : USINT := 0;
    DI_1_gateaway_2 : USINT := 0;
    DI_1_gateaway_3 : USINT := 0;
    DI_1_usb_local_ip_0 : USINT := 0;
    DI_1_usb_local_ip_1 : USINT := 0;
    DI_1_usb_local_ip_2 : USINT := 0;
    DI_1_usb_local_ip_3 : USINT := 0;
    DI_1_reset_num : UINT := 0;
    DI_1_last_reset : UINT := 0;
    DI_1_sys_tick_counter : ULINT := 0;
    DI_1_unix_time_sec : DINT := 0;
    DI_1_os_version_0 : USINT := 0;
    DI_1_os_version_1 : USINT := 0;
    DI_1_os_version_2 : USINT := 0;
    DI_1_os_version_3 : USINT := 0;
    DI_1_mac_addr_0 : USINT := 0;
    DI_1_mac_addr_1 : USINT := 0;
    DI_1_mac_addr_2 : USINT := 0;
    DI_1_mac_addr_3 : USINT := 0;
    DI_1_mac_addr_4 : USINT := 0;
    DI_1_mac_addr_5 : USINT := 0;
    DI_1_internal_temp : REAL := 0;
    DI_1_v_pwr : REAL := 0;
    DI_1_v_bat : REAL := 0;
    DI_1_di_test_result : UDINT := 0;
    DI_1_sofi_test_result : UDINT := 0;
    DI_1_sofi_test_blocks : UDINT := 0;
    DI_1_run_test : UDINT := 0;
    DI_1_state : UDINT := 0;
    DI_1_command : UINT := 0;
    DI_1_uart1_sets : UINT := 0;
    DI_1_uart3_sets : UINT := 0;
    DI_1_di_noise_fltr_us_0 : UINT := 0;
    DI_1_di_noise_fltr_us_1 : UINT := 0;
    DI_1_di_noise_fltr_us_2 : UINT := 0;
    DI_1_di_noise_fltr_us_3 : UINT := 0;
    DI_1_di_noise_fltr_us_4 : UINT := 0;
    DI_1_di_noise_fltr_us_5 : UINT := 0;
    DI_1_di_noise_fltr_us_6 : UINT := 0;
    DI_1_di_noise_fltr_us_7 : UINT := 0;
    DI_1_di_noise_fltr_us_8 : UINT := 0;
    DI_1_di_noise_fltr_us_9 : UINT := 0;
    DI_1_di_noise_fltr_us_10 : UINT := 0;
    DI_1_di_noise_fltr_us_11 : UINT := 0;
    DI_1_di_noise_fltr_us_12 : UINT := 0;
    DI_1_di_noise_fltr_us_13 : UINT := 0;
    DI_1_di_noise_fltr_us_14 : UINT := 0;
    DI_1_di_noise_fltr_us_15 : UINT := 0;
    DI_1_di_pulseless_ms_0 : UDINT := 0;
    DI_1_di_pulseless_ms_1 : UDINT := 0;
    DI_1_di_pulseless_ms_2 : UDINT := 0;
    DI_1_di_pulseless_ms_3 : UDINT := 0;
    DI_1_di_pulseless_ms_4 : UDINT := 0;
    DI_1_di_pulseless_ms_5 : UDINT := 0;
    DI_1_di_pulseless_ms_6 : UDINT := 0;
    DI_1_di_pulseless_ms_7 : UDINT := 0;
    DI_1_di_pulseless_ms_8 : UDINT := 0;
    DI_1_di_pulseless_ms_9 : UDINT := 0;
    DI_1_di_pulseless_ms_10 : UDINT := 0;
    DI_1_di_pulseless_ms_11 : UDINT := 0;
    DI_1_di_pulseless_ms_12 : UDINT := 0;
    DI_1_di_pulseless_ms_13 : UDINT := 0;
    DI_1_di_pulseless_ms_14 : UDINT := 0;
    DI_1_di_pulseless_ms_15 : UDINT := 0;
    DI_1_di_mode_0 : UINT := 0;
    DI_1_di_mode_1 : UINT := 0;
    DI_1_di_mode_2 : UINT := 0;
    DI_1_di_mode_3 : UINT := 0;
    DI_1_di_mode_4 : UINT := 0;
    DI_1_di_mode_5 : UINT := 0;
    DI_1_di_mode_6 : UINT := 0;
    DI_1_di_mode_7 : UINT := 0;
    DI_1_di_mode_8 : UINT := 0;
    DI_1_di_mode_9 : UINT := 0;
    DI_1_di_mode_10 : UINT := 0;
    DI_1_di_mode_11 : UINT := 0;
    DI_1_di_mode_12 : UINT := 0;
    DI_1_di_mode_13 : UINT := 0;
    DI_1_di_mode_14 : UINT := 0;
    DI_1_di_mode_15 : UINT := 0;
    DI_1_di_state : UDINT := 0;
    DI_1_di_cnt_0 : ULINT := 0;
    DI_1_di_cnt_1 : ULINT := 0;
    DI_1_di_cnt_2 : ULINT := 0;
    DI_1_di_cnt_3 : ULINT := 0;
    DI_1_di_cnt_4 : ULINT := 0;
    DI_1_di_cnt_5 : ULINT := 0;
    DI_1_di_cnt_6 : ULINT := 0;
    DI_1_di_cnt_7 : ULINT := 0;
    DI_1_di_cnt_8 : ULINT := 0;
    DI_1_di_cnt_9 : ULINT := 0;
    DI_1_di_cnt_10 : ULINT := 0;
    DI_1_di_cnt_11 : ULINT := 0;
    DI_1_di_cnt_12 : ULINT := 0;
    DI_1_di_cnt_13 : ULINT := 0;
    DI_1_di_cnt_14 : ULINT := 0;
    DI_1_di_cnt_15 : ULINT := 0;
    DI_1_di_freq_0 : REAL := 0;
    DI_1_di_freq_1 : REAL := 0;
    DI_1_di_freq_2 : REAL := 0;
    DI_1_di_freq_3 : REAL := 0;
    DI_1_di_freq_4 : REAL := 0;
    DI_1_di_freq_5 : REAL := 0;
    DI_1_di_freq_6 : REAL := 0;
    DI_1_di_freq_7 : REAL := 0;
    DI_1_di_freq_8 : REAL := 0;
    DI_1_di_freq_9 : REAL := 0;
    DI_1_di_freq_10 : REAL := 0;
    DI_1_di_freq_11 : REAL := 0;
    DI_1_di_freq_12 : REAL := 0;
    DI_1_di_freq_13 : REAL := 0;
    DI_1_di_freq_14 : REAL := 0;
    DI_1_di_freq_15 : REAL := 0;
    DI_1_isol_pwr_state : UINT := 0;
  END_VAR

  RESOURCE resource1 ON PLC
    TASK task0(INTERVAL := T#20ms,PRIORITY := 0);
    PROGRAM instance0 WITH task0 : program0;
  END_RESOURCE
END_CONFIGURATION
