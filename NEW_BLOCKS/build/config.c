/*******************************************/
/*     FILE GENERATED BY iec2c             */
/* Editing this file is not recommended... */
/*******************************************/

#include "iec_std_lib.h"

#include "accessor.h"

#include "POUS.h"

// CONFIGURATION CONFIG
__DECLARE_GLOBAL(UINT,CONFIG,DI_NUMBER)
__DECLARE_GLOBAL(UDINT,CONFIG,VAL)
__DECLARE_GLOBAL(UINT,CONFIG,VAL0)
__DECLARE_GLOBAL(UINT,CONFIG,VAL1)
__DECLARE_GLOBAL(USINT,CONFIG,RT1)
__DECLARE_GLOBAL(USINT,CONFIG,RT2)
__DECLARE_GLOBAL(USINT,CONFIG,RT3)
__DECLARE_GLOBAL(USINT,CONFIG,RT4)
__DECLARE_GLOBAL(USINT,CONFIG,RT5)
__DECLARE_GLOBAL(USINT,CONFIG,RT6)
__DECLARE_GLOBAL(USINT,CONFIG,RT7)
__DECLARE_GLOBAL(USINT,CONFIG,RT8)
__DECLARE_GLOBAL(UINT,CONFIG,RT9)
__DECLARE_GLOBAL(UINT,CONFIG,CTRL)
__DECLARE_GLOBAL(USINT,CONFIG,NUM)
__DECLARE_GLOBAL(UINT,CONFIG,ADR)
__DECLARE_GLOBAL(UINT,CONFIG,U1)
__DECLARE_GLOBAL(UINT,CONFIG,U2)
__DECLARE_GLOBAL(UINT,CONFIG,U3)
__DECLARE_GLOBAL(UINT,CONFIG,U5)
__DECLARE_GLOBAL(UINT,CONFIG,U6)
__DECLARE_GLOBAL(UINT,CONFIG,U7)
__DECLARE_GLOBAL(UINT,CONFIG,AIS)
__DECLARE_GLOBAL(UDINT,CONFIG,TI)
__DECLARE_GLOBAL(UINT,CONFIG,WSF)
__DECLARE_GLOBAL(UINT,CONFIG,WSE)
__DECLARE_GLOBAL(UINT,CONFIG,DOPF)
__DECLARE_GLOBAL(ULINT,CONFIG,CNT)
__DECLARE_GLOBAL(REAL,CONFIG,FREQ)
__DECLARE_GLOBAL(USINT,CONFIG,DOOU)
__DECLARE_GLOBAL(REAL,CONFIG,PWR_V)
__DECLARE_GLOBAL(REAL,CONFIG,BAT_V)
__DECLARE_GLOBAL(UINT,CONFIG,RESET_NUM)
__DECLARE_GLOBAL(UINT,CONFIG,LAST_RESET)
__DECLARE_GLOBAL(ULINT,CONFIG,SYS_TICK)
__DECLARE_GLOBAL(UINT,CONFIG,DOV)
__DECLARE_GLOBAL(UINT,CONFIG,DOM)

void RESOURCE1_init__(void);

void config_init__(void) {
  BOOL retain;
  retain = 0;
  __INIT_GLOBAL(UINT,DI_NUMBER,__INITIAL_VALUE(4),retain)
  __INIT_GLOBAL(UDINT,VAL,__INITIAL_VALUE(7000),retain)
  __INIT_GLOBAL(UINT,VAL0,__INITIAL_VALUE(14),retain)
  __INIT_GLOBAL(UINT,VAL1,__INITIAL_VALUE(3),retain)
  __INIT_GLOBAL(USINT,RT1,__INITIAL_VALUE(0),retain)
  __INIT_GLOBAL(USINT,RT2,__INITIAL_VALUE(0),retain)
  __INIT_GLOBAL(USINT,RT3,__INITIAL_VALUE(0),retain)
  __INIT_GLOBAL(USINT,RT4,__INITIAL_VALUE(0),retain)
  __INIT_GLOBAL(USINT,RT5,__INITIAL_VALUE(0),retain)
  __INIT_GLOBAL(USINT,RT6,__INITIAL_VALUE(0),retain)
  __INIT_GLOBAL(USINT,RT7,__INITIAL_VALUE(0),retain)
  __INIT_GLOBAL(USINT,RT8,__INITIAL_VALUE(0),retain)
  __INIT_GLOBAL(UINT,RT9,__INITIAL_VALUE(0),retain)
  __INIT_GLOBAL(UINT,CTRL,__INITIAL_VALUE(2),retain)
  __INIT_GLOBAL(USINT,NUM,__INITIAL_VALUE(4),retain)
  __INIT_GLOBAL(UINT,ADR,__INITIAL_VALUE(4),retain)
  __INIT_GLOBAL(UINT,U1,__INITIAL_VALUE(51291),retain)
  __INIT_GLOBAL(UINT,U2,__INITIAL_VALUE(51291),retain)
  __INIT_GLOBAL(UINT,U3,__INITIAL_VALUE(89),retain)
  __INIT_GLOBAL(UINT,U5,__INITIAL_VALUE(51291),retain)
  __INIT_GLOBAL(UINT,U6,__INITIAL_VALUE(51291),retain)
  __INIT_GLOBAL(UINT,U7,__INITIAL_VALUE(51291),retain)
  __INIT_GLOBAL(UINT,AIS,__INITIAL_VALUE(0),retain)
  __INIT_GLOBAL(UDINT,TI,__INITIAL_VALUE(100),retain)
  __INIT_GLOBAL(UINT,WSF,__INITIAL_VALUE(6),retain)
  __INIT_GLOBAL(UINT,WSE,__INITIAL_VALUE(9),retain)
  __INIT_GLOBAL(UINT,DOPF,__INITIAL_VALUE(15),retain)
  __INIT_GLOBAL(ULINT,CNT,__INITIAL_VALUE(0),retain)
  __INIT_GLOBAL(REAL,FREQ,__INITIAL_VALUE(0),retain)
  __INIT_GLOBAL(USINT,DOOU,__INITIAL_VALUE(0),retain)
  __INIT_GLOBAL(REAL,PWR_V,__INITIAL_VALUE(0),retain)
  __INIT_GLOBAL(REAL,BAT_V,__INITIAL_VALUE(0),retain)
  __INIT_GLOBAL(UINT,RESET_NUM,__INITIAL_VALUE(0),retain)
  __INIT_GLOBAL(UINT,LAST_RESET,__INITIAL_VALUE(0),retain)
  __INIT_GLOBAL(ULINT,SYS_TICK,__INITIAL_VALUE(0),retain)
  __INIT_GLOBAL(UINT,DOV,__INITIAL_VALUE(11),retain)
  __INIT_GLOBAL(UINT,DOM,__INITIAL_VALUE(15),retain)
  RESOURCE1_init__();
}

void RESOURCE1_run__(unsigned long tick);

void config_run__(unsigned long tick) {
  RESOURCE1_run__(tick);
}
unsigned long long common_ticktime__ = 20000000ULL; /*ns*/
unsigned long greatest_tick_count__ = (unsigned long)0UL; /*tick*/
