/**
 * @file beremiz_regs_description.c
 * @author Shoma Gane <shomagan@gmail.com>
 *         Ayrat Girfanov <girfanov.ayrat@yandex.ru>
 * @defgroup beremiz
 * @ingroup beremiz
 * @version 0.1 
 * @brief  TODO!!! write brief in 
 */
/*
 * Copyright (c) 2018 Snema Service
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
 * SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY
 * OF SUCH DAMAGE.
 *
 * This file is part of the sofi PLC.
 *
 * Author: Shoma Gane <shomagan@gmail.com>
 *         Ayrat Girfanov <girfanov.ayrat@yandex.ru>
 */
#ifndef BEREMIZ_REGS_DESCRIPTION_C
#define BEREMIZ_REGS_DESCRIPTION_C 1
#include "beremiz_regs_description.h"
#include "regs_description.h"
#include "sofi_config.h"
#include "beremiz_task.h"
#include "bkram_access.h"
#include "matiec/iec_types_all.h"


extern u8 mdb_address_space[];
extern u8 mdb_array_address_space[];



extern __IEC_UINT_t CONFIG__DI_NUMBER;
extern __IEC_UDINT_t CONFIG__VAL;
extern __IEC_UINT_t CONFIG__VAL0;
extern __IEC_UINT_t CONFIG__VAL1;
extern __IEC_USINT_t CONFIG__RT1;
extern __IEC_USINT_t CONFIG__RT2;
extern __IEC_USINT_t CONFIG__RT3;
extern __IEC_USINT_t CONFIG__RT4;
extern __IEC_USINT_t CONFIG__RT5;
extern __IEC_USINT_t CONFIG__RT6;
extern __IEC_USINT_t CONFIG__RT7;
extern __IEC_USINT_t CONFIG__RT8;
extern __IEC_UINT_t CONFIG__RT9;
extern __IEC_UINT_t CONFIG__CTRL;
extern __IEC_USINT_t CONFIG__NUM;
extern __IEC_UINT_t CONFIG__ADR;
extern __IEC_UINT_t CONFIG__U1;
extern __IEC_UINT_t CONFIG__U2;
extern __IEC_UINT_t CONFIG__U3;
extern __IEC_UINT_t CONFIG__U5;
extern __IEC_UINT_t CONFIG__U6;
extern __IEC_UINT_t CONFIG__U7;
extern __IEC_UINT_t CONFIG__AIS;
extern __IEC_UDINT_t CONFIG__TI;
extern __IEC_UINT_t CONFIG__WSF;
extern __IEC_UINT_t CONFIG__WSE;
extern __IEC_UINT_t CONFIG__DOPF;
extern __IEC_ULINT_t CONFIG__CNT;
extern __IEC_REAL_t CONFIG__FREQ;
extern __IEC_USINT_t CONFIG__DOOU;
extern __IEC_REAL_t CONFIG__PWR_V;
extern __IEC_REAL_t CONFIG__BAT_V;
extern __IEC_UINT_t CONFIG__RESET_NUM;
extern __IEC_UINT_t CONFIG__LAST_RESET;
extern __IEC_ULINT_t CONFIG__SYS_TICK;
extern __IEC_UINT_t CONFIG__DOV;
extern __IEC_UINT_t CONFIG__DOM;


regs_description_t const beremiz_regs_description[BEREMIZ_REGS_VAR_NUM]={
    
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_UINT_t *)(&CONFIG__DI_NUMBER))->value,/*saves address*/0,/*description*/"DI_NUMBER",/*name*/"DI_NUMBER",/*type*/U16_REGS_FLAG,/*ind*/0  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_UDINT_t *)(&CONFIG__VAL))->value,/*saves address*/0,/*description*/"VAL",/*name*/"VAL",/*type*/U32_REGS_FLAG,/*ind*/1  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_UINT_t *)(&CONFIG__VAL0))->value,/*saves address*/0,/*description*/"VAL0",/*name*/"VAL0",/*type*/U16_REGS_FLAG,/*ind*/2  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_UINT_t *)(&CONFIG__VAL1))->value,/*saves address*/0,/*description*/"VAL1",/*name*/"VAL1",/*type*/U16_REGS_FLAG,/*ind*/3  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_USINT_t *)(&CONFIG__RT1))->value,/*saves address*/0,/*description*/"RT1",/*name*/"RT1",/*type*/U8_REGS_FLAG,/*ind*/4  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_USINT_t *)(&CONFIG__RT2))->value,/*saves address*/0,/*description*/"RT2",/*name*/"RT2",/*type*/U8_REGS_FLAG,/*ind*/5  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_USINT_t *)(&CONFIG__RT3))->value,/*saves address*/0,/*description*/"RT3",/*name*/"RT3",/*type*/U8_REGS_FLAG,/*ind*/6  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_USINT_t *)(&CONFIG__RT4))->value,/*saves address*/0,/*description*/"RT4",/*name*/"RT4",/*type*/U8_REGS_FLAG,/*ind*/7  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_USINT_t *)(&CONFIG__RT5))->value,/*saves address*/0,/*description*/"RT5",/*name*/"RT5",/*type*/U8_REGS_FLAG,/*ind*/8  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_USINT_t *)(&CONFIG__RT6))->value,/*saves address*/0,/*description*/"RT6",/*name*/"RT6",/*type*/U8_REGS_FLAG,/*ind*/9  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_USINT_t *)(&CONFIG__RT7))->value,/*saves address*/0,/*description*/"RT7",/*name*/"RT7",/*type*/U8_REGS_FLAG,/*ind*/10  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_USINT_t *)(&CONFIG__RT8))->value,/*saves address*/0,/*description*/"RT8",/*name*/"RT8",/*type*/U8_REGS_FLAG,/*ind*/11  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_UINT_t *)(&CONFIG__RT9))->value,/*saves address*/0,/*description*/"RT9",/*name*/"RT9",/*type*/U16_REGS_FLAG,/*ind*/12  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_UINT_t *)(&CONFIG__CTRL))->value,/*saves address*/0,/*description*/"CTRL",/*name*/"CTRL",/*type*/U16_REGS_FLAG,/*ind*/13  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_USINT_t *)(&CONFIG__NUM))->value,/*saves address*/0,/*description*/"NUM",/*name*/"NUM",/*type*/U8_REGS_FLAG,/*ind*/14  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_UINT_t *)(&CONFIG__ADR))->value,/*saves address*/0,/*description*/"ADR",/*name*/"ADR",/*type*/U16_REGS_FLAG,/*ind*/15  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_UINT_t *)(&CONFIG__U1))->value,/*saves address*/0,/*description*/"U1",/*name*/"U1",/*type*/U16_REGS_FLAG,/*ind*/16  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_UINT_t *)(&CONFIG__U2))->value,/*saves address*/0,/*description*/"U2",/*name*/"U2",/*type*/U16_REGS_FLAG,/*ind*/17  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_UINT_t *)(&CONFIG__U3))->value,/*saves address*/0,/*description*/"U3",/*name*/"U3",/*type*/U16_REGS_FLAG,/*ind*/18  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_UINT_t *)(&CONFIG__U5))->value,/*saves address*/0,/*description*/"U5",/*name*/"U5",/*type*/U16_REGS_FLAG,/*ind*/19  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_UINT_t *)(&CONFIG__U6))->value,/*saves address*/0,/*description*/"U6",/*name*/"U6",/*type*/U16_REGS_FLAG,/*ind*/20  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_UINT_t *)(&CONFIG__U7))->value,/*saves address*/0,/*description*/"U7",/*name*/"U7",/*type*/U16_REGS_FLAG,/*ind*/21  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_UINT_t *)(&CONFIG__AIS))->value,/*saves address*/0,/*description*/"AIS",/*name*/"AIS",/*type*/U16_REGS_FLAG,/*ind*/22  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_UDINT_t *)(&CONFIG__TI))->value,/*saves address*/0,/*description*/"TI",/*name*/"TI",/*type*/U32_REGS_FLAG,/*ind*/23  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_UINT_t *)(&CONFIG__WSF))->value,/*saves address*/0,/*description*/"WSF",/*name*/"WSF",/*type*/U16_REGS_FLAG,/*ind*/24  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_UINT_t *)(&CONFIG__WSE))->value,/*saves address*/0,/*description*/"WSE",/*name*/"WSE",/*type*/U16_REGS_FLAG,/*ind*/25  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_UINT_t *)(&CONFIG__DOPF))->value,/*saves address*/0,/*description*/"DOPF",/*name*/"DOPF",/*type*/U16_REGS_FLAG,/*ind*/26  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_ULINT_t *)(&CONFIG__CNT))->value,/*saves address*/0,/*description*/"CNT",/*name*/"CNT",/*type*/U64_REGS_FLAG,/*ind*/27  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__FREQ))->value,/*saves address*/0,/*description*/"FREQ",/*name*/"FREQ",/*type*/FLOAT_REGS_FLAG,/*ind*/28  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_USINT_t *)(&CONFIG__DOOU))->value,/*saves address*/0,/*description*/"DOOU",/*name*/"DOOU",/*type*/U8_REGS_FLAG,/*ind*/29  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__PWR_V))->value,/*saves address*/0,/*description*/"PWR_V",/*name*/"PWR_V",/*type*/FLOAT_REGS_FLAG,/*ind*/30  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__BAT_V))->value,/*saves address*/0,/*description*/"BAT_V",/*name*/"BAT_V",/*type*/FLOAT_REGS_FLAG,/*ind*/31  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_UINT_t *)(&CONFIG__RESET_NUM))->value,/*saves address*/0,/*description*/"RESET_NUM",/*name*/"RESET_NUM",/*type*/U16_REGS_FLAG,/*ind*/32  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_UINT_t *)(&CONFIG__LAST_RESET))->value,/*saves address*/0,/*description*/"LAST_RESET",/*name*/"LAST_RESET",/*type*/U16_REGS_FLAG,/*ind*/33  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_ULINT_t *)(&CONFIG__SYS_TICK))->value,/*saves address*/0,/*description*/"SYS_TICK",/*name*/"SYS_TICK",/*type*/U64_REGS_FLAG,/*ind*/34  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_UINT_t *)(&CONFIG__DOV))->value,/*saves address*/0,/*description*/"DOV",/*name*/"DOV",/*type*/U16_REGS_FLAG,/*ind*/35  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_UINT_t *)(&CONFIG__DOM))->value,/*saves address*/0,/*description*/"DOM",/*name*/"DOM",/*type*/U16_REGS_FLAG,/*ind*/36  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS},

};
#endif//BEREMIZ_REGS_DESCRIPTION_C
