/*
 * DEBUGGER code
 * 
 * On "publish", when buffer is free, debugger stores arbitrary variables 
 * content into, and mark this buffer as filled
 * 
 * 
 * Buffer content is read asynchronously, (from non real time part), 
 * and then buffer marked free again.
 *  
 * 
 * */

#ifdef TARGET_DEBUG_DISABLE

void __init_debug    (void){}
void __cleanup_debug (void){}
void __retrieve_debug(void){}
void __publish_debug (void){}

#else
#include "regs_description.h"
#include "sofi_config.h"
#include "beremiz_task.h"
#include "bkram_access.h"
#include "matiec/iec_types_all.h"
#include "POUS.h"
/*for memcpy*/
#include <string.h>
#include <stdio.h>

#define BUFFER_SIZE 12
/* Atomically accessed variable for buffer state */
#define BUFFER_FREE 0
#define BUFFER_BUSY 1
static long buffer_state = BUFFER_FREE;

/* The buffer itself */
char debug_buffer[BUFFER_SIZE];

/* Buffer's cursor*/
static char* buffer_cursor = debug_buffer;
static unsigned int retain_offset = RETAIN_BKRAM_START;
/***
 * Declare programs 
 **/
extern PROGRAM0 RESOURCE1__INSTANCE0;

/***
 * Declare global variables from resources and conf 
 **/
extern __IEC_UINT_t CONFIG__DI_NUMBER;
extern __IEC_UDINT_t CONFIG__VAL;
extern __IEC_UINT_t CONFIG__VAL0;
extern __IEC_UINT_t CONFIG__VAL1;
extern __IEC_USINT_t CONFIG__RT1;
extern __IEC_USINT_t CONFIG__RT2;
extern __IEC_USINT_t CONFIG__RT3;
extern __IEC_USINT_t CONFIG__RT4;
extern __IEC_USINT_t CONFIG__RT5;
extern __IEC_USINT_t CONFIG__RT6;
extern __IEC_USINT_t CONFIG__RT7;
extern __IEC_USINT_t CONFIG__RT8;
extern __IEC_UINT_t CONFIG__RT9;
extern __IEC_UINT_t CONFIG__CTRL;
extern __IEC_USINT_t CONFIG__NUM;
extern __IEC_UINT_t CONFIG__ADR;
extern __IEC_UINT_t CONFIG__U1;
extern __IEC_UINT_t CONFIG__U2;
extern __IEC_UINT_t CONFIG__U3;
extern __IEC_UINT_t CONFIG__U5;
extern __IEC_UINT_t CONFIG__U6;
extern __IEC_UINT_t CONFIG__U7;
extern __IEC_UINT_t CONFIG__AIS;
extern __IEC_UDINT_t CONFIG__TI;
extern __IEC_UINT_t CONFIG__WSF;
extern __IEC_UINT_t CONFIG__WSE;
extern __IEC_UINT_t CONFIG__DOPF;
extern __IEC_ULINT_t CONFIG__CNT;
extern __IEC_REAL_t CONFIG__FREQ;
extern __IEC_USINT_t CONFIG__DOOU;
extern __IEC_REAL_t CONFIG__PWR_V;
extern __IEC_REAL_t CONFIG__BAT_V;
extern __IEC_UINT_t CONFIG__RESET_NUM;
extern __IEC_UINT_t CONFIG__LAST_RESET;
extern __IEC_ULINT_t CONFIG__SYS_TICK;
extern __IEC_UINT_t CONFIG__DOV;
extern __IEC_UINT_t CONFIG__DOM;
extern       PROGRAM0   RESOURCE1__INSTANCE0;

typedef const struct {
    void *ptr;
    __IEC_types_enum type;
    u16 size_in_byte;
} dbgvardsc_t;

static dbgvardsc_t dbgvardsc[] = {
{&(CONFIG__DI_NUMBER), UINT_ENUM, 0},
{&(CONFIG__VAL), UDINT_ENUM, 0},
{&(CONFIG__VAL0), UINT_ENUM, 0},
{&(CONFIG__VAL1), UINT_ENUM, 0},
{&(CONFIG__RT1), USINT_ENUM, 0},
{&(CONFIG__RT2), USINT_ENUM, 0},
{&(CONFIG__RT3), USINT_ENUM, 0},
{&(CONFIG__RT4), USINT_ENUM, 0},
{&(CONFIG__RT5), USINT_ENUM, 0},
{&(CONFIG__RT6), USINT_ENUM, 0},
{&(CONFIG__RT7), USINT_ENUM, 0},
{&(CONFIG__RT8), USINT_ENUM, 0},
{&(CONFIG__RT9), UINT_ENUM, 0},
{&(CONFIG__CTRL), UINT_ENUM, 0},
{&(CONFIG__NUM), USINT_ENUM, 0},
{&(CONFIG__ADR), UINT_ENUM, 0},
{&(CONFIG__U1), UINT_ENUM, 0},
{&(CONFIG__U2), UINT_ENUM, 0},
{&(CONFIG__U3), UINT_ENUM, 0},
{&(CONFIG__U5), UINT_ENUM, 0},
{&(CONFIG__U6), UINT_ENUM, 0},
{&(CONFIG__U7), UINT_ENUM, 0},
{&(CONFIG__AIS), UINT_ENUM, 0},
{&(CONFIG__TI), UDINT_ENUM, 0},
{&(CONFIG__WSF), UINT_ENUM, 0},
{&(CONFIG__WSE), UINT_ENUM, 0},
{&(CONFIG__DOPF), UINT_ENUM, 0},
{&(CONFIG__CNT), ULINT_ENUM, 0},
{&(CONFIG__FREQ), REAL_ENUM, 0},
{&(CONFIG__DOOU), USINT_ENUM, 0},
{&(CONFIG__PWR_V), REAL_ENUM, 0},
{&(CONFIG__BAT_V), REAL_ENUM, 0},
{&(CONFIG__RESET_NUM), UINT_ENUM, 0},
{&(CONFIG__LAST_RESET), UINT_ENUM, 0},
{&(CONFIG__SYS_TICK), ULINT_ENUM, 0},
{&(CONFIG__DOV), UINT_ENUM, 0},
{&(CONFIG__DOM), UINT_ENUM, 0},
{&(RESOURCE1__INSTANCE0.WRITE_DI_PULSELESS0.EN), BOOL_ENUM, 0},
{&(RESOURCE1__INSTANCE0.WRITE_DI_PULSELESS0.ENO), BOOL_ENUM, 0},
{&(RESOURCE1__INSTANCE0.WRITE_DI_PULSELESS0.DI_NUMBER), UINT_ENUM, 0},
{&(RESOURCE1__INSTANCE0.WRITE_DI_PULSELESS0.DI_PULSELESS_VALUE), UDINT_ENUM, 0},
{&(RESOURCE1__INSTANCE0.WRITE_DI_MODE0.EN), BOOL_ENUM, 0},
{&(RESOURCE1__INSTANCE0.WRITE_DI_MODE0.ENO), BOOL_ENUM, 0},
{&(RESOURCE1__INSTANCE0.WRITE_DI_MODE0.DI_NUMBER), UINT_ENUM, 0},
{&(RESOURCE1__INSTANCE0.WRITE_DI_MODE0.DI_MODE_VALUE), UINT_ENUM, 0},
{&(RESOURCE1__INSTANCE0.WRITE_UART_SETS0.EN), BOOL_ENUM, 0},
{&(RESOURCE1__INSTANCE0.WRITE_UART_SETS0.ENO), BOOL_ENUM, 0},
{&(RESOURCE1__INSTANCE0.WRITE_UART_SETS0.MESO_UART), UINT_ENUM, 0},
{&(RESOURCE1__INSTANCE0.WRITE_UART_SETS0.SET_RS_485_2), UINT_ENUM, 0},
{&(RESOURCE1__INSTANCE0.WRITE_UART_SETS0.SET_RS_232), UINT_ENUM, 0},
{&(RESOURCE1__INSTANCE0.WRITE_UART_SETS0.SET_RS_485_1), UINT_ENUM, 0},
{&(RESOURCE1__INSTANCE0.WRITE_UART_SETS0.SET_RS_485_IMMO), UINT_ENUM, 0},
{&(RESOURCE1__INSTANCE0.WRITE_UART_SETS0.SET_HART), UINT_ENUM, 0},
{&(RESOURCE1__INSTANCE0.WRITE_CH_TIMEOUT0.EN), BOOL_ENUM, 0},
{&(RESOURCE1__INSTANCE0.WRITE_CH_TIMEOUT0.ENO), BOOL_ENUM, 0},
{&(RESOURCE1__INSTANCE0.WRITE_CH_TIMEOUT0.CH_NUMBER), USINT_ENUM, 0},
{&(RESOURCE1__INSTANCE0.WRITE_CH_TIMEOUT0.CHANNEL_TIMEOUT), UDINT_ENUM, 0},
{&(RESOURCE1__INSTANCE0.WRITE_DO_PWM_FREQ0.EN), BOOL_ENUM, 0},
{&(RESOURCE1__INSTANCE0.WRITE_DO_PWM_FREQ0.ENO), BOOL_ENUM, 0},
{&(RESOURCE1__INSTANCE0.WRITE_DO_PWM_FREQ0.DO_PWM_FREQ), UINT_ENUM, 0},
{&(RESOURCE1__INSTANCE0.WRITE_MDB_ADDRESS0.EN), BOOL_ENUM, 0},
{&(RESOURCE1__INSTANCE0.WRITE_MDB_ADDRESS0.ENO), BOOL_ENUM, 0},
{&(RESOURCE1__INSTANCE0.WRITE_MDB_ADDRESS0.MDB_ADDR), UINT_ENUM, 0},
{&(RESOURCE1__INSTANCE0.READ_SYS_TICK_COUNTER0.EN), BOOL_ENUM, 0},
{&(RESOURCE1__INSTANCE0.READ_SYS_TICK_COUNTER0.ENO), BOOL_ENUM, 0},
{&(RESOURCE1__INSTANCE0.READ_SYS_TICK_COUNTER0.SYS_TICK_COUNTER_VALUE), ULINT_ENUM, 0},
{&(RESOURCE1__INSTANCE0.READ_AI_STATE0.EN), BOOL_ENUM, 0},
{&(RESOURCE1__INSTANCE0.READ_AI_STATE0.ENO), BOOL_ENUM, 0},
{&(RESOURCE1__INSTANCE0.READ_AI_STATE0.AI_STATE_VALUE), UINT_ENUM, 0},
{&(RESOURCE1__INSTANCE0.READ_RESET0.EN), BOOL_ENUM, 0},
{&(RESOURCE1__INSTANCE0.READ_RESET0.ENO), BOOL_ENUM, 0},
{&(RESOURCE1__INSTANCE0.READ_RESET0.RESET_NUM), UINT_ENUM, 0},
{&(RESOURCE1__INSTANCE0.READ_RESET0.LAST_RESET), UINT_ENUM, 0},
{&(RESOURCE1__INSTANCE0.READ_PWR0.EN), BOOL_ENUM, 0},
{&(RESOURCE1__INSTANCE0.READ_PWR0.ENO), BOOL_ENUM, 0},
{&(RESOURCE1__INSTANCE0.READ_PWR0.V_PWR), REAL_ENUM, 0},
{&(RESOURCE1__INSTANCE0.READ_PWR0.V_BAT), REAL_ENUM, 0},
{&(RESOURCE1__INSTANCE0.READ_DO0.EN), BOOL_ENUM, 0},
{&(RESOURCE1__INSTANCE0.READ_DO0.ENO), BOOL_ENUM, 0},
{&(RESOURCE1__INSTANCE0.READ_DO0.DO_OUT), USINT_ENUM, 0},
{&(RESOURCE1__INSTANCE0.READ_DI_FREQ0.EN), BOOL_ENUM, 0},
{&(RESOURCE1__INSTANCE0.READ_DI_FREQ0.ENO), BOOL_ENUM, 0},
{&(RESOURCE1__INSTANCE0.READ_DI_FREQ0.DI_NUMBER), UINT_ENUM, 0},
{&(RESOURCE1__INSTANCE0.READ_DI_FREQ0.DI_FREQ_VALUE), REAL_ENUM, 0},
{&(RESOURCE1__INSTANCE0.STRUCT_REAL_TIME0.EN), BOOL_ENUM, 0},
{&(RESOURCE1__INSTANCE0.STRUCT_REAL_TIME0.ENO), BOOL_ENUM, 0},
{&(RESOURCE1__INSTANCE0.STRUCT_REAL_TIME0.HOUR_TIME), USINT_ENUM, 0},
{&(RESOURCE1__INSTANCE0.STRUCT_REAL_TIME0.MINUTE_TIME), USINT_ENUM, 0},
{&(RESOURCE1__INSTANCE0.STRUCT_REAL_TIME0.SEC_TIME), USINT_ENUM, 0},
{&(RESOURCE1__INSTANCE0.STRUCT_REAL_TIME0.SUB_SEC_TIME), USINT_ENUM, 0},
{&(RESOURCE1__INSTANCE0.STRUCT_REAL_TIME0.WEEK_DAY_TIME), USINT_ENUM, 0},
{&(RESOURCE1__INSTANCE0.STRUCT_REAL_TIME0.MONTH_TIME), USINT_ENUM, 0},
{&(RESOURCE1__INSTANCE0.STRUCT_REAL_TIME0.DATE_TIME), USINT_ENUM, 0},
{&(RESOURCE1__INSTANCE0.STRUCT_REAL_TIME0.YEAR_TIME), USINT_ENUM, 0},
{&(RESOURCE1__INSTANCE0.STRUCT_REAL_TIME0.YEAR_DAY_TIME), UINT_ENUM, 0},
{&(RESOURCE1__INSTANCE0.READ_DO_SC0.EN), BOOL_ENUM, 0},
{&(RESOURCE1__INSTANCE0.READ_DO_SC0.ENO), BOOL_ENUM, 0},
{&(RESOURCE1__INSTANCE0.READ_DO_SC0.DO_SC_FLAG), USINT_ENUM, 0},
{&(RESOURCE1__INSTANCE0.READ_DO_SC0.DO_SC_EN), USINT_ENUM, 0},
{&(RESOURCE1__INSTANCE0.WRITE_DO0.EN), BOOL_ENUM, 0},
{&(RESOURCE1__INSTANCE0.WRITE_DO0.ENO), BOOL_ENUM, 0},
{&(RESOURCE1__INSTANCE0.WRITE_DO0.DO_VALUE), UINT_ENUM, 0},
{&(RESOURCE1__INSTANCE0.WRITE_DO0.DO_MASK), UINT_ENUM, 0},
{&(RESOURCE1__INSTANCE0.WRITE_DO_SC0.EN), BOOL_ENUM, 0},
{&(RESOURCE1__INSTANCE0.WRITE_DO_SC0.ENO), BOOL_ENUM, 0},
{&(RESOURCE1__INSTANCE0.WRITE_DO_SC0.DO_SC_FLAG), UINT_ENUM, 0},
{&(RESOURCE1__INSTANCE0.WRITE_DO_SC0.DO_SC_EN), UINT_ENUM, 0},
{&(RESOURCE1__INSTANCE0.READ_DI_CNT1.EN), BOOL_ENUM, 0},
{&(RESOURCE1__INSTANCE0.READ_DI_CNT1.ENO), BOOL_ENUM, 0},
{&(RESOURCE1__INSTANCE0.READ_DI_CNT1.DI_NUMBER), UINT_ENUM, 0},
{&(RESOURCE1__INSTANCE0.READ_DI_CNT1.DI_CNT_VALUE), ULINT_ENUM, 0},
{&(RESOURCE1__INSTANCE0.WRITE_DO_PWM_CTRL0.EN), BOOL_ENUM, 0},
{&(RESOURCE1__INSTANCE0.WRITE_DO_PWM_CTRL0.ENO), BOOL_ENUM, 0},
{&(RESOURCE1__INSTANCE0.WRITE_DO_PWM_CTRL0.DO_NUMBER), USINT_ENUM, 0},
{&(RESOURCE1__INSTANCE0.WRITE_DO_PWM_CTRL0.DO_PWM_CTRL), UINT_ENUM, 0},
{&(RESOURCE1__INSTANCE0.READ_INTERNAL_TEMP0.EN), BOOL_ENUM, 0},
{&(RESOURCE1__INSTANCE0.READ_INTERNAL_TEMP0.ENO), BOOL_ENUM, 0},
{&(RESOURCE1__INSTANCE0.READ_INTERNAL_TEMP0.INTERNAL_TEMP_OUT), REAL_ENUM, 0},
{&(RESOURCE1__INSTANCE0.DI_NUMBER), UINT_P_ENUM, 0},
{&(RESOURCE1__INSTANCE0.VAL), UDINT_P_ENUM, 0},
{&(RESOURCE1__INSTANCE0.VAL0), UINT_P_ENUM, 0},
{&(RESOURCE1__INSTANCE0.VAL1), UINT_P_ENUM, 0},
{&(RESOURCE1__INSTANCE0.RT1), USINT_P_ENUM, 0},
{&(RESOURCE1__INSTANCE0.RT2), USINT_P_ENUM, 0},
{&(RESOURCE1__INSTANCE0.RT3), USINT_P_ENUM, 0},
{&(RESOURCE1__INSTANCE0.RT4), USINT_P_ENUM, 0},
{&(RESOURCE1__INSTANCE0.RT5), USINT_P_ENUM, 0},
{&(RESOURCE1__INSTANCE0.RT6), USINT_P_ENUM, 0},
{&(RESOURCE1__INSTANCE0.RT7), USINT_P_ENUM, 0},
{&(RESOURCE1__INSTANCE0.RT8), USINT_P_ENUM, 0},
{&(RESOURCE1__INSTANCE0.RT9), UINT_P_ENUM, 0},
{&(RESOURCE1__INSTANCE0.NUM), USINT_P_ENUM, 0},
{&(RESOURCE1__INSTANCE0.CTRL), UINT_P_ENUM, 0},
{&(RESOURCE1__INSTANCE0.ADR), UINT_P_ENUM, 0},
{&(RESOURCE1__INSTANCE0.U1), UINT_P_ENUM, 0},
{&(RESOURCE1__INSTANCE0.U2), UINT_P_ENUM, 0},
{&(RESOURCE1__INSTANCE0.U3), UINT_P_ENUM, 0},
{&(RESOURCE1__INSTANCE0.U5), UINT_P_ENUM, 0},
{&(RESOURCE1__INSTANCE0.U6), UINT_P_ENUM, 0},
{&(RESOURCE1__INSTANCE0.U7), UINT_P_ENUM, 0},
{&(RESOURCE1__INSTANCE0.AIS), UINT_P_ENUM, 0},
{&(RESOURCE1__INSTANCE0.TI), UDINT_P_ENUM, 0},
{&(RESOURCE1__INSTANCE0.WSF), UINT_P_ENUM, 0},
{&(RESOURCE1__INSTANCE0.WSE), UINT_P_ENUM, 0},
{&(RESOURCE1__INSTANCE0.DOPF), UINT_P_ENUM, 0},
{&(RESOURCE1__INSTANCE0.CNT), ULINT_P_ENUM, 0},
{&(RESOURCE1__INSTANCE0.FREQ), REAL_P_ENUM, 0},
{&(RESOURCE1__INSTANCE0.DOOU), USINT_P_ENUM, 0},
{&(RESOURCE1__INSTANCE0.PWR_V), REAL_P_ENUM, 0},
{&(RESOURCE1__INSTANCE0.BAT_V), REAL_P_ENUM, 0},
{&(RESOURCE1__INSTANCE0.RESET_NUM), UINT_P_ENUM, 0},
{&(RESOURCE1__INSTANCE0.LAST_RESET), UINT_P_ENUM, 0},
{&(RESOURCE1__INSTANCE0.SYS_TICK), ULINT_P_ENUM, 0},
{&(RESOURCE1__INSTANCE0.DOV), UINT_P_ENUM, 0},
{&(RESOURCE1__INSTANCE0.DOM), UINT_P_ENUM, 0},
{&(RESOURCE1__INSTANCE0.DSF), USINT_ENUM, 0},
{&(RESOURCE1__INSTANCE0.DSE), USINT_ENUM, 0},
{&(RESOURCE1__INSTANCE0.ITO), REAL_ENUM, 0},
{&(RESOURCE1__INSTANCE0.WRITE_DI_NOISE_FLTR_10US0.EN), BOOL_ENUM, 0},
{&(RESOURCE1__INSTANCE0.WRITE_DI_NOISE_FLTR_10US0.ENO), BOOL_ENUM, 0},
{&(RESOURCE1__INSTANCE0.WRITE_DI_NOISE_FLTR_10US0.DI_NUMBER), UINT_ENUM, 0},
{&(RESOURCE1__INSTANCE0.WRITE_DI_NOISE_FLTR_10US0.DI_NOISE_FLTR_VALUE_10US), UINT_ENUM, 0},
};

typedef void(*__for_each_variable_do_fp)(dbgvardsc_t*);
void __for_each_variable_do(__for_each_variable_do_fp fp)
{
    unsigned int i;
    for(i = 0; i < sizeof(dbgvardsc)/sizeof(dbgvardsc_t); i++){
        dbgvardsc_t *dsc = &dbgvardsc[i];
        if(dsc->type != UNKNOWN_ENUM) 
            (*fp)(dsc);
    }
}

#define __Unpack_case_t(TYPENAME) \
        case TYPENAME##_ENUM :\
            *flags = ((__IEC_##TYPENAME##_t *)varp)->flags;\
            forced_value_p = *real_value_p = &((__IEC_##TYPENAME##_t *)varp)->value;\
            break;

#define __Unpack_case_p(TYPENAME)\
        case TYPENAME##_O_ENUM :\
            *flags = __IEC_OUTPUT_FLAG;\
        case TYPENAME##_P_ENUM :\
            *flags |= ((__IEC_##TYPENAME##_p *)varp)->flags;\
            *real_value_p = ((__IEC_##TYPENAME##_p *)varp)->value;\
            forced_value_p = &((__IEC_##TYPENAME##_p *)varp)->fvalue;\
            break;

void* UnpackVar(dbgvardsc_t *dsc, void **real_value_p, char *flags)
{
    void *varp = dsc->ptr;
    void *forced_value_p = NULL;
    *flags = 0;
    /* find data to copy*/
    switch(dsc->type){
        __ANY(__Unpack_case_t)
        __ANY(__Unpack_case_p)
    default:
        break;
    }
    if (*flags & __IEC_FORCE_FLAG)
        return forced_value_p;
    return *real_value_p;
}

void Remind(unsigned int offset, unsigned int count, void * p){
    p_link_functions->bkram_access_read((u16)offset,p,(u16)count);
}

void RemindIterator(dbgvardsc_t *dsc)
{
    void *real_value_p = NULL;
    char flags = 0;
    UnpackVar(dsc, &real_value_p, &flags);
    if(flags & __IEC_RETAIN_FLAG){
        USINT size ;
        if(flags & __IEC_ARRAY_FLAG){
            size = dsc->size_in_byte;
        }else{
            size = __get_type_enum_size(dsc->type);
        }
        
        /* compute next cursor positon*/
        unsigned int next_retain_offset = retain_offset + size;
        /* if buffer not full */
        Remind(retain_offset, size, real_value_p);
        /* increment cursor according size*/
        retain_offset = next_retain_offset;
    }
}

static int CheckRetainBuffer(void){
    int result = 0;

    u64 bkram_verify_number = 0;
    result = p_link_functions->bkram_access_read(BKRAM_VERIFY_ADDR_USER,(u8*)&bkram_verify_number,8);
    if(bkram_verify_number!=BKRAM_VERIFY_NUMBER_USER){
        result = -1;
    }else{
        result = 1;
    }
    return result;
}
static void InitRetain(void){

}

void __init_debug(void)
{
    /* init local static vars */
    buffer_cursor = debug_buffer;
    retain_offset = RETAIN_BKRAM_START;
    buffer_state = BUFFER_FREE;
    InitRetain();
    /* Iterate over all variables to fill debug buffer */
    if(CheckRetainBuffer()>0){
        p_link_functions->printf("retain variable");
    	__for_each_variable_do(RemindIterator);
    }else{
        p_link_functions->printf("RETAIN memory invalid - defaults used");
    }
    retain_offset = RETAIN_BKRAM_START;
}

static void InitiateDebugTransfer(void){
    return;
}
extern void CleanupRetain(void);

extern unsigned long __tick;

void __cleanup_debug(void)
{
    buffer_cursor = debug_buffer;
    InitiateDebugTransfer();
    CleanupRetain();
}

void __retrieve_debug(void)
{
}


void Retain(unsigned int offset, unsigned int count, void * p){
    p_link_functions->bkram_access_write((u16)offset,p,(u16)count);
}

static inline void BufferIterator(dbgvardsc_t *dsc, int do_debug)
{
    void *real_value_p = NULL;
    void *visible_value_p = NULL;
    char flags = 0;

    visible_value_p = UnpackVar(dsc, &real_value_p, &flags);

    if(flags & ( __IEC_DEBUG_FLAG | __IEC_RETAIN_FLAG)){
        USINT size;
        if(flags & __IEC_ARRAY_FLAG){
            size = dsc->size_in_byte;
        }else{
            size = __get_type_enum_size(dsc->type);
        }


        if(flags & __IEC_DEBUG_FLAG){
            /* copy visible variable to buffer */;
            if(do_debug){
                /* compute next cursor positon.
                   No need to check overflow, as BUFFER_SIZE
                   is computed large enough */
                if((dsc->type == STRING_ENUM)   ||
		          (dsc->type == STRING_P_ENUM) ||
		          (dsc->type == STRING_O_ENUM)){
                    /* optimization for strings */
                    size = (u8)(((STRING*)visible_value_p)->len + 1);
                }
                char* next_cursor = buffer_cursor + size;
                /* copy data to the buffer */
                memcpy(buffer_cursor, visible_value_p, size);
                /* increment cursor according size*/
                buffer_cursor = next_cursor;
            }
            /* re-force real value of outputs (M and Q)*/
            if((flags & __IEC_FORCE_FLAG) && (flags & __IEC_OUTPUT_FLAG)){
                memcpy(real_value_p, visible_value_p, size);
            }
        }
        if(flags & __IEC_RETAIN_FLAG){
            static u32 itt =0;
            /* compute next cursor positon*/
            unsigned int next_retain_offset = retain_offset + size;
            /* if buffer not full */
            Retain(retain_offset, size, real_value_p);
            /* increment cursor according size*/
            retain_offset = next_retain_offset;
        }
    }
}

void DebugIterator(dbgvardsc_t *dsc){
    BufferIterator(dsc, 1);
}

void RetainIterator(dbgvardsc_t *dsc){
    BufferIterator(dsc, 0);
}


static unsigned int retain_size = 0;

/* GetRetainSizeIterator */
void GetRetainSizeIterator(dbgvardsc_t *dsc)
{
    void *real_value_p = NULL;
    char flags = 0;
    UnpackVar(dsc, &real_value_p, &flags);

    if(flags & __IEC_RETAIN_FLAG){
        USINT size;
        if(flags & __IEC_ARRAY_FLAG){
            size = dsc->size_in_byte;
        }else{
            size = __get_type_enum_size(dsc->type);
        }
        /* Calc retain buffer size */
        retain_size += size;
    }
}

/* Return size of all retain variables */
unsigned int GetRetainSize(void)
{
    __for_each_variable_do(GetRetainSizeIterator);
    return retain_size;
}


extern void PLC_GetTime(IEC_TIME*);
static int TryEnterDebugSection(void){
    return 0;
}
static long AtomicCompareExchange(long* l, long k, long m){
    (void)l;
    (void)k;
    (void)m;
    return 0;
}
extern long long AtomicCompareExchange64(long long* , long long , long long);
static void LeaveDebugSection(void){
    return;
}
static void ValidateRetainBuffer(void){
    u64 bkram_verify_number;
    bkram_verify_number = BKRAM_VERIFY_NUMBER_USER;
    p_link_functions->bkram_access_write(BKRAM_VERIFY_ADDR_USER,(u8*)&bkram_verify_number,8);
}
extern void InValidateRetainBuffer(void){
    return;
}

void __publish_debug(void)
{
    retain_offset = RETAIN_BKRAM_START;
    InValidateRetainBuffer();
    /* Check there is no running debugger re-configuration */
    if(TryEnterDebugSection()){
        /* Lock buffer */
        long latest_state = AtomicCompareExchange(
            &buffer_state,
            BUFFER_FREE,
            BUFFER_BUSY);
            
        /* If buffer was free */
        if(latest_state == BUFFER_FREE)
        {
            /* Reset buffer cursor */
            buffer_cursor = debug_buffer;
            /* Iterate over all variables to fill debug buffer */
            __for_each_variable_do(DebugIterator);
            
            /* Leave debug section,
             * Trigger asynchronous transmission 
             * (returns immediately) */
            InitiateDebugTransfer(); /* size */
        }else{
            /* when not debugging, do only retain */
            __for_each_variable_do(RetainIterator);
        }
        LeaveDebugSection();
    }else{
        /* when not debugging, do only retain */
        __for_each_variable_do(RetainIterator);
    }
    ValidateRetainBuffer();
}

#define __RegisterDebugVariable_case_t(TYPENAME) \
        case TYPENAME##_ENUM :\
            ((__IEC_##TYPENAME##_t *)varp)->flags |= flags;\
            if(force)\
             ((__IEC_##TYPENAME##_t *)varp)->value = *((TYPENAME *)force);\
            break;
#define __RegisterDebugVariable_case_p(TYPENAME)\
        case TYPENAME##_P_ENUM :\
            ((__IEC_##TYPENAME##_p *)varp)->flags |= flags;\
            if(force)\
             ((__IEC_##TYPENAME##_p *)varp)->fvalue = *((TYPENAME *)force);\
            break;\
        case TYPENAME##_O_ENUM :\
            ((__IEC_##TYPENAME##_p *)varp)->flags |= flags;\
            if(force){\
             ((__IEC_##TYPENAME##_p *)varp)->fvalue = *((TYPENAME *)force);\
             *(((__IEC_##TYPENAME##_p *)varp)->value) = *((TYPENAME *)force);\
            }\
            break;
void RegisterDebugVariable(unsigned int idx, void* force)
{
    if(idx  < sizeof(dbgvardsc)/sizeof(dbgvardsc_t)){
        unsigned char flags = force ?
            __IEC_DEBUG_FLAG | __IEC_FORCE_FLAG :
            __IEC_DEBUG_FLAG;
        dbgvardsc_t *dsc = &dbgvardsc[idx];
        void *varp = dsc->ptr;
        switch(dsc->type){
            __ANY(__RegisterDebugVariable_case_t)
            __ANY(__RegisterDebugVariable_case_p)
        default:
            break;
        }
    }
}

#define __ResetDebugVariablesIterator_case_t(TYPENAME) \
        case TYPENAME##_ENUM :\
            ((__IEC_##TYPENAME##_t *)varp)->flags &= ~(__IEC_DEBUG_FLAG|__IEC_FORCE_FLAG);\
            break;

#define __ResetDebugVariablesIterator_case_p(TYPENAME)\
        case TYPENAME##_P_ENUM :\
        case TYPENAME##_O_ENUM :\
            ((__IEC_##TYPENAME##_p *)varp)->flags &= ~(__IEC_DEBUG_FLAG|__IEC_FORCE_FLAG);\
            break;

void ResetDebugVariablesIterator(dbgvardsc_t *dsc)
{
    /* force debug flag to 0*/
    void *varp = dsc->ptr;
    switch(dsc->type){
        __ANY(__ResetDebugVariablesIterator_case_t)
        __ANY(__ResetDebugVariablesIterator_case_p)
    default:
        break;
    }
}

void ResetDebugVariables(void)
{
    __for_each_variable_do(ResetDebugVariablesIterator);
}

void FreeDebugData(void)
{
    /* atomically mark buffer as free */
    AtomicCompareExchange(
        &buffer_state,
        BUFFER_BUSY,
        BUFFER_FREE);
}
int WaitDebugData(unsigned long *tick);
/* Wait until debug data ready and return pointer to it */
int GetDebugData(unsigned long *tick, unsigned long *size, void **buffer){
    int wait_error = WaitDebugData(tick);
    if(!wait_error){
        *size =(u32)(buffer_cursor - debug_buffer);
        *buffer = debug_buffer;
    }
    return wait_error;
}

#endif

