/**
 * @file C:\Beremiz\beremiz_test_projects\NEW_BLOCKS\build//sofi/freertos/src/POUS.c
 * @author Shoma Gane <shomagan@gmail.com>
 *         Ayrat Girfanov <girfanov.ayrat@yandex.ru>
 * @defgroup beremiz 
 * @ingroup beremiz 
 * @version 0.1 
 * @brief  TODO!!! write brief in 
 */
    /*
     * Copyright (c) 2018 Snema Service
     * All rights reserved.
     *
     * Redistribution and use in source and binary forms, with or without modification,
     * are permitted provided that the following conditions are met:
     *
     * 1. Redistributions of source code must retain the above copyright notice,
     *    this list of conditions and the following disclaimer.
     * 2. Redistributions in binary form must reproduce the above copyright notice,
     *    this list of conditions and the following disclaimer in the documentation
     *    and/or other materials provided with the distribution.
     * 3. The name of the author may not be used to endorse or promote products
     *    derived from this software without specific prior written permission.
     *
     * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
     * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
     * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
     * SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
     * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
     * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
     * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
     * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
     * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY
     * OF SUCH DAMAGE.
     *
     * This file is part of the sofi PLC.
     *
     * Author: Shoma Gane <shomagan@gmail.com>
     *         Ayrat Girfanov <girfanov.ayrat@yandex.ru>
     */
#ifndef POUS_C
#define POUS_C
#include "POUS.h"
#include "config_task.h"
#include "plc_main.h"
void PROGRAM0_init__(PROGRAM0 *data__, BOOL retain) {
  WRITE_DI_PULSELESS_init__(&data__->WRITE_DI_PULSELESS0,retain);
  WRITE_DI_MODE_init__(&data__->WRITE_DI_MODE0,retain);
  WRITE_UART_SETS_init__(&data__->WRITE_UART_SETS0,retain);
  WRITE_CH_TIMEOUT_init__(&data__->WRITE_CH_TIMEOUT0,retain);
  WRITE_DO_PWM_FREQ_init__(&data__->WRITE_DO_PWM_FREQ0,retain);
  WRITE_MDB_ADDRESS_init__(&data__->WRITE_MDB_ADDRESS0,retain);
  READ_SYS_TICK_COUNTER_init__(&data__->READ_SYS_TICK_COUNTER0,retain);
  READ_AI_STATE_init__(&data__->READ_AI_STATE0,retain);
  READ_RESET_init__(&data__->READ_RESET0,retain);
  READ_PWR_init__(&data__->READ_PWR0,retain);
  READ_DO_init__(&data__->READ_DO0,retain);
  READ_DI_FREQ_init__(&data__->READ_DI_FREQ0,retain);
  STRUCT_REAL_TIME_init__(&data__->STRUCT_REAL_TIME0,retain);
  READ_DO_SC_init__(&data__->READ_DO_SC0,retain);
  WRITE_DO_init__(&data__->WRITE_DO0,retain);
  WRITE_DO_SC_init__(&data__->WRITE_DO_SC0,retain);
  READ_DI_CNT_init__(&data__->READ_DI_CNT1,retain);
  WRITE_DO_PWM_CTRL_init__(&data__->WRITE_DO_PWM_CTRL0,retain);
  READ_INTERNAL_TEMP_init__(&data__->READ_INTERNAL_TEMP0,retain);
  __INIT_EXTERNAL(UINT,DI_NUMBER,data__->DI_NUMBER,retain)
  __INIT_EXTERNAL(UDINT,VAL,data__->VAL,retain)
  __INIT_EXTERNAL(UINT,VAL0,data__->VAL0,retain)
  __INIT_EXTERNAL(UINT,VAL1,data__->VAL1,retain)
  __INIT_EXTERNAL(USINT,RT1,data__->RT1,retain)
  __INIT_EXTERNAL(USINT,RT2,data__->RT2,retain)
  __INIT_EXTERNAL(USINT,RT3,data__->RT3,retain)
  __INIT_EXTERNAL(USINT,RT4,data__->RT4,retain)
  __INIT_EXTERNAL(USINT,RT5,data__->RT5,retain)
  __INIT_EXTERNAL(USINT,RT6,data__->RT6,retain)
  __INIT_EXTERNAL(USINT,RT7,data__->RT7,retain)
  __INIT_EXTERNAL(USINT,RT8,data__->RT8,retain)
  __INIT_EXTERNAL(UINT,RT9,data__->RT9,retain)
  __INIT_EXTERNAL(USINT,NUM,data__->NUM,retain)
  __INIT_EXTERNAL(UINT,CTRL,data__->CTRL,retain)
  __INIT_EXTERNAL(UINT,ADR,data__->ADR,retain)
  __INIT_EXTERNAL(UINT,U1,data__->U1,retain)
  __INIT_EXTERNAL(UINT,U2,data__->U2,retain)
  __INIT_EXTERNAL(UINT,U3,data__->U3,retain)
  __INIT_EXTERNAL(UINT,U5,data__->U5,retain)
  __INIT_EXTERNAL(UINT,U6,data__->U6,retain)
  __INIT_EXTERNAL(UINT,U7,data__->U7,retain)
  __INIT_EXTERNAL(UINT,AIS,data__->AIS,retain)
  __INIT_EXTERNAL(UDINT,TI,data__->TI,retain)
  __INIT_EXTERNAL(UINT,WSF,data__->WSF,retain)
  __INIT_EXTERNAL(UINT,WSE,data__->WSE,retain)
  __INIT_EXTERNAL(UINT,DOPF,data__->DOPF,retain)
  __INIT_EXTERNAL(ULINT,CNT,data__->CNT,retain)
  __INIT_EXTERNAL(REAL,FREQ,data__->FREQ,retain)
  __INIT_EXTERNAL(USINT,DOOU,data__->DOOU,retain)
  __INIT_EXTERNAL(REAL,PWR_V,data__->PWR_V,retain)
  __INIT_EXTERNAL(REAL,BAT_V,data__->BAT_V,retain)
  __INIT_EXTERNAL(UINT,RESET_NUM,data__->RESET_NUM,retain)
  __INIT_EXTERNAL(UINT,LAST_RESET,data__->LAST_RESET,retain)
  __INIT_EXTERNAL(ULINT,SYS_TICK,data__->SYS_TICK,retain)
  __INIT_EXTERNAL(UINT,DOV,data__->DOV,retain)
  __INIT_EXTERNAL(UINT,DOM,data__->DOM,retain)
  __INIT_VAR(data__->DSF,0,retain)
  __INIT_VAR(data__->DSE,0,retain)
  __INIT_VAR(data__->ITO,0,retain)
  WRITE_DI_NOISE_FLTR_10US_init__(&data__->WRITE_DI_NOISE_FLTR_10US0,retain);
}

// Code part
void PROGRAM0_body__(PROGRAM0 *data__) {
  // Initialise TEMP variables

  READ_DO_body__(&data__->READ_DO0);
  __SET_EXTERNAL(data__->,DOOU,,__GET_VAR(data__->READ_DO0.DO_OUT,));
  READ_PWR_body__(&data__->READ_PWR0);
  __SET_EXTERNAL(data__->,PWR_V,,__GET_VAR(data__->READ_PWR0.V_PWR,));
  __SET_EXTERNAL(data__->,BAT_V,,__GET_VAR(data__->READ_PWR0.V_BAT,));
  READ_RESET_body__(&data__->READ_RESET0);
  __SET_EXTERNAL(data__->,RESET_NUM,,__GET_VAR(data__->READ_RESET0.RESET_NUM,));
  __SET_EXTERNAL(data__->,LAST_RESET,,__GET_VAR(data__->READ_RESET0.LAST_RESET,));
  STRUCT_REAL_TIME_body__(&data__->STRUCT_REAL_TIME0);
  __SET_EXTERNAL(data__->,RT1,,__GET_VAR(data__->STRUCT_REAL_TIME0.HOUR_TIME,));
  READ_AI_STATE_body__(&data__->READ_AI_STATE0);
  __SET_EXTERNAL(data__->,AIS,,__GET_VAR(data__->READ_AI_STATE0.AI_STATE_VALUE,));
  __SET_EXTERNAL(data__->,RT2,,__GET_VAR(data__->STRUCT_REAL_TIME0.MINUTE_TIME,));
  __SET_VAR(data__->READ_DI_CNT1.,DI_NUMBER,,__GET_EXTERNAL(data__->DI_NUMBER,));
  READ_DI_CNT_body__(&data__->READ_DI_CNT1);
  __SET_EXTERNAL(data__->,CNT,,__GET_VAR(data__->READ_DI_CNT1.DI_CNT_VALUE,));
  __SET_EXTERNAL(data__->,RT3,,__GET_VAR(data__->STRUCT_REAL_TIME0.SEC_TIME,));
  READ_SYS_TICK_COUNTER_body__(&data__->READ_SYS_TICK_COUNTER0);
  __SET_EXTERNAL(data__->,SYS_TICK,,__GET_VAR(data__->READ_SYS_TICK_COUNTER0.SYS_TICK_COUNTER_VALUE,));
  __SET_EXTERNAL(data__->,RT4,,__GET_VAR(data__->STRUCT_REAL_TIME0.SUB_SEC_TIME,));
  __SET_VAR(data__->READ_DI_FREQ0.,DI_NUMBER,,__GET_EXTERNAL(data__->DI_NUMBER,));
  READ_DI_FREQ_body__(&data__->READ_DI_FREQ0);
  __SET_EXTERNAL(data__->,FREQ,,__GET_VAR(data__->READ_DI_FREQ0.DI_FREQ_VALUE,));
  __SET_EXTERNAL(data__->,RT5,,__GET_VAR(data__->STRUCT_REAL_TIME0.WEEK_DAY_TIME,));
  __SET_EXTERNAL(data__->,RT6,,__GET_VAR(data__->STRUCT_REAL_TIME0.MONTH_TIME,));
  READ_DO_SC_body__(&data__->READ_DO_SC0);
  __SET_VAR(data__->,DSF,,__GET_VAR(data__->READ_DO_SC0.DO_SC_FLAG,));
  __SET_EXTERNAL(data__->,RT7,,__GET_VAR(data__->STRUCT_REAL_TIME0.DATE_TIME,));
  __SET_VAR(data__->,DSE,,__GET_VAR(data__->READ_DO_SC0.DO_SC_EN,));
  READ_INTERNAL_TEMP_body__(&data__->READ_INTERNAL_TEMP0);
  __SET_VAR(data__->,ITO,,__GET_VAR(data__->READ_INTERNAL_TEMP0.INTERNAL_TEMP_OUT,));
  __SET_EXTERNAL(data__->,RT8,,__GET_VAR(data__->STRUCT_REAL_TIME0.YEAR_TIME,));
  __SET_EXTERNAL(data__->,RT9,,__GET_VAR(data__->STRUCT_REAL_TIME0.YEAR_DAY_TIME,));
  __SET_VAR(data__->WRITE_DI_PULSELESS0.,DI_NUMBER,,__GET_EXTERNAL(data__->DI_NUMBER,));
  __SET_VAR(data__->WRITE_DI_PULSELESS0.,DI_PULSELESS_VALUE,,__GET_EXTERNAL(data__->VAL,));
  WRITE_DI_PULSELESS_body__(&data__->WRITE_DI_PULSELESS0);
  __SET_VAR(data__->WRITE_DO_PWM_FREQ0.,DO_PWM_FREQ,,__GET_EXTERNAL(data__->DOPF,));
  WRITE_DO_PWM_FREQ_body__(&data__->WRITE_DO_PWM_FREQ0);
  __SET_VAR(data__->WRITE_UART_SETS0.,MESO_UART,,__GET_EXTERNAL(data__->U1,));
  __SET_VAR(data__->WRITE_UART_SETS0.,SET_RS_485_2,,__GET_EXTERNAL(data__->U2,));
  __SET_VAR(data__->WRITE_UART_SETS0.,SET_RS_232,,__GET_EXTERNAL(data__->U3,));
  __SET_VAR(data__->WRITE_UART_SETS0.,SET_RS_485_1,,__GET_EXTERNAL(data__->U5,));
  __SET_VAR(data__->WRITE_UART_SETS0.,SET_RS_485_IMMO,,__GET_EXTERNAL(data__->U6,));
  __SET_VAR(data__->WRITE_UART_SETS0.,SET_HART,,__GET_EXTERNAL(data__->U7,));
  WRITE_UART_SETS_body__(&data__->WRITE_UART_SETS0);
  __SET_VAR(data__->WRITE_DO_SC0.,DO_SC_FLAG,,__GET_EXTERNAL(data__->WSF,));
  __SET_VAR(data__->WRITE_DO_SC0.,DO_SC_EN,,__GET_EXTERNAL(data__->WSE,));
  WRITE_DO_SC_body__(&data__->WRITE_DO_SC0);
  __SET_VAR(data__->WRITE_DI_NOISE_FLTR_10US0.,DI_NUMBER,,__GET_EXTERNAL(data__->DI_NUMBER,));
  __SET_VAR(data__->WRITE_DI_NOISE_FLTR_10US0.,DI_NOISE_FLTR_VALUE_10US,,__GET_EXTERNAL(data__->VAL0,));
  WRITE_DI_NOISE_FLTR_10US_body__(&data__->WRITE_DI_NOISE_FLTR_10US0);
  __SET_VAR(data__->WRITE_MDB_ADDRESS0.,MDB_ADDR,,__GET_EXTERNAL(data__->ADR,));
  WRITE_MDB_ADDRESS_body__(&data__->WRITE_MDB_ADDRESS0);
  __SET_VAR(data__->WRITE_DI_MODE0.,DI_NUMBER,,__GET_EXTERNAL(data__->DI_NUMBER,));
  __SET_VAR(data__->WRITE_DI_MODE0.,DI_MODE_VALUE,,__GET_EXTERNAL(data__->VAL1,));
  WRITE_DI_MODE_body__(&data__->WRITE_DI_MODE0);
  __SET_VAR(data__->WRITE_DO_PWM_CTRL0.,DO_NUMBER,,__GET_EXTERNAL(data__->NUM,));
  __SET_VAR(data__->WRITE_DO_PWM_CTRL0.,DO_PWM_CTRL,,__GET_EXTERNAL(data__->CTRL,));
  WRITE_DO_PWM_CTRL_body__(&data__->WRITE_DO_PWM_CTRL0);
  __SET_VAR(data__->WRITE_CH_TIMEOUT0.,CH_NUMBER,,__GET_EXTERNAL(data__->NUM,));
  __SET_VAR(data__->WRITE_CH_TIMEOUT0.,CHANNEL_TIMEOUT,,__GET_EXTERNAL(data__->TI,));
  WRITE_CH_TIMEOUT_body__(&data__->WRITE_CH_TIMEOUT0);
  __SET_VAR(data__->WRITE_DO0.,DO_VALUE,,__GET_EXTERNAL(data__->DOV,));
  __SET_VAR(data__->WRITE_DO0.,DO_MASK,,__GET_EXTERNAL(data__->DOM,));
  WRITE_DO_body__(&data__->WRITE_DO0);

  goto __end;

__end:
  return;
} // PROGRAM0_body__() 





#endif //POUS_C
