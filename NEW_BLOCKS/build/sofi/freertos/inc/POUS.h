/**
 * @file C:\Beremiz\beremiz_test_projects\NEW_BLOCKS\build//sofi/freertos/inc/POUS.h
 * @author Shoma Gane <shomagan@gmail.com>
 *         Ayrat Girfanov <girfanov.ayrat@yandex.ru>
 * @defgroup beremiz 
 * @ingroup beremiz 
 * @version 0.1 
 * @brief  TODO!!! write brief in 
 */
    /*
     * Copyright (c) 2018 Snema Service
     * All rights reserved.
     *
     * Redistribution and use in source and binary forms, with or without modification,
     * are permitted provided that the following conditions are met:
     *
     * 1. Redistributions of source code must retain the above copyright notice,
     *    this list of conditions and the following disclaimer.
     * 2. Redistributions in binary form must reproduce the above copyright notice,
     *    this list of conditions and the following disclaimer in the documentation
     *    and/or other materials provided with the distribution.
     * 3. The name of the author may not be used to endorse or promote products
     *    derived from this software without specific prior written permission.
     *
     * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
     * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
     * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
     * SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
     * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
     * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
     * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
     * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
     * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY
     * OF SUCH DAMAGE.
     *
     * This file is part of the sofi PLC.
     *
     * Author: Shoma Gane <shomagan@gmail.com>
     *         Ayrat Girfanov <girfanov.ayrat@yandex.ru>
     */
#ifndef POUS_H
#define POUS_H
/*add includes below */
#include "sofi_beremiz.h"
#include "type_def.h"
#include "sofi_init.h"

/*add includes before */
#include "beremiz.h"
//#ifndef __POUS_H$deleted after handing SNEMA generator
//#define __POUS_H$deleted after handing SNEMA generator
#define BKRAM_VERIFY_NUMBER_USER 0x68388ccdb2869c28 //random value after compile

#if defined __cplusplus
extern "C" {
#endif
#include "matiec/accessor.h"
#include "matiec/iec_std_lib.h"

// PROGRAM PROGRAM0
// Data part
typedef struct {
  // PROGRAM Interface - IN, OUT, IN_OUT variables

  // PROGRAM private variables - TEMP, private and located variables
  WRITE_DI_PULSELESS WRITE_DI_PULSELESS0;
  WRITE_DI_MODE WRITE_DI_MODE0;
  WRITE_UART_SETS WRITE_UART_SETS0;
  WRITE_CH_TIMEOUT WRITE_CH_TIMEOUT0;
  WRITE_DO_PWM_FREQ WRITE_DO_PWM_FREQ0;
  WRITE_MDB_ADDRESS WRITE_MDB_ADDRESS0;
  READ_SYS_TICK_COUNTER READ_SYS_TICK_COUNTER0;
  READ_AI_STATE READ_AI_STATE0;
  READ_RESET READ_RESET0;
  READ_PWR READ_PWR0;
  READ_DO READ_DO0;
  READ_DI_FREQ READ_DI_FREQ0;
  STRUCT_REAL_TIME STRUCT_REAL_TIME0;
  READ_DO_SC READ_DO_SC0;
  WRITE_DO WRITE_DO0;
  WRITE_DO_SC WRITE_DO_SC0;
  READ_DI_CNT READ_DI_CNT1;
  WRITE_DO_PWM_CTRL WRITE_DO_PWM_CTRL0;
  READ_INTERNAL_TEMP READ_INTERNAL_TEMP0;
  __DECLARE_EXTERNAL(UINT,DI_NUMBER)
  __DECLARE_EXTERNAL(UDINT,VAL)
  __DECLARE_EXTERNAL(UINT,VAL0)
  __DECLARE_EXTERNAL(UINT,VAL1)
  __DECLARE_EXTERNAL(USINT,RT1)
  __DECLARE_EXTERNAL(USINT,RT2)
  __DECLARE_EXTERNAL(USINT,RT3)
  __DECLARE_EXTERNAL(USINT,RT4)
  __DECLARE_EXTERNAL(USINT,RT5)
  __DECLARE_EXTERNAL(USINT,RT6)
  __DECLARE_EXTERNAL(USINT,RT7)
  __DECLARE_EXTERNAL(USINT,RT8)
  __DECLARE_EXTERNAL(UINT,RT9)
  __DECLARE_EXTERNAL(USINT,NUM)
  __DECLARE_EXTERNAL(UINT,CTRL)
  __DECLARE_EXTERNAL(UINT,ADR)
  __DECLARE_EXTERNAL(UINT,U1)
  __DECLARE_EXTERNAL(UINT,U2)
  __DECLARE_EXTERNAL(UINT,U3)
  __DECLARE_EXTERNAL(UINT,U5)
  __DECLARE_EXTERNAL(UINT,U6)
  __DECLARE_EXTERNAL(UINT,U7)
  __DECLARE_EXTERNAL(UINT,AIS)
  __DECLARE_EXTERNAL(UDINT,TI)
  __DECLARE_EXTERNAL(UINT,WSF)
  __DECLARE_EXTERNAL(UINT,WSE)
  __DECLARE_EXTERNAL(UINT,DOPF)
  __DECLARE_EXTERNAL(ULINT,CNT)
  __DECLARE_EXTERNAL(REAL,FREQ)
  __DECLARE_EXTERNAL(USINT,DOOU)
  __DECLARE_EXTERNAL(REAL,PWR_V)
  __DECLARE_EXTERNAL(REAL,BAT_V)
  __DECLARE_EXTERNAL(UINT,RESET_NUM)
  __DECLARE_EXTERNAL(UINT,LAST_RESET)
  __DECLARE_EXTERNAL(ULINT,SYS_TICK)
  __DECLARE_EXTERNAL(UINT,DOV)
  __DECLARE_EXTERNAL(UINT,DOM)
  __DECLARE_VAR(USINT,DSF)
  __DECLARE_VAR(USINT,DSE)
  __DECLARE_VAR(REAL,ITO)
  WRITE_DI_NOISE_FLTR_10US WRITE_DI_NOISE_FLTR_10US0;

} PROGRAM0;

void PROGRAM0_init__(PROGRAM0 *data__, BOOL retain);
// Code part
void PROGRAM0_body__(PROGRAM0 *data__);
//#endif //__POUS_H$deleted after handing SNEMA generator

#if defined __cplusplus
}
#endif
#endif //POUS_H
