/**
 * @file C:\Beremiz\beremiz_test_projects\NEW_BLOCKS\build//sofi/freertos/inc/config_task.h
 * @author Shoma Gane <shomagan@gmail.com>
 *         Ayrat Girfanov <girfanov.ayrat@yandex.ru>
 * @defgroup beremiz 
 * @ingroup beremiz 
 * @version 0.1 
 * @brief  TODO!!! write brief in 
 */
    /*
     * Copyright (c) 2018 Snema Service
     * All rights reserved.
     *
     * Redistribution and use in source and binary forms, with or without modification,
     * are permitted provided that the following conditions are met:
     *
     * 1. Redistributions of source code must retain the above copyright notice,
     *    this list of conditions and the following disclaimer.
     * 2. Redistributions in binary form must reproduce the above copyright notice,
     *    this list of conditions and the following disclaimer in the documentation
     *    and/or other materials provided with the distribution.
     * 3. The name of the author may not be used to endorse or promote products
     *    derived from this software without specific prior written permission.
     *
     * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
     * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
     * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
     * SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
     * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
     * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
     * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
     * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
     * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY
     * OF SUCH DAMAGE.
     *
     * This file is part of the sofi PLC.
     *
     * Author: Shoma Gane <shomagan@gmail.com>
     *         Ayrat Girfanov <girfanov.ayrat@yandex.ru>
     */
#ifndef CONFIG_H
#define CONFIG_H
/*add includes below */
#include "sofi_beremiz.h"
#include "type_def.h"
#include "sofi_init.h"

/*add includes before */

#if defined __cplusplus
extern "C" {
#endif
#include "matiec/accessor.h"
void config_init__(void);
void config_run__(unsigned long long tick);
__DECLARE_GLOBAL_PROTOTYPE(UINT,DI_NUMBER)
__DECLARE_GLOBAL_PROTOTYPE(UDINT,VAL)
__DECLARE_GLOBAL_PROTOTYPE(UINT,VAL0)
__DECLARE_GLOBAL_PROTOTYPE(UINT,VAL1)
__DECLARE_GLOBAL_PROTOTYPE(USINT,RT1)
__DECLARE_GLOBAL_PROTOTYPE(USINT,RT2)
__DECLARE_GLOBAL_PROTOTYPE(USINT,RT3)
__DECLARE_GLOBAL_PROTOTYPE(USINT,RT4)
__DECLARE_GLOBAL_PROTOTYPE(USINT,RT5)
__DECLARE_GLOBAL_PROTOTYPE(USINT,RT6)
__DECLARE_GLOBAL_PROTOTYPE(USINT,RT7)
__DECLARE_GLOBAL_PROTOTYPE(USINT,RT8)
__DECLARE_GLOBAL_PROTOTYPE(UINT,RT9)
__DECLARE_GLOBAL_PROTOTYPE(UINT,CTRL)
__DECLARE_GLOBAL_PROTOTYPE(USINT,NUM)
__DECLARE_GLOBAL_PROTOTYPE(UINT,ADR)
__DECLARE_GLOBAL_PROTOTYPE(UINT,U1)
__DECLARE_GLOBAL_PROTOTYPE(UINT,U2)
__DECLARE_GLOBAL_PROTOTYPE(UINT,U3)
__DECLARE_GLOBAL_PROTOTYPE(UINT,U5)
__DECLARE_GLOBAL_PROTOTYPE(UINT,U6)
__DECLARE_GLOBAL_PROTOTYPE(UINT,U7)
__DECLARE_GLOBAL_PROTOTYPE(UINT,AIS)
__DECLARE_GLOBAL_PROTOTYPE(UDINT,TI)
__DECLARE_GLOBAL_PROTOTYPE(UINT,WSF)
__DECLARE_GLOBAL_PROTOTYPE(UINT,WSE)
__DECLARE_GLOBAL_PROTOTYPE(UINT,DOPF)
__DECLARE_GLOBAL_PROTOTYPE(ULINT,CNT)
__DECLARE_GLOBAL_PROTOTYPE(REAL,FREQ)
__DECLARE_GLOBAL_PROTOTYPE(USINT,DOOU)
__DECLARE_GLOBAL_PROTOTYPE(REAL,PWR_V)
__DECLARE_GLOBAL_PROTOTYPE(REAL,BAT_V)
__DECLARE_GLOBAL_PROTOTYPE(UINT,RESET_NUM)
__DECLARE_GLOBAL_PROTOTYPE(UINT,LAST_RESET)
__DECLARE_GLOBAL_PROTOTYPE(ULINT,SYS_TICK)
__DECLARE_GLOBAL_PROTOTYPE(UINT,DOV)
__DECLARE_GLOBAL_PROTOTYPE(UINT,DOM)


#if defined __cplusplus
}
#endif
#endif //CONFIG_H
