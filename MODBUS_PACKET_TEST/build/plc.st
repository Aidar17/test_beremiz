FUNCTION_BLOCK checc_test
  VAR_EXTERNAL
    REG1 : UINT;
    REG2 : UINT;
    REG3 : UINT;
  END_VAR
  VAR
    READ_DI0 : READ_DI;
    WRITE_DO0 : WRITE_DO;
    num1 : UINT := 65535;
    MUX11_OUT : UINT;
  END_VAR

  READ_DI0();
  MUX11_OUT := MUX(READ_DI0.DI_OUT, REG1, REG2, REG3);
  WRITE_DO0(DO_VALUE := MUX11_OUT, DO_MASK := num1);
END_FUNCTION_BLOCK

FUNCTION_BLOCK stresss
  VAR CONSTANT
    a1 : DINT := 5;
  END_VAR
  VAR RETAIN
    a2 : DINT := 6;
  END_VAR
  VAR
    a3 : DINT;
    ADD1_OUT : DINT;
  END_VAR

  ADD1_OUT := ADD(a2, a1);
  a3 := ADD1_OUT;
END_FUNCTION_BLOCK

FUNCTION_BLOCK perfomens
  VAR CONSTANT
    a1 : DINT := 3;
    a2 : DINT := 3;
  END_VAR
  VAR
    a3 : DINT;
    ADD1_OUT : DINT;
  END_VAR

  ADD1_OUT := ADD(a2, a1);
  a3 := ADD1_OUT;
END_FUNCTION_BLOCK

PROGRAM MAIN0
  VAR_EXTERNAL
    TEST_config : DINT;
  END_VAR
  VAR
    test1 : checc_test;
    test2 : stresss;
    test3 : perfomens;
  END_VAR

  IF TEST_config=1 
    THEN test1();
    END_IF; 

    IF TEST_config=2 
    THEN test2();
  ELSE test3();
  END_IF;
END_PROGRAM


CONFIGURATION config
  VAR_GLOBAL RETAIN
    REG10 AT %MW0.0 : UINT := 111;
    REG20 AT %MW0.1 : UINT := 5;
    REG30 AT %MW0.2 : UINT := 10;
    REG40 AT %IW0.3 : UINT := 10;
    REG1 AT %IW1.0.1.0 : UINT;
    REG2 AT %IW1.0.1.1 : UINT;
    REG3 AT %IW1.0.1.2 : UINT;
    SELF_ADDRESS AT %IW1.0.0.0 : UINT;
    Writereg AT %QW1.0.2.0 : WORD;
    TEST_config : DINT := 1;
  END_VAR

  RESOURCE resource1 ON PLC
    TASK task0(INTERVAL := T#20ms,PRIORITY := 0);
    PROGRAM instance0 WITH task0 : MAIN0;
  END_RESOURCE
END_CONFIGURATION
