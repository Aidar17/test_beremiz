/* The total number of nodes, needed to support _all_ instances of the modbus plugin */
#define TOTAL_TCPNODE_COUNT       0
#define TOTAL_RTUNODE_COUNT       1
#define TOTAL_ASCNODE_COUNT       0
#define TOTAL_ROUT_NODE_COUNT     0
#define TOTAL_MODBUS_AREA_COUNT   0
/* Values for instance 1 of the modbus plugin */
#define MAX_NUMBER_OF_TCPCLIENTS  0
#define NUMBER_OF_TCPSERVER_NODES 0
#define NUMBER_OF_TCPCLIENT_NODES 0
#define NUMBER_OF_TCPCLIENT_REQTS 0

#define NUMBER_OF_RTUSERVER_NODES 0
#define NUMBER_OF_RTUCLIENT_NODES 1
#define NUMBER_OF_RTUCLIENT_REQTS 3

#define NUMBER_OF_ASCIISERVER_NODES 0
#define NUMBER_OF_ASCIICLIENT_NODES 0
#define NUMBER_OF_ASCIICLIENT_REQTS 0

#define NUMBER_OF_SERVER_NODES (NUMBER_OF_TCPSERVER_NODES + \
                                NUMBER_OF_RTUSERVER_NODES + \
                               NUMBER_OF_ASCIISERVER_NODES)

#define NUMBER_OF_CLIENT_NODES (NUMBER_OF_TCPCLIENT_NODES + \
                                NUMBER_OF_RTUCLIENT_NODES + \
                                NUMBER_OF_ASCIICLIENT_NODES)

#define NUMBER_OF_CLIENT_REQTS (NUMBER_OF_TCPCLIENT_REQTS + \
                                NUMBER_OF_RTUCLIENT_REQTS + \
                                NUMBER_OF_ASCIICLIENT_REQTS)

#define MAX_READ_BITS 254
#define MAX_WORD_NUM 127
#define MAX_PACKET_LEN (MAX_WORD_NUM*2 + 7)

#define COILS_01 1
#define INPUT_DISCRETES_02 2
#define HOLDING_REGISTERS_03 3
#define INPUT_REGISTERS_04 4
client_node_t		client_nodes[NUMBER_OF_CLIENT_NODES] = {
{"1.0"/*location*/, {naf_rtu, {.rtu = {RS_485_1_UART, 115200 /*baud*/, 0 /*parity*/, 8 /*data bits*/, 1 /*stop bits*/, 0 /* ignore echo */}}}/*node_addr_t*/, -1 /* mb_nd */, 0 /* init_state */, 100 /* communication period */,0,NULL},
};

client_request_t	client_requests[NUMBER_OF_CLIENT_REQTS] = {
{"1_0_0"/*location*/,RS_485_1_UART/*channel*/, 0/*client_node_id*/, 3/*slave_id*/, req_input/*req_type*/, 3/*mb_function*/, 0 /*first reg address*/,
3/*reg number*/,DEF_REQ_SEND_RETRIES/*retries*/, 0 /* error_code */, 0 /* prev_code */, 90/* timeout */,
NULL, NULL},
{"1_0_1"/*location*/,RS_485_1_UART/*channel*/, 0/*client_node_id*/, 3/*slave_id*/, req_input/*req_type*/, 3/*mb_function*/, 40000 /*first reg address*/,
3/*reg number*/,DEF_REQ_SEND_RETRIES/*retries*/, 0 /* error_code */, 0 /* prev_code */, 90/* timeout */,
NULL, NULL},
{"1_0_2"/*location*/,RS_485_1_UART/*channel*/, 0/*client_node_id*/, 3/*slave_id*/, req_output/*req_type*/, 16/*mb_function*/, 40003 /*first reg address*/,
1/*reg number*/,DEF_REQ_SEND_RETRIES/*retries*/, 0 /* error_code */, 0 /* prev_code */, 90/* timeout */,
NULL, NULL},
};

static route_node_t	route_nodes[TOTAL_ROUT_NODE_COUNT] = {
};

static area_node_t	area_nodes[TOTAL_MODBUS_AREA_COUNT] = {
};

UINT *__IW1_0_1_0;
UINT *__IW1_0_1_1;
UINT *__IW1_0_1_2;
UINT *__IW1_0_0_0;
WORD *__QW1_0_2_0;
u16 plcv_buffer_req_0[1] = {0}; // now it's not used 
u16 com_buffer_req_0[3] = {0,0,0,};
u16 plcv_buffer_req_1[1] = {0}; // now it's not used 
u16 com_buffer_req_1[3] = {0,0,0,};
u16 plcv_buffer_req_2[1] = {0}; // now it's not used 
u16 com_buffer_req_2[1] = {0,};
