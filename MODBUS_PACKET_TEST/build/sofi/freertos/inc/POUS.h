/**
 * @file C:\Beremiz\beremiz_test_projects\MODBUS_PACKET_TEST\build//sofi/freertos/inc/POUS.h
 * @author Shoma Gane <shomagan@gmail.com>
 *         Ayrat Girfanov <girfanov.ayrat@yandex.ru>
 * @defgroup beremiz 
 * @ingroup beremiz 
 * @version 0.1 
 * @brief  TODO!!! write brief in 
 */
    /*
     * Copyright (c) 2018 Snema Service
     * All rights reserved.
     *
     * Redistribution and use in source and binary forms, with or without modification,
     * are permitted provided that the following conditions are met:
     *
     * 1. Redistributions of source code must retain the above copyright notice,
     *    this list of conditions and the following disclaimer.
     * 2. Redistributions in binary form must reproduce the above copyright notice,
     *    this list of conditions and the following disclaimer in the documentation
     *    and/or other materials provided with the distribution.
     * 3. The name of the author may not be used to endorse or promote products
     *    derived from this software without specific prior written permission.
     *
     * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
     * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
     * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
     * SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
     * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
     * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
     * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
     * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
     * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY
     * OF SUCH DAMAGE.
     *
     * This file is part of the sofi PLC.
     *
     * Author: Shoma Gane <shomagan@gmail.com>
     *         Ayrat Girfanov <girfanov.ayrat@yandex.ru>
     */
#ifndef POUS_H
#define POUS_H
/*add includes below */
#include "sofi_beremiz.h"
#include "type_def.h"
#include "sofi_init.h"

/*add includes before */
#include "beremiz.h"
//#ifndef __POUS_H$deleted after handing SNEMA generator
//#define __POUS_H$deleted after handing SNEMA generator
#define BKRAM_VERIFY_NUMBER_USER 0xf8f600e40cfdadf9 //random value after compile

#if defined __cplusplus
extern "C" {
#endif
#include "matiec/accessor.h"
#include "matiec/iec_std_lib.h"

// FUNCTION_BLOCK CHECC_TEST
// Data part
typedef struct {
  // FB Interface - IN, OUT, IN_OUT variables
  __DECLARE_VAR(BOOL,EN)
  __DECLARE_VAR(BOOL,ENO)

  // FB private variables - TEMP, private and located variables
  __DECLARE_EXTERNAL(UINT,REG1)
  __DECLARE_EXTERNAL(UINT,REG2)
  __DECLARE_EXTERNAL(UINT,REG3)
  READ_DI READ_DI0;
  WRITE_DO WRITE_DO0;
  __DECLARE_VAR(UINT,NUM1)
  __DECLARE_VAR(UINT,MUX11_OUT)

} CHECC_TEST;

void CHECC_TEST_init__(CHECC_TEST *data__, BOOL retain);
// Code part
void CHECC_TEST_body__(CHECC_TEST *data__);
// FUNCTION_BLOCK STRESSS
// Data part
typedef struct {
  // FB Interface - IN, OUT, IN_OUT variables
  __DECLARE_VAR(BOOL,EN)
  __DECLARE_VAR(BOOL,ENO)

  // FB private variables - TEMP, private and located variables
  __DECLARE_VAR(DINT,A1)
  __DECLARE_VAR(DINT,A2)
  __DECLARE_VAR(DINT,A3)
  __DECLARE_VAR(DINT,ADD1_OUT)

} STRESSS;

void STRESSS_init__(STRESSS *data__, BOOL retain);
// Code part
void STRESSS_body__(STRESSS *data__);
// FUNCTION_BLOCK PERFOMENS
// Data part
typedef struct {
  // FB Interface - IN, OUT, IN_OUT variables
  __DECLARE_VAR(BOOL,EN)
  __DECLARE_VAR(BOOL,ENO)

  // FB private variables - TEMP, private and located variables
  __DECLARE_VAR(DINT,A1)
  __DECLARE_VAR(DINT,A2)
  __DECLARE_VAR(DINT,A3)
  __DECLARE_VAR(DINT,ADD1_OUT)

} PERFOMENS;

void PERFOMENS_init__(PERFOMENS *data__, BOOL retain);
// Code part
void PERFOMENS_body__(PERFOMENS *data__);
// PROGRAM MAIN0
// Data part
typedef struct {
  // PROGRAM Interface - IN, OUT, IN_OUT variables

  // PROGRAM private variables - TEMP, private and located variables
  __DECLARE_EXTERNAL(DINT,TEST_CONFIG)
  CHECC_TEST TEST1;
  STRESSS TEST2;
  PERFOMENS TEST3;

} MAIN0;

void MAIN0_init__(MAIN0 *data__, BOOL retain);
// Code part
void MAIN0_body__(MAIN0 *data__);
//#endif //__POUS_H$deleted after handing SNEMA generator

#if defined __cplusplus
}
#endif
#endif //POUS_H
