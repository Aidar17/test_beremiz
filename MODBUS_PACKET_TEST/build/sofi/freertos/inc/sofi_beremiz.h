/**
 * @file C:\Beremiz\beremiz_test_projects\MODBUS_PACKET_TEST\build//sofi/freertos/inc/sofi_beremiz.h
 * @author Shoma Gane <shomagan@gmail.com>
 *         Ayrat Girfanov <girfanov.ayrat@yandex.ru>
 * @defgroup beremiz 
 * @ingroup beremiz 
 * @version 0.1 
 * @brief  TODO!!! write brief in 
 */
    /*
     * Copyright (c) 2018 Snema Service
     * All rights reserved.
     *
     * Redistribution and use in source and binary forms, with or without modification,
     * are permitted provided that the following conditions are met:
     *
     * 1. Redistributions of source code must retain the above copyright notice,
     *    this list of conditions and the following disclaimer.
     * 2. Redistributions in binary form must reproduce the above copyright notice,
     *    this list of conditions and the following disclaimer in the documentation
     *    and/or other materials provided with the distribution.
     * 3. The name of the author may not be used to endorse or promote products
     *    derived from this software without specific prior written permission.
     *
     * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
     * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
     * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
     * SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
     * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
     * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
     * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
     * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
     * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY
     * OF SUCH DAMAGE.
     *
     * This file is part of the sofi PLC.
     *
     * Author: Shoma Gane <shomagan@gmail.com>
     *         Ayrat Girfanov <girfanov.ayrat@yandex.ru>
     */
#ifndef SOFI_BEREMIZ_H
#define SOFI_BEREMIZ_H
/*add includes below */
#include "sofi_beremiz.h"
#include "type_def.h"
#include "sofi_init.h"

/*add includes before */
#include "beremiz.h"

#include "matiec/accessor.h"
#include "matiec/iec_std_lib.h"

// FUNCTION_BLOCK READ_PARAM_TEMPLATE
// Data part
typedef struct {
  // FB Interface - IN, OUT, IN_OUT variables
  __DECLARE_VAR(BOOL,EN)
  __DECLARE_VAR(BOOL,ENO)
  __DECLARE_VAR(BOOL,ENABLE)
  __DECLARE_VAR(DINT,ADDRESS)
  __DECLARE_VAR(BOOL,CHECK)
  __DECLARE_VAR(DINT,VALUE)
  // FB private variables - TEMP, private and located variables
} READ_PARAM_UDINT;
typedef struct {
  // FB Interface - IN, OUT, IN_OUT variables
  __DECLARE_VAR(BOOL,EN)
  __DECLARE_VAR(BOOL,ENO)
  __DECLARE_VAR(BOOL,ENABLE)
  __DECLARE_VAR(DINT,ADDRESS)
  __DECLARE_VAR(DINT,VALUE)
  __DECLARE_VAR(BOOL,CHECK)
  // FB private variables - TEMP, private and located variables
} SET_PARAM_UDINT;
// FUNCTION_BLOCK READ_PARAM_TEMPLATE
// Data part
typedef struct {
  // FB Interface - IN, OUT, IN_OUT variables
  __DECLARE_VAR(BOOL,EN)
  __DECLARE_VAR(BOOL,ENO)
  __DECLARE_VAR(BOOL,ENABLE)
  __DECLARE_VAR(DINT,ADDRESS)
  __DECLARE_VAR(BOOL,CHECK)
  __DECLARE_VAR(INT,VALUE)
  // FB private variables - TEMP, private and located variables
} READ_PARAM_UINT;
typedef struct {
  // FB Interface - IN, OUT, IN_OUT variables
  __DECLARE_VAR(BOOL,EN)
  __DECLARE_VAR(BOOL,ENO)
  __DECLARE_VAR(BOOL,ENABLE)
  __DECLARE_VAR(DINT,ADDRESS)
  __DECLARE_VAR(INT,VALUE)
  __DECLARE_VAR(BOOL,CHECK)
  // FB private variables - TEMP, private and located variables
} SET_PARAM_UINT;
// FUNCTION_BLOCK READ_PARAM_TEMPLATE
// Data part
typedef struct {
  // FB Interface - IN, OUT, IN_OUT variables
  __DECLARE_VAR(BOOL,EN)
  __DECLARE_VAR(BOOL,ENO)
  __DECLARE_VAR(BOOL,ENABLE)
  __DECLARE_VAR(DINT,ADDRESS)
  __DECLARE_VAR(BOOL,CHECK)
  __DECLARE_VAR(SINT,VALUE)
  // FB private variables - TEMP, private and located variables
} READ_PARAM_USINT;
typedef struct {
  // FB Interface - IN, OUT, IN_OUT variables
  __DECLARE_VAR(BOOL,EN)
  __DECLARE_VAR(BOOL,ENO)
  __DECLARE_VAR(BOOL,ENABLE)
  __DECLARE_VAR(DINT,ADDRESS)
  __DECLARE_VAR(SINT,VALUE)
  __DECLARE_VAR(BOOL,CHECK)
  // FB private variables - TEMP, private and located variables
} SET_PARAM_USINT;

// FUNCTION_BLOCK READ_DI
// Data part
typedef struct {
  // FB Interface - IN, OUT, IN_OUT variables
  __DECLARE_VAR(BOOL,EN)
  __DECLARE_VAR(BOOL,ENO)
  __DECLARE_VAR(UDINT,DI_OUT)
  // FB private variables - TEMP, private and located variables
} READ_DI;

typedef struct {
  // FB Interface - IN, OUT, IN_OUT variables
  __DECLARE_VAR(BOOL,EN)
  __DECLARE_VAR(BOOL,ENO)
  __DECLARE_VAR(UINT,DI_NUMBER)
  __DECLARE_VAR(ULINT,DI_CNT_VALUE)
  // FB private variables - TEMP, private and located variables
} READ_DI_CNT;

typedef struct {
  // FB Interface - IN, OUT, IN_OUT variables
  __DECLARE_VAR(BOOL,EN)
  __DECLARE_VAR(BOOL,ENO)
  __DECLARE_VAR(UINT,DI_NUMBER)
  __DECLARE_VAR(REAL,DI_FREQ_VALUE)
  // FB private variables - TEMP, private and located variables
} READ_DI_FREQ;

typedef struct {
  // FB Interface - IN, OUT, IN_OUT variables
  __DECLARE_VAR(BOOL,EN)
  __DECLARE_VAR(BOOL,ENO)
  __DECLARE_VAR(UINT,AI_STATE_VALUE)
  // FB private variables - TEMP, private and located variables
} READ_AI_STATE;

typedef struct {
  // FB Interface - IN, OUT, IN_OUT variables
  __DECLARE_VAR(BOOL,EN)
  __DECLARE_VAR(BOOL,ENO)
  __DECLARE_VAR(UINT,AI_NUMBER)
  __DECLARE_VAR(UINT,AI_VALUE)
  // FB private variables - TEMP, private and located variables
} READ_AI;

// Data part
typedef struct {
  // FB Interface - IN, OUT, IN_OUT variables
  __DECLARE_VAR(BOOL,EN)
  __DECLARE_VAR(BOOL,ENO)
  __DECLARE_VAR(USINT,DO_SC_FLAG)
  __DECLARE_VAR(USINT,DO_SC_EN)
  // FB private variables - TEMP, private and located variables
} READ_DO_SC;

typedef struct {
  // FB Interface - IN, OUT, IN_OUT variables
  __DECLARE_VAR(BOOL,EN)
  __DECLARE_VAR(BOOL,ENO)
  __DECLARE_VAR(USINT,DO_OUT)
  // FB private variables - TEMP, private and located variables
} READ_DO;

typedef struct {
  // FB Interface - IN, OUT, IN_OUT variables
  __DECLARE_VAR(BOOL,EN)
  __DECLARE_VAR(BOOL,ENO)
  __DECLARE_VAR(UINT,RESET_NUM)
  __DECLARE_VAR(UINT,LAST_RESET)
  // FB private variables - TEMP, private and located variables
} READ_RESET;

typedef struct {
  // FB Interface - IN, OUT, IN_OUT variables
  __DECLARE_VAR(BOOL,EN)
  __DECLARE_VAR(BOOL,ENO)
  __DECLARE_VAR(REAL,V_PWR)
  __DECLARE_VAR(REAL,V_BAT)
  // FB private variables - TEMP, private and located variables
} READ_PWR;

// FUNCTION_BLOCK READ_INTERNAL_TEMP
// Data part
typedef struct {
  // FB Interface - IN, OUT, IN_OUT variables
  __DECLARE_VAR(BOOL,EN)
  __DECLARE_VAR(BOOL,ENO)
  __DECLARE_VAR(REAL,INTERNAL_TEMP_OUT)
  // FB private variables - TEMP, private and located variables
} READ_INTERNAL_TEMP;

typedef struct {
  // FB Interface - IN, OUT, IN_OUT variables
  __DECLARE_VAR(BOOL,EN)
  __DECLARE_VAR(BOOL,ENO)
  __DECLARE_VAR(ULINT,SYS_TICK_COUNTER_VALUE)
  // FB private variables - TEMP, private and located variables
} READ_SYS_TICK_COUNTER;

typedef struct {
  // FB Interface - IN, OUT, IN_OUT variables
  __DECLARE_VAR(BOOL,EN)
  __DECLARE_VAR(BOOL,ENO)
  __DECLARE_VAR(UINT,MDB_ADDR)
  // FB private variables - TEMP, private and located variables
} WRITE_MDB_ADDRESS;

typedef struct {
  // FB Interface - IN, OUT, IN_OUT variables
  __DECLARE_VAR(BOOL,EN)
  __DECLARE_VAR(BOOL,ENO)
  __DECLARE_VAR(UINT,MESO_UART)
  __DECLARE_VAR(UINT,SET_RS_485_2)
  __DECLARE_VAR(UINT,SET_RS_232)
  __DECLARE_VAR(UINT,SET_RS_485_1)
  __DECLARE_VAR(UINT,SET_RS_485_IMMO)
  __DECLARE_VAR(UINT,SET_HART)
  // FB private variables - TEMP, private and located variables
} WRITE_UART_SETS;

typedef struct {
  // FB Interface - IN, OUT, IN_OUT variables
  __DECLARE_VAR(BOOL,EN)
  __DECLARE_VAR(BOOL,ENO)
  __DECLARE_VAR(USINT,CH_NUMBER)
  __DECLARE_VAR(UDINT,CHANNEL_TIMEOUT)
  // FB private variables - TEMP, private and located variables
} WRITE_CH_TIMEOUT;

typedef struct {
  // FB Interface - IN, OUT, IN_OUT variables
  __DECLARE_VAR(BOOL,EN)
  __DECLARE_VAR(BOOL,ENO)
  __DECLARE_VAR(UINT,DI_NUMBER)
  __DECLARE_VAR(UINT,DI_NOISE_FLTR_VALUE_10US)
  // FB private variables - TEMP, private and located variables
} WRITE_DI_NOISE_FLTR_10US;

typedef struct {
  // FB Interface - IN, OUT, IN_OUT variables
  __DECLARE_VAR(BOOL,EN)
  __DECLARE_VAR(BOOL,ENO)
  __DECLARE_VAR(UINT,DI_NUMBER)
  __DECLARE_VAR(UDINT,DI_PULSELESS_VALUE)
  // FB private variables - TEMP, private and located variables
} WRITE_DI_PULSELESS;

typedef struct {
  // FB Interface - IN, OUT, IN_OUT variables
  __DECLARE_VAR(BOOL,EN)
  __DECLARE_VAR(BOOL,ENO)
  __DECLARE_VAR(UINT,DI_NUMBER)
  __DECLARE_VAR(UINT,DI_MODE_VALUE)
  // FB private variables - TEMP, private and located variables
} WRITE_DI_MODE;

// FUNCTION_BLOCK WRITE_DO
// Data part
typedef struct {
  // FB Interface - IN, OUT, IN_OUT variables
  __DECLARE_VAR(BOOL,EN)
  __DECLARE_VAR(BOOL,ENO)
  __DECLARE_VAR(UINT,DO_VALUE)
  __DECLARE_VAR(UINT,DO_MASK)
  // FB private variables - TEMP, private and located variables
} WRITE_DO;

// Data part
typedef struct {
  // FB Interface - IN, OUT, IN_OUT variables
  __DECLARE_VAR(BOOL,EN)
  __DECLARE_VAR(BOOL,ENO)
  __DECLARE_VAR(UINT,DO_SC_FLAG)
  __DECLARE_VAR(UINT,DO_SC_EN)
  // FB private variables - TEMP, private and located variables
} WRITE_DO_SC;

// Data part
typedef struct {
  // FB Interface - IN, OUT, IN_OUT variables
  __DECLARE_VAR(BOOL,EN)
  __DECLARE_VAR(BOOL,ENO)
  __DECLARE_VAR(UINT,DO_PWM_FREQ)
  // FB private variables - TEMP, private and located variables
} WRITE_DO_PWM_FREQ;

typedef struct {
  // FB Interface - IN, OUT, IN_OUT variables
  __DECLARE_VAR(BOOL,EN)
  __DECLARE_VAR(BOOL,ENO)
  __DECLARE_VAR(USINT,DO_NUMBER)
  __DECLARE_VAR(UINT,DO_PWM_CTRL)
  // FB private variables - TEMP, private and located variables
} WRITE_DO_PWM_CTRL;

typedef struct {
  __DECLARE_VAR(BOOL,EN)
  __DECLARE_VAR(BOOL,ENO)
  __DECLARE_VAR(USINT,HOUR_TIME)
  __DECLARE_VAR(USINT,MINUTE_TIME)
  __DECLARE_VAR(USINT,SEC_TIME)
  __DECLARE_VAR(USINT,SUB_SEC_TIME)
  __DECLARE_VAR(USINT,WEEK_DAY_TIME)
  __DECLARE_VAR(USINT,MONTH_TIME)
  __DECLARE_VAR(USINT,DATE_TIME)
  __DECLARE_VAR(USINT,YEAR_TIME)
  __DECLARE_VAR(UINT,YEAR_DAY_TIME)
  // FB private variables - TEMP, private and located variables
} STRUCT_REAL_TIME;

typedef struct {
  __DECLARE_VAR(BOOL,EN)
  __DECLARE_VAR(BOOL,ENO)
  __DECLARE_VAR(USINT,SEC_IN_MIN)
  __DECLARE_VAR(USINT,MINUTE_IN_HOUR)
  __DECLARE_VAR(USINT,HOUR_IN_DAY)
  __DECLARE_VAR(USINT,DATE_IN_MONTH)
  __DECLARE_VAR(USINT,MONTH_IN_YEAR)
  __DECLARE_VAR(USINT,YEAR_SINCE_2000)
  __DECLARE_VAR(USINT,SEC_IN_MIN_WRITED)
  __DECLARE_VAR(USINT,MINUTE_IN_HOUR_WRITED)
  __DECLARE_VAR(USINT,HOUR_IN_DAY_WRITED)
  __DECLARE_VAR(USINT,DATE_IN_MONTH_WRITED)
  __DECLARE_VAR(USINT,MONTH_IN_YEAR_WRITED)
  __DECLARE_VAR(USINT,YEAR_SINCE_2000_WRITED)
  // FB private variables - TEMP, private and located variables
} WRITE_STRUCT_TIME;


// FUNCTION_BLOCK UNIX_TIME
// Data part
typedef struct {
  // FB Interface - IN, OUT, IN_OUT variables
  __DECLARE_VAR(BOOL,EN)
  __DECLARE_VAR(BOOL,ENO)
  __DECLARE_VAR(UDINT,UNIX_TIME_WRITE)
  __DECLARE_VAR(UDINT,UNIX_TIME_READ)
  __DECLARE_VAR(UDINT,UNIX_TIME_WRITED)
} UNIX_TIME;

/*functions for read parametrs from controller*/
void READ_PARAM_UDINT_init__(READ_PARAM_UDINT *data__, BOOL retain);
void READ_PARAM_UDINT_body__(READ_PARAM_UDINT *data__);
void SET_PARAM_UDINT_init__(SET_PARAM_UDINT *data__, BOOL retain);
void SET_PARAM_UDINT_body__(SET_PARAM_UDINT *data__);
void READ_PARAM_UINT_init__(READ_PARAM_UINT *data__, BOOL retain);
void READ_PARAM_UINT_body__(READ_PARAM_UINT *data__);
void SET_PARAM_UINT_init__(SET_PARAM_UINT *data__, BOOL retain);
void SET_PARAM_UINT_body__(SET_PARAM_UINT *data__);
void READ_PARAM_USINT_init__(READ_PARAM_USINT *data__, BOOL retain);
void READ_PARAM_USINT_body__(READ_PARAM_USINT *data__);
void SET_PARAM_USINT_init__(SET_PARAM_USINT *data__, BOOL retain);
void SET_PARAM_USINT_body__(SET_PARAM_USINT *data__);

void READ_DI_init__(READ_DI *data__, BOOL retain);
void READ_DI_body__(READ_DI *data__);
void READ_DI_CNT_init__(READ_DI_CNT *data__, BOOL retain);
void READ_DI_CNT_body__(READ_DI_CNT *data__);
void READ_DI_FREQ_init__(READ_DI_FREQ *data__, BOOL retain);
void READ_DI_FREQ_body__(READ_DI_FREQ *data__);
void READ_AI_STATE_init__(READ_AI_STATE *data__, BOOL retain);
void READ_AI_STATE_body__(READ_AI_STATE *data__);
void READ_AI_init__(READ_AI *data__, BOOL retain);
void READ_AI_body__(READ_AI *data__);
void READ_DO_SC_init__(READ_DO_SC *data__, BOOL retain);
void READ_DO_SC_body__(READ_DO_SC *data__);
void READ_DO_init__(READ_DO *data__, BOOL retain);
void READ_DO_body__(READ_DO *data__);
void READ_RESET_init__(READ_RESET *data__, BOOL retain);
void READ_RESET_body__(READ_RESET *data__);
void READ_PWR_init__(READ_PWR *data__, BOOL retain);
void READ_PWR_body__(READ_PWR *data__);
void READ_INTERNAL_TEMP_init__(READ_INTERNAL_TEMP *data__, BOOL retain);
void READ_INTERNAL_TEMP_body__(READ_INTERNAL_TEMP *data__);
void READ_SYS_TICK_COUNTER_init__(READ_SYS_TICK_COUNTER *data__, BOOL retain);
void READ_SYS_TICK_COUNTER_body__(READ_SYS_TICK_COUNTER *data__);
void WRITE_MDB_ADDRESS_init__(WRITE_MDB_ADDRESS *data__, BOOL retain);
void WRITE_MDB_ADDRESS_body__(WRITE_MDB_ADDRESS *data__);
void WRITE_UART_SETS_init__(WRITE_UART_SETS *data__, BOOL retain);
void WRITE_UART_SETS_body__(WRITE_UART_SETS *data__);
void WRITE_CH_TIMEOUT_init__(WRITE_CH_TIMEOUT *data__, BOOL retain);
void WRITE_CH_TIMEOUT_body__(WRITE_CH_TIMEOUT *data__);
void WRITE_DI_NOISE_FLTR_10US_init__(WRITE_DI_NOISE_FLTR_10US *data__, BOOL retain);
void WRITE_DI_NOISE_FLTR_10US_body__(WRITE_DI_NOISE_FLTR_10US *data__);
void WRITE_DI_PULSELESS_init__(WRITE_DI_PULSELESS *data__, BOOL retain);
void WRITE_DI_PULSELESS_body__(WRITE_DI_PULSELESS *data__);
void WRITE_DI_MODE_init__(WRITE_DI_MODE *data__, BOOL retain);
void WRITE_DI_MODE_body__(WRITE_DI_MODE *data__);
void WRITE_DO_init__(WRITE_DO *data__, BOOL retain);
void WRITE_DO_body__(WRITE_DO *data__);
void WRITE_DO_SC_init__(WRITE_DO_SC *data__, BOOL retain);
void WRITE_DO_SC_body__(WRITE_DO_SC *data__);
void WRITE_DO_PWM_FREQ_init__(WRITE_DO_PWM_FREQ *data__, BOOL retain);
void WRITE_DO_PWM_FREQ_body__(WRITE_DO_PWM_FREQ *data__);
void WRITE_DO_PWM_CTRL_init__(WRITE_DO_PWM_CTRL *data__, BOOL retain);
void WRITE_DO_PWM_CTRL_body__(WRITE_DO_PWM_CTRL *data__);
void STRUCT_REAL_TIME_init__(STRUCT_REAL_TIME *data__, BOOL retain);
void STRUCT_REAL_TIME_body__(STRUCT_REAL_TIME *data__);
void UNIX_TIME_init__(UNIX_TIME *data__, BOOL retain);
void UNIX_TIME_body__(UNIX_TIME *data__);
void WRITE_STRUCT_TIME_init__(WRITE_STRUCT_TIME *data__, BOOL retain);
void WRITE_STRUCT_TIME_body__(WRITE_STRUCT_TIME *data__);



#endif //SOFI_BEREMIZ_H
