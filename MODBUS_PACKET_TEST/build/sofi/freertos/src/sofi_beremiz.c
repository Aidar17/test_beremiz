/**
 * @file C:\Beremiz\beremiz_test_projects\MODBUS_PACKET_TEST\build//sofi/freertos/src/sofi_beremiz.c
 * @author Shoma Gane <shomagan@gmail.com>
 *         Ayrat Girfanov <girfanov.ayrat@yandex.ru>
 * @defgroup beremiz 
 * @ingroup beremiz 
 * @version 0.1 
 * @brief  TODO!!! write brief in 
 */
    /*
     * Copyright (c) 2018 Snema Service
     * All rights reserved.
     *
     * Redistribution and use in source and binary forms, with or without modification,
     * are permitted provided that the following conditions are met:
     *
     * 1. Redistributions of source code must retain the above copyright notice,
     *    this list of conditions and the following disclaimer.
     * 2. Redistributions in binary form must reproduce the above copyright notice,
     *    this list of conditions and the following disclaimer in the documentation
     *    and/or other materials provided with the distribution.
     * 3. The name of the author may not be used to endorse or promote products
     *    derived from this software without specific prior written permission.
     *
     * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
     * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
     * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
     * SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
     * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
     * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
     * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
     * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
     * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY
     * OF SUCH DAMAGE.
     *
     * This file is part of the sofi PLC.
     *
     * Author: Shoma Gane <shomagan@gmail.com>
     *         Ayrat Girfanov <girfanov.ayrat@yandex.ru>
     */
#ifndef SOFI_BEREMIZ_C
#define SOFI_BEREMIZ_C
#include "regs.h"
#include "sofi_dev.h"
#include "sofi_beremiz.h"

#include "link_functions.h"
#include "os_service.h"

extern link_functions_t * p_link_functions;
void READ_PARAM_UDINT_init__(READ_PARAM_UDINT *data__, BOOL retain) {
    __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
    __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
    __INIT_VAR(data__->ENABLE,0,retain)
    __INIT_VAR(data__->ADDRESS,0,retain)
    __INIT_VAR(data__->CHECK,0,retain)
    __INIT_VAR(data__->VALUE,0,retain)
}

// Code part
void READ_PARAM_UDINT_body__(READ_PARAM_UDINT *data) {
    // Control execution
    __SET_VAR(data->,CHECK,,0);
    if (!__GET_VAR(data->EN)) {
        __SET_VAR(data->,ENO,,__BOOL_LITERAL(FALSE));
        return;
    }  else {
        __SET_VAR(data->,ENO,,__BOOL_LITERAL(TRUE));
    }
    // Initialise TEMP variables
    if (__GET_VAR(data->ENABLE,)) {
        int res;
        u64 value;
        regs_flag_t flag = U32_REGS_FLAG;
        res = get_value_by_address((u32)__GET_VAR(data->ADDRESS,),&value,flag);
        if(res!=0){
            __SET_VAR(data->,CHECK,,0);
            __SET_VAR(data->,ENO,,__BOOL_LITERAL(FALSE));
        }else{
            __SET_VAR(data->,VALUE,,(IEC_DINT)value);
            __SET_VAR(data->,CHECK,,1);
        }
    } else {
        __SET_VAR(data->,VALUE,,0);
        __SET_VAR(data->,CHECK,,0);
        __SET_VAR(data->,ENO,,__BOOL_LITERAL(FALSE));
    }
    return;
} // READ_PARAM_body__() 
void SET_PARAM_UDINT_init__(SET_PARAM_UDINT *data__, BOOL retain) {
    __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
    __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
    __INIT_VAR(data__->ENABLE,0,retain)
    __INIT_VAR(data__->ADDRESS,0,retain)
    __INIT_VAR(data__->VALUE,0,retain)
    __INIT_VAR(data__->CHECK,0,retain)
}

// Code part
void SET_PARAM_UDINT_body__(SET_PARAM_UDINT *data) {
    // Control execution
    __SET_VAR(data->,CHECK,,0);
    if (!__GET_VAR(data->EN)) {
        __SET_VAR(data->,ENO,,__BOOL_LITERAL(FALSE));
    return;
    }  else {
        __SET_VAR(data->,ENO,,__BOOL_LITERAL(TRUE));
    }
    // Initialise TEMP variables
    if (__GET_VAR(data->ENABLE,)) {
        int res;
        regs_flag_t flag = U32_REGS_FLAG;
        res = set_value_by_address((u32)__GET_VAR(data->ADDRESS,),(u64)__GET_VAR(data->VALUE,),flag);
        if(res!=0){
            __SET_VAR(data->,CHECK,,0);
            __SET_VAR(data->,ENO,,__BOOL_LITERAL(FALSE));
        }else{
            __SET_VAR(data->,CHECK,,1);
        }
    } else {
        __SET_VAR(data->,CHECK,,0);
        __SET_VAR(data->,ENO,,__BOOL_LITERAL(FALSE));
    }
    return;
} // SET_PARAM_body__()
void READ_PARAM_UINT_init__(READ_PARAM_UINT *data__, BOOL retain) {
    __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
    __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
    __INIT_VAR(data__->ENABLE,0,retain)
    __INIT_VAR(data__->ADDRESS,0,retain)
    __INIT_VAR(data__->CHECK,0,retain)
    __INIT_VAR(data__->VALUE,0,retain)
}

void READ_PARAM_UINT_body__(READ_PARAM_UINT *data) {
    // Control execution
    __SET_VAR(data->,CHECK,,0);
    if (!__GET_VAR(data->EN)) {
        __SET_VAR(data->,ENO,,__BOOL_LITERAL(FALSE));
        return;
    }  else {
        __SET_VAR(data->,ENO,,__BOOL_LITERAL(TRUE));
    }
    // Initialise TEMP variables
    if (__GET_VAR(data->ENABLE,)) {
        int res;
        u64 value;
        regs_flag_t flag = U16_REGS_FLAG;
        res = get_value_by_address((u32)__GET_VAR(data->ADDRESS,),&value,flag);
        if(res!=0){
            __SET_VAR(data->,CHECK,,0);
            __SET_VAR(data->,ENO,,__BOOL_LITERAL(FALSE));
        }else{
            __SET_VAR(data->,VALUE,,(IEC_INT)value);
            __SET_VAR(data->,CHECK,,1);
        }
    } else {
        __SET_VAR(data->,VALUE,,0);
        __SET_VAR(data->,CHECK,,0);
        __SET_VAR(data->,ENO,,__BOOL_LITERAL(FALSE));
    }
    return;
} // READ_PARAM_body__() 
void SET_PARAM_UINT_init__(SET_PARAM_UINT *data__, BOOL retain) {
    __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
    __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
    __INIT_VAR(data__->ENABLE,0,retain)
    __INIT_VAR(data__->ADDRESS,0,retain)
    __INIT_VAR(data__->VALUE,0,retain)
    __INIT_VAR(data__->CHECK,0,retain)
}

// Code part
void SET_PARAM_UINT_body__(SET_PARAM_UINT *data) {
    // Control execution
    __SET_VAR(data->,CHECK,,0);
    if (!__GET_VAR(data->EN)) {
        __SET_VAR(data->,ENO,,__BOOL_LITERAL(FALSE));
        return;
    }  else {
        __SET_VAR(data->,ENO,,__BOOL_LITERAL(TRUE));
    }
    // Initialise TEMP variables
    if (__GET_VAR(data->ENABLE,)) {
        int res;
        regs_flag_t flag = U16_REGS_FLAG;
        res = set_value_by_address((u32)__GET_VAR(data->ADDRESS,),(u64)__GET_VAR(data->VALUE,),flag);
        if(res!=0){
            __SET_VAR(data->,CHECK,,0);
            __SET_VAR(data->,ENO,,__BOOL_LITERAL(FALSE));
        }else{
            __SET_VAR(data->,CHECK,,1);
        }
    } else {
        __SET_VAR(data->,CHECK,,0);
        __SET_VAR(data->,ENO,,__BOOL_LITERAL(FALSE));
    }
    return;
} // SET_PARAM_body__()
void READ_PARAM_USINT_init__(READ_PARAM_USINT *data__, BOOL retain) {
    __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
    __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
    __INIT_VAR(data__->ENABLE,0,retain)
    __INIT_VAR(data__->ADDRESS,0,retain)
    __INIT_VAR(data__->CHECK,0,retain)
    __INIT_VAR(data__->VALUE,0,retain)
}

void READ_PARAM_USINT_body__(READ_PARAM_USINT *data) {
    // Control execution
    __SET_VAR(data->,CHECK,,0);
    if (!__GET_VAR(data->EN)) {
        __SET_VAR(data->,ENO,,__BOOL_LITERAL(FALSE));
        return;
    }  else {
        __SET_VAR(data->,ENO,,__BOOL_LITERAL(TRUE));
    }
    // Initialise TEMP variables
    if (__GET_VAR(data->ENABLE,)) {
        int res;
        u64 value;
        regs_flag_t flag = U8_REGS_FLAG;
        res = get_value_by_address((u32)__GET_VAR(data->ADDRESS,),&value,flag);
        if(res!=0){
            __SET_VAR(data->,CHECK,,0);
            __SET_VAR(data->,ENO,,__BOOL_LITERAL(FALSE));
        }else{
            __SET_VAR(data->,VALUE,,(IEC_SINT)value);
            __SET_VAR(data->,CHECK,,1);
        }
    } else {
        __SET_VAR(data->,ENO,,__BOOL_LITERAL(FALSE));
        __SET_VAR(data->,VALUE,,0);
        __SET_VAR(data->,CHECK,,0);
    }
    return;
} // READ_PARAM_body__() 
void SET_PARAM_USINT_init__(SET_PARAM_USINT *data__, BOOL retain) {
    __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
    __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
    __INIT_VAR(data__->ENABLE,0,retain)
    __INIT_VAR(data__->ADDRESS,0,retain)
    __INIT_VAR(data__->VALUE,0,retain)
    __INIT_VAR(data__->CHECK,0,retain)
}

// Code part
void SET_PARAM_USINT_body__(SET_PARAM_USINT *data) {
    // Control execution
    __SET_VAR(data->,CHECK,,0);
    if (!__GET_VAR(data->EN)) {
        __SET_VAR(data->,ENO,,__BOOL_LITERAL(FALSE));
        return;
    }  else {
        __SET_VAR(data->,ENO,,__BOOL_LITERAL(TRUE));
    }
    // Initialise TEMP variables
    if (__GET_VAR(data->ENABLE,)) {
        int res;
        regs_flag_t flag = U8_REGS_FLAG;
        res = set_value_by_address((u32)__GET_VAR(data->ADDRESS,),(u64)__GET_VAR(data->VALUE,),flag);
        if(res!=0){
            __SET_VAR(data->,CHECK,,0);
            __SET_VAR(data->,ENO,,__BOOL_LITERAL(FALSE));
        }else{
            __SET_VAR(data->,CHECK,,1);
        }
    } else {
        __SET_VAR(data->,CHECK,,0);
        __SET_VAR(data->,ENO,,__BOOL_LITERAL(FALSE));
    }
    return;
} // SET_PARAM_body__()

void READ_DI_init__(READ_DI *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->DI_OUT,0,retain)
}

// Code part
void READ_DI_body__(READ_DI *data__) {
  // Control execution
  if (!__GET_VAR(data__->EN)) {
    __SET_VAR(data__->,ENO,,__BOOL_LITERAL(FALSE));
    return;
  }
  __SET_VAR(data__->,ENO,,__BOOL_LITERAL(FALSE));
  // Initialise TEMP variables
  char di_name[] = "di_state";
  regs_template_t regs_template;
  regs_template.name = di_name;
  if(p_link_functions->regs_description_get_by_name(&regs_template)==0){
      regs_access_t reg;
      reg.value.op_u64 = 0;
      reg.flag = regs_template.type;
      u16 address = regs_template.guid & GUID_ADDRESS_MASK;
      if(p_link_functions->regs_get(address,&reg)==0){
          __SET_VAR(data__->,DI_OUT,,reg.value.op_u32);
          __SET_VAR(data__->,ENO,,__BOOL_LITERAL(TRUE));
      }else{
          __SET_VAR(data__->,ENO,,__BOOL_LITERAL(FALSE));
      }
  }else{
      __SET_VAR(data__->,ENO,,__BOOL_LITERAL(FALSE));
  }
  return;
} // READ_DI_body__()

void READ_DI_CNT_init__(READ_DI_CNT *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->DI_NUMBER,0,retain)
  __INIT_VAR(data__->DI_CNT_VALUE,0,retain)
}
// Code part
void READ_DI_CNT_body__(READ_DI_CNT *data__) {
    // Control execution
    if (!__GET_VAR(data__->EN)) {
        __SET_VAR(data__->,ENO,,__BOOL_LITERAL(FALSE));
      return;
    }  else {
        __SET_VAR(data__->,ENO,,__BOOL_LITERAL(TRUE));
    }
    // Initialise TEMP variables
    char cnt_name[] = "di_cnt";
    regs_template_t regs_template;
    regs_template.name = cnt_name;
    if(p_link_functions->regs_description_get_by_name(&regs_template)==0){
        regs_access_t reg;
        reg.value.op_u64 = 0;
        reg.flag = regs_template.type;
        u16 address = regs_template.guid & GUID_ADDRESS_MASK;
        u16 shift = p_link_functions->regs_size_in_byte(regs_template.type) * __GET_VAR(data__->DI_NUMBER,);
        address +=  shift;
        if(p_link_functions->regs_get(address,&reg)==0){
              __SET_VAR(data__->,DI_CNT_VALUE,,(u64)reg.value.op_u64);
        }else{
            __SET_VAR(data__->,ENO,,__BOOL_LITERAL(FALSE));
        }
    }else{
        __SET_VAR(data__->,ENO,,__BOOL_LITERAL(FALSE));
    }
    return;
} // READ_DI_CNT_body__()

void READ_DI_FREQ_init__(READ_DI_FREQ *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->DI_NUMBER,0,retain)
  __INIT_VAR(data__->DI_FREQ_VALUE,0,retain)
}
// Code part
void READ_DI_FREQ_body__(READ_DI_FREQ *data__) {
    // Control execution
    if (!__GET_VAR(data__->EN)) {
        __SET_VAR(data__->,ENO,,__BOOL_LITERAL(FALSE));
      return;
    }  else {
        __SET_VAR(data__->,ENO,,__BOOL_LITERAL(TRUE));
    }
    // Initialise TEMP variables
    char freq_name[] = "di_freq";
    regs_template_t regs_template;
    regs_template.name = freq_name;
    if(p_link_functions->regs_description_get_by_name(&regs_template)==0){
        regs_access_t reg;
        reg.value.op_f = 0.0;
        reg.flag = regs_template.type;
        u16 address = regs_template.guid & GUID_ADDRESS_MASK;
        u16 shift = p_link_functions->regs_size_in_byte(regs_template.type) * __GET_VAR(data__->DI_NUMBER,);
        address +=  shift;
        if(p_link_functions->regs_get(address,&reg)==0){
              __SET_VAR(data__->,DI_FREQ_VALUE,,reg.value.op_f);
        }else{
            __SET_VAR(data__->,ENO,,__BOOL_LITERAL(FALSE));
        }
    }else{
        __SET_VAR(data__->,ENO,,__BOOL_LITERAL(FALSE));
    }
    return;
} // DI_FREQ_body__()

void READ_AI_STATE_init__(READ_AI_STATE *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->AI_STATE_VALUE,0,retain)
}

// Code part
void READ_AI_STATE_body__(READ_AI_STATE *data__) {
  // Control execution
  if (!__GET_VAR(data__->EN)) {
    __SET_VAR(data__->,ENO,,__BOOL_LITERAL(FALSE));
    return;
  }
  __SET_VAR(data__->,ENO,,__BOOL_LITERAL(FALSE));
  // Initialise TEMP variables
  char ai_state_name[] = "ai_state";
  regs_template_t regs_template;
  regs_template.name = ai_state_name;
  if(p_link_functions->regs_description_get_by_name(&regs_template)==0){
      regs_access_t reg;
      reg.value.op_u64 = 0;
      reg.flag = regs_template.type;
      u16 address = regs_template.guid & GUID_ADDRESS_MASK;
      if(p_link_functions->regs_get(address,&reg)==0){
          __SET_VAR(data__->,AI_STATE_VALUE,,reg.value.op_u16);
          __SET_VAR(data__->,ENO,,__BOOL_LITERAL(TRUE));
      }else{
          __SET_VAR(data__->,ENO,,__BOOL_LITERAL(FALSE));
      }
  }else{
      __SET_VAR(data__->,ENO,,__BOOL_LITERAL(FALSE));
  }

  return;
} // READ_AI_STATE_body__()

void READ_AI_init__(READ_AI *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->AI_NUMBER,0,retain)
  __INIT_VAR(data__->AI_VALUE,0,retain)
}

// Code part
void READ_AI_body__(READ_AI *data__) {
  // Control execution
  if (!__GET_VAR(data__->EN)) {
    __SET_VAR(data__->,ENO,,__BOOL_LITERAL(FALSE));
    return;
  }  else {
    __SET_VAR(data__->,ENO,,__BOOL_LITERAL(TRUE));
  }
  // Initialise TEMP variables

  char ai_name[] = "ai_unit";
  regs_template_t regs_template;
  regs_template.name = ai_name;
  /*@todo add control ai_number*/
  if(p_link_functions->regs_description_get_by_name(&regs_template)==0){
      regs_access_t reg;
      reg.value.op_u64 = 0;
      reg.flag = regs_template.type;
      u16 address = regs_template.guid & GUID_ADDRESS_MASK;
      u16 shift = p_link_functions->regs_size_in_byte(regs_template.type) * __GET_VAR(data__->AI_NUMBER,);
      address +=  shift;
      if(p_link_functions->regs_get((u16)address,&reg)==0){
          __SET_VAR(data__->,AI_VALUE,,(u16)reg.value.op_u32);
      }else{
          __SET_VAR(data__->,ENO,,__BOOL_LITERAL(FALSE));
      }
  }else{
      __SET_VAR(data__->,ENO,,__BOOL_LITERAL(FALSE));
  }
  return;
} // READ_AI_body__()

void READ_DO_SC_init__(READ_DO_SC *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->DO_SC_FLAG,0,retain)
  __INIT_VAR(data__->DO_SC_EN,0,retain)
}

// Code part
void READ_DO_SC_body__(READ_DO_SC *data__) {
  // Control execution
  if (!__GET_VAR(data__->EN)) {
    __SET_VAR(data__->,ENO,,__BOOL_LITERAL(FALSE));
    return;
  }
  __SET_VAR(data__->,ENO,,__BOOL_LITERAL(FALSE));
  // Initialise TEMP variables
  char do_name[] = "do_sc_ctrl";
  regs_template_t regs_template;
  regs_template.name = do_name;
  if(p_link_functions->regs_description_get_by_name(&regs_template)==0){
      regs_access_t reg;
      reg.value.op_u64 = 0;
      reg.flag = regs_template.type;
      u16 address = regs_template.guid & GUID_ADDRESS_MASK;
      if(p_link_functions->regs_get(address,&reg)==0){
          __SET_VAR(data__->,DO_SC_FLAG,,(reg.value.op_u8>>4));   
          __SET_VAR(data__->,DO_SC_EN,,(reg.value.op_u8&0x0F));
          __SET_VAR(data__->,ENO,,__BOOL_LITERAL(TRUE));
      }else{
          __SET_VAR(data__->,ENO,,__BOOL_LITERAL(FALSE));
      }
  }else{
      __SET_VAR(data__->,ENO,,__BOOL_LITERAL(FALSE));
  }
  return;
} // READ_DO_SC_body__()

void READ_DO_init__(READ_DO *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->DO_OUT,0,retain)
}

// Code part
void READ_DO_body__(READ_DO *data__) {
  // Control execution
  if (!__GET_VAR(data__->EN)) {
    __SET_VAR(data__->,ENO,,__BOOL_LITERAL(FALSE));
    return;
  }
  __SET_VAR(data__->,ENO,,__BOOL_LITERAL(FALSE));
  // Initialise TEMP variables
  char do_name[] = "do_state";
  regs_template_t regs_template;
  regs_template.name = do_name;
  if(p_link_functions->regs_description_get_by_name(&regs_template)==0){
      regs_access_t reg;
      reg.value.op_u64 = 0;
      reg.flag = regs_template.type;
      u16 address = regs_template.guid & GUID_ADDRESS_MASK;
      if(p_link_functions->regs_get(address,&reg)==0){
          __SET_VAR(data__->,DO_OUT,,reg.value.op_u8);
          __SET_VAR(data__->,ENO,,__BOOL_LITERAL(TRUE));
      }else{
          __SET_VAR(data__->,ENO,,__BOOL_LITERAL(FALSE));
      }

  }else{
       __SET_VAR(data__->,ENO,,__BOOL_LITERAL(FALSE));
  }
  return;
} // READ_DO_body__()

void READ_RESET_init__(READ_RESET *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->RESET_NUM,0,retain)
  __INIT_VAR(data__->LAST_RESET,0,retain)
}

// Code part
void READ_RESET_body__(READ_RESET *data__) {
  // Control execution
  if (!__GET_VAR(data__->EN)) {
    __SET_VAR(data__->,ENO,,__BOOL_LITERAL(FALSE));
    return;
  }
  __SET_VAR(data__->,ENO,,__BOOL_LITERAL(FALSE));
  // Initialise TEMP variables
  char num_name[] = "reset_num";
  char last_name[] = "last_reset";
  regs_template_t regs_template;
  regs_template.name = num_name;
  if(p_link_functions->regs_description_get_by_name(&regs_template)==0){
      regs_access_t reg;
      reg.value.op_u64 = 0;
      reg.flag = regs_template.type;
      u16 address = regs_template.guid & GUID_ADDRESS_MASK;
      if(p_link_functions->regs_get(address,&reg)==0){
          __SET_VAR(data__->,RESET_NUM,,reg.value.op_u16);
          __SET_VAR(data__->,ENO,,__BOOL_LITERAL(TRUE));
      }else{
          __SET_VAR(data__->,ENO,,__BOOL_LITERAL(FALSE));
      }
      regs_template.name = last_name;
      if(p_link_functions->regs_description_get_by_name(&regs_template)==0){
          regs_access_t reg;
          reg.value.op_u64 = 0;
          reg.flag = regs_template.type;
          u16 address = regs_template.guid & GUID_ADDRESS_MASK;
          if(p_link_functions->regs_get(address,&reg)==0){
              __SET_VAR(data__->,LAST_RESET,,reg.value.op_u16);
              __SET_VAR(data__->,ENO,,__BOOL_LITERAL(TRUE));
          }else{
              __SET_VAR(data__->,ENO,,__BOOL_LITERAL(FALSE));
          }
      }else{
          __SET_VAR(data__->,ENO,,__BOOL_LITERAL(FALSE));
      }
  }else{
      __SET_VAR(data__->,ENO,,__BOOL_LITERAL(FALSE));
  }
  return;
} // READ_RESET_body__()

void READ_PWR_init__(READ_PWR *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->V_PWR,0,retain)
  __INIT_VAR(data__->V_BAT,0,retain)
}

// Code part
void READ_PWR_body__(READ_PWR *data__) {
  // Control execution
  if (!__GET_VAR(data__->EN)) {
    __SET_VAR(data__->,ENO,,__BOOL_LITERAL(FALSE));
    return;
  }
  __SET_VAR(data__->,ENO,,__BOOL_LITERAL(FALSE));
  // Initialise TEMP variables
  char bat_name[] = "v_bat";
  char external_pwr_name[] = "v_pwr";
  regs_template_t regs_template;
  regs_template.name = bat_name;
  if(p_link_functions->regs_description_get_by_name(&regs_template)==0){
      regs_access_t reg;
      reg.value.op_u64 = 0;
      reg.flag = regs_template.type;
      u16 address = regs_template.guid & GUID_ADDRESS_MASK;
      if(p_link_functions->regs_get(address,&reg)==0){
          __SET_VAR(data__->,V_BAT,,reg.value.op_f);
          __SET_VAR(data__->,ENO,,__BOOL_LITERAL(TRUE));
      }else{
          __SET_VAR(data__->,ENO,,__BOOL_LITERAL(FALSE));
      }
  }else{
      __SET_VAR(data__->,ENO,,__BOOL_LITERAL(FALSE));
  }
  regs_template.name = external_pwr_name;
  if(p_link_functions->regs_description_get_by_name(&regs_template)==0){
      regs_access_t reg;
      reg.value.op_u64 = 0;
      reg.flag = regs_template.type;
      u16 address = regs_template.guid & GUID_ADDRESS_MASK;
      if(p_link_functions->regs_get(address,&reg)==0){
          __SET_VAR(data__->,V_PWR,,reg.value.op_f);
          __SET_VAR(data__->,ENO,,__BOOL_LITERAL(TRUE));
      }else{
          __SET_VAR(data__->,ENO,,__BOOL_LITERAL(FALSE));
      }
  }else{
      __SET_VAR(data__->,ENO,,__BOOL_LITERAL(FALSE));
  }
  return;
} // READ_PWR_body__()

void READ_INTERNAL_TEMP_init__(READ_INTERNAL_TEMP *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->INTERNAL_TEMP_OUT,0,retain)
}

// Code part
void READ_INTERNAL_TEMP_body__(READ_INTERNAL_TEMP *data__) {
  // Control execution
  if (!__GET_VAR(data__->EN)) {
    __SET_VAR(data__->,ENO,,__BOOL_LITERAL(FALSE));
    return;
  }
  __SET_VAR(data__->,ENO,,__BOOL_LITERAL(FALSE));
  // Initialise TEMP variables
  char int_temp_name[] = "internal_temp";
  regs_template_t regs_template;
  regs_template.name = int_temp_name;
  if(p_link_functions->regs_description_get_by_name(&regs_template)==0){
      regs_access_t reg;
      reg.value.op_u64 = 0;
      reg.flag = regs_template.type;
      u16 address = regs_template.guid & GUID_ADDRESS_MASK;
      if(p_link_functions->regs_get(address,&reg)==0){
          __SET_VAR(data__->,INTERNAL_TEMP_OUT,,reg.value.op_f);
          __SET_VAR(data__->,ENO,,__BOOL_LITERAL(TRUE));
      }else{
          __SET_VAR(data__->,ENO,,__BOOL_LITERAL(FALSE));
      }
  }else{
      __SET_VAR(data__->,ENO,,__BOOL_LITERAL(FALSE));
  }
  return;
} // READ_INTERNAL_TEMP_body__()

void READ_SYS_TICK_COUNTER_init__(READ_SYS_TICK_COUNTER *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->SYS_TICK_COUNTER_VALUE,0,retain)
}
// Code part
void READ_SYS_TICK_COUNTER_body__(READ_SYS_TICK_COUNTER *data__) {
  // Control execution
  if (!__GET_VAR(data__->EN)) {
    __SET_VAR(data__->,ENO,,__BOOL_LITERAL(FALSE));
    return;
  }
  __SET_VAR(data__->,ENO,,__BOOL_LITERAL(FALSE));
  // Initialise TEMP variables
  char tick_name[] = "sys_tick_counter";
  regs_template_t regs_template;
  regs_template.name = tick_name;
  if(p_link_functions->regs_description_get_by_name(&regs_template)==0){
      regs_access_t reg;
      reg.value.op_u64 = 0;
      reg.flag = regs_template.type;
      u16 address = regs_template.guid & GUID_ADDRESS_MASK;
      if(p_link_functions->regs_get(address,&reg)==0){
          __SET_VAR(data__->,SYS_TICK_COUNTER_VALUE,,reg.value.op_u64);
          __SET_VAR(data__->,ENO,,__BOOL_LITERAL(TRUE));
      }else{
          __SET_VAR(data__->,ENO,,__BOOL_LITERAL(FALSE));
      }
  }else{
      __SET_VAR(data__->,ENO,,__BOOL_LITERAL(FALSE));
  }
  return;
} // READ_SYS_TICK_COUNTER_body__()

void WRITE_MDB_ADDRESS_init__(WRITE_MDB_ADDRESS *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->MDB_ADDR,0,retain)
}

// Code part
void WRITE_MDB_ADDRESS_body__(WRITE_MDB_ADDRESS *data__) {
  // Control execution
  if (!__GET_VAR(data__->EN)) {
    __SET_VAR(data__->,ENO,,__BOOL_LITERAL(FALSE));
    return;
  }
  // Initialise TEMP variables
  char mdb_name[] = "mdb_addr";
  regs_template_t regs_template;
  regs_template.name = mdb_name;
  if(p_link_functions->regs_description_get_by_name(&regs_template)==0){
      regs_access_t reg;
      reg.value.op_u16 = (u16)__GET_VAR(data__->MDB_ADDR,);
      reg.flag = regs_template.type;
      u16 address = regs_template.guid & GUID_ADDRESS_MASK;
      if(p_link_functions->regs_set(address,reg)==0){
          __SET_VAR(data__->,ENO,,__BOOL_LITERAL(TRUE));
      }else{
          __SET_VAR(data__->,ENO,,__BOOL_LITERAL(FALSE));
      }
  }else{
      __SET_VAR(data__->,ENO,,__BOOL_LITERAL(FALSE));
  }
  return;
} // WRITE_MDB_ADDRESS_body__()

void WRITE_UART_SETS_init__(WRITE_UART_SETS *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->MESO_UART,0,retain)
  __INIT_VAR(data__->SET_RS_485_2,0,retain)
  __INIT_VAR(data__->SET_RS_232,0,retain)
  __INIT_VAR(data__->SET_RS_485_1,0,retain)
  __INIT_VAR(data__->SET_RS_485_IMMO,0,retain)
  __INIT_VAR(data__->SET_HART,0,retain)
}

// Code part
void WRITE_UART_SETS_body__(WRITE_UART_SETS *data__) {
  // Control execution
    if (!__GET_VAR(data__->EN)) {
        __SET_VAR(data__->,ENO,,__BOOL_LITERAL(FALSE));
        return;
    }
  // Initialise TEMP variables
    const char name_uart1[] = "uart1_sets";
    const char name_uart2[] = "uart2_sets";
    const char name_uart3[] = "uart3_sets";
    const char name_uart5[] = "uart5_sets";
    const char name_uart6[] = "uart6_sets";
    const char name_uart7[] = "uart7_sets";
    const char * name;
    u16 uart_settings = 0;
    for (u8 i=0;i<6;i++){
        regs_template_t regs_template;
        switch(i){
        case (0):
            regs_template.name = name_uart1;
            uart_settings = (u16)__GET_VAR(data__->MESO_UART,);
            break;
        case (1):
            regs_template.name = name_uart2;
            uart_settings = (u16)__GET_VAR(data__->SET_RS_485_2,);
            break;
        case (2):
            regs_template.name = name_uart3;
            uart_settings = (u16)__GET_VAR(data__->SET_RS_232,);
            break;
        case (3):
            regs_template.name = name_uart5;
            uart_settings = (u16)__GET_VAR(data__->SET_RS_485_1,);
            break;
        case (4):
            regs_template.name = name_uart6;
            uart_settings = (u16)__GET_VAR(data__->SET_RS_485_IMMO,);
            break;
        case (5):
            regs_template.name = name_uart7;
            uart_settings = (u16)__GET_VAR(data__->SET_HART,);
            break;
        default:
            __SET_VAR(data__->,ENO,,__BOOL_LITERAL(FALSE));
            break;
        }
        if(p_link_functions->regs_description_get_by_name(&regs_template)==0){
            regs_access_t reg;
            reg.value.op_u16 = uart_settings;
            reg.flag = regs_template.type;
            u16 address = regs_template.guid & GUID_ADDRESS_MASK;
            if(p_link_functions->regs_set(address,reg)==0){
                __SET_VAR(data__->,ENO,,__BOOL_LITERAL(TRUE));
            }else{
                __SET_VAR(data__->,ENO,,__BOOL_LITERAL(FALSE));
                break;
            }
        }else{
            __SET_VAR(data__->,ENO,,__BOOL_LITERAL(FALSE));
            break;
        }

    }
    return;
} // WRITE_UART_SETS_body__()

void WRITE_CH_TIMEOUT_init__(WRITE_CH_TIMEOUT *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->CH_NUMBER,0,retain)
  __INIT_VAR(data__->CHANNEL_TIMEOUT,0,retain)
}
// Code part
void WRITE_CH_TIMEOUT_body__(WRITE_CH_TIMEOUT *data__) {
    // Control execution
    if (!__GET_VAR(data__->EN)) {
        __SET_VAR(data__->,ENO,,__BOOL_LITERAL(FALSE));
      return;
    }
    // Initialise TEMP variables
    char ch_timeout_name[] = "channels_timeout";
    regs_template_t regs_template;
    regs_template.name = ch_timeout_name;
    if(p_link_functions->regs_description_get_by_name(&regs_template)==0){
        regs_access_t reg;
        reg.flag = regs_template.type;
        u8 address = regs_template.guid & GUID_ADDRESS_MASK;
        u8 shift = p_link_functions->regs_size_in_byte(regs_template.type) * __GET_VAR(data__->CH_NUMBER,);
        address +=  shift;
        reg.value.op_u32 = __GET_VAR(data__->CHANNEL_TIMEOUT,);
        if(p_link_functions->regs_set(address,reg)==0){
            __SET_VAR(data__->,ENO,,__BOOL_LITERAL(TRUE));
        }else{
            __SET_VAR(data__->,ENO,,__BOOL_LITERAL(FALSE));
        }
    }else{
        __SET_VAR(data__->,ENO,,__BOOL_LITERAL(FALSE));
    }
    return;
} // WRITE_CH_TIMEOUT_body__()

void WRITE_DI_NOISE_FLTR_10US_init__(WRITE_DI_NOISE_FLTR_10US *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->DI_NUMBER,0,retain)
  __INIT_VAR(data__->DI_NOISE_FLTR_VALUE_10US,0,retain)
}
// Code part
void WRITE_DI_NOISE_FLTR_10US_body__(WRITE_DI_NOISE_FLTR_10US *data__) {
    // Control execution
    if (!__GET_VAR(data__->EN)) {
        __SET_VAR(data__->,ENO,,__BOOL_LITERAL(FALSE));
      return;
    }
    // Initialise TEMP variables
    char noise_name[] = "di_noise_fltr_us";
    regs_template_t regs_template;
    regs_template.name = noise_name;
    if(p_link_functions->regs_description_get_by_name(&regs_template)==0){
        regs_access_t reg;
        reg.flag = regs_template.type;
        u16 address = regs_template.guid & GUID_ADDRESS_MASK;
        u16 shift = p_link_functions->regs_size_in_byte(regs_template.type) * __GET_VAR(data__->DI_NUMBER,);
        address +=  shift;
        reg.value.op_u16 = __GET_VAR(data__->DI_NOISE_FLTR_VALUE_10US,);
        if(p_link_functions->regs_set(address,reg)==0){
            __SET_VAR(data__->,ENO,,__BOOL_LITERAL(TRUE));
        }else{
            __SET_VAR(data__->,ENO,,__BOOL_LITERAL(FALSE));
        }
    }else{
        __SET_VAR(data__->,ENO,,__BOOL_LITERAL(FALSE));
    }
    return;
} // DI_PULSELESS_body__()

void WRITE_DI_PULSELESS_init__(WRITE_DI_PULSELESS *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->DI_NUMBER,0,retain)
  __INIT_VAR(data__->DI_PULSELESS_VALUE,0,retain)
}
// Code part
void WRITE_DI_PULSELESS_body__(WRITE_DI_PULSELESS *data__) {
    // Control execution
    if (!__GET_VAR(data__->EN)) {
        __SET_VAR(data__->,ENO,,__BOOL_LITERAL(FALSE));
      return;
    }
    // Initialise TEMP variables
    char pulseless_name[] = "di_pulseless";
    regs_template_t regs_template;
    regs_template.name = pulseless_name;
    if(p_link_functions->regs_description_get_by_name(&regs_template)==0){
        regs_access_t reg;
        reg.flag = regs_template.type;
        u16 address = regs_template.guid & GUID_ADDRESS_MASK;
        u16 shift = p_link_functions->regs_size_in_byte(regs_template.type) * __GET_VAR(data__->DI_NUMBER,);
        address +=  shift;
        reg.value.op_u32 = __GET_VAR(data__->DI_PULSELESS_VALUE,);
        if(p_link_functions->regs_set(address,reg)==0){
            __SET_VAR(data__->,ENO,,__BOOL_LITERAL(TRUE));
        }else{
            __SET_VAR(data__->,ENO,,__BOOL_LITERAL(FALSE));
        }
    }else{
        __SET_VAR(data__->,ENO,,__BOOL_LITERAL(FALSE));
    }
    return;
} // WRITE_DI_PULSELESS_body__()

void WRITE_DI_MODE_init__(WRITE_DI_MODE *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->DI_NUMBER,0,retain)
  __INIT_VAR(data__->DI_MODE_VALUE,0,retain)
}
// Code part
void WRITE_DI_MODE_body__(WRITE_DI_MODE *data__) {
    // Control execution
    if (!__GET_VAR(data__->EN)) {
        __SET_VAR(data__->,ENO,,__BOOL_LITERAL(FALSE));
      return;
    }
    // Initialise TEMP variables
    char mode_name[] = "di_mode";
    regs_template_t regs_template;
    regs_template.name = mode_name;
    if(p_link_functions->regs_description_get_by_name(&regs_template)==0){
        regs_access_t reg;
        reg.flag = regs_template.type;
        u16 address = regs_template.guid & GUID_ADDRESS_MASK;
        u16 shift = p_link_functions->regs_size_in_byte(regs_template.type) * __GET_VAR(data__->DI_NUMBER,);
        address +=  shift;
        reg.value.op_u16 = __GET_VAR(data__->DI_MODE_VALUE,);
        if(p_link_functions->regs_set(address,reg)==0){
            __SET_VAR(data__->,ENO,,__BOOL_LITERAL(TRUE));
        }else{
            __SET_VAR(data__->,ENO,,__BOOL_LITERAL(FALSE));
        }
    }else{
        __SET_VAR(data__->,ENO,,__BOOL_LITERAL(FALSE));
    }
    return;
} // DI_MODE_body__()

void WRITE_DO_init__(WRITE_DO *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->DO_VALUE,0,retain)
  __INIT_VAR(data__->DO_MASK,0,retain)
}

// Code part
void WRITE_DO_body__(WRITE_DO *data__) {
  // Control execution
  if (!__GET_VAR(data__->EN)) {
    __SET_VAR(data__->,ENO,,__BOOL_LITERAL(FALSE));
    return;
  }
  // Initialise TEMP variables
  char do_name[] = "do_ctrl";
  regs_template_t regs_template;
  regs_template.name = do_name;
  if(p_link_functions->regs_description_get_by_name(&regs_template)==0){
      regs_access_t reg;
      reg.value.op_u16 = (u16)__GET_VAR(data__->DO_VALUE,) | (((u16)__GET_VAR(data__->DO_MASK,))<<4);
      reg.flag = regs_template.type;
      u16 address = regs_template.guid & GUID_ADDRESS_MASK;
      if(p_link_functions->regs_set(address,reg)==0){
          __SET_VAR(data__->,ENO,,__BOOL_LITERAL(TRUE));
      }else{
          __SET_VAR(data__->,ENO,,__BOOL_LITERAL(FALSE));
      }
  }else{
      __SET_VAR(data__->,ENO,,__BOOL_LITERAL(FALSE));
  }
  return;
} // WRITE_DO_body__()

void WRITE_DO_SC_init__(WRITE_DO_SC *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->DO_SC_FLAG,0,retain)
  __INIT_VAR(data__->DO_SC_EN,0,retain)
}

// Code part
void WRITE_DO_SC_body__(WRITE_DO_SC *data__) {
  // Control execution
  if (!__GET_VAR(data__->EN)) {
    __SET_VAR(data__->,ENO,,__BOOL_LITERAL(FALSE));
    return;
  }
  // Initialise TEMP variables
  char do_sc_name[] = "do_sc_ctrl";
  regs_template_t regs_template;
  regs_template.name = do_sc_name;
  if(p_link_functions->regs_description_get_by_name(&regs_template)==0){
      regs_access_t reg;
      reg.value.op_u16 = (u16)__GET_VAR(data__->DO_SC_FLAG,) | (((u16)__GET_VAR(data__->DO_SC_EN,))<<4);
      reg.flag = regs_template.type;
      u16 address = regs_template.guid & GUID_ADDRESS_MASK;
      if(p_link_functions->regs_set(address,reg)==0){
          __SET_VAR(data__->,ENO,,__BOOL_LITERAL(TRUE));
      }else{
          __SET_VAR(data__->,ENO,,__BOOL_LITERAL(FALSE));
      }
  }else{
      __SET_VAR(data__->,ENO,,__BOOL_LITERAL(FALSE));
  }
  return;
} // WRITE_DO_SC_body__()

void WRITE_DO_PWM_FREQ_init__(WRITE_DO_PWM_FREQ *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->DO_PWM_FREQ,0,retain)
}

// Code part
void WRITE_DO_PWM_FREQ_body__(WRITE_DO_PWM_FREQ *data__) {
  // Control execution
  if (!__GET_VAR(data__->EN)) {
    __SET_VAR(data__->,ENO,,__BOOL_LITERAL(FALSE));
    return;
  }
  // Initialise TEMP variables
  char do_freq_name[] = "do_pwm_freq";
  regs_template_t regs_template;
  regs_template.name = do_freq_name;
  if(p_link_functions->regs_description_get_by_name(&regs_template)==0){
      regs_access_t reg;
      reg.value.op_u16 = (u16)__GET_VAR(data__->DO_PWM_FREQ,);
      reg.flag = regs_template.type;
      u16 address = regs_template.guid & GUID_ADDRESS_MASK;
      if(p_link_functions->regs_set(address,reg)==0){
          __SET_VAR(data__->,ENO,,__BOOL_LITERAL(TRUE));
      }else{
          __SET_VAR(data__->,ENO,,__BOOL_LITERAL(FALSE));
      }
  }else{
      __SET_VAR(data__->,ENO,,__BOOL_LITERAL(FALSE));
  }  
  return;
} // WRITE_DO_PWM_FREQ_body__()

void WRITE_DO_PWM_CTRL_init__(WRITE_DO_PWM_CTRL *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->DO_NUMBER,0,retain)
  __INIT_VAR(data__->DO_PWM_CTRL,0,retain)
}
// Code part
void WRITE_DO_PWM_CTRL_body__(WRITE_DO_PWM_CTRL *data__) {
    // Control execution
    if (!__GET_VAR(data__->EN)) {
        __SET_VAR(data__->,ENO,,__BOOL_LITERAL(FALSE));
      return;
    }
    // Initialise TEMP variables
    char ctrl_name[] = "do_pwm_ctrl";
    regs_template_t regs_template;
    regs_template.name = ctrl_name;
    if(p_link_functions->regs_description_get_by_name(&regs_template)==0){
        regs_access_t reg;
        reg.flag = regs_template.type;
        u8 address = regs_template.guid & GUID_ADDRESS_MASK;
        u8 shift = p_link_functions->regs_size_in_byte(regs_template.type) * __GET_VAR(data__->DO_NUMBER,);
        address +=  shift;
        reg.value.op_u16 = __GET_VAR(data__->DO_PWM_CTRL,);
        if(p_link_functions->regs_set(address,reg)==0){
            __SET_VAR(data__->,ENO,,__BOOL_LITERAL(TRUE));
        }else{
            __SET_VAR(data__->,ENO,,__BOOL_LITERAL(FALSE));
        }
    }else{
        __SET_VAR(data__->,ENO,,__BOOL_LITERAL(FALSE));
    }
    return;
} // WRITE_DO_PWM_CTRL_body__()

void STRUCT_REAL_TIME_init__(STRUCT_REAL_TIME *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->HOUR_TIME,0,retain)
  __INIT_VAR(data__->MINUTE_TIME,0,retain)
  __INIT_VAR(data__->SEC_TIME,0,retain)
  __INIT_VAR(data__->SUB_SEC_TIME,0,retain)
  __INIT_VAR(data__->WEEK_DAY_TIME,0,retain)
  __INIT_VAR(data__->MONTH_TIME,0,retain)
  __INIT_VAR(data__->DATE_TIME,0,retain)
  __INIT_VAR(data__->YEAR_TIME,0,retain)
  __INIT_VAR(data__->YEAR_DAY_TIME,0,retain)
}

// Code part
void STRUCT_REAL_TIME_body__(STRUCT_REAL_TIME *data__) {
  // Control execution
  if (!__GET_VAR(data__->EN)) {
    __SET_VAR(data__->,ENO,,__BOOL_LITERAL(FALSE));
    return;
  }
  __SET_VAR(data__->,ENO,,__BOOL_LITERAL(FALSE));
  // Initialise TEMP variables
  char time_name[] = "time_hms";
  regs_template_t regs_template;
  regs_template.name = time_name;
  if(p_link_functions->regs_description_get_by_name(&regs_template)==0){
      sofi_time_r time_temp;
      u16 address = regs_template.guid & GUID_ADDRESS_MASK;
      if(p_link_functions->regs_get_buffer(address,(u8*)(void*)&time_temp,sizeof(sofi_time_r))==0){
            __SET_VAR(data__->,HOUR_TIME,,time_temp.hour);
            __SET_VAR(data__->,MINUTE_TIME,,time_temp.min);
            __SET_VAR(data__->,SEC_TIME,,time_temp.sec);
            __SET_VAR(data__->,SUB_SEC_TIME,,time_temp.sub_sec);
            __SET_VAR(data__->,WEEK_DAY_TIME,,time_temp.week_day);
            __SET_VAR(data__->,MONTH_TIME,,time_temp.month);
            __SET_VAR(data__->,DATE_TIME,,time_temp.date);
            __SET_VAR(data__->,YEAR_TIME,,time_temp.year);
            __SET_VAR(data__->,YEAR_DAY_TIME,,time_temp.year_day);
            __SET_VAR(data__->,ENO,,__BOOL_LITERAL(TRUE));
      }else{
          __SET_VAR(data__->,ENO,,__BOOL_LITERAL(FALSE));
      }
  }else{
      __SET_VAR(data__->,ENO,,__BOOL_LITERAL(FALSE));
  }
  return;
} // STRUCT_REAL_TIME_body__()

void UNIX_TIME_init__(UNIX_TIME *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->UNIX_TIME_WRITE,0,retain)
  __INIT_VAR(data__->UNIX_TIME_READ,0,retain)
  __INIT_VAR(data__->UNIX_TIME_WRITED,0,retain)
}

// Code part
void UNIX_TIME_body__(UNIX_TIME *data__) {
  // Control execution
    if (!__GET_VAR(data__->EN)) {
        __SET_VAR(data__->,ENO,,__BOOL_LITERAL(FALSE));
        return;
    }
  // Initialise TEMP variables
    u32 write, writed;
    write = __GET_VAR(data__->UNIX_TIME_WRITE,);
    writed = __GET_VAR(data__->UNIX_TIME_WRITED,);
    char name[] = "unix_time_sec";
    regs_template_t regs_template;
    regs_template.name = name;
    if(p_link_functions->regs_description_get_by_name(&regs_template)==0){
        s32 unix_time_read;
        u16 address = regs_template.guid & GUID_ADDRESS_MASK;
        if(write!=writed){
            regs_access_t reg;
            __SET_VAR(data__->,UNIX_TIME_WRITED,,write);
            reg.value.op_s32 = (s32)write;//data__->UNIX_TIME_WRITE
            reg.flag = regs_template.type;
            if(p_link_functions->regs_set((u16)address,reg)==0){
                __SET_VAR(data__->,ENO,,__BOOL_LITERAL(TRUE));
            }else{
                __SET_VAR(data__->,ENO,,__BOOL_LITERAL(FALSE));
            }
        }
        if(p_link_functions->regs_get_buffer(address,(u8*)(void*)&unix_time_read,sizeof(s32))==0){
            __SET_VAR(data__->,UNIX_TIME_READ,,unix_time_read);
        }else{
            __SET_VAR(data__->,ENO,,__BOOL_LITERAL(FALSE));
        }
    }else{
        __SET_VAR(data__->,ENO,,__BOOL_LITERAL(FALSE));
    }

    return;
} // UNIX_TIME_body__()

void WRITE_STRUCT_TIME_init__(WRITE_STRUCT_TIME *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->SEC_IN_MIN,0,retain)
  __INIT_VAR(data__->MINUTE_IN_HOUR,0,retain)
  __INIT_VAR(data__->HOUR_IN_DAY,0,retain)
  __INIT_VAR(data__->DATE_IN_MONTH,0,retain)
  __INIT_VAR(data__->MONTH_IN_YEAR,0,retain)
  __INIT_VAR(data__->YEAR_SINCE_2000,0,retain)
  __INIT_VAR(data__->SEC_IN_MIN_WRITED,0,retain)
  __INIT_VAR(data__->MINUTE_IN_HOUR_WRITED,0,retain)
  __INIT_VAR(data__->HOUR_IN_DAY_WRITED,0,retain)
  __INIT_VAR(data__->DATE_IN_MONTH_WRITED,0,retain)
  __INIT_VAR(data__->MONTH_IN_YEAR_WRITED,0,retain)
  __INIT_VAR(data__->YEAR_SINCE_2000_WRITED,0,retain)
}

// Code part
void WRITE_STRUCT_TIME_body__(WRITE_STRUCT_TIME *data__) {
  // Control execution
    if (!__GET_VAR(data__->EN)) {
        __SET_VAR(data__->,ENO,,__BOOL_LITERAL(FALSE));
        return;
    }
  // Initialise TEMP variables
    u32 write, writed;
    if((__GET_VAR(data__->SEC_IN_MIN,)!= __GET_VAR(data__->SEC_IN_MIN_WRITED,))||
       (__GET_VAR(data__->MINUTE_IN_HOUR,)!= __GET_VAR(data__->MINUTE_IN_HOUR_WRITED,))||        
       (__GET_VAR(data__->HOUR_IN_DAY,)!= __GET_VAR(data__->HOUR_IN_DAY_WRITED,))||
       (__GET_VAR(data__->DATE_IN_MONTH,)!= __GET_VAR(data__->DATE_IN_MONTH_WRITED,))||
       (__GET_VAR(data__->MONTH_IN_YEAR,)!= __GET_VAR(data__->MONTH_IN_YEAR_WRITED,))||
       (__GET_VAR(data__->YEAR_SINCE_2000,)!= __GET_VAR(data__->YEAR_SINCE_2000_WRITED,))){
        sofi_time_r sofi_time;
        sofi_time.sub_sec = 0;
        sofi_time.sec = __GET_VAR(data__->SEC_IN_MIN,);
        sofi_time.min = __GET_VAR(data__->MINUTE_IN_HOUR,);
        sofi_time.hour = __GET_VAR(data__->HOUR_IN_DAY,);
        sofi_time.week_day = 1;
        sofi_time.date = __GET_VAR(data__->DATE_IN_MONTH,);
        sofi_time.month = __GET_VAR(data__->MONTH_IN_YEAR,);
        sofi_time.year = __GET_VAR(data__->YEAR_SINCE_2000,);
        sofi_time.year_day = 0;
        __SET_VAR(data__->,SEC_IN_MIN_WRITED,,__GET_VAR(data__->SEC_IN_MIN,));
        __SET_VAR(data__->,MINUTE_IN_HOUR_WRITED,,__GET_VAR(data__->MINUTE_IN_HOUR,));
        __SET_VAR(data__->,HOUR_IN_DAY_WRITED,,__GET_VAR(data__->HOUR_IN_DAY,));
        __SET_VAR(data__->,DATE_IN_MONTH_WRITED,,__GET_VAR(data__->DATE_IN_MONTH,));
        __SET_VAR(data__->,MONTH_IN_YEAR_WRITED,,__GET_VAR(data__->MONTH_IN_YEAR,));
        __SET_VAR(data__->,YEAR_SINCE_2000_WRITED,,__GET_VAR(data__->YEAR_SINCE_2000,));
        if((sofi_time.month>0) && (sofi_time.month<=12) && (sofi_time.hour<=23)
            && (sofi_time.min<=59) && (sofi_time.sec<=59) && (sofi_time.year<=99)){
            char time_name[] = "time_hms";
            regs_template_t regs_template;
            regs_template.name = time_name;
            if(p_link_functions->regs_description_get_by_name(&regs_template)==0){
                u16 address = regs_template.guid & GUID_ADDRESS_MASK;
                if(p_link_functions->regs_set_buffer(address,(u8*)(void*)&sofi_time,sizeof(sofi_time_r))!=0){
                    __SET_VAR(data__->,ENO,,__BOOL_LITERAL(FALSE));
                }
            }else{
                __SET_VAR(data__->,ENO,,__BOOL_LITERAL(FALSE));
            }
        }else{
            __SET_VAR(data__->,ENO,,__BOOL_LITERAL(FALSE));
        }
    }
    return;
} // UNIX_TIME_body__()

#endif //SOFI_BEREMIZ_C
