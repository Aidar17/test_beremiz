/**
 * @file C:\Beremiz\beremiz_test_projects\MODBUS_PACKET_TEST\build//sofi/freertos/src/POUS.c
 * @author Shoma Gane <shomagan@gmail.com>
 *         Ayrat Girfanov <girfanov.ayrat@yandex.ru>
 * @defgroup beremiz 
 * @ingroup beremiz 
 * @version 0.1 
 * @brief  TODO!!! write brief in 
 */
    /*
     * Copyright (c) 2018 Snema Service
     * All rights reserved.
     *
     * Redistribution and use in source and binary forms, with or without modification,
     * are permitted provided that the following conditions are met:
     *
     * 1. Redistributions of source code must retain the above copyright notice,
     *    this list of conditions and the following disclaimer.
     * 2. Redistributions in binary form must reproduce the above copyright notice,
     *    this list of conditions and the following disclaimer in the documentation
     *    and/or other materials provided with the distribution.
     * 3. The name of the author may not be used to endorse or promote products
     *    derived from this software without specific prior written permission.
     *
     * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
     * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
     * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
     * SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
     * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
     * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
     * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
     * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
     * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY
     * OF SUCH DAMAGE.
     *
     * This file is part of the sofi PLC.
     *
     * Author: Shoma Gane <shomagan@gmail.com>
     *         Ayrat Girfanov <girfanov.ayrat@yandex.ru>
     */
#ifndef POUS_C
#define POUS_C
#include "POUS.h"
#include "config_task.h"
#include "plc_main.h"
void CHECC_TEST_init__(CHECC_TEST *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  __INIT_EXTERNAL(UINT,REG1,data__->REG1,retain)
  __INIT_EXTERNAL(UINT,REG2,data__->REG2,retain)
  __INIT_EXTERNAL(UINT,REG3,data__->REG3,retain)
  READ_DI_init__(&data__->READ_DI0,retain);
  WRITE_DO_init__(&data__->WRITE_DO0,retain);
  __INIT_VAR(data__->NUM1,65535,retain)
  __INIT_VAR(data__->MUX11_OUT,0,retain)
}

// Code part
void CHECC_TEST_body__(CHECC_TEST *data__) {
  // Control execution
  if (!__GET_VAR(data__->EN)) {
    __SET_VAR(data__->,ENO,,__BOOL_LITERAL(FALSE));
    return;
  }
  else {
    __SET_VAR(data__->,ENO,,__BOOL_LITERAL(TRUE));
  }
  // Initialise TEMP variables

  READ_DI_body__(&data__->READ_DI0);
  __SET_VAR(data__->,MUX11_OUT,,MUX__UINT__UDINT__UINT(
    (BOOL)__BOOL_LITERAL(TRUE),
    NULL,
    (UDINT)__GET_VAR(data__->READ_DI0.DI_OUT,),
    (UINT)3,
    (UINT)__GET_EXTERNAL(data__->REG1,),
    (UINT)__GET_EXTERNAL(data__->REG2,),
    (UINT)__GET_EXTERNAL(data__->REG3,)));
  __SET_VAR(data__->WRITE_DO0.,DO_VALUE,,__GET_VAR(data__->MUX11_OUT,));
  __SET_VAR(data__->WRITE_DO0.,DO_MASK,,__GET_VAR(data__->NUM1,));
  WRITE_DO_body__(&data__->WRITE_DO0);

  goto __end;

__end:
  return;
} // CHECC_TEST_body__() 





void STRESSS_init__(STRESSS *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->A1,5,retain)
  __INIT_VAR(data__->A2,6,1)
  __INIT_VAR(data__->A3,0,retain)
  __INIT_VAR(data__->ADD1_OUT,0,retain)
}

// Code part
void STRESSS_body__(STRESSS *data__) {
  // Control execution
  if (!__GET_VAR(data__->EN)) {
    __SET_VAR(data__->,ENO,,__BOOL_LITERAL(FALSE));
    return;
  }
  else {
    __SET_VAR(data__->,ENO,,__BOOL_LITERAL(TRUE));
  }
  // Initialise TEMP variables

  __SET_VAR(data__->,ADD1_OUT,,ADD__DINT__DINT(
    (BOOL)__BOOL_LITERAL(TRUE),
    NULL,
    (UINT)2,
    (DINT)__GET_VAR(data__->A2,),
    (DINT)__GET_VAR(data__->A1,)));
  __SET_VAR(data__->,A3,,__GET_VAR(data__->ADD1_OUT,));

  goto __end;

__end:
  return;
} // STRESSS_body__() 





void PERFOMENS_init__(PERFOMENS *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->A1,3,retain)
  __INIT_VAR(data__->A2,3,retain)
  __INIT_VAR(data__->A3,0,retain)
  __INIT_VAR(data__->ADD1_OUT,0,retain)
}

// Code part
void PERFOMENS_body__(PERFOMENS *data__) {
  // Control execution
  if (!__GET_VAR(data__->EN)) {
    __SET_VAR(data__->,ENO,,__BOOL_LITERAL(FALSE));
    return;
  }
  else {
    __SET_VAR(data__->,ENO,,__BOOL_LITERAL(TRUE));
  }
  // Initialise TEMP variables

  __SET_VAR(data__->,ADD1_OUT,,ADD__DINT__DINT(
    (BOOL)__BOOL_LITERAL(TRUE),
    NULL,
    (UINT)2,
    (DINT)__GET_VAR(data__->A2,),
    (DINT)__GET_VAR(data__->A1,)));
  __SET_VAR(data__->,A3,,__GET_VAR(data__->ADD1_OUT,));

  goto __end;

__end:
  return;
} // PERFOMENS_body__() 





void MAIN0_init__(MAIN0 *data__, BOOL retain) {
  __INIT_EXTERNAL(DINT,TEST_CONFIG,data__->TEST_CONFIG,retain)
  CHECC_TEST_init__(&data__->TEST1,retain);
  STRESSS_init__(&data__->TEST2,retain);
  PERFOMENS_init__(&data__->TEST3,retain);
}

// Code part
void MAIN0_body__(MAIN0 *data__) {
  // Initialise TEMP variables

  if ((__GET_EXTERNAL(data__->TEST_CONFIG,) == 1)) {
    CHECC_TEST_body__(&data__->TEST1);
  };
  if ((__GET_EXTERNAL(data__->TEST_CONFIG,) == 2)) {
    STRESSS_body__(&data__->TEST2);
  } else {
    PERFOMENS_body__(&data__->TEST3);
  };

  goto __end;

__end:
  return;
} // MAIN0_body__() 





#endif //POUS_C
