/**
 * @file C:\Beremiz\beremiz_test_projects\MODBUS_PACKET_TEST\build//sofi/freertos/src/sofi_dev.c
 * @author Shoma Gane <shomagan@gmail.com>
 *         Ayrat Girfanov <girfanov.ayrat@yandex.ru>
 * @defgroup beremiz 
 * @ingroup beremiz 
 * @version 0.1 
 * @brief  TODO!!! write brief in 
 */
    /*
     * Copyright (c) 2018 Snema Service
     * All rights reserved.
     *
     * Redistribution and use in source and binary forms, with or without modification,
     * are permitted provided that the following conditions are met:
     *
     * 1. Redistributions of source code must retain the above copyright notice,
     *    this list of conditions and the following disclaimer.
     * 2. Redistributions in binary form must reproduce the above copyright notice,
     *    this list of conditions and the following disclaimer in the documentation
     *    and/or other materials provided with the distribution.
     * 3. The name of the author may not be used to endorse or promote products
     *    derived from this software without specific prior written permission.
     *
     * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
     * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
     * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
     * SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
     * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
     * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
     * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
     * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
     * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY
     * OF SUCH DAMAGE.
     *
     * This file is part of the sofi PLC.
     *
     * Author: Shoma Gane <shomagan@gmail.com>
     *         Ayrat Girfanov <girfanov.ayrat@yandex.ru>
     */
#ifndef SOFI_DEV_C
#define SOFI_DEV_C
#include "regs.h"
#include "sofi_dev.h" 
#include "link_functions.h"
#include "os_service.h"
extern link_functions_t * p_link_functions;
/*@brief read param by address value
 * @return non zero value if error success
 * */
int get_value_by_address(u32 address,u64* value,regs_flag_t flag){
    int res;
    regs_access_t reg;
    reg.flag = flag;
    res = 0;
    reg.value.op_u64 = 0;
    if(p_link_functions->regs_get((u16)address,&reg)!=0){
        *value = 0;
        res = -1;
    }else{
        *value = reg.value.op_u64;
    }
    return res;
}
int set_value_by_address(u32 address,u64 value,regs_flag_t flag){
    int res;
    regs_access_t reg;
    reg.flag = flag;
    reg.value.op_u64 = value;
    res = 0;
    if(p_link_functions->regs_set((u16)address,reg)!=0){
        res = -1;
    }
    return res;
}

#endif //SOFI_DEV_C
