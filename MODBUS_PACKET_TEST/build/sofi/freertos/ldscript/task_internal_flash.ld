/*
*****************************************************************************
**

**  File        : LinkerScript.ld
**
**  Abstract    : Linker script for STM32F767ZITx Device with
**                2048KByte FLASH, 512KByte RAM
**
**                Set heap size, stack size and stack location according
**                to application requirements.
**
**                Set memory bank area and size if external memory is used.
**
**  Target      : STMicroelectronics STM32
**
**
**  Distribution: The file is distributed as is, without any warranty
**                of any kind.
**
**  (c)Copyright Ac6.
**  You may use this file as-is or modify it according to the needs of your
**  project. Distribution of this file (unmodified or modified) is not
**  permitted. Ac6 permit registered System Workbench for MCU users the
**  rights to distribute the assembled, compiled & linked contents of this
**  file as part of an application binary file, provided that it is built
**  using the System Workbench for MCU toolchain.
**
*****************************************************************************
*/


/* Highest address of the user mode stack */
_estack = 0x20080000;    /* end of RAM */
/* Generate a link error if heap and stack don't fit into RAM */
_min_heap_size = 0x1000;      /* required amount of heap  */
_min_stack_size = 0x1000; /* required amount of stack */
_flash_start_address = 0x8000000;
_ram_start_address = 0x20000000;
_task_ram_size = 115K;
_kernel_ram_start_address = _ram_start_address + 128K;
_internal_flash_size = 2048*1024;
_internal_flash_task_address = _flash_start_address + 1024*1024;
_qspi_flash_start_address = 0x90000000;
_bksram_address = 0x40024000;
/* Specify the memory areas */
MEMORY
{
RAM (xrw)      : ORIGIN = _kernel_ram_start_address, LENGTH = 368K
TASK_MEM(xrw)  : ORIGIN = _ram_start_address, LENGTH = _task_ram_size
BK_RAM(xrw)    : ORIGIN = _bksram_address, LENGTH = 4k
FLASH (rx)     : ORIGIN = _flash_start_address, LENGTH = 2048K
QSPI (rx)  : ORIGIN = _qspi_flash_start_address, LENGTH = 16384K
}

/* Define output sections */
SECTIONS
{
  _sofi_link_address = _flash_start_address + 1k;
  .sofi_link _sofi_link_address :
  {
    . = ALIGN(4);
    _sofi_link_start = .;        /* create a global symbol at start */
    KEEP(*(.sofi_link));
    . = ALIGN(4);
    _sofi_link_end = .;         /* define a global symbols at end of */
  } >FLASH 

  _task_link_address = _internal_flash_task_address + 4;
  .task_text _task_link_address :
  {
    . = ALIGN(4);
    KEEP(*(.task_text)) /* Startup code */
    *(.text)           /* .text sections (code) */
    *(.text*)          /* .text* sections (code) */
    *(.glue_7)         /* glue arm to thumb code */
    *(.glue_7t)        /* glue thumb to arm code */
    *(.text.*)
    *(.eh_frame)
    KEEP (*(.init))
    KEEP (*(.fini))
    . = ALIGN(4);
    _etext = .;        /* define a global symbols at end of code */
    . = ALIGN(4);
  } >FLASH
  _task_base = LOADADDR(.task_text);
  _task_length = SIZEOF(.task_text);

  .preinit_array     :
  {
    PROVIDE_HIDDEN (__preinit_array_start = .);
    KEEP (*(.preinit_array*))
    PROVIDE_HIDDEN (__preinit_array_end = .);
  } >FLASH

  .init_array :
  {
    PROVIDE_HIDDEN (__init_array_start = .);
    KEEP (*(SORT(.init_array.*)))
    KEEP (*(.init_array*))
    PROVIDE_HIDDEN (__init_array_end = .);
  } >FLASH

  .fini_array :
  {
    PROVIDE_HIDDEN (__fini_array_start = .);
    KEEP (*(SORT(.fini_array.*)))
    KEEP (*(.fini_array*))
    PROVIDE_HIDDEN (__fini_array_end = .);
  } >FLASH


  /* Constant data goes into FLASH */
  .rodata :
  {
    . = ALIGN(4);
    *(.rodata)         /* .rodata sections (constants, strings, etc.) */
    *(.rodata*)        /* .rodata* sections (constants, strings, etc.) */
    . = ALIGN(4);
  } >FLASH

  /* used by the startup to initialize data */
  _sidata = LOADADDR(.data);

  /* Initialized data sections goes into RAM, load LMA copy after code */
  .data : 
  {
    . = ALIGN(4);
    _sdata = .;        /* create a global symbol at data start */
    *(.data)           /* .data sections */
    *(.data*)          /* .data* sections */

    . = ALIGN(4);
    _edata = .;        /* define a global symbol at data end */
  } >TASK_MEM AT> FLASH

  .bksram  :
  {
    _bksram_start = .;        /* create a global symbol at start */
    KEEP(*(.bksram)) ;
    _bksram_end = .;         /* define a global symbols at end of  */
  }>BK_RAM

  /* Uninitialized data section */
  . = ALIGN(4);
  .bss :
  {
    /* This is used by the startup in order to initialize the .bss secion */
    _sbss = .;         /* define a global symbol at bss start */
    __bss_start__ = _sbss;
    *(.bss)
    *(.bss*)
    *(COMMON)

    . = ALIGN(4);
    _ebss = .;         /* define a global symbol at bss end */
    __bss_end__ = _ebss;
  } >TASK_MEM

  /* User_heap_stack section, used to check that there is enough RAM left */
  ._user_heap_stack :
  {
    . = ALIGN(8);
    PROVIDE ( end = . );
    PROVIDE ( _end = . );
    . = . + _min_heap_size;
    . = . + _min_stack_size;
    . = ALIGN(8);
  } >TASK_MEM

  /* Remove information from the standard libraries */
  /DISCARD/ :
  {
    libc.a ( * )
    libm.a ( * )
    libgcc.a ( * )
  }

  .ARM.attributes 0 : { *(.ARM.attributes) }
}


