#include "beremiz.h"
#ifndef __POUS_H
#define __POUS_H

#include "accessor.h"
#include "iec_std_lib.h"

// FUNCTION_BLOCK CHECC_TEST
// Data part
typedef struct {
  // FB Interface - IN, OUT, IN_OUT variables
  __DECLARE_VAR(BOOL,EN)
  __DECLARE_VAR(BOOL,ENO)

  // FB private variables - TEMP, private and located variables
  __DECLARE_EXTERNAL(UINT,REG1)
  __DECLARE_EXTERNAL(UINT,REG2)
  __DECLARE_EXTERNAL(UINT,REG3)
  READ_DI READ_DI0;
  WRITE_DO WRITE_DO0;
  __DECLARE_VAR(UINT,NUM1)
  __DECLARE_VAR(UINT,MUX11_OUT)

} CHECC_TEST;

void CHECC_TEST_init__(CHECC_TEST *data__, BOOL retain);
// Code part
void CHECC_TEST_body__(CHECC_TEST *data__);
// FUNCTION_BLOCK STRESSS
// Data part
typedef struct {
  // FB Interface - IN, OUT, IN_OUT variables
  __DECLARE_VAR(BOOL,EN)
  __DECLARE_VAR(BOOL,ENO)

  // FB private variables - TEMP, private and located variables
  __DECLARE_VAR(DINT,A1)
  __DECLARE_VAR(DINT,A2)
  __DECLARE_VAR(DINT,A3)
  __DECLARE_VAR(DINT,ADD1_OUT)

} STRESSS;

void STRESSS_init__(STRESSS *data__, BOOL retain);
// Code part
void STRESSS_body__(STRESSS *data__);
// FUNCTION_BLOCK PERFOMENS
// Data part
typedef struct {
  // FB Interface - IN, OUT, IN_OUT variables
  __DECLARE_VAR(BOOL,EN)
  __DECLARE_VAR(BOOL,ENO)

  // FB private variables - TEMP, private and located variables
  __DECLARE_VAR(DINT,A1)
  __DECLARE_VAR(DINT,A2)
  __DECLARE_VAR(DINT,A3)
  __DECLARE_VAR(DINT,ADD1_OUT)

} PERFOMENS;

void PERFOMENS_init__(PERFOMENS *data__, BOOL retain);
// Code part
void PERFOMENS_body__(PERFOMENS *data__);
// PROGRAM MAIN0
// Data part
typedef struct {
  // PROGRAM Interface - IN, OUT, IN_OUT variables

  // PROGRAM private variables - TEMP, private and located variables
  __DECLARE_EXTERNAL(DINT,TEST_CONFIG)
  CHECC_TEST TEST1;
  STRESSS TEST2;
  PERFOMENS TEST3;

} MAIN0;

void MAIN0_init__(MAIN0 *data__, BOOL retain);
// Code part
void MAIN0_body__(MAIN0 *data__);
#endif //__POUS_H
