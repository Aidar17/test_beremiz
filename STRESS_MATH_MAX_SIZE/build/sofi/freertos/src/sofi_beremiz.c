/**
 * @file C:\Beremiz\beremiz_test_projects\STRESS_MATH_MAX_SIZE\build//sofi/freertos/src/sofi_beremiz.c
 * @author Shoma Gane <shomagan@gmail.com>
 *         Ayrat Girfanov <girfanov.ayrat@yandex.ru>
 * @defgroup beremiz 
 * @ingroup beremiz 
 * @version 0.1 
 * @brief  TODO!!! write brief in 
 */
    /*
     * Copyright (c) 2018 Snema Service
     * All rights reserved.
     *
     * Redistribution and use in source and binary forms, with or without modification,
     * are permitted provided that the following conditions are met:
     *
     * 1. Redistributions of source code must retain the above copyright notice,
     *    this list of conditions and the following disclaimer.
     * 2. Redistributions in binary form must reproduce the above copyright notice,
     *    this list of conditions and the following disclaimer in the documentation
     *    and/or other materials provided with the distribution.
     * 3. The name of the author may not be used to endorse or promote products
     *    derived from this software without specific prior written permission.
     *
     * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
     * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
     * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
     * SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
     * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
     * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
     * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
     * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
     * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY
     * OF SUCH DAMAGE.
     *
     * This file is part of the sofi PLC.
     *
     * Author: Shoma Gane <shomagan@gmail.com>
     *         Ayrat Girfanov <girfanov.ayrat@yandex.ru>
     */
#ifndef SOFI_BEREMIZ_C
#define SOFI_BEREMIZ_C
#include "regs.h"
#include "sofi_dev.h"
#include "sofi_beremiz.h"

#include "link_functions.h"
#include "os_service.h"

extern link_functions_t * p_link_functions;
void READ_PARAM_UDINT_init__(READ_PARAM_UDINT *data__, BOOL retain) {
    __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
    __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
    __INIT_VAR(data__->ENABLE,0,retain)
    __INIT_VAR(data__->ADDRESS,0,retain)
    __INIT_VAR(data__->CHECK,0,retain)
    __INIT_VAR(data__->VALUE,0,retain)
}

// Code part
void READ_PARAM_UDINT_body__(READ_PARAM_UDINT *data) {
    // Control execution
    __SET_VAR(data->,CHECK,,0);
    if (!__GET_VAR(data->EN)) {
    __SET_VAR(data->,ENO,,__BOOL_LITERAL(FALSE));
    return;
    }  else {
    __SET_VAR(data->,ENO,,__BOOL_LITERAL(TRUE));
    }
    // Initialise TEMP variables
    if (__GET_VAR(data->ENABLE,)) {
        int res;
        u64 value;
        regs_flag_t flag = U32_REGS_FLAG;
        res = get_value_by_address((u32)__GET_VAR(data->ADDRESS,),&value,flag);
        if(res!=0){
            __SET_VAR(data->,CHECK,,0);
        }else{
            __SET_VAR(data->,VALUE,,(IEC_DINT)value);
            __SET_VAR(data->,CHECK,,1);
        }
    } else {
        __SET_VAR(data->,VALUE,,0);
        __SET_VAR(data->,CHECK,,0);
    };
    return;
} // READ_PARAM_body__() 
void SET_PARAM_UDINT_init__(SET_PARAM_UDINT *data__, BOOL retain) {
    __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
    __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
    __INIT_VAR(data__->ENABLE,0,retain)
    __INIT_VAR(data__->ADDRESS,0,retain)
    __INIT_VAR(data__->VALUE,0,retain)
    __INIT_VAR(data__->CHECK,0,retain)
}

// Code part
void SET_PARAM_UDINT_body__(SET_PARAM_UDINT *data) {
    // Control execution
    __SET_VAR(data->,CHECK,,0);
    if (!__GET_VAR(data->EN)) {
    __SET_VAR(data->,ENO,,__BOOL_LITERAL(FALSE));
    return;
    }  else {
    __SET_VAR(data->,ENO,,__BOOL_LITERAL(TRUE));
    }
    // Initialise TEMP variables
    if (__GET_VAR(data->ENABLE,)) {
        int res;
        regs_flag_t flag = U32_REGS_FLAG;
        res = set_value_by_address((u32)__GET_VAR(data->ADDRESS,),(u64)__GET_VAR(data->VALUE,),flag);
        if(res!=0){
            __SET_VAR(data->,CHECK,,0);
        }else{
            __SET_VAR(data->,CHECK,,1);
        }
    } else {
        __SET_VAR(data->,CHECK,,0);
    };
    return;
} // SET_PARAM_body__()
void READ_PARAM_UINT_init__(READ_PARAM_UINT *data__, BOOL retain) {
    __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
    __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
    __INIT_VAR(data__->ENABLE,0,retain)
    __INIT_VAR(data__->ADDRESS,0,retain)
    __INIT_VAR(data__->CHECK,0,retain)
    __INIT_VAR(data__->VALUE,0,retain)
}

void READ_PARAM_UINT_body__(READ_PARAM_UINT *data) {
    // Control execution
    __SET_VAR(data->,CHECK,,0);
    if (!__GET_VAR(data->EN)) {
    __SET_VAR(data->,ENO,,__BOOL_LITERAL(FALSE));
    return;
    }  else {
    __SET_VAR(data->,ENO,,__BOOL_LITERAL(TRUE));
    }
    // Initialise TEMP variables
    if (__GET_VAR(data->ENABLE,)) {
        int res;
        u64 value;
        regs_flag_t flag = U16_REGS_FLAG;
        res = get_value_by_address((u32)__GET_VAR(data->ADDRESS,),&value,flag);
        if(res!=0){
            __SET_VAR(data->,CHECK,,0);
        }else{
            __SET_VAR(data->,VALUE,,(IEC_INT)value);
            __SET_VAR(data->,CHECK,,1);
        }
    } else {
        __SET_VAR(data->,VALUE,,0);
        __SET_VAR(data->,CHECK,,0);
    };
    return;
} // READ_PARAM_body__() 
void SET_PARAM_UINT_init__(SET_PARAM_UINT *data__, BOOL retain) {
    __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
    __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
    __INIT_VAR(data__->ENABLE,0,retain)
    __INIT_VAR(data__->ADDRESS,0,retain)
    __INIT_VAR(data__->VALUE,0,retain)
    __INIT_VAR(data__->CHECK,0,retain)
}

// Code part
void SET_PARAM_UINT_body__(SET_PARAM_UINT *data) {
    // Control execution
    __SET_VAR(data->,CHECK,,0);
    if (!__GET_VAR(data->EN)) {
    __SET_VAR(data->,ENO,,__BOOL_LITERAL(FALSE));
    return;
    }  else {
    __SET_VAR(data->,ENO,,__BOOL_LITERAL(TRUE));
    }
    // Initialise TEMP variables
    if (__GET_VAR(data->ENABLE,)) {
        int res;
        regs_flag_t flag = U16_REGS_FLAG;
        res = set_value_by_address((u32)__GET_VAR(data->ADDRESS,),(u64)__GET_VAR(data->VALUE,),flag);
        if(res!=0){
            __SET_VAR(data->,CHECK,,0);
        }else{
            __SET_VAR(data->,CHECK,,1);
        }
    } else {
        __SET_VAR(data->,CHECK,,0);
    };
    return;
} // SET_PARAM_body__()
void READ_PARAM_USINT_init__(READ_PARAM_USINT *data__, BOOL retain) {
    __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
    __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
    __INIT_VAR(data__->ENABLE,0,retain)
    __INIT_VAR(data__->ADDRESS,0,retain)
    __INIT_VAR(data__->CHECK,0,retain)
    __INIT_VAR(data__->VALUE,0,retain)
}

void READ_PARAM_USINT_body__(READ_PARAM_USINT *data) {
    // Control execution
    __SET_VAR(data->,CHECK,,0);
    if (!__GET_VAR(data->EN)) {
    __SET_VAR(data->,ENO,,__BOOL_LITERAL(FALSE));
    return;
    }  else {
    __SET_VAR(data->,ENO,,__BOOL_LITERAL(TRUE));
    }
    // Initialise TEMP variables
    if (__GET_VAR(data->ENABLE,)) {
        int res;
        u64 value;
        regs_flag_t flag = U8_REGS_FLAG;
        res = get_value_by_address((u32)__GET_VAR(data->ADDRESS,),&value,flag);
        if(res!=0){
            __SET_VAR(data->,CHECK,,0);
        }else{
            __SET_VAR(data->,VALUE,,(IEC_SINT)value);
            __SET_VAR(data->,CHECK,,1);
        }
    } else {
        __SET_VAR(data->,VALUE,,0);
        __SET_VAR(data->,CHECK,,0);
    };
    return;
} // READ_PARAM_body__() 
void SET_PARAM_USINT_init__(SET_PARAM_USINT *data__, BOOL retain) {
    __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
    __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
    __INIT_VAR(data__->ENABLE,0,retain)
    __INIT_VAR(data__->ADDRESS,0,retain)
    __INIT_VAR(data__->VALUE,0,retain)
    __INIT_VAR(data__->CHECK,0,retain)
}

// Code part
void SET_PARAM_USINT_body__(SET_PARAM_USINT *data) {
    // Control execution
    __SET_VAR(data->,CHECK,,0);
    if (!__GET_VAR(data->EN)) {
    __SET_VAR(data->,ENO,,__BOOL_LITERAL(FALSE));
    return;
    }  else {
    __SET_VAR(data->,ENO,,__BOOL_LITERAL(TRUE));
    }
    // Initialise TEMP variables
    if (__GET_VAR(data->ENABLE,)) {
        int res;
        regs_flag_t flag = U8_REGS_FLAG;
        res = set_value_by_address((u32)__GET_VAR(data->ADDRESS,),(u64)__GET_VAR(data->VALUE,),flag);
        if(res!=0){
            __SET_VAR(data->,CHECK,,0);
        }else{
            __SET_VAR(data->,CHECK,,1);
        }
    } else {
        __SET_VAR(data->,CHECK,,0);
    };
    return;
} // SET_PARAM_body__()

void READ_AI_init__(READ_AI *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->AI_NUMBER,0,retain)
  __INIT_VAR(data__->AI_VALUE,0,retain)
}

// Code part
void READ_AI_body__(READ_AI *data__) {
  // Control execution
  if (!__GET_VAR(data__->EN)) {
    __SET_VAR(data__->,ENO,,__BOOL_LITERAL(FALSE));
    return;
  }  else {
    __SET_VAR(data__->,ENO,,__BOOL_LITERAL(TRUE));
  }
  // Initialise TEMP variables

  char ai_name[] = "ai_unit";
  regs_template_t regs_template;
  regs_template.name = ai_name;
  /*@todo add control ai_number*/
  if(p_link_functions->regs_description_get_by_name(&regs_template)==0){
      regs_access_t reg;
      reg.value.op_u64 = 0;
      reg.flag = regs_template.type;
      u16 address = regs_template.guid & GUID_ADDRESS_MASK;
      u16 shift = p_link_functions->regs_size_in_byte(regs_template.type) * __GET_VAR(data__->AI_NUMBER,);
      address +=  shift;
      if(p_link_functions->regs_get((u16)address,&reg)==0){
            __SET_VAR(data__->,AI_VALUE,,(u16)reg.value.op_u32);
      }
  }
  return;
} // READ_AI_body__()

void WRITE_DO_init__(WRITE_DO *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->DO_VALUE,0,retain)
  __INIT_VAR(data__->DO_MASK,0,retain)
}

// Code part
void WRITE_DO_body__(WRITE_DO *data__) {
  // Control execution
  if (!__GET_VAR(data__->EN)) {
    __SET_VAR(data__->,ENO,,__BOOL_LITERAL(FALSE));
    return;
  }
  // Initialise TEMP variables
  char do_name[] = "do_ctrl";
  regs_template_t regs_template;
  regs_template.name = do_name;
  if(p_link_functions->regs_description_get_by_name(&regs_template)==0){
      regs_access_t reg;
      reg.value.op_u16 = (u16)__GET_VAR(data__->DO_VALUE,) | (((u16)__GET_VAR(data__->DO_MASK,))<<4);
      reg.flag = regs_template.type;
      u16 address = regs_template.guid & GUID_ADDRESS_MASK;
      if(p_link_functions->regs_set((u16)address,reg)==0){
          __SET_VAR(data__->,ENO,,__BOOL_LITERAL(TRUE));
      }else{
          __SET_VAR(data__->,ENO,,__BOOL_LITERAL(FALSE));
      }
  }
  return;
} // WRITE_DO_body__()
void READ_DI_init__(READ_DI *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->DI_OUT,0,retain)
}

// Code part
void READ_DI_body__(READ_DI *data__) {
  // Control execution
  if (!__GET_VAR(data__->EN)) {
    __SET_VAR(data__->,ENO,,__BOOL_LITERAL(FALSE));
    return;
  }
  __SET_VAR(data__->,ENO,,__BOOL_LITERAL(FALSE));
  // Initialise TEMP variables
  char ai_name[] = "di_state";
  regs_template_t regs_template;
  regs_template.name = ai_name;
  if(p_link_functions->regs_description_get_by_name(&regs_template)==0){
      p_link_functions->task_enter_critical();
      u32 * value = (u32*)regs_template.p_value;
      __SET_VAR(data__->,DI_OUT,,*value);
      __SET_VAR(data__->,ENO,,__BOOL_LITERAL(TRUE));
      p_link_functions->task_exit_critical();
  }

  return;
} // READ_DI_body__()


#endif //SOFI_BEREMIZ_C
