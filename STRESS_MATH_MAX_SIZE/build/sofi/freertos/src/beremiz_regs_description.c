/**
 * @file beremiz_regs_description.c
 * @author Shoma Gane <shomagan@gmail.com>
 *         Ayrat Girfanov <girfanov.ayrat@yandex.ru>
 * @defgroup beremiz
 * @ingroup beremiz
 * @version 0.1 
 * @brief  TODO!!! write brief in 
 */
/*
 * Copyright (c) 2018 Snema Service
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
 * SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY
 * OF SUCH DAMAGE.
 *
 * This file is part of the sofi PLC.
 *
 * Author: Shoma Gane <shomagan@gmail.com>
 *         Ayrat Girfanov <girfanov.ayrat@yandex.ru>
 */
#ifndef BEREMIZ_REGS_DESCRIPTION_C
#define BEREMIZ_REGS_DESCRIPTION_C 1
#include "beremiz_regs_description.h"
#include "regs_description.h"
#include "sofi_config.h"
#include "beremiz_task.h"
#include "bkram_access.h"
#include "matiec/iec_types_all.h"


extern u8 mdb_address_space[];
extern u8 mdb_array_address_space[];



extern __IEC_DINT_t CONFIG__TEST_CONFIG;
extern __IEC_UDINT_t CONFIG__TIME_STRAT;
extern __IEC_UINT_p CONFIG__ANALOG_I_0;
extern __IEC_UINT_t CONFIG__ANALOG_I_1;
extern __IEC_UINT_t CONFIG__ANALOG_I_2;
extern __IEC_UINT_t CONFIG__ANALOG_I_3;
extern __IEC_UINT_t CONFIG__ANALOG_I_4;
extern __IEC_UINT_t CONFIG__ANALOG_I_5;
extern __IEC_UINT_t CONFIG__ANALOG_I_6;
extern __IEC_UINT_t CONFIG__ANALOG_I_7;
extern __IEC_UINT_t CONFIG__ANALOG_MAX_0;
extern __IEC_UINT_t CONFIG__ANALOG_MIN_0;
extern __IEC_UINT_t CONFIG__ANALOG_MAX_1;
extern __IEC_UINT_t CONFIG__ANALOG_MIN_1;
extern __IEC_UINT_t CONFIG__ANALOG_MAX_2;
extern __IEC_UINT_t CONFIG__ANALOG_MIN_2;
extern __IEC_UINT_t CONFIG__ANALOG_MAX_4;
extern __IEC_UINT_t CONFIG__ANALOG_MIN_4;
extern __IEC_UINT_t CONFIG__ANALOG_MAX_6;
extern __IEC_UINT_t CONFIG__ANALOG_MIN_6;
extern __IEC_UINT_t CONFIG__ANALOG_MAX_3;
extern __IEC_UINT_t CONFIG__ANALOG_MIN_3;
extern __IEC_UINT_t CONFIG__ANALOG_MAX_5;
extern __IEC_UINT_t CONFIG__ANALOG_MIN_5;
extern __IEC_UINT_t CONFIG__ANALOG_MAX_7;
extern __IEC_UINT_t CONFIG__ANALOG_MIN_7;
extern __IEC_UDINT_t CONFIG__ANALOG_TIME_MAX_0;
extern __IEC_UDINT_t CONFIG__ANALOG_TIME_MAX_1;
extern __IEC_UDINT_t CONFIG__ANALOG_TIME_MAX_2;
extern __IEC_UDINT_t CONFIG__ANALOG_TIME_MAX_3;
extern __IEC_UDINT_t CONFIG__ANALOG_TIME_MAX_4;
extern __IEC_UDINT_t CONFIG__ANALOG_TIME_MAX_5;
extern __IEC_UDINT_t CONFIG__ANALOG_TIME_MAX_6;
extern __IEC_UDINT_t CONFIG__ANALOG_TIME_MAX_7;
extern __IEC_UDINT_t CONFIG__ANALOG_TIME_MIN_0;
extern __IEC_UDINT_t CONFIG__ANALOG_TIME_MIN_1;
extern __IEC_UDINT_t CONFIG__ANALOG_TIME_MIN_2;
extern __IEC_UDINT_t CONFIG__ANALOG_TIME_MIN_3;
extern __IEC_UDINT_t CONFIG__ANALOG_TIME_MIN_4;
extern __IEC_UDINT_t CONFIG__ANALOG_TIME_MIN_5;
extern __IEC_UDINT_t CONFIG__ANALOG_TIME_MIN_6;
extern __IEC_UDINT_t CONFIG__ANALOG_TIME_MIN_7;
extern __IEC_REAL_t CONFIG__A_1;
extern __IEC_REAL_t CONFIG__A_2;
extern __IEC_REAL_t CONFIG__A_3;
extern __IEC_REAL_t CONFIG__A_4;
extern __IEC_REAL_t CONFIG__A_5;
extern __IEC_REAL_t CONFIG__A_6;
extern __IEC_REAL_t CONFIG__A_7;
extern __IEC_REAL_t CONFIG__A_8;
extern __IEC_REAL_t CONFIG__A_9;
extern __IEC_REAL_t CONFIG__A_10;
extern __IEC_REAL_t CONFIG__X_11;
extern __IEC_REAL_t CONFIG__X_12;
extern __IEC_REAL_t CONFIG__X_13;
extern __IEC_REAL_t CONFIG__X_14;
extern __IEC_REAL_t CONFIG__X_15;
extern __IEC_REAL_t CONFIG__X_16;
extern __IEC_REAL_t CONFIG__X_17;
extern __IEC_REAL_t CONFIG__X_18;
extern __IEC_REAL_t CONFIG__X_19;
extern __IEC_REAL_t CONFIG__X_21;
extern __IEC_REAL_t CONFIG__X_22;
extern __IEC_REAL_t CONFIG__X_23;
extern __IEC_REAL_t CONFIG__X_24;
extern __IEC_REAL_t CONFIG__X_25;
extern __IEC_REAL_t CONFIG__X_26;
extern __IEC_REAL_t CONFIG__X_27;
extern __IEC_REAL_t CONFIG__X_28;
extern __IEC_REAL_t CONFIG__X_29;
extern __IEC_REAL_t CONFIG__X_31;
extern __IEC_REAL_t CONFIG__X_32;
extern __IEC_REAL_t CONFIG__X_33;
extern __IEC_REAL_t CONFIG__X_34;
extern __IEC_REAL_t CONFIG__X_35;
extern __IEC_REAL_t CONFIG__X_36;
extern __IEC_REAL_t CONFIG__X_37;
extern __IEC_REAL_t CONFIG__X_38;
extern __IEC_REAL_t CONFIG__X_39;
extern __IEC_REAL_t CONFIG__X_41;
extern __IEC_REAL_t CONFIG__X_42;
extern __IEC_REAL_t CONFIG__X_43;
extern __IEC_REAL_t CONFIG__X_44;
extern __IEC_REAL_t CONFIG__X_45;
extern __IEC_REAL_t CONFIG__X_46;
extern __IEC_REAL_t CONFIG__X_47;
extern __IEC_REAL_t CONFIG__X_48;
extern __IEC_REAL_t CONFIG__X_49;
extern __IEC_REAL_t CONFIG__X_51;
extern __IEC_REAL_t CONFIG__X_52;
extern __IEC_REAL_t CONFIG__X_53;
extern __IEC_REAL_t CONFIG__X_54;
extern __IEC_REAL_t CONFIG__X_55;
extern __IEC_REAL_t CONFIG__X_56;
extern __IEC_REAL_t CONFIG__X_57;
extern __IEC_REAL_t CONFIG__X_58;
extern __IEC_REAL_t CONFIG__X_59;
extern __IEC_REAL_t CONFIG__X_61;
extern __IEC_REAL_t CONFIG__X_62;
extern __IEC_REAL_t CONFIG__X_63;
extern __IEC_REAL_t CONFIG__X_64;
extern __IEC_REAL_t CONFIG__X_65;
extern __IEC_REAL_t CONFIG__X_66;
extern __IEC_REAL_t CONFIG__X_67;
extern __IEC_REAL_t CONFIG__X_68;
extern __IEC_REAL_t CONFIG__X_69;
extern __IEC_REAL_t CONFIG__X_71;
extern __IEC_REAL_t CONFIG__X_72;
extern __IEC_REAL_t CONFIG__X_73;
extern __IEC_REAL_t CONFIG__X_74;
extern __IEC_REAL_t CONFIG__X_75;
extern __IEC_REAL_t CONFIG__X_76;
extern __IEC_REAL_t CONFIG__X_77;
extern __IEC_REAL_t CONFIG__X_78;
extern __IEC_REAL_t CONFIG__X_79;
extern __IEC_REAL_t CONFIG__X_81;
extern __IEC_REAL_t CONFIG__X_82;
extern __IEC_REAL_t CONFIG__X_83;
extern __IEC_REAL_t CONFIG__X_84;
extern __IEC_REAL_t CONFIG__X_85;
extern __IEC_REAL_t CONFIG__X_86;
extern __IEC_REAL_t CONFIG__X_87;
extern __IEC_REAL_t CONFIG__X_88;
extern __IEC_REAL_t CONFIG__X_89;
extern __IEC_REAL_t CONFIG__X_91;
extern __IEC_REAL_t CONFIG__X_92;
extern __IEC_REAL_t CONFIG__X_93;
extern __IEC_REAL_t CONFIG__X_94;
extern __IEC_REAL_t CONFIG__X_95;
extern __IEC_REAL_t CONFIG__X_96;
extern __IEC_REAL_t CONFIG__X_97;
extern __IEC_REAL_t CONFIG__X_98;
extern __IEC_REAL_t CONFIG__X_99;
extern __IEC_REAL_t CONFIG__X0_11;
extern __IEC_REAL_t CONFIG__X0_12;
extern __IEC_REAL_t CONFIG__X0_13;
extern __IEC_REAL_t CONFIG__X0_14;
extern __IEC_REAL_t CONFIG__X0_15;
extern __IEC_REAL_t CONFIG__X0_16;
extern __IEC_REAL_t CONFIG__X0_17;
extern __IEC_REAL_t CONFIG__X0_18;
extern __IEC_REAL_t CONFIG__X0_19;
extern __IEC_REAL_t CONFIG__X0_21;
extern __IEC_REAL_t CONFIG__X0_22;
extern __IEC_REAL_t CONFIG__X0_23;
extern __IEC_REAL_t CONFIG__X0_24;
extern __IEC_REAL_t CONFIG__X0_25;
extern __IEC_REAL_t CONFIG__X0_26;
extern __IEC_REAL_t CONFIG__X0_27;
extern __IEC_REAL_t CONFIG__X0_28;
extern __IEC_REAL_t CONFIG__X0_29;
extern __IEC_REAL_t CONFIG__X0_31;
extern __IEC_REAL_t CONFIG__X0_32;
extern __IEC_REAL_t CONFIG__X0_33;
extern __IEC_REAL_t CONFIG__X0_34;
extern __IEC_REAL_t CONFIG__X0_35;
extern __IEC_REAL_t CONFIG__X0_36;
extern __IEC_REAL_t CONFIG__X0_37;
extern __IEC_REAL_t CONFIG__X0_38;
extern __IEC_REAL_t CONFIG__X0_39;
extern __IEC_REAL_t CONFIG__X0_41;
extern __IEC_REAL_t CONFIG__X0_42;
extern __IEC_REAL_t CONFIG__X0_43;
extern __IEC_REAL_t CONFIG__X0_44;
extern __IEC_REAL_t CONFIG__X0_45;
extern __IEC_REAL_t CONFIG__X0_46;
extern __IEC_REAL_t CONFIG__X0_47;
extern __IEC_REAL_t CONFIG__X0_48;
extern __IEC_REAL_t CONFIG__X0_49;
extern __IEC_REAL_t CONFIG__X0_51;
extern __IEC_REAL_t CONFIG__X0_52;
extern __IEC_REAL_t CONFIG__X0_53;
extern __IEC_REAL_t CONFIG__X0_54;
extern __IEC_REAL_t CONFIG__X0_55;
extern __IEC_REAL_t CONFIG__X0_56;
extern __IEC_REAL_t CONFIG__X0_57;
extern __IEC_REAL_t CONFIG__X0_58;
extern __IEC_REAL_t CONFIG__X0_59;
extern __IEC_REAL_t CONFIG__X0_61;
extern __IEC_REAL_t CONFIG__X0_62;
extern __IEC_REAL_t CONFIG__X0_63;
extern __IEC_REAL_t CONFIG__X0_64;
extern __IEC_REAL_t CONFIG__X0_65;
extern __IEC_REAL_t CONFIG__X0_66;
extern __IEC_REAL_t CONFIG__X0_67;
extern __IEC_REAL_t CONFIG__X0_68;
extern __IEC_REAL_t CONFIG__X0_69;
extern __IEC_REAL_t CONFIG__X0_71;
extern __IEC_REAL_t CONFIG__X0_72;
extern __IEC_REAL_t CONFIG__X0_73;
extern __IEC_REAL_t CONFIG__X0_74;
extern __IEC_REAL_t CONFIG__X0_75;
extern __IEC_REAL_t CONFIG__X0_76;
extern __IEC_REAL_t CONFIG__X0_77;
extern __IEC_REAL_t CONFIG__X0_78;
extern __IEC_REAL_t CONFIG__X0_79;
extern __IEC_REAL_t CONFIG__X0_81;
extern __IEC_REAL_t CONFIG__X0_82;
extern __IEC_REAL_t CONFIG__X0_83;
extern __IEC_REAL_t CONFIG__X0_84;
extern __IEC_REAL_t CONFIG__X0_85;
extern __IEC_REAL_t CONFIG__X0_86;
extern __IEC_REAL_t CONFIG__X0_87;
extern __IEC_REAL_t CONFIG__X0_88;
extern __IEC_REAL_t CONFIG__X0_89;
extern __IEC_REAL_t CONFIG__X0_91;
extern __IEC_REAL_t CONFIG__X0_92;
extern __IEC_REAL_t CONFIG__X0_93;
extern __IEC_REAL_t CONFIG__X0_94;
extern __IEC_REAL_t CONFIG__X0_95;
extern __IEC_REAL_t CONFIG__X0_96;
extern __IEC_REAL_t CONFIG__X0_97;
extern __IEC_REAL_t CONFIG__X0_98;
extern __IEC_REAL_t CONFIG__X0_99;


regs_description_t const beremiz_regs_description[BEREMIZ_REGS_VAR_NUM]={
    
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_DINT_t *)(&CONFIG__TEST_CONFIG))->value,/*saves address*/0,/*description*/"TEST_CONFIG",/*name*/"TEST_CONFIG",/*ind*/S32_REGS_FLAG,/*type*/0  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_UDINT_t *)(&CONFIG__TIME_STRAT))->value,/*saves address*/0,/*description*/"TIME_STRAT",/*name*/"TIME_STRAT",/*ind*/U32_REGS_FLAG,/*type*/1  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&mdb_address_space[0],/*saves address*/0,/*description*/"ANALOG_I_0",/*name*/"ANALOG_I_0",/*ind*/U16_REGS_FLAG,/*type*/2  ,/*guid*/GUID_USER_HEAD|(GUID_USER_ADDRESS_HEAD + 0 )  , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_UINT_t *)(&CONFIG__ANALOG_I_1))->value,/*saves address*/0,/*description*/"ANALOG_I_1",/*name*/"ANALOG_I_1",/*ind*/U16_REGS_FLAG,/*type*/3  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_UINT_t *)(&CONFIG__ANALOG_I_2))->value,/*saves address*/0,/*description*/"ANALOG_I_2",/*name*/"ANALOG_I_2",/*ind*/U16_REGS_FLAG,/*type*/4  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_UINT_t *)(&CONFIG__ANALOG_I_3))->value,/*saves address*/0,/*description*/"ANALOG_I_3",/*name*/"ANALOG_I_3",/*ind*/U16_REGS_FLAG,/*type*/5  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_UINT_t *)(&CONFIG__ANALOG_I_4))->value,/*saves address*/0,/*description*/"ANALOG_I_4",/*name*/"ANALOG_I_4",/*ind*/U16_REGS_FLAG,/*type*/6  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_UINT_t *)(&CONFIG__ANALOG_I_5))->value,/*saves address*/0,/*description*/"ANALOG_I_5",/*name*/"ANALOG_I_5",/*ind*/U16_REGS_FLAG,/*type*/7  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_UINT_t *)(&CONFIG__ANALOG_I_6))->value,/*saves address*/0,/*description*/"ANALOG_I_6",/*name*/"ANALOG_I_6",/*ind*/U16_REGS_FLAG,/*type*/8  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_UINT_t *)(&CONFIG__ANALOG_I_7))->value,/*saves address*/0,/*description*/"ANALOG_I_7",/*name*/"ANALOG_I_7",/*ind*/U16_REGS_FLAG,/*type*/9  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_UINT_t *)(&CONFIG__ANALOG_MAX_0))->value,/*saves address*/0,/*description*/"ANALOG_MAX_0",/*name*/"ANALOG_MAX_0",/*ind*/U16_REGS_FLAG,/*type*/10  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_UINT_t *)(&CONFIG__ANALOG_MIN_0))->value,/*saves address*/0,/*description*/"ANALOG_MIN_0",/*name*/"ANALOG_MIN_0",/*ind*/U16_REGS_FLAG,/*type*/11  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_UINT_t *)(&CONFIG__ANALOG_MAX_1))->value,/*saves address*/0,/*description*/"ANALOG_MAX_1",/*name*/"ANALOG_MAX_1",/*ind*/U16_REGS_FLAG,/*type*/12  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_UINT_t *)(&CONFIG__ANALOG_MIN_1))->value,/*saves address*/0,/*description*/"ANALOG_MIN_1",/*name*/"ANALOG_MIN_1",/*ind*/U16_REGS_FLAG,/*type*/13  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_UINT_t *)(&CONFIG__ANALOG_MAX_2))->value,/*saves address*/0,/*description*/"ANALOG_MAX_2",/*name*/"ANALOG_MAX_2",/*ind*/U16_REGS_FLAG,/*type*/14  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_UINT_t *)(&CONFIG__ANALOG_MIN_2))->value,/*saves address*/0,/*description*/"ANALOG_MIN_2",/*name*/"ANALOG_MIN_2",/*ind*/U16_REGS_FLAG,/*type*/15  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_UINT_t *)(&CONFIG__ANALOG_MAX_4))->value,/*saves address*/0,/*description*/"ANALOG_MAX_4",/*name*/"ANALOG_MAX_4",/*ind*/U16_REGS_FLAG,/*type*/16  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_UINT_t *)(&CONFIG__ANALOG_MIN_4))->value,/*saves address*/0,/*description*/"ANALOG_MIN_4",/*name*/"ANALOG_MIN_4",/*ind*/U16_REGS_FLAG,/*type*/17  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_UINT_t *)(&CONFIG__ANALOG_MAX_6))->value,/*saves address*/0,/*description*/"ANALOG_MAX_6",/*name*/"ANALOG_MAX_6",/*ind*/U16_REGS_FLAG,/*type*/18  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_UINT_t *)(&CONFIG__ANALOG_MIN_6))->value,/*saves address*/0,/*description*/"ANALOG_MIN_6",/*name*/"ANALOG_MIN_6",/*ind*/U16_REGS_FLAG,/*type*/19  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_UINT_t *)(&CONFIG__ANALOG_MAX_3))->value,/*saves address*/0,/*description*/"ANALOG_MAX_3",/*name*/"ANALOG_MAX_3",/*ind*/U16_REGS_FLAG,/*type*/20  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_UINT_t *)(&CONFIG__ANALOG_MIN_3))->value,/*saves address*/0,/*description*/"ANALOG_MIN_3",/*name*/"ANALOG_MIN_3",/*ind*/U16_REGS_FLAG,/*type*/21  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_UINT_t *)(&CONFIG__ANALOG_MAX_5))->value,/*saves address*/0,/*description*/"ANALOG_MAX_5",/*name*/"ANALOG_MAX_5",/*ind*/U16_REGS_FLAG,/*type*/22  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_UINT_t *)(&CONFIG__ANALOG_MIN_5))->value,/*saves address*/0,/*description*/"ANALOG_MIN_5",/*name*/"ANALOG_MIN_5",/*ind*/U16_REGS_FLAG,/*type*/23  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_UINT_t *)(&CONFIG__ANALOG_MAX_7))->value,/*saves address*/0,/*description*/"ANALOG_MAX_7",/*name*/"ANALOG_MAX_7",/*ind*/U16_REGS_FLAG,/*type*/24  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_UINT_t *)(&CONFIG__ANALOG_MIN_7))->value,/*saves address*/0,/*description*/"ANALOG_MIN_7",/*name*/"ANALOG_MIN_7",/*ind*/U16_REGS_FLAG,/*type*/25  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_UDINT_t *)(&CONFIG__ANALOG_TIME_MAX_0))->value,/*saves address*/0,/*description*/"ANALOG_TIME_MAX_0",/*name*/"ANALOG_TIME_MAX_0",/*ind*/U32_REGS_FLAG,/*type*/26  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_UDINT_t *)(&CONFIG__ANALOG_TIME_MAX_1))->value,/*saves address*/0,/*description*/"ANALOG_TIME_MAX_1",/*name*/"ANALOG_TIME_MAX_1",/*ind*/U32_REGS_FLAG,/*type*/27  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_UDINT_t *)(&CONFIG__ANALOG_TIME_MAX_2))->value,/*saves address*/0,/*description*/"ANALOG_TIME_MAX_2",/*name*/"ANALOG_TIME_MAX_2",/*ind*/U32_REGS_FLAG,/*type*/28  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_UDINT_t *)(&CONFIG__ANALOG_TIME_MAX_3))->value,/*saves address*/0,/*description*/"ANALOG_TIME_MAX_3",/*name*/"ANALOG_TIME_MAX_3",/*ind*/U32_REGS_FLAG,/*type*/29  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_UDINT_t *)(&CONFIG__ANALOG_TIME_MAX_4))->value,/*saves address*/0,/*description*/"ANALOG_TIME_MAX_4",/*name*/"ANALOG_TIME_MAX_4",/*ind*/U32_REGS_FLAG,/*type*/30  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_UDINT_t *)(&CONFIG__ANALOG_TIME_MAX_5))->value,/*saves address*/0,/*description*/"ANALOG_TIME_MAX_5",/*name*/"ANALOG_TIME_MAX_5",/*ind*/U32_REGS_FLAG,/*type*/31  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_UDINT_t *)(&CONFIG__ANALOG_TIME_MAX_6))->value,/*saves address*/0,/*description*/"ANALOG_TIME_MAX_6",/*name*/"ANALOG_TIME_MAX_6",/*ind*/U32_REGS_FLAG,/*type*/32  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_UDINT_t *)(&CONFIG__ANALOG_TIME_MAX_7))->value,/*saves address*/0,/*description*/"ANALOG_TIME_MAX_7",/*name*/"ANALOG_TIME_MAX_7",/*ind*/U32_REGS_FLAG,/*type*/33  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_UDINT_t *)(&CONFIG__ANALOG_TIME_MIN_0))->value,/*saves address*/0,/*description*/"ANALOG_TIME_MIN_0",/*name*/"ANALOG_TIME_MIN_0",/*ind*/U32_REGS_FLAG,/*type*/34  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_UDINT_t *)(&CONFIG__ANALOG_TIME_MIN_1))->value,/*saves address*/0,/*description*/"ANALOG_TIME_MIN_1",/*name*/"ANALOG_TIME_MIN_1",/*ind*/U32_REGS_FLAG,/*type*/35  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_UDINT_t *)(&CONFIG__ANALOG_TIME_MIN_2))->value,/*saves address*/0,/*description*/"ANALOG_TIME_MIN_2",/*name*/"ANALOG_TIME_MIN_2",/*ind*/U32_REGS_FLAG,/*type*/36  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_UDINT_t *)(&CONFIG__ANALOG_TIME_MIN_3))->value,/*saves address*/0,/*description*/"ANALOG_TIME_MIN_3",/*name*/"ANALOG_TIME_MIN_3",/*ind*/U32_REGS_FLAG,/*type*/37  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_UDINT_t *)(&CONFIG__ANALOG_TIME_MIN_4))->value,/*saves address*/0,/*description*/"ANALOG_TIME_MIN_4",/*name*/"ANALOG_TIME_MIN_4",/*ind*/U32_REGS_FLAG,/*type*/38  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_UDINT_t *)(&CONFIG__ANALOG_TIME_MIN_5))->value,/*saves address*/0,/*description*/"ANALOG_TIME_MIN_5",/*name*/"ANALOG_TIME_MIN_5",/*ind*/U32_REGS_FLAG,/*type*/39  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_UDINT_t *)(&CONFIG__ANALOG_TIME_MIN_6))->value,/*saves address*/0,/*description*/"ANALOG_TIME_MIN_6",/*name*/"ANALOG_TIME_MIN_6",/*ind*/U32_REGS_FLAG,/*type*/40  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_UDINT_t *)(&CONFIG__ANALOG_TIME_MIN_7))->value,/*saves address*/0,/*description*/"ANALOG_TIME_MIN_7",/*name*/"ANALOG_TIME_MIN_7",/*ind*/U32_REGS_FLAG,/*type*/41  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__A_1))->value,/*saves address*/0,/*description*/"A_1",/*name*/"A_1",/*ind*/FLOAT_REGS_FLAG,/*type*/42  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__A_2))->value,/*saves address*/0,/*description*/"A_2",/*name*/"A_2",/*ind*/FLOAT_REGS_FLAG,/*type*/43  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__A_3))->value,/*saves address*/0,/*description*/"A_3",/*name*/"A_3",/*ind*/FLOAT_REGS_FLAG,/*type*/44  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__A_4))->value,/*saves address*/0,/*description*/"A_4",/*name*/"A_4",/*ind*/FLOAT_REGS_FLAG,/*type*/45  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__A_5))->value,/*saves address*/0,/*description*/"A_5",/*name*/"A_5",/*ind*/FLOAT_REGS_FLAG,/*type*/46  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__A_6))->value,/*saves address*/0,/*description*/"A_6",/*name*/"A_6",/*ind*/FLOAT_REGS_FLAG,/*type*/47  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__A_7))->value,/*saves address*/0,/*description*/"A_7",/*name*/"A_7",/*ind*/FLOAT_REGS_FLAG,/*type*/48  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__A_8))->value,/*saves address*/0,/*description*/"A_8",/*name*/"A_8",/*ind*/FLOAT_REGS_FLAG,/*type*/49  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__A_9))->value,/*saves address*/0,/*description*/"A_9",/*name*/"A_9",/*ind*/FLOAT_REGS_FLAG,/*type*/50  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__A_10))->value,/*saves address*/0,/*description*/"A_10",/*name*/"A_10",/*ind*/FLOAT_REGS_FLAG,/*type*/51  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__X_11))->value,/*saves address*/0,/*description*/"X_11",/*name*/"X_11",/*ind*/FLOAT_REGS_FLAG,/*type*/52  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__X_12))->value,/*saves address*/0,/*description*/"X_12",/*name*/"X_12",/*ind*/FLOAT_REGS_FLAG,/*type*/53  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__X_13))->value,/*saves address*/0,/*description*/"X_13",/*name*/"X_13",/*ind*/FLOAT_REGS_FLAG,/*type*/54  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__X_14))->value,/*saves address*/0,/*description*/"X_14",/*name*/"X_14",/*ind*/FLOAT_REGS_FLAG,/*type*/55  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__X_15))->value,/*saves address*/0,/*description*/"X_15",/*name*/"X_15",/*ind*/FLOAT_REGS_FLAG,/*type*/56  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__X_16))->value,/*saves address*/0,/*description*/"X_16",/*name*/"X_16",/*ind*/FLOAT_REGS_FLAG,/*type*/57  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__X_17))->value,/*saves address*/0,/*description*/"X_17",/*name*/"X_17",/*ind*/FLOAT_REGS_FLAG,/*type*/58  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__X_18))->value,/*saves address*/0,/*description*/"X_18",/*name*/"X_18",/*ind*/FLOAT_REGS_FLAG,/*type*/59  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__X_19))->value,/*saves address*/0,/*description*/"X_19",/*name*/"X_19",/*ind*/FLOAT_REGS_FLAG,/*type*/60  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__X_21))->value,/*saves address*/0,/*description*/"X_21",/*name*/"X_21",/*ind*/FLOAT_REGS_FLAG,/*type*/61  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__X_22))->value,/*saves address*/0,/*description*/"X_22",/*name*/"X_22",/*ind*/FLOAT_REGS_FLAG,/*type*/62  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__X_23))->value,/*saves address*/0,/*description*/"X_23",/*name*/"X_23",/*ind*/FLOAT_REGS_FLAG,/*type*/63  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__X_24))->value,/*saves address*/0,/*description*/"X_24",/*name*/"X_24",/*ind*/FLOAT_REGS_FLAG,/*type*/64  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__X_25))->value,/*saves address*/0,/*description*/"X_25",/*name*/"X_25",/*ind*/FLOAT_REGS_FLAG,/*type*/65  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__X_26))->value,/*saves address*/0,/*description*/"X_26",/*name*/"X_26",/*ind*/FLOAT_REGS_FLAG,/*type*/66  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__X_27))->value,/*saves address*/0,/*description*/"X_27",/*name*/"X_27",/*ind*/FLOAT_REGS_FLAG,/*type*/67  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__X_28))->value,/*saves address*/0,/*description*/"X_28",/*name*/"X_28",/*ind*/FLOAT_REGS_FLAG,/*type*/68  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__X_29))->value,/*saves address*/0,/*description*/"X_29",/*name*/"X_29",/*ind*/FLOAT_REGS_FLAG,/*type*/69  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__X_31))->value,/*saves address*/0,/*description*/"X_31",/*name*/"X_31",/*ind*/FLOAT_REGS_FLAG,/*type*/70  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__X_32))->value,/*saves address*/0,/*description*/"X_32",/*name*/"X_32",/*ind*/FLOAT_REGS_FLAG,/*type*/71  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__X_33))->value,/*saves address*/0,/*description*/"X_33",/*name*/"X_33",/*ind*/FLOAT_REGS_FLAG,/*type*/72  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__X_34))->value,/*saves address*/0,/*description*/"X_34",/*name*/"X_34",/*ind*/FLOAT_REGS_FLAG,/*type*/73  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__X_35))->value,/*saves address*/0,/*description*/"X_35",/*name*/"X_35",/*ind*/FLOAT_REGS_FLAG,/*type*/74  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__X_36))->value,/*saves address*/0,/*description*/"X_36",/*name*/"X_36",/*ind*/FLOAT_REGS_FLAG,/*type*/75  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__X_37))->value,/*saves address*/0,/*description*/"X_37",/*name*/"X_37",/*ind*/FLOAT_REGS_FLAG,/*type*/76  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__X_38))->value,/*saves address*/0,/*description*/"X_38",/*name*/"X_38",/*ind*/FLOAT_REGS_FLAG,/*type*/77  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__X_39))->value,/*saves address*/0,/*description*/"X_39",/*name*/"X_39",/*ind*/FLOAT_REGS_FLAG,/*type*/78  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__X_41))->value,/*saves address*/0,/*description*/"X_41",/*name*/"X_41",/*ind*/FLOAT_REGS_FLAG,/*type*/79  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__X_42))->value,/*saves address*/0,/*description*/"X_42",/*name*/"X_42",/*ind*/FLOAT_REGS_FLAG,/*type*/80  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__X_43))->value,/*saves address*/0,/*description*/"X_43",/*name*/"X_43",/*ind*/FLOAT_REGS_FLAG,/*type*/81  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__X_44))->value,/*saves address*/0,/*description*/"X_44",/*name*/"X_44",/*ind*/FLOAT_REGS_FLAG,/*type*/82  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__X_45))->value,/*saves address*/0,/*description*/"X_45",/*name*/"X_45",/*ind*/FLOAT_REGS_FLAG,/*type*/83  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__X_46))->value,/*saves address*/0,/*description*/"X_46",/*name*/"X_46",/*ind*/FLOAT_REGS_FLAG,/*type*/84  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__X_47))->value,/*saves address*/0,/*description*/"X_47",/*name*/"X_47",/*ind*/FLOAT_REGS_FLAG,/*type*/85  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__X_48))->value,/*saves address*/0,/*description*/"X_48",/*name*/"X_48",/*ind*/FLOAT_REGS_FLAG,/*type*/86  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__X_49))->value,/*saves address*/0,/*description*/"X_49",/*name*/"X_49",/*ind*/FLOAT_REGS_FLAG,/*type*/87  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__X_51))->value,/*saves address*/0,/*description*/"X_51",/*name*/"X_51",/*ind*/FLOAT_REGS_FLAG,/*type*/88  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__X_52))->value,/*saves address*/0,/*description*/"X_52",/*name*/"X_52",/*ind*/FLOAT_REGS_FLAG,/*type*/89  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__X_53))->value,/*saves address*/0,/*description*/"X_53",/*name*/"X_53",/*ind*/FLOAT_REGS_FLAG,/*type*/90  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__X_54))->value,/*saves address*/0,/*description*/"X_54",/*name*/"X_54",/*ind*/FLOAT_REGS_FLAG,/*type*/91  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__X_55))->value,/*saves address*/0,/*description*/"X_55",/*name*/"X_55",/*ind*/FLOAT_REGS_FLAG,/*type*/92  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__X_56))->value,/*saves address*/0,/*description*/"X_56",/*name*/"X_56",/*ind*/FLOAT_REGS_FLAG,/*type*/93  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__X_57))->value,/*saves address*/0,/*description*/"X_57",/*name*/"X_57",/*ind*/FLOAT_REGS_FLAG,/*type*/94  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__X_58))->value,/*saves address*/0,/*description*/"X_58",/*name*/"X_58",/*ind*/FLOAT_REGS_FLAG,/*type*/95  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__X_59))->value,/*saves address*/0,/*description*/"X_59",/*name*/"X_59",/*ind*/FLOAT_REGS_FLAG,/*type*/96  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__X_61))->value,/*saves address*/0,/*description*/"X_61",/*name*/"X_61",/*ind*/FLOAT_REGS_FLAG,/*type*/97  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__X_62))->value,/*saves address*/0,/*description*/"X_62",/*name*/"X_62",/*ind*/FLOAT_REGS_FLAG,/*type*/98  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__X_63))->value,/*saves address*/0,/*description*/"X_63",/*name*/"X_63",/*ind*/FLOAT_REGS_FLAG,/*type*/99  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__X_64))->value,/*saves address*/0,/*description*/"X_64",/*name*/"X_64",/*ind*/FLOAT_REGS_FLAG,/*type*/100  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__X_65))->value,/*saves address*/0,/*description*/"X_65",/*name*/"X_65",/*ind*/FLOAT_REGS_FLAG,/*type*/101  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__X_66))->value,/*saves address*/0,/*description*/"X_66",/*name*/"X_66",/*ind*/FLOAT_REGS_FLAG,/*type*/102  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__X_67))->value,/*saves address*/0,/*description*/"X_67",/*name*/"X_67",/*ind*/FLOAT_REGS_FLAG,/*type*/103  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__X_68))->value,/*saves address*/0,/*description*/"X_68",/*name*/"X_68",/*ind*/FLOAT_REGS_FLAG,/*type*/104  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__X_69))->value,/*saves address*/0,/*description*/"X_69",/*name*/"X_69",/*ind*/FLOAT_REGS_FLAG,/*type*/105  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__X_71))->value,/*saves address*/0,/*description*/"X_71",/*name*/"X_71",/*ind*/FLOAT_REGS_FLAG,/*type*/106  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__X_72))->value,/*saves address*/0,/*description*/"X_72",/*name*/"X_72",/*ind*/FLOAT_REGS_FLAG,/*type*/107  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__X_73))->value,/*saves address*/0,/*description*/"X_73",/*name*/"X_73",/*ind*/FLOAT_REGS_FLAG,/*type*/108  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__X_74))->value,/*saves address*/0,/*description*/"X_74",/*name*/"X_74",/*ind*/FLOAT_REGS_FLAG,/*type*/109  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__X_75))->value,/*saves address*/0,/*description*/"X_75",/*name*/"X_75",/*ind*/FLOAT_REGS_FLAG,/*type*/110  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__X_76))->value,/*saves address*/0,/*description*/"X_76",/*name*/"X_76",/*ind*/FLOAT_REGS_FLAG,/*type*/111  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__X_77))->value,/*saves address*/0,/*description*/"X_77",/*name*/"X_77",/*ind*/FLOAT_REGS_FLAG,/*type*/112  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__X_78))->value,/*saves address*/0,/*description*/"X_78",/*name*/"X_78",/*ind*/FLOAT_REGS_FLAG,/*type*/113  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__X_79))->value,/*saves address*/0,/*description*/"X_79",/*name*/"X_79",/*ind*/FLOAT_REGS_FLAG,/*type*/114  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__X_81))->value,/*saves address*/0,/*description*/"X_81",/*name*/"X_81",/*ind*/FLOAT_REGS_FLAG,/*type*/115  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__X_82))->value,/*saves address*/0,/*description*/"X_82",/*name*/"X_82",/*ind*/FLOAT_REGS_FLAG,/*type*/116  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__X_83))->value,/*saves address*/0,/*description*/"X_83",/*name*/"X_83",/*ind*/FLOAT_REGS_FLAG,/*type*/117  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__X_84))->value,/*saves address*/0,/*description*/"X_84",/*name*/"X_84",/*ind*/FLOAT_REGS_FLAG,/*type*/118  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__X_85))->value,/*saves address*/0,/*description*/"X_85",/*name*/"X_85",/*ind*/FLOAT_REGS_FLAG,/*type*/119  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__X_86))->value,/*saves address*/0,/*description*/"X_86",/*name*/"X_86",/*ind*/FLOAT_REGS_FLAG,/*type*/120  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__X_87))->value,/*saves address*/0,/*description*/"X_87",/*name*/"X_87",/*ind*/FLOAT_REGS_FLAG,/*type*/121  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__X_88))->value,/*saves address*/0,/*description*/"X_88",/*name*/"X_88",/*ind*/FLOAT_REGS_FLAG,/*type*/122  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__X_89))->value,/*saves address*/0,/*description*/"X_89",/*name*/"X_89",/*ind*/FLOAT_REGS_FLAG,/*type*/123  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__X_91))->value,/*saves address*/0,/*description*/"X_91",/*name*/"X_91",/*ind*/FLOAT_REGS_FLAG,/*type*/124  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__X_92))->value,/*saves address*/0,/*description*/"X_92",/*name*/"X_92",/*ind*/FLOAT_REGS_FLAG,/*type*/125  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__X_93))->value,/*saves address*/0,/*description*/"X_93",/*name*/"X_93",/*ind*/FLOAT_REGS_FLAG,/*type*/126  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__X_94))->value,/*saves address*/0,/*description*/"X_94",/*name*/"X_94",/*ind*/FLOAT_REGS_FLAG,/*type*/127  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__X_95))->value,/*saves address*/0,/*description*/"X_95",/*name*/"X_95",/*ind*/FLOAT_REGS_FLAG,/*type*/128  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__X_96))->value,/*saves address*/0,/*description*/"X_96",/*name*/"X_96",/*ind*/FLOAT_REGS_FLAG,/*type*/129  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__X_97))->value,/*saves address*/0,/*description*/"X_97",/*name*/"X_97",/*ind*/FLOAT_REGS_FLAG,/*type*/130  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__X_98))->value,/*saves address*/0,/*description*/"X_98",/*name*/"X_98",/*ind*/FLOAT_REGS_FLAG,/*type*/131  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__X_99))->value,/*saves address*/0,/*description*/"X_99",/*name*/"X_99",/*ind*/FLOAT_REGS_FLAG,/*type*/132  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__X0_11))->value,/*saves address*/0,/*description*/"X0_11",/*name*/"X0_11",/*ind*/FLOAT_REGS_FLAG,/*type*/133  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__X0_12))->value,/*saves address*/0,/*description*/"X0_12",/*name*/"X0_12",/*ind*/FLOAT_REGS_FLAG,/*type*/134  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__X0_13))->value,/*saves address*/0,/*description*/"X0_13",/*name*/"X0_13",/*ind*/FLOAT_REGS_FLAG,/*type*/135  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__X0_14))->value,/*saves address*/0,/*description*/"X0_14",/*name*/"X0_14",/*ind*/FLOAT_REGS_FLAG,/*type*/136  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__X0_15))->value,/*saves address*/0,/*description*/"X0_15",/*name*/"X0_15",/*ind*/FLOAT_REGS_FLAG,/*type*/137  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__X0_16))->value,/*saves address*/0,/*description*/"X0_16",/*name*/"X0_16",/*ind*/FLOAT_REGS_FLAG,/*type*/138  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__X0_17))->value,/*saves address*/0,/*description*/"X0_17",/*name*/"X0_17",/*ind*/FLOAT_REGS_FLAG,/*type*/139  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__X0_18))->value,/*saves address*/0,/*description*/"X0_18",/*name*/"X0_18",/*ind*/FLOAT_REGS_FLAG,/*type*/140  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__X0_19))->value,/*saves address*/0,/*description*/"X0_19",/*name*/"X0_19",/*ind*/FLOAT_REGS_FLAG,/*type*/141  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__X0_21))->value,/*saves address*/0,/*description*/"X0_21",/*name*/"X0_21",/*ind*/FLOAT_REGS_FLAG,/*type*/142  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__X0_22))->value,/*saves address*/0,/*description*/"X0_22",/*name*/"X0_22",/*ind*/FLOAT_REGS_FLAG,/*type*/143  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__X0_23))->value,/*saves address*/0,/*description*/"X0_23",/*name*/"X0_23",/*ind*/FLOAT_REGS_FLAG,/*type*/144  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__X0_24))->value,/*saves address*/0,/*description*/"X0_24",/*name*/"X0_24",/*ind*/FLOAT_REGS_FLAG,/*type*/145  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__X0_25))->value,/*saves address*/0,/*description*/"X0_25",/*name*/"X0_25",/*ind*/FLOAT_REGS_FLAG,/*type*/146  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__X0_26))->value,/*saves address*/0,/*description*/"X0_26",/*name*/"X0_26",/*ind*/FLOAT_REGS_FLAG,/*type*/147  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__X0_27))->value,/*saves address*/0,/*description*/"X0_27",/*name*/"X0_27",/*ind*/FLOAT_REGS_FLAG,/*type*/148  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__X0_28))->value,/*saves address*/0,/*description*/"X0_28",/*name*/"X0_28",/*ind*/FLOAT_REGS_FLAG,/*type*/149  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__X0_29))->value,/*saves address*/0,/*description*/"X0_29",/*name*/"X0_29",/*ind*/FLOAT_REGS_FLAG,/*type*/150  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__X0_31))->value,/*saves address*/0,/*description*/"X0_31",/*name*/"X0_31",/*ind*/FLOAT_REGS_FLAG,/*type*/151  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__X0_32))->value,/*saves address*/0,/*description*/"X0_32",/*name*/"X0_32",/*ind*/FLOAT_REGS_FLAG,/*type*/152  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__X0_33))->value,/*saves address*/0,/*description*/"X0_33",/*name*/"X0_33",/*ind*/FLOAT_REGS_FLAG,/*type*/153  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__X0_34))->value,/*saves address*/0,/*description*/"X0_34",/*name*/"X0_34",/*ind*/FLOAT_REGS_FLAG,/*type*/154  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__X0_35))->value,/*saves address*/0,/*description*/"X0_35",/*name*/"X0_35",/*ind*/FLOAT_REGS_FLAG,/*type*/155  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__X0_36))->value,/*saves address*/0,/*description*/"X0_36",/*name*/"X0_36",/*ind*/FLOAT_REGS_FLAG,/*type*/156  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__X0_37))->value,/*saves address*/0,/*description*/"X0_37",/*name*/"X0_37",/*ind*/FLOAT_REGS_FLAG,/*type*/157  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__X0_38))->value,/*saves address*/0,/*description*/"X0_38",/*name*/"X0_38",/*ind*/FLOAT_REGS_FLAG,/*type*/158  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__X0_39))->value,/*saves address*/0,/*description*/"X0_39",/*name*/"X0_39",/*ind*/FLOAT_REGS_FLAG,/*type*/159  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__X0_41))->value,/*saves address*/0,/*description*/"X0_41",/*name*/"X0_41",/*ind*/FLOAT_REGS_FLAG,/*type*/160  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__X0_42))->value,/*saves address*/0,/*description*/"X0_42",/*name*/"X0_42",/*ind*/FLOAT_REGS_FLAG,/*type*/161  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__X0_43))->value,/*saves address*/0,/*description*/"X0_43",/*name*/"X0_43",/*ind*/FLOAT_REGS_FLAG,/*type*/162  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__X0_44))->value,/*saves address*/0,/*description*/"X0_44",/*name*/"X0_44",/*ind*/FLOAT_REGS_FLAG,/*type*/163  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__X0_45))->value,/*saves address*/0,/*description*/"X0_45",/*name*/"X0_45",/*ind*/FLOAT_REGS_FLAG,/*type*/164  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__X0_46))->value,/*saves address*/0,/*description*/"X0_46",/*name*/"X0_46",/*ind*/FLOAT_REGS_FLAG,/*type*/165  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__X0_47))->value,/*saves address*/0,/*description*/"X0_47",/*name*/"X0_47",/*ind*/FLOAT_REGS_FLAG,/*type*/166  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__X0_48))->value,/*saves address*/0,/*description*/"X0_48",/*name*/"X0_48",/*ind*/FLOAT_REGS_FLAG,/*type*/167  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__X0_49))->value,/*saves address*/0,/*description*/"X0_49",/*name*/"X0_49",/*ind*/FLOAT_REGS_FLAG,/*type*/168  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__X0_51))->value,/*saves address*/0,/*description*/"X0_51",/*name*/"X0_51",/*ind*/FLOAT_REGS_FLAG,/*type*/169  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__X0_52))->value,/*saves address*/0,/*description*/"X0_52",/*name*/"X0_52",/*ind*/FLOAT_REGS_FLAG,/*type*/170  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__X0_53))->value,/*saves address*/0,/*description*/"X0_53",/*name*/"X0_53",/*ind*/FLOAT_REGS_FLAG,/*type*/171  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__X0_54))->value,/*saves address*/0,/*description*/"X0_54",/*name*/"X0_54",/*ind*/FLOAT_REGS_FLAG,/*type*/172  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__X0_55))->value,/*saves address*/0,/*description*/"X0_55",/*name*/"X0_55",/*ind*/FLOAT_REGS_FLAG,/*type*/173  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__X0_56))->value,/*saves address*/0,/*description*/"X0_56",/*name*/"X0_56",/*ind*/FLOAT_REGS_FLAG,/*type*/174  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__X0_57))->value,/*saves address*/0,/*description*/"X0_57",/*name*/"X0_57",/*ind*/FLOAT_REGS_FLAG,/*type*/175  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__X0_58))->value,/*saves address*/0,/*description*/"X0_58",/*name*/"X0_58",/*ind*/FLOAT_REGS_FLAG,/*type*/176  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__X0_59))->value,/*saves address*/0,/*description*/"X0_59",/*name*/"X0_59",/*ind*/FLOAT_REGS_FLAG,/*type*/177  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__X0_61))->value,/*saves address*/0,/*description*/"X0_61",/*name*/"X0_61",/*ind*/FLOAT_REGS_FLAG,/*type*/178  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__X0_62))->value,/*saves address*/0,/*description*/"X0_62",/*name*/"X0_62",/*ind*/FLOAT_REGS_FLAG,/*type*/179  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__X0_63))->value,/*saves address*/0,/*description*/"X0_63",/*name*/"X0_63",/*ind*/FLOAT_REGS_FLAG,/*type*/180  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__X0_64))->value,/*saves address*/0,/*description*/"X0_64",/*name*/"X0_64",/*ind*/FLOAT_REGS_FLAG,/*type*/181  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__X0_65))->value,/*saves address*/0,/*description*/"X0_65",/*name*/"X0_65",/*ind*/FLOAT_REGS_FLAG,/*type*/182  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__X0_66))->value,/*saves address*/0,/*description*/"X0_66",/*name*/"X0_66",/*ind*/FLOAT_REGS_FLAG,/*type*/183  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__X0_67))->value,/*saves address*/0,/*description*/"X0_67",/*name*/"X0_67",/*ind*/FLOAT_REGS_FLAG,/*type*/184  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__X0_68))->value,/*saves address*/0,/*description*/"X0_68",/*name*/"X0_68",/*ind*/FLOAT_REGS_FLAG,/*type*/185  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__X0_69))->value,/*saves address*/0,/*description*/"X0_69",/*name*/"X0_69",/*ind*/FLOAT_REGS_FLAG,/*type*/186  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__X0_71))->value,/*saves address*/0,/*description*/"X0_71",/*name*/"X0_71",/*ind*/FLOAT_REGS_FLAG,/*type*/187  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__X0_72))->value,/*saves address*/0,/*description*/"X0_72",/*name*/"X0_72",/*ind*/FLOAT_REGS_FLAG,/*type*/188  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__X0_73))->value,/*saves address*/0,/*description*/"X0_73",/*name*/"X0_73",/*ind*/FLOAT_REGS_FLAG,/*type*/189  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__X0_74))->value,/*saves address*/0,/*description*/"X0_74",/*name*/"X0_74",/*ind*/FLOAT_REGS_FLAG,/*type*/190  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__X0_75))->value,/*saves address*/0,/*description*/"X0_75",/*name*/"X0_75",/*ind*/FLOAT_REGS_FLAG,/*type*/191  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__X0_76))->value,/*saves address*/0,/*description*/"X0_76",/*name*/"X0_76",/*ind*/FLOAT_REGS_FLAG,/*type*/192  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__X0_77))->value,/*saves address*/0,/*description*/"X0_77",/*name*/"X0_77",/*ind*/FLOAT_REGS_FLAG,/*type*/193  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__X0_78))->value,/*saves address*/0,/*description*/"X0_78",/*name*/"X0_78",/*ind*/FLOAT_REGS_FLAG,/*type*/194  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__X0_79))->value,/*saves address*/0,/*description*/"X0_79",/*name*/"X0_79",/*ind*/FLOAT_REGS_FLAG,/*type*/195  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__X0_81))->value,/*saves address*/0,/*description*/"X0_81",/*name*/"X0_81",/*ind*/FLOAT_REGS_FLAG,/*type*/196  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__X0_82))->value,/*saves address*/0,/*description*/"X0_82",/*name*/"X0_82",/*ind*/FLOAT_REGS_FLAG,/*type*/197  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__X0_83))->value,/*saves address*/0,/*description*/"X0_83",/*name*/"X0_83",/*ind*/FLOAT_REGS_FLAG,/*type*/198  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__X0_84))->value,/*saves address*/0,/*description*/"X0_84",/*name*/"X0_84",/*ind*/FLOAT_REGS_FLAG,/*type*/199  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__X0_85))->value,/*saves address*/0,/*description*/"X0_85",/*name*/"X0_85",/*ind*/FLOAT_REGS_FLAG,/*type*/200  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__X0_86))->value,/*saves address*/0,/*description*/"X0_86",/*name*/"X0_86",/*ind*/FLOAT_REGS_FLAG,/*type*/201  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__X0_87))->value,/*saves address*/0,/*description*/"X0_87",/*name*/"X0_87",/*ind*/FLOAT_REGS_FLAG,/*type*/202  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__X0_88))->value,/*saves address*/0,/*description*/"X0_88",/*name*/"X0_88",/*ind*/FLOAT_REGS_FLAG,/*type*/203  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__X0_89))->value,/*saves address*/0,/*description*/"X0_89",/*name*/"X0_89",/*ind*/FLOAT_REGS_FLAG,/*type*/204  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__X0_91))->value,/*saves address*/0,/*description*/"X0_91",/*name*/"X0_91",/*ind*/FLOAT_REGS_FLAG,/*type*/205  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__X0_92))->value,/*saves address*/0,/*description*/"X0_92",/*name*/"X0_92",/*ind*/FLOAT_REGS_FLAG,/*type*/206  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__X0_93))->value,/*saves address*/0,/*description*/"X0_93",/*name*/"X0_93",/*ind*/FLOAT_REGS_FLAG,/*type*/207  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__X0_94))->value,/*saves address*/0,/*description*/"X0_94",/*name*/"X0_94",/*ind*/FLOAT_REGS_FLAG,/*type*/208  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__X0_95))->value,/*saves address*/0,/*description*/"X0_95",/*name*/"X0_95",/*ind*/FLOAT_REGS_FLAG,/*type*/209  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__X0_96))->value,/*saves address*/0,/*description*/"X0_96",/*name*/"X0_96",/*ind*/FLOAT_REGS_FLAG,/*type*/210  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__X0_97))->value,/*saves address*/0,/*description*/"X0_97",/*name*/"X0_97",/*ind*/FLOAT_REGS_FLAG,/*type*/211  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__X0_98))->value,/*saves address*/0,/*description*/"X0_98",/*name*/"X0_98",/*ind*/FLOAT_REGS_FLAG,/*type*/212  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__X0_99))->value,/*saves address*/0,/*description*/"X0_99",/*name*/"X0_99",/*ind*/FLOAT_REGS_FLAG,/*type*/213  ,/*guid*/GUID_USER_HEAD | 131070 , /*size*/1 , /*flags*/USER_VARS|SAVING},

};
#endif//BEREMIZ_REGS_DESCRIPTION_C
