cmake_minimum_required(VERSION 2.8)
set(PRJ_NAME sofi_task)

include(STM32Toolchain.cmake)
set(MCU_FAMILY STM32F7xx)
set(MCU_LINE STM32F767xx)
add_definitions(-D${MCU_LINE})

project(${PRJ_NAME} C CXX ASM)
set(CMAKE_BUILD_TYPE Release)
set(SOFI_TYPE arm)
#check beremiz
set(SOURCES "")
set(SOURCES_PREFIX ${CMAKE_SOURCE_DIR}/src)
add_subdirectory(src)
set(BEREMIZ_SOURCES ${SOURCES})
file(GLOB LINKER_FILE ${MCU_LINKER_SCRIPT})
set (LIB_PATH C:/Beremiz/sofi/generator/template/freertos/lib/)
include_directories(${LIB_PATH}/stm32)
include_directories(${LIB_PATH}/sofi)
include_directories(${LIB_PATH}/lwip/system)
include_directories(${LIB_PATH}/lwip/src/include)
include_directories(${LIB_PATH}/freertos)
include_directories(${LIB_PATH}/usb)
include_directories(inc)

set(BUILD_TARGET ${PROJECT_NAME}.elf)
add_executable(${BUILD_TARGET} ${BEREMIZ_SOURCES} ${LINKER_FILE})
set(MAP_FILE ${PROJECT_SOURCE_DIR}/build/output.map)

set_target_properties(${BUILD_TARGET} PROPERTIES LINK_FLAGS 
    "-mcpu=${MCU_ARCH}\
    -mthumb\
    -mfloat-abi=${MCU_FLOAT_ABI}\
    -mfpu=${MCU_FPU}\
    -O0\
    -specs=nosys.specs\
    -Wl,-Map,${MAP_FILE}\
    -Wl,-gc-sections -T${PROJECT_SOURCE_DIR}/${MCU_LINKER_SCRIPT}")

set(compile_options -mcpu=${MCU_ARCH}
                    -mthumb -mthumb-interwork
                    -mfloat-abi=${MCU_FLOAT_ABI} -g -O0 -mfpu=${MCU_FPU}
                    -ffunction-sections -fdata-sections -fno-common -fmessage-length=0)
target_link_libraries(${BUILD_TARGET} m)
target_compile_options(${BUILD_TARGET} PUBLIC ${compile_options})


file(MAKE_DIRECTORY ${PROJECT_SOURCE_DIR}/build)
set(HEX_FILE ${PROJECT_SOURCE_DIR}/build/${PROJECT_NAME}.hex)
set(BIN_FILE ${PROJECT_SOURCE_DIR}/build/${PROJECT_NAME}.bin)
set(LIST_FILE ${PROJECT_SOURCE_DIR}/build/${PROJECT_NAME}.lst)
set(ELF_FILE ${PROJECT_SOURCE_DIR}/build/${PROJECT_NAME}.elf)

add_custom_command(TARGET ${BUILD_TARGET} POST_BUILD
        COMMAND ${CMAKE_OBJCOPY} -Oihex $<TARGET_FILE:${BUILD_TARGET}> ${HEX_FILE}
        COMMAND ${CMAKE_OBJCOPY} -j .task_text -j .text -j .preinit_array -j .init_array -j .fini_array -j .data -j .rodata -Obinary $<TARGET_FILE:${BUILD_TARGET}> ${BIN_FILE}
        COMMAND ${CMAKE_SIZE} --format=berkeley ${BUILD_TARGET}
        COMMENT "Building ${HEX_FILE} \nBuilding ${BIN_FILE}")

add_custom_command(TARGET ${BUILD_TARGET} POST_BUILD
    COMMAND C:/Beremiz/gcc_6_3_1/bin/../../python3/python-3.7.2/python ${PROJECT_SOURCE_DIR}/adder.py
    WORKING_DIRECTORY ${PROJECT_SOURCE_DIR}
    COMMENT "Running CRC calc..."
)


