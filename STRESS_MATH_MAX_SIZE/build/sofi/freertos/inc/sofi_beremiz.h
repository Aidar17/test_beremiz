/**
 * @file C:\Beremiz\beremiz_test_projects\STRESS_MATH_MAX_SIZE\build//sofi/freertos/inc/sofi_beremiz.h
 * @author Shoma Gane <shomagan@gmail.com>
 *         Ayrat Girfanov <girfanov.ayrat@yandex.ru>
 * @defgroup beremiz 
 * @ingroup beremiz 
 * @version 0.1 
 * @brief  TODO!!! write brief in 
 */
    /*
     * Copyright (c) 2018 Snema Service
     * All rights reserved.
     *
     * Redistribution and use in source and binary forms, with or without modification,
     * are permitted provided that the following conditions are met:
     *
     * 1. Redistributions of source code must retain the above copyright notice,
     *    this list of conditions and the following disclaimer.
     * 2. Redistributions in binary form must reproduce the above copyright notice,
     *    this list of conditions and the following disclaimer in the documentation
     *    and/or other materials provided with the distribution.
     * 3. The name of the author may not be used to endorse or promote products
     *    derived from this software without specific prior written permission.
     *
     * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
     * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
     * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
     * SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
     * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
     * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
     * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
     * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
     * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY
     * OF SUCH DAMAGE.
     *
     * This file is part of the sofi PLC.
     *
     * Author: Shoma Gane <shomagan@gmail.com>
     *         Ayrat Girfanov <girfanov.ayrat@yandex.ru>
     */
#ifndef SOFI_BEREMIZ_H
#define SOFI_BEREMIZ_H
/*add includes below */
#include "sofi_beremiz.h"
#include "type_def.h"
#include "sofi_init.h"

/*add includes before */
#include "beremiz.h"

#include "matiec/accessor.h"
#include "matiec/iec_std_lib.h"

// FUNCTION_BLOCK READ_PARAM_TEMPLATE
// Data part
typedef struct {
  // FB Interface - IN, OUT, IN_OUT variables
  __DECLARE_VAR(BOOL,EN)
  __DECLARE_VAR(BOOL,ENO)
  __DECLARE_VAR(BOOL,ENABLE)
  __DECLARE_VAR(DINT,ADDRESS)
  __DECLARE_VAR(BOOL,CHECK)
  __DECLARE_VAR(DINT,VALUE)
  // FB private variables - TEMP, private and located variables
} READ_PARAM_UDINT;
typedef struct {
  // FB Interface - IN, OUT, IN_OUT variables
  __DECLARE_VAR(BOOL,EN)
  __DECLARE_VAR(BOOL,ENO)
  __DECLARE_VAR(BOOL,ENABLE)
  __DECLARE_VAR(DINT,ADDRESS)
  __DECLARE_VAR(DINT,VALUE)
  __DECLARE_VAR(BOOL,CHECK)
  // FB private variables - TEMP, private and located variables
} SET_PARAM_UDINT;
// FUNCTION_BLOCK READ_PARAM_TEMPLATE
// Data part
typedef struct {
  // FB Interface - IN, OUT, IN_OUT variables
  __DECLARE_VAR(BOOL,EN)
  __DECLARE_VAR(BOOL,ENO)
  __DECLARE_VAR(BOOL,ENABLE)
  __DECLARE_VAR(DINT,ADDRESS)
  __DECLARE_VAR(BOOL,CHECK)
  __DECLARE_VAR(INT,VALUE)
  // FB private variables - TEMP, private and located variables
} READ_PARAM_UINT;
typedef struct {
  // FB Interface - IN, OUT, IN_OUT variables
  __DECLARE_VAR(BOOL,EN)
  __DECLARE_VAR(BOOL,ENO)
  __DECLARE_VAR(BOOL,ENABLE)
  __DECLARE_VAR(DINT,ADDRESS)
  __DECLARE_VAR(INT,VALUE)
  __DECLARE_VAR(BOOL,CHECK)
  // FB private variables - TEMP, private and located variables
} SET_PARAM_UINT;
// FUNCTION_BLOCK READ_PARAM_TEMPLATE
// Data part
typedef struct {
  // FB Interface - IN, OUT, IN_OUT variables
  __DECLARE_VAR(BOOL,EN)
  __DECLARE_VAR(BOOL,ENO)
  __DECLARE_VAR(BOOL,ENABLE)
  __DECLARE_VAR(DINT,ADDRESS)
  __DECLARE_VAR(BOOL,CHECK)
  __DECLARE_VAR(SINT,VALUE)
  // FB private variables - TEMP, private and located variables
} READ_PARAM_USINT;
typedef struct {
  // FB Interface - IN, OUT, IN_OUT variables
  __DECLARE_VAR(BOOL,EN)
  __DECLARE_VAR(BOOL,ENO)
  __DECLARE_VAR(BOOL,ENABLE)
  __DECLARE_VAR(DINT,ADDRESS)
  __DECLARE_VAR(SINT,VALUE)
  __DECLARE_VAR(BOOL,CHECK)
  // FB private variables - TEMP, private and located variables
} SET_PARAM_USINT;

typedef struct {
  // FB Interface - IN, OUT, IN_OUT variables
  __DECLARE_VAR(BOOL,EN)
  __DECLARE_VAR(BOOL,ENO)
  __DECLARE_VAR(UINT,AI_NUMBER)
  __DECLARE_VAR(UINT,AI_VALUE)

  // FB private variables - TEMP, private and located variables

} READ_AI;


// FUNCTION_BLOCK WRITE_DO
// Data part
typedef struct {
  // FB Interface - IN, OUT, IN_OUT variables
  __DECLARE_VAR(BOOL,EN)
  __DECLARE_VAR(BOOL,ENO)
  __DECLARE_VAR(UINT,DO_VALUE)
  __DECLARE_VAR(UINT,DO_MASK)
  // FB private variables - TEMP, private and located variables
} WRITE_DO;
// FUNCTION_BLOCK READ_DI
// Data part
typedef struct {
  // FB Interface - IN, OUT, IN_OUT variables
  __DECLARE_VAR(BOOL,EN)
  __DECLARE_VAR(BOOL,ENO)
  __DECLARE_VAR(UDINT,DI_OUT)
  // FB private variables - TEMP, private and located variables
} READ_DI;


/*functions for read parametrs from controller*/
void READ_PARAM_UDINT_init__(READ_PARAM_UDINT *data__, BOOL retain);
void READ_PARAM_UDINT_body__(READ_PARAM_UDINT *data__);
void SET_PARAM_UDINT_init__(SET_PARAM_UDINT *data__, BOOL retain);
void SET_PARAM_UDINT_body__(SET_PARAM_UDINT *data__);
void READ_PARAM_UINT_init__(READ_PARAM_UINT *data__, BOOL retain);
void READ_PARAM_UINT_body__(READ_PARAM_UINT *data__);
void SET_PARAM_UINT_init__(SET_PARAM_UINT *data__, BOOL retain);
void SET_PARAM_UINT_body__(SET_PARAM_UINT *data__);
void READ_PARAM_USINT_init__(READ_PARAM_USINT *data__, BOOL retain);
void READ_PARAM_USINT_body__(READ_PARAM_USINT *data__);
void SET_PARAM_USINT_init__(SET_PARAM_USINT *data__, BOOL retain);
void SET_PARAM_USINT_body__(SET_PARAM_USINT *data__);

void READ_AI_init__(READ_AI *data__, BOOL retain);
void READ_AI_body__(READ_AI *data__);
void WRITE_DO_init__(WRITE_DO *data__, BOOL retain);
void WRITE_DO_body__(WRITE_DO *data__);
void READ_DI_init__(READ_DI *data__, BOOL retain);
void READ_DI_body__(READ_DI *data__);
#endif //SOFI_BEREMIZ_H
