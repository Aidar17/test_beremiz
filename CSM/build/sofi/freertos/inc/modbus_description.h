/* The total number of nodes, needed to support _all_ instances of the modbus plugin */
#define TOTAL_TCPNODE_COUNT       0
#define TOTAL_RTUNODE_COUNT       1
#define TOTAL_ASCNODE_COUNT       0
#define TOTAL_ROUT_NODE_COUNT     0
#define TOTAL_MODBUS_AREA_COUNT   0
/* Values for instance 1 of the modbus plugin */
#define MAX_NUMBER_OF_TCPCLIENTS  0
#define NUMBER_OF_TCPSERVER_NODES 0
#define NUMBER_OF_TCPCLIENT_NODES 0
#define NUMBER_OF_TCPCLIENT_REQTS 0

#define NUMBER_OF_RTUSERVER_NODES 0
#define NUMBER_OF_RTUCLIENT_NODES 0
#define NUMBER_OF_RTUCLIENT_REQTS 0

#define NUMBER_OF_ASCIISERVER_NODES 0
#define NUMBER_OF_ASCIICLIENT_NODES 0
#define NUMBER_OF_ASCIICLIENT_REQTS 0

#define NUMBER_OF_SERVER_NODES (NUMBER_OF_TCPSERVER_NODES + \
                                NUMBER_OF_RTUSERVER_NODES + \
                               NUMBER_OF_ASCIISERVER_NODES)

#define NUMBER_OF_CLIENT_NODES (NUMBER_OF_TCPCLIENT_NODES + \
                                NUMBER_OF_RTUCLIENT_NODES + \
                                NUMBER_OF_ASCIICLIENT_NODES)

#define NUMBER_OF_CLIENT_REQTS (NUMBER_OF_TCPCLIENT_REQTS + \
                                NUMBER_OF_RTUCLIENT_REQTS + \
                                NUMBER_OF_ASCIICLIENT_REQTS)

#define MAX_READ_BITS 254
#define MAX_WORD_NUM 127
#define MAX_PACKET_LEN (MAX_WORD_NUM*2 + 7)

#define COILS_01 1
#define INPUT_DISCRETES_02 2
#define HOLDING_REGISTERS_03 3
#define INPUT_REGISTERS_04 4
client_node_t		client_nodes[NUMBER_OF_CLIENT_NODES] = {
};

client_request_t	client_requests[NUMBER_OF_CLIENT_REQTS] = {
};

static route_node_t	route_nodes[TOTAL_ROUT_NODE_COUNT] = {
};

static area_node_t	area_nodes[TOTAL_MODBUS_AREA_COUNT] = {
};

