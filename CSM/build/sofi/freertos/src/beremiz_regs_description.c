/**
 * @file beremiz_regs_description.c
 * @author Shoma Gane <shomagan@gmail.com>
 *         Ayrat Girfanov <girfanov.ayrat@yandex.ru>
 * @defgroup beremiz
 * @ingroup beremiz
 * @version 0.1 
 * @brief  TODO!!! write brief in 
 */
/*
 * Copyright (c) 2018 Snema Service
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
 * SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY
 * OF SUCH DAMAGE.
 *
 * This file is part of the sofi PLC.
 *
 * Author: Shoma Gane <shomagan@gmail.com>
 *         Ayrat Girfanov <girfanov.ayrat@yandex.ru>
 */
#ifndef BEREMIZ_REGS_DESCRIPTION_C
#define BEREMIZ_REGS_DESCRIPTION_C 1
#include "beremiz_regs_description.h"
#include "regs_description.h"
#include "sofi_config.h"
#include "beremiz_task.h"
#include "bkram_access.h"
#include "matiec/iec_types_all.h"


extern u8 mdb_address_space[];
extern u8 mdb_array_address_space[];



extern __IEC_ULINT_t CONFIG__MODULE_DI_0;
extern __IEC_ULINT_t CONFIG__MODULE_DI_1;
extern __IEC_ULINT_t CONFIG__MODULE_DI_2;
extern __IEC_ULINT_t CONFIG__MODULE_DI_3;
extern __IEC_ULINT_t CONFIG__MODULE_DI_4;
extern __IEC_ULINT_t CONFIG__MODULE_DI_5;
extern __IEC_ULINT_t CONFIG__MODULE_DI_6;
extern __IEC_ULINT_t CONFIG__MODULE_DI_7;
extern __IEC_ULINT_t CONFIG__MODULE_DI_8;
extern __IEC_ULINT_t CONFIG__MODULE_DI_9;
extern __IEC_ULINT_t CONFIG__MODULE_DI_10;
extern __IEC_ULINT_t CONFIG__MODULE_DI_11;
extern __IEC_ULINT_t CONFIG__MODULE_DI_12;
extern __IEC_ULINT_t CONFIG__MODULE_DI_13;
extern __IEC_ULINT_t CONFIG__MODULE_DI_14;
extern __IEC_ULINT_t CONFIG__MODULE_DI_15;
extern __IEC_REAL_t CONFIG__MODULE_AI_0;
extern __IEC_REAL_t CONFIG__MODULE_AI_1;
extern __IEC_REAL_t CONFIG__MODULE_AI_2;
extern __IEC_REAL_t CONFIG__MODULE_AI_3;
extern __IEC_REAL_t CONFIG__MODULE_AI_4;
extern __IEC_REAL_t CONFIG__MODULE_AI_5;
extern __IEC_REAL_t CONFIG__MODULE_AI_6;
extern __IEC_REAL_t CONFIG__MODULE_AI_7;
extern __IEC_REAL_t CONFIG__MODULE_AI_8;
extern __IEC_REAL_t CONFIG__MODULE_AI_9;
extern __IEC_REAL_t CONFIG__MODULE_AI_10;
extern __IEC_REAL_t CONFIG__MODULE_AI_11;
extern __IEC_REAL_t CONFIG__MODULE_AI_12;
extern __IEC_REAL_t CONFIG__MODULE_AI_13;
extern __IEC_REAL_t CONFIG__MODULE_AI_14;
extern __IEC_REAL_t CONFIG__MODULE_AI_15;
extern __IEC_REAL_t CONFIG__MODULE_AO_0;
extern __IEC_REAL_t CONFIG__MODULE_AO_1;
extern __IEC_REAL_t CONFIG__MODULE_AO_2;
extern __IEC_REAL_t CONFIG__MODULE_AO_3;


regs_description_t const beremiz_regs_description[BEREMIZ_REGS_VAR_NUM]={
    
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_ULINT_t *)(&CONFIG__MODULE_DI_0))->value,/*saves address*/0,/*description*/"MODULE_DI_0",/*name*/"MODULE_DI_0",/*type*/U64_REGS_FLAG,/*ind*/0  ,/*guid*/GUID_USER_HEAD | 131072 , /*size*/1 , /*flags*/USER_VARS},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_ULINT_t *)(&CONFIG__MODULE_DI_1))->value,/*saves address*/0,/*description*/"MODULE_DI_1",/*name*/"MODULE_DI_1",/*type*/U64_REGS_FLAG,/*ind*/1  ,/*guid*/GUID_USER_HEAD | 131073 , /*size*/1 , /*flags*/USER_VARS},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_ULINT_t *)(&CONFIG__MODULE_DI_2))->value,/*saves address*/0,/*description*/"MODULE_DI_2",/*name*/"MODULE_DI_2",/*type*/U64_REGS_FLAG,/*ind*/2  ,/*guid*/GUID_USER_HEAD | 131074 , /*size*/1 , /*flags*/USER_VARS},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_ULINT_t *)(&CONFIG__MODULE_DI_3))->value,/*saves address*/0,/*description*/"MODULE_DI_3",/*name*/"MODULE_DI_3",/*type*/U64_REGS_FLAG,/*ind*/3  ,/*guid*/GUID_USER_HEAD | 131075 , /*size*/1 , /*flags*/USER_VARS},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_ULINT_t *)(&CONFIG__MODULE_DI_4))->value,/*saves address*/0,/*description*/"MODULE_DI_4",/*name*/"MODULE_DI_4",/*type*/U64_REGS_FLAG,/*ind*/4  ,/*guid*/GUID_USER_HEAD | 131076 , /*size*/1 , /*flags*/USER_VARS},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_ULINT_t *)(&CONFIG__MODULE_DI_5))->value,/*saves address*/0,/*description*/"MODULE_DI_5",/*name*/"MODULE_DI_5",/*type*/U64_REGS_FLAG,/*ind*/5  ,/*guid*/GUID_USER_HEAD | 131077 , /*size*/1 , /*flags*/USER_VARS},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_ULINT_t *)(&CONFIG__MODULE_DI_6))->value,/*saves address*/0,/*description*/"MODULE_DI_6",/*name*/"MODULE_DI_6",/*type*/U64_REGS_FLAG,/*ind*/6  ,/*guid*/GUID_USER_HEAD | 131078 , /*size*/1 , /*flags*/USER_VARS},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_ULINT_t *)(&CONFIG__MODULE_DI_7))->value,/*saves address*/0,/*description*/"MODULE_DI_7",/*name*/"MODULE_DI_7",/*type*/U64_REGS_FLAG,/*ind*/7  ,/*guid*/GUID_USER_HEAD | 131079 , /*size*/1 , /*flags*/USER_VARS},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_ULINT_t *)(&CONFIG__MODULE_DI_8))->value,/*saves address*/0,/*description*/"MODULE_DI_8",/*name*/"MODULE_DI_8",/*type*/U64_REGS_FLAG,/*ind*/8  ,/*guid*/GUID_USER_HEAD | 131080 , /*size*/1 , /*flags*/USER_VARS},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_ULINT_t *)(&CONFIG__MODULE_DI_9))->value,/*saves address*/0,/*description*/"MODULE_DI_9",/*name*/"MODULE_DI_9",/*type*/U64_REGS_FLAG,/*ind*/9  ,/*guid*/GUID_USER_HEAD | 131081 , /*size*/1 , /*flags*/USER_VARS},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_ULINT_t *)(&CONFIG__MODULE_DI_10))->value,/*saves address*/0,/*description*/"MODULE_DI_10",/*name*/"MODULE_DI_10",/*type*/U64_REGS_FLAG,/*ind*/10  ,/*guid*/GUID_USER_HEAD | 131082 , /*size*/1 , /*flags*/USER_VARS},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_ULINT_t *)(&CONFIG__MODULE_DI_11))->value,/*saves address*/0,/*description*/"MODULE_DI_11",/*name*/"MODULE_DI_11",/*type*/U64_REGS_FLAG,/*ind*/11  ,/*guid*/GUID_USER_HEAD | 131083 , /*size*/1 , /*flags*/USER_VARS},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_ULINT_t *)(&CONFIG__MODULE_DI_12))->value,/*saves address*/0,/*description*/"MODULE_DI_12",/*name*/"MODULE_DI_12",/*type*/U64_REGS_FLAG,/*ind*/12  ,/*guid*/GUID_USER_HEAD | 131084 , /*size*/1 , /*flags*/USER_VARS},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_ULINT_t *)(&CONFIG__MODULE_DI_13))->value,/*saves address*/0,/*description*/"MODULE_DI_13",/*name*/"MODULE_DI_13",/*type*/U64_REGS_FLAG,/*ind*/13  ,/*guid*/GUID_USER_HEAD | 131085 , /*size*/1 , /*flags*/USER_VARS},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_ULINT_t *)(&CONFIG__MODULE_DI_14))->value,/*saves address*/0,/*description*/"MODULE_DI_14",/*name*/"MODULE_DI_14",/*type*/U64_REGS_FLAG,/*ind*/14  ,/*guid*/GUID_USER_HEAD | 131086 , /*size*/1 , /*flags*/USER_VARS},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_ULINT_t *)(&CONFIG__MODULE_DI_15))->value,/*saves address*/0,/*description*/"MODULE_DI_15",/*name*/"MODULE_DI_15",/*type*/U64_REGS_FLAG,/*ind*/15  ,/*guid*/GUID_USER_HEAD | 131087 , /*size*/1 , /*flags*/USER_VARS},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__MODULE_AI_0))->value,/*saves address*/0,/*description*/"MODULE_AI_0",/*name*/"MODULE_AI_0",/*type*/FLOAT_REGS_FLAG,/*ind*/16  ,/*guid*/GUID_USER_HEAD | 131088 , /*size*/1 , /*flags*/USER_VARS},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__MODULE_AI_1))->value,/*saves address*/0,/*description*/"MODULE_AI_1",/*name*/"MODULE_AI_1",/*type*/FLOAT_REGS_FLAG,/*ind*/17  ,/*guid*/GUID_USER_HEAD | 131089 , /*size*/1 , /*flags*/USER_VARS},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__MODULE_AI_2))->value,/*saves address*/0,/*description*/"MODULE_AI_2",/*name*/"MODULE_AI_2",/*type*/FLOAT_REGS_FLAG,/*ind*/18  ,/*guid*/GUID_USER_HEAD | 131090 , /*size*/1 , /*flags*/USER_VARS},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__MODULE_AI_3))->value,/*saves address*/0,/*description*/"MODULE_AI_3",/*name*/"MODULE_AI_3",/*type*/FLOAT_REGS_FLAG,/*ind*/19  ,/*guid*/GUID_USER_HEAD | 131091 , /*size*/1 , /*flags*/USER_VARS},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__MODULE_AI_4))->value,/*saves address*/0,/*description*/"MODULE_AI_4",/*name*/"MODULE_AI_4",/*type*/FLOAT_REGS_FLAG,/*ind*/20  ,/*guid*/GUID_USER_HEAD | 131092 , /*size*/1 , /*flags*/USER_VARS},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__MODULE_AI_5))->value,/*saves address*/0,/*description*/"MODULE_AI_5",/*name*/"MODULE_AI_5",/*type*/FLOAT_REGS_FLAG,/*ind*/21  ,/*guid*/GUID_USER_HEAD | 131093 , /*size*/1 , /*flags*/USER_VARS},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__MODULE_AI_6))->value,/*saves address*/0,/*description*/"MODULE_AI_6",/*name*/"MODULE_AI_6",/*type*/FLOAT_REGS_FLAG,/*ind*/22  ,/*guid*/GUID_USER_HEAD | 131094 , /*size*/1 , /*flags*/USER_VARS},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__MODULE_AI_7))->value,/*saves address*/0,/*description*/"MODULE_AI_7",/*name*/"MODULE_AI_7",/*type*/FLOAT_REGS_FLAG,/*ind*/23  ,/*guid*/GUID_USER_HEAD | 131095 , /*size*/1 , /*flags*/USER_VARS},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__MODULE_AI_8))->value,/*saves address*/0,/*description*/"MODULE_AI_8",/*name*/"MODULE_AI_8",/*type*/FLOAT_REGS_FLAG,/*ind*/24  ,/*guid*/GUID_USER_HEAD | 131096 , /*size*/1 , /*flags*/USER_VARS},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__MODULE_AI_9))->value,/*saves address*/0,/*description*/"MODULE_AI_9",/*name*/"MODULE_AI_9",/*type*/FLOAT_REGS_FLAG,/*ind*/25  ,/*guid*/GUID_USER_HEAD | 131097 , /*size*/1 , /*flags*/USER_VARS},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__MODULE_AI_10))->value,/*saves address*/0,/*description*/"MODULE_AI_10",/*name*/"MODULE_AI_10",/*type*/FLOAT_REGS_FLAG,/*ind*/26  ,/*guid*/GUID_USER_HEAD | 131098 , /*size*/1 , /*flags*/USER_VARS},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__MODULE_AI_11))->value,/*saves address*/0,/*description*/"MODULE_AI_11",/*name*/"MODULE_AI_11",/*type*/FLOAT_REGS_FLAG,/*ind*/27  ,/*guid*/GUID_USER_HEAD | 131099 , /*size*/1 , /*flags*/USER_VARS},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__MODULE_AI_12))->value,/*saves address*/0,/*description*/"MODULE_AI_12",/*name*/"MODULE_AI_12",/*type*/FLOAT_REGS_FLAG,/*ind*/28  ,/*guid*/GUID_USER_HEAD | 131100 , /*size*/1 , /*flags*/USER_VARS},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__MODULE_AI_13))->value,/*saves address*/0,/*description*/"MODULE_AI_13",/*name*/"MODULE_AI_13",/*type*/FLOAT_REGS_FLAG,/*ind*/29  ,/*guid*/GUID_USER_HEAD | 131101 , /*size*/1 , /*flags*/USER_VARS},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__MODULE_AI_14))->value,/*saves address*/0,/*description*/"MODULE_AI_14",/*name*/"MODULE_AI_14",/*type*/FLOAT_REGS_FLAG,/*ind*/30  ,/*guid*/GUID_USER_HEAD | 131102 , /*size*/1 , /*flags*/USER_VARS},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__MODULE_AI_15))->value,/*saves address*/0,/*description*/"MODULE_AI_15",/*name*/"MODULE_AI_15",/*type*/FLOAT_REGS_FLAG,/*ind*/31  ,/*guid*/GUID_USER_HEAD | 131103 , /*size*/1 , /*flags*/USER_VARS},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__MODULE_AO_0))->value,/*saves address*/0,/*description*/"MODULE_AO_0",/*name*/"MODULE_AO_0",/*type*/FLOAT_REGS_FLAG,/*ind*/32  ,/*guid*/GUID_USER_HEAD | 131104 , /*size*/1 , /*flags*/USER_VARS},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__MODULE_AO_1))->value,/*saves address*/0,/*description*/"MODULE_AO_1",/*name*/"MODULE_AO_1",/*type*/FLOAT_REGS_FLAG,/*ind*/33  ,/*guid*/GUID_USER_HEAD | 131105 , /*size*/1 , /*flags*/USER_VARS},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__MODULE_AO_2))->value,/*saves address*/0,/*description*/"MODULE_AO_2",/*name*/"MODULE_AO_2",/*type*/FLOAT_REGS_FLAG,/*ind*/34  ,/*guid*/GUID_USER_HEAD | 131106 , /*size*/1 , /*flags*/USER_VARS},
{/*default value*/NULL,/*pointer to value*/(u8*)&((__IEC_REAL_t *)(&CONFIG__MODULE_AO_3))->value,/*saves address*/0,/*description*/"MODULE_AO_3",/*name*/"MODULE_AO_3",/*type*/FLOAT_REGS_FLAG,/*ind*/35  ,/*guid*/GUID_USER_HEAD | 131107 , /*size*/1 , /*flags*/USER_VARS},

};
#endif//BEREMIZ_REGS_DESCRIPTION_C
