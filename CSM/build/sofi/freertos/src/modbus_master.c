#include "link_functions.h"
#include "beremiz_task.h"
#include "modbus_master.h"
#ifndef DEBUG
#define DEBUG           1
#endif
#define DEFAULT_BAUDE_RATE 12

static int execute_mb_request(int request_id);

static int modbus_packet_transaction(u16 channel,u8  *packet,u16 send_length,
                                     u16 recv_length,u32 timeout) ;
static int read_bits(u8  function,u8  slave, u16 start_addr, u16 count,
                     u8 *dest, int dest_size, u16 channel,u8  *error_code,
                     u32 response_timeout);
static int read_registers(u8  function,u8  slave,u16 start_addr,u16 count,
                          u16 *dest,int dest_size,u16 channel,u8  *error_code,
                          u32 response_timeout) ;
static int set_single(u8  function,u8  slave,u16 start_addr,u16 value,
                      u16 channel,u8  *error_code,u32 response_timeout) ;
static int write_output_bits(u8  function,u8  slave,u16 start_addr,u8* data,u8 coil_count,
                      u16 channel,u8  *error_code,u32 response_timeout) ;
static int uarts_settings_write(u16 channel,int baud_rate,int data_bits,int bit_stop,int parity);
static u8 get_uart_mask(int baud_rate);
int write_output_words(u8  function,u8  slave,u16 start_addr,u8* data,u8 reg_numm,
                       u16 channel,u8  *error_code,u32 response_timeout);

#include "modbus_description.h"
static int node_id[] = {};
static osThreadId modbus_master_id;

int start_modbus_master(void){
    int res = 0;         
    res = modbus_master_init();
    return res;
}

int modbus_master_init(void){
    int res = 0;
    for(u16 i=0;(i<TOTAL_ROUT_NODE_COUNT)&&(i<MAX_ROUTES);i++){
        if(p_link_functions->repeater_add_route(route_nodes[i].channel_from, route_nodes[i].channel_to, route_nodes[i].mdb_address)<0){
            p_link_functions->printf("USER_TASK:add route procces failed \n");
            res -= 1;
        }
    }
    /*additional manual added dinamic address space*/
    
    for(u16 i=0;(i<TOTAL_MODBUS_AREA_COUNT);i++){
        u32 len = area_nodes[i].regs_number;
        p_link_functions->modbus_bind_address_space_by_command(area_nodes[i].mdb_or_coil_address, area_nodes[i].data, &len, area_nodes[i].command);
        if (len==0){
            p_link_functions->printf("USER_TASK:add area add failed \n");
            res -= 1;
        }
    }
    return res;
}
int modbus_master_deinit(void){
    int res = 0;
    osPoolId mdb_enh_pool =NULL;
    u8 temp_mdb_address_space[1];
    u32 len1 = 1;
    for(u16 i=0;(i<TOTAL_ROUT_NODE_COUNT)&&(i<MAX_ROUTES);i++){
        if(p_link_functions->repeater_delete_route(route_nodes[i].mdb_address)<0){
            p_link_functions->printf("USER_TASK:add route procces failed \n");
            res -= 1;
        }
    }
    /*use for get pointer to pool*/
    mdb_enh_pool = p_link_functions->modbus_bind_address_space(BEREMIZ_ADDRESS_SPACE_START,temp_mdb_address_space,&len1);
    if(mdb_enh_pool !=NULL){
        for(u8 i=0;i<mdb_enh_pool->pool_sz;i++){
            dinamic_address_space_t* pointer;
            pointer = p_link_functions->os_pool_get_by_index(mdb_enh_pool,i);
            if(pointer!=NULL){
                /*freing only pool allocated by user task*/
                if(((u32)pointer->data >= (u32)(&_ram_start_address)) && \
                        ((u32)pointer->data < ((u32)(&_ram_start_address) + (u32)(&_task_ram_size)))){
                    p_link_functions->os_pool_free(mdb_enh_pool,pointer);
                }
            }
        }
    }

    return res;
}

/* Execute a Query/Response suspended transaction between client and server */
/* returns: <0    -> ERROR: error codes
 *          >2    -> SUCCESS: frame length
 *
 */
int modbus_packet_transaction(u16 channel,u8  *packet,u16 send_length,
                              u16 recv_length,u32 timeout) {
    send_options_t * send_options = NULL;
    int guid=0;
    int res =0;
    send_options = p_link_functions->packet_add_new_send_packet(channel,packet,send_length,timeout);
    guid = p_link_functions->packet_get_guid(send_options);
    if(guid >0)  {
        do {
            if(p_link_functions->packet_have_for_handle(send_options,guid)!=0){
                int guid_temp;
                guid_temp= p_link_functions->packet_get(send_options,packet,&recv_length);
                if(guid_temp!=guid){
                    res = -1;
                    p_link_functions->led_error_on(TEST_ERROR_MS);
                }else{
                    res = (int)recv_length;
                }
                send_options=NULL;
            }
            p_link_functions->refresh_watchdog();
            p_link_functions->os_delay(1);
        } while (send_options!=NULL);
    }else{
#if DEBUG     
        p_link_functions->printf("packet not added %i",guid);
#endif

        res =-1;
    }
    return res;
}

/* this thread may be started for all nodes(port),one thread for one nodes
 * may be have maximume DIRECT CHANNEL(RS485-1,RS485-2,RS232,HART,IMMODULE) representations
 * */

void modbus_master_thread(const void * param){
    int client_node_id = *(const int*)param;
    /* client_nodes[client_node_id].comm_period comm_period is in ms */
    TickType_t next_wake_time;
    // get the current time
    next_wake_time = p_link_functions->os_kernel_sys_tick();
    p_link_functions->printf("modbus master thread is working");
    u8 used = 0;
    while (1) {
        int req;
        used = 0;
        for (req=0; req < NUMBER_OF_CLIENT_REQTS; req++){
                /*just do the requests belonging to the client */
            if (client_requests[req].client_node_id != client_node_id){
                continue;
            }
            used = 1;
            int res_tmp = execute_mb_request(req);
            switch (res_tmp) {
                case PORT_FAILURE:
                {
                    p_link_functions->led_error_on(100);
                    client_nodes[client_node_id].prev_error = res_tmp;
                    break;
                }
                case INVALID_FRAME:
                {
                    p_link_functions->led_error_on(100);
                    client_requests[req].prev_error = res_tmp;
                    break;
                }
                case TIMEOUT:
                {
                    p_link_functions->led_error_on(100);
                    client_requests[req].prev_error = res_tmp;
                    break;
                }
                case MODBUS_ERROR:
                {
                    p_link_functions->led_error_on(100);
                    client_requests[req].prev_error = client_requests[req].error_code;
                    break;
                }
                default:
                {
                    client_nodes[client_node_id].prev_error = 0;
                    client_requests[req]        .prev_error = 0;
                    break;
                }
            }
            p_link_functions->os_delay_until(&next_wake_time,client_nodes[client_node_id].comm_period);
        }
        if(used==0){
            p_link_functions->os_delay(1);
        }
    }
}
/* Execute a modbus client transaction/request */
static int execute_mb_request(int request_id){

    switch (client_requests[request_id].mb_function){
    case  1: /* read coils */
        return read_bits(1 ,client_requests[request_id].slave_id,\
                         client_requests[request_id].address, client_requests[request_id].count,
                         (u8*)client_requests[request_id].coms_buffer, (int) client_requests[request_id].count,\
                         client_requests[request_id].channel,&client_requests[request_id].error_code,
                         client_requests[request_id].resp_timeout);

    case  2: /* read discrete inputs */
        return read_bits(2 ,client_requests[request_id].slave_id,\
                         client_requests[request_id].address, client_requests[request_id].count,
                         (u8*)client_requests[request_id].coms_buffer, (int) client_requests[request_id].count,\
                         client_requests[request_id].channel,&client_requests[request_id].error_code,
                         client_requests[request_id].resp_timeout);

    case  3: /* read holding registers */
        return read_registers(3 ,client_requests[request_id].slave_id,\
                             client_requests[request_id].address, client_requests[request_id].count,
                             client_requests[request_id].coms_buffer, (int) client_requests[request_id].count,\
                             client_requests[request_id].channel,&client_requests[request_id].error_code,
                             client_requests[request_id].resp_timeout);
    case  4: /* read input registers */
        return read_registers(4 ,client_requests[request_id].slave_id,\
                         client_requests[request_id].address, client_requests[request_id].count,
                         client_requests[request_id].coms_buffer, (int) client_requests[request_id].count,\
                         client_requests[request_id].channel,&client_requests[request_id].error_code,
                         client_requests[request_id].resp_timeout);

    case  5: /* write single coil */
        return set_single(5,client_requests[request_id].slave_id,client_requests[request_id].address,
                              client_requests[request_id].coms_buffer[0],client_requests[request_id].channel,\
                            &(client_requests[request_id].error_code),client_requests[request_id].resp_timeout);
    case  6: /* write single register */
        return set_single(6,client_requests[request_id].slave_id,client_requests[request_id].address,
                          client_requests[request_id].coms_buffer[0],client_requests[request_id].channel,\
                        &(client_requests[request_id].error_code),client_requests[request_id].resp_timeout);

    case 15: /* write multiple coils */
        return write_output_bits(15,client_requests[request_id].slave_id,client_requests[request_id].address,(u8*)client_requests[request_id].coms_buffer,\
                              (u8)client_requests[request_id].count,client_requests[request_id].channel,&(client_requests[request_id].error_code),\
                              client_requests[request_id].resp_timeout) ;


    case 16: /* write multiple registers */
        return write_output_words(16,client_requests[request_id].slave_id,client_requests[request_id].address,(u8*)client_requests[request_id].coms_buffer,\
                                  (u8)client_requests[request_id].count,client_requests[request_id].channel,&(client_requests[request_id].error_code),\
                                  client_requests[request_id].resp_timeout);

    default: break;  /* should never occur, if file generation is correct */
    }

    return -1;
}
/* Execute a transaction for functions that READ BITS.
 * Bits are stored on an int array, one bit per int.
 * Called by:  read_input_bits()
 *             read_output_bits()
 */
static int read_bits(u8  function,u8  slave,u16 start_addr,u16 count,
                     u8 *dest,int dest_size,u16 channel,
                     u8  *error_code,u32 response_timeout) {
    u8 packet[MAX_PACKET_LEN];  //use for sending and receiving data 
    int response_length;
    int query_length;
    int i, bit;
    u8 temp;
    int dest_pos = 0;
    int coils_processed = 0;
    int res = 0;
    (void)error_code;
    query_length = p_link_functions->modbus_make_packet(slave, function, start_addr, count, NULL, packet);
    if (query_length <= 0){
        res = INTERNAL_ERROR;
    }else{
        response_length = modbus_packet_transaction (channel,packet,(u16)query_length,
        MAX_PACKET_LEN,response_timeout);
        if (response_length  <= 0){
            res = response_length;
        }else{
            /* NOTE: Integer division. (count+7)/8 is equivalent to ceil(count/8) */
            if ((packet[2]  != (count+7)/8)||(packet[0]!=slave))    {
                res = INVALID_FRAME;
            }else{
                u16 calc_len = packet[2] + 3 + 2;//address + function + byte_num + crc + data
                if (p_link_functions->modbus_crc16_check(packet,calc_len) && (dest !=0)){
                    p_link_functions->task_enter_critical();
                    for( i = 0; (i < packet[2]) && (i < dest_size); i++ ) {
                        temp = packet[3 + i];
                        for( bit = 0x01; (bit & 0xff) && (coils_processed < count); ) {
                            if((temp & (u8)bit)){
                                dest[dest_pos] = 1;
                            }else{
                                dest[dest_pos] = 0;
                            }
                            coils_processed++;
                            bit = bit << 1;
                            dest_pos++;
                        }
                    }
                    p_link_functions->task_exit_critical();
                }
                res = response_length;
            }
        }
    }
    return res;
}


/* Execute a transaction for functions that READ REGISTERS.
 */
static int read_registers(u8  function,u8  slave,u16 start_addr,u16 count,
                          u16 *dest,int dest_size,u16 channel,u8  *error_code,
                          u32 response_timeout) {
    u8 packet[MAX_PACKET_LEN];
    (void)error_code;
    int response_length;
    int query_length;
    int i= 0;
    int res = 0;
    query_length = p_link_functions->modbus_make_packet(slave, function, start_addr, count, NULL, packet);
    if (query_length <= 0){
        res = INTERNAL_ERROR;
    }else{
        response_length = modbus_packet_transaction (channel,packet,(u16)query_length,MAX_PACKET_LEN,response_timeout);
        res = response_length;
        if(response_length <= 0){
            p_link_functions->led_error_on(100);
        }else if((packet[2]   != 2*count)||(packet[0]!=slave))    {res = INVALID_FRAME;
        }else if(dest!=NULL){
            u16 calc_len = packet[2] + 3 + 2;//address + function + byte_num + crc + data
            if (p_link_functions->modbus_crc16_check(packet,calc_len)){
                p_link_functions->task_enter_critical();
                for(i = 0; (i < (u8)(packet[2]/2)) && (i < dest_size); i++ ) {
                    dest[i] = (u16)(packet[3 + i *2] << 8) | (u16)packet[4 + i * 2];    /* copy reg hi byte to temp hi byte*/
                }
                p_link_functions->task_exit_critical();
            }else{
                p_link_functions->led_error_on(100);
            }
        }        
    }
    return res;
}

/* Execute a transaction for functions that WRITE a sinlge BIT.
 */
static int set_single(u8  function,u8  slave,u16 start_addr,u16 value,
                      u16 channel,u8  *error_code,u32 response_timeout) {
  u8 packet[MAX_PACKET_LEN];
  int query_length, response_length,res;
  (void)error_code;
  query_length = p_link_functions->modbus_make_packet(slave, function, start_addr, 1, (u8*)&value, packet);
  if (query_length < 0){
      res = INTERNAL_ERROR;
  }else{
      response_length = modbus_packet_transaction (channel,packet,(u16)query_length,
      MAX_PACKET_LEN,response_timeout);
      u16 calc_len = 8;//address + function + byte_num + crc + data
      if (response_length  < calc_len)  {
          res = INVALID_FRAME;
      }else if ((p_link_functions->modbus_crc16_check(packet,calc_len)==0)||(packet[0]!=slave)){
          res = INVALID_FRAME;
      }else{
          res = response_length ;
      }
  }
  return res;
}
/* FUNCTION 0x0F   - Force Multiple Coils */
int write_output_bits(u8  function,u8  slave,u16 start_addr,u8* data,u8 coil_count,
                      u16 channel,u8  *error_code,u32 response_timeout) {
    int query_length, response_length, res ;
    u8 packet[MAX_PACKET_LEN];
    (void)error_code;
    if( coil_count > MAX_READ_BITS) {
        coil_count = MAX_READ_BITS;
#if DEBUG
        p_link_functions->printf("Writing to too many coils.\n");
#endif
    }
    query_length = p_link_functions->modbus_make_packet(slave, function, start_addr, coil_count, data, packet);

    if (query_length < 0){
        res = INTERNAL_ERROR;
    }else{
        /* NOTE: Integer division. (count+7)/8 is equivalent to ceil(count/8) */
        response_length = modbus_packet_transaction (channel,packet,(u16)query_length,
                                                     MAX_PACKET_LEN,response_timeout);
        if (response_length  <= 0){
           res = response_length;
        }else  if ((packet[0] != slave) ||
            (packet[1] != function) ||
            (packet[2] != (u8)((start_addr>>8) & 0xff))||
            (packet[3] != (u8)(start_addr & 0xff))||
            (packet[4] != 0)||
            (packet[5] != coil_count)){
            res = INVALID_FRAME;
        }else{
            res = response_length;
        }
    }
    return res;
}





/* FUNCTION 0x10   - Force Multiple Registers */
int write_output_words(u8  function,u8  slave,u16 start_addr,u8* data,u8 reg_numm,
                       u16 channel,u8  *error_code,u32 response_timeout) {
    int query_length, response_length, res ;
    u8 packet[MAX_PACKET_LEN];
    (void)error_code;
    if( reg_numm > MAX_WORD_NUM) {
        reg_numm = MAX_WORD_NUM;
#if DEBUG
        p_link_functions->printf("Writing to too many coils.\n");
#endif
    }
    query_length = p_link_functions->modbus_make_packet(slave, function, start_addr, reg_numm, data, packet);

    if (query_length < 0){
        res = INTERNAL_ERROR;}
    else{
        /* NOTE: Integer division. (count+7)/8 is equivalent to ceil(count/8) */
        response_length = modbus_packet_transaction (channel,packet,(u16)query_length,
                                                    MAX_PACKET_LEN,response_timeout);
        if (response_length < 8){      res = INVALID_FRAME;
        }else if ((packet[0] != slave) ||
            (packet[1] != function) ||
            (packet[2] != (u8)((start_addr>>8) & 0xff))||
            (packet[3] != (u8)(start_addr & 0xff))||
            (packet[4] != 0)||
            (packet[5] != reg_numm)){   res = INVALID_FRAME;
        }else{
            res = response_length;
        }
    }
    return res;
}
static int uarts_settings_write(u16 channel,int baud_rate,int data_bits,int bit_stop,int parity){
    int res =0;
    regs_template_t regs_template;
    char rs232_name[] = "uart3_sets";
    char rs485_1_name[] = "uart5_sets";
    char rs485_2_name[] = "uart2_sets";
    char rs485_immo_name[] = "uart6_sets";
    switch(channel){
    case(RS_232_UART):
        regs_template.name  = rs232_name;
        break;
    case(RS_485_1_UART):
        regs_template.name  = rs485_1_name;
        break;
    case(RS_485_2_UART):
        regs_template.name = rs485_2_name;
        break;
    case(RS_485_IMMO_UART):
        regs_template.name = rs485_immo_name;
        break;

    }
    if(p_link_functions->regs_description_get_by_name(&regs_template)==0){
        regs_access_t reg;
        u16 address = regs_template.guid & GUID_ADDRESS_MASK;
        u8 baud_maks = 0;
        u8 data_bits_mask = 0;
        u8 stop_bits_mask = 0;
        u8 parity_mask = 0;
        u8 rx_delay = 50;
        u32 value;
        value = 0;
        parity_mask = (u8)parity;
        stop_bits_mask = (u8)bit_stop;
        baud_maks = get_uart_mask(baud_rate);
        switch(data_bits){
        case(7):
            data_bits_mask =0;
            break;
        case(8):
            data_bits_mask =1;
            break;
        case(9):
            data_bits_mask =2;
            break;
        }
        reg.value.op_u64 = 0;
        reg.flag = regs_template.type;
        reg.value.op_u16 |= (u16)baud_maks;
        reg.value.op_u16 |= (u16)(data_bits_mask<<4);
        reg.value.op_u16 |= (u16)(stop_bits_mask<<6);
        reg.value.op_u16 |= (u16)(parity_mask<<8);
        reg.value.op_u16 |= (u16)(rx_delay <<10);
        p_link_functions->regs_set(address,reg);
    }
    return res;
}
static u8 get_uart_mask(int baud_rate){
    u8 baud;
    switch(baud_rate){
    case(0):
        baud = 0;
        break;
    case(2400):
        baud = 1;
        break;
    case(4800):
        baud =2;
        break;
    case(9600):
        baud =3;
        break;
    case(14400):
        baud=4;
        break;
    case(19200):
        baud=5;
        break;
    case(28800):
        baud=6;
        break;
    case(38400):
        baud=7;
        break;
    case(56000):
        baud=8;
        break;
    case(57600):
        baud=9;
        break;
    case(76800):
        baud=10;
        break;
    case(115200):
        baud=11;
        break;
    case(DEFAULT_BAUDE_RATE):
        baud=12;
        break;
    default:
        baud=12;
        break;
    }
    return baud;
}
